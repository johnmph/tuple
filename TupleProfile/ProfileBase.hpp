//
//  ProfileBase.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ProfileBase__
#define __Tuple__ProfileBase__

#include <iostream>


class ProfileBase {
    // Methods
    virtual void startProfile() = 0;
    
public:
    // Constructor
    //ProfileBase();
    
    // Destructor
    virtual ~ProfileBase() = 0;
    
    // Methods
    void profile();
};

#endif /* defined(__Tuple__ProfileBase__) */
