//
//  main.cpp
//  TupleProfile
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include <iostream>
#include "EntityManagerProfile.hpp"
#include "EntityListManagerProfile.hpp"
#include "CacheProfile.hpp"
#include "EntityLoopProfile.hpp"
#include "EntityManagerWithMap.hpp"


int main(int argc, const char * argv[]) {/*
    // Create cache profile
    std::unique_ptr<ProfileBase> profiler(new CacheProfile(1000, 1000));
    
    // Create entity manager profile
    std::unique_ptr<ProfileBase> profiler(new EntityManagerProfile());
    
    // Create entity list manager profile
    std::unique_ptr<ProfileBase> profiler(new EntityListManagerProfile());
    
    */
    // Create entity loop profile
    std::unique_ptr<ProfileBase> profiler(new EntityLoopProfile());
    
    // Create entity manager with map profile
    //std::unique_ptr<ProfileBase> profiler(new EntityManagerWithMapProfile());
    
    // Profile
    profiler->profile();
    
    // Exit
    return 0;
}
