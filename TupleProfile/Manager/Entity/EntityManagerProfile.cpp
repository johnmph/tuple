//
//  EntityManagerProfile.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "EntityManagerProfile.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "PoolMemoryAllocator.hpp"
#include "TransformComponent.hpp"
#include "BodyBox2DComponent.hpp"
#include "SpriteCocos2DComponent.hpp"


EntityManagerProfile::EntityManagerProfile() {
    // Create managers
    _memoryManager = std::make_shared<MemoryManager>();
    std::shared_ptr<NotificationManager> notificationManager = std::make_shared<NotificationManager>();
    _entityManager = std::make_shared<MyEntityManager>(_memoryManager, notificationManager);
    
    // Register allocators for components
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(TransformComponent), 1000)));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(BodyBox2DComponent), 1000)));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(SpriteCocos2DComponent), 1000)));//TODO: tester les differents allocators dans le ProfileMemoryManager
    
    /*_memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));// bizarre c'est presque aussi rapide que PoolMemoryAllocator ??
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));*/
}

EntityManagerProfile::~EntityManagerProfile() {
}

void EntityManagerProfile::startProfile() {
    profileEntityManagerCreateEntity(100000);//TODO:si je les fait un apres les autres, je ne libere pas les objects créés dans les tests car c dans le destructor qu'ils sont libérés, donc surement faire une methode start et stop a appeler a chaque profile pour avoir tjs le meme depart pour les tests
    profileEntityManagerAddComponent(100000);
    profileEntityManagerGetAllEntityIDsForComponentClasses(100000, 1000);
}

void EntityManagerProfile::profileEntityManagerCreateEntity(unsigned int const entityNumber) {
    // Create delete entity indexs
    int deleteEntityIndexs[] = { 18, 92, 57, 63, 45, 1, 99, 5, 72, 71 };
    int deleteEntityIndexsCount = sizeof(deleteEntityIndexs) / sizeof(deleteEntityIndexs[0]);
    
    // Create entities loop
    for (int x = 0; x < entityNumber; ++x) {
        // Check if time to delete entities
        if (((x % 100) == 0) && x) {
            // Get offset
            int offset = (((int) (x / 100)) - 1) * 100;
            
            // Delete entities loop
            for (int x2 = 0; x2 < deleteEntityIndexsCount; ++x2) {
                // Delete entity
                _entityManager->removeEntity(offset + deleteEntityIndexs[x2]);
            }
        }
        
        // Create entity
        _entityManager->createEntity();//TODO: tres lent car teste chaque key jusqu'a trouver une key non utilisée, essayer plutot avec un vector qui tient les key libérées + un index pour la key libre suivante (ou peut etre meme avoir dans le vector l'index pour la key suivante aussi) MAIS pour ca réécrire les tests pour supprimer quelques entities au hasard (des indexs pris dans un array avec des valeurs au hasard pré ecrites pour avoir les memes a chaque profile) et créer de nouvelles entities (pour voir si le fait d'avoir des keys libres modifie la vitesse) DONC par exemple tous les mod EntityNumber / 100 == 0, on supprime x entities et ensuite on continue la loop normalement
        //TODO: par apres, separer les profiles (un pour la creation des entities, un pour la creation des components, un pour le addComponent, ...
    }
}
/*
void EntityManagerProfile::profileMemoryManagerCreateObject(unsigned int const objectNumber) {//TODO: peut etre plutot dans profileMemoryManager
    // Create object loop
    for (int x = 0; x < objectNumber; ++x) {
        // Create transform component
        _memoryManager->newDataWithType<TransformComponent>();
        
        // Create body box2D component
        _memoryManager->newDataWithType<BodyBox2DComponent>();
        
        // Create sprite cocos2D component
        _memoryManager->newDataWithType<SpriteCocos2DComponent>();
    }
}*/

void EntityManagerProfile::profileEntityManagerAddComponent(unsigned int const entityNumber) {
    // Create transform component
    ComponentLinkType<TransformComponent> transformComponent(_memoryManager->newDataWithType<TransformComponent>());
    
    // Create body box2D component
    ComponentLinkType<BodyBox2DComponent> bodyBox2DComponent(_memoryManager->newDataWithType<BodyBox2DComponent>());
    
    // Create sprite cocos2D component
    ComponentLinkType<SpriteCocos2DComponent> spriteCocos2DComponent(_memoryManager->newDataWithType<SpriteCocos2DComponent>());
    
    // Create entities loop
    for (int x = 0; x < entityNumber; ++x) {
        // Create entity
        EntityID eid = _entityManager->createEntity();
        
        // Add components to entity
        _entityManager->addComponentInEntity(eid, transformComponent);
        _entityManager->addComponentInEntity(eid, bodyBox2DComponent);
        _entityManager->addComponentInEntity(eid, spriteCocos2DComponent);
    }
}

void EntityManagerProfile::profileEntityManagerGetAllEntityIDsForComponentClasses(unsigned int const entityNumber, unsigned int const repeatTimes) {
    // Create entities loop
    for (int x = 0; x < entityNumber; ++x) {
        // Create entity
        EntityID eid = _entityManager->createEntity();
        
        if (x & 0x1) {
            // Create transform component
            ComponentLinkType<TransformComponent> transformComponent(_memoryManager->newDataWithType<TransformComponent>());
            _entityManager->addComponentInEntity(eid, transformComponent);
        }
        
        if (x & 0x2) {
            // Create body box2D component
            ComponentLinkType<BodyBox2DComponent> bodyBox2DComponent(_memoryManager->newDataWithType<BodyBox2DComponent>());
            _entityManager->addComponentInEntity(eid, bodyBox2DComponent);
        }
        
        if (x & 0x4) {
            // Create sprite cocos2D component
            ComponentLinkType<SpriteCocos2DComponent> spriteCocos2DComponent(_memoryManager->newDataWithType<SpriteCocos2DComponent>());
            _entityManager->addComponentInEntity(eid, spriteCocos2DComponent);
        }
    }
    
    // Repeat time loop
    for (int x = 0; x < repeatTimes; ++x) {
        // Get all entity IDs for component classes
        _entityManager->getAllEntityIDsForComponentClasses<TransformComponent>();
        
        // Get all entity IDs for component classes
        _entityManager->getAllEntityIDsForComponentClasses<TransformComponent, TransformComponent>();
        
        // Get all entity IDs for component classes
        _entityManager->getAllEntityIDsForComponentClasses<TransformComponent, TransformComponent, TransformComponent>();
    }
}
