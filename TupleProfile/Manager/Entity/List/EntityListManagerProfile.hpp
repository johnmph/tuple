//
//  EntityListManagerProfile.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityListManagerProfile__
#define __Tuple__EntityListManagerProfile__

#include "ProfileBase.hpp"
#include "EntityListManager.hpp"
#include "ComponentSharingPolicyOptimizedSizeShare.hpp"
#include "ComponentSharingPolicyOptimizedSpeedShare.hpp"
#include "ComponentSharingPolicySmartLinkShare.hpp"


using namespace Tuple;

class EntityListManagerProfile : public ProfileBase {
    //template <class TType> using MySmartLink = SmartLink<TType>;//TODO: ca ne compile pas si je passe directement SmartLink a l'entity manager
    //using MyEntityManager = EntityManager<MySmartLink>;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
    template <class TType> using MyMemoryLink = MemoryHandle<TType>;
    template <template <class> class TComponentLinkType> using MyComponentSharingPolicy = ComponentSharingPolicySmartLinkShare<TComponentLinkType, OwnershipPolicySimpleCount>;
    
    using MyEntityManager = EntityManager<MyMemoryLink, /*MyComponentSharingPolicy*/ComponentSharingPolicyOptimizedSizeShare>;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
    using MyEntityListManager = EntityListManager<MyEntityManager>;
    using EntityID = typename MyEntityManager::EntityID;
    //typedef typename MyEntityManager::Components Components;
    template <class TType> using ComponentLinkType = typename MyEntityManager::template ComponentLinkType<TType>;
    
    // Members
    std::shared_ptr<MemoryManager> _memoryManager;
    std::shared_ptr<MyEntityManager> _entityManager;
    std::shared_ptr<MyEntityListManager> _entityListManager;
    
    // Methods
    void startProfile();
    
public:
    // Constructor
    EntityListManagerProfile();
    
    // Destructor
    ~EntityListManagerProfile();
    
    // Methods
};

#endif /* defined(__Tuple__EntityListManagerProfile__) */
