//
//  EntityListManagerProfile.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "EntityListManagerProfile.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "PoolMemoryAllocator.hpp"
#include "TransformComponent.hpp"
#include "BodyBox2DComponent.hpp"
#include "SpriteCocos2DComponent.hpp"

/*
 
 - La technique avec les ordered set et le std::includes pour la methode componentAdded est plus lente d'environ 1000ms
 - Un unordered set ou un ordered set pour les keys de l'entity list a des performances +- equivalente
 
*/


EntityListManagerProfile::EntityListManagerProfile() {
    // Create managers
    _memoryManager = std::make_shared<MemoryManager>();
    std::shared_ptr<NotificationManager> notificationManager = std::make_shared<NotificationManager>();
    _entityManager = std::make_shared<MyEntityManager>(_memoryManager, notificationManager);
    _entityListManager = std::make_shared<MyEntityListManager>(_entityManager);
    
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(TransformComponent), 1000)));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(BodyBox2DComponent), 1000)));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(SpriteCocos2DComponent), 1000)));
    /*
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));*/
}

EntityListManagerProfile::~EntityListManagerProfile() {
}

void EntityListManagerProfile::startProfile() {
    // Create entity list for TransformComponent and BodyBox2DComponent
    auto entityList = _entityListManager->getEntityListForComponentClasses<TransformComponent, BodyBox2DComponent>();
    
    // Create components
    ComponentLinkType<TransformComponent> transformComponent(_memoryManager->newDataWithType<TransformComponent>());
    ComponentLinkType<BodyBox2DComponent> bodyBox2DComponent(_memoryManager->newDataWithType<BodyBox2DComponent>());
    ComponentLinkType<SpriteCocos2DComponent> spriteCocos2DComponent(_memoryManager->newDataWithType<SpriteCocos2DComponent>());
    
    // Repeat times loop
    for (int x = 0; x < 1000000; ++x) {
        // Create entity
        EntityID eid = _entityManager->createEntity();
        
        // Add component to entity
        _entityManager->addComponentInEntity(eid, transformComponent);
        _entityManager->addComponentInEntity(eid, spriteCocos2DComponent);
        _entityManager->addComponentInEntity(eid, bodyBox2DComponent);
    }
    
    //std::cout << "entity list size = " << entityList->size() << std::endl;
}
