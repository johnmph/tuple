//
//  EntityLoopProfile.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 1/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "EntityLoopProfile.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "PoolMemoryAllocator.hpp"
#include <emmintrin.h>

/*
 
 - Ca prend environ 550ms de traiter (copier 2 int) une liste de 2500 entities 1000x d'affilées (donc +- 0.55ms pour traiter 1x la liste de 2500 entities)
 - Utiliser un iterator ou un index reviens +- au meme (une dizaine de ms de gagné avec un iterator)
 - Prefetcher fait perdre +- 200ms a cause du fait qu'on doit quand meme appeler getComponentWithClassFromEntity et d'avoir accès a l'element + 16 de la liste (donc un acces random a cette liste d'entity + un appel de methode assez lourd (avec 1 find dans 2 unordered_map + une creation d'un MemoryHandle downcasté), donc dans cette configuration, prefetcher ne sert vraiment a rien
 - +- 135ms ainsi (avec les unordered_map), si map +- 335ms (surement a cause du cache) !!!
 - +- 14ms avec une struct de Body et Transform et une unordered_map de ca mais pas possible en reel car sinon aucune flexibilité pour créer les entities en les composants de components !!!
 - +- 17ms avec des vectors pour garder les MemoryHandle des components ensemble (dans les entity lists) !!! Flexibilité possible et très bonne performances !!!
 - +- 7ms en utilisant les MemoryPointer a la place des MemoryHandle et avec un allocator qui alloue contigu en mémoire (hacké le DefaultMemoryAllocator pour tester)
 
*/


EntityLoopProfile::EntityLoopProfile() {
    // Create managers
    _memoryManager = std::make_shared<MemoryManager>();
    std::shared_ptr<NotificationManager> notificationManager = std::make_shared<NotificationManager>();
    _entityManager = std::make_shared<MyEntityManager>(_memoryManager, notificationManager);
    _entityListManager = std::make_shared<MyEntityListManager>(_entityManager);
    
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<TransformComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(TransformComponent), 5000)));
    _memoryManager->registerMemoryAllocatorForClass<BodyBox2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(BodyBox2DComponent), 5000)));
    _memoryManager->registerMemoryAllocatorForClass<SpriteCocos2DComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(SpriteCocos2DComponent), 5000)));
    
    // Create entity list for TransformComponent and BodyBox2DComponent
    _entityList = _entityListManager->getEntityListForComponentClasses<BodyBox2DComponent, TransformComponent>();
    
    // Entities loop
    for (int x = 0; x < 10000; ++x) {
        // Create entity
        EntityID eid = _entityManager->createEntity();
        
        if (x & 0x1) {
            // Create transform component
            ComponentLinkType<TransformComponent> transformComponent(_memoryManager->newDataWithType<TransformComponent>());
            transformComponent->x = 0;
            transformComponent->y = 0;
            _entityManager->addComponentInEntity(eid, std::move(transformComponent));//TODO: pas obligé move, juste pour optimisation
        }
        
        if (x & 0x2) {
            // Create body box2D component
            ComponentLinkType<BodyBox2DComponent> bodyBox2DComponent(_memoryManager->newDataWithType<BodyBox2DComponent>());
            bodyBox2DComponent->x = x;
            bodyBox2DComponent->y = x * 2;
            _entityManager->addComponentInEntity(eid, std::move(bodyBox2DComponent));
        }
        
        if (x & 0x4) {
            // Create sprite cocos2D component
            ComponentLinkType<SpriteCocos2DComponent> spriteCocos2DComponent(_memoryManager->newDataWithType<SpriteCocos2DComponent>());
            _entityManager->addComponentInEntity(eid, std::move(spriteCocos2DComponent));
        }
    }
}

EntityLoopProfile::~EntityLoopProfile() {
}

void EntityLoopProfile::startProfile() {
    // TODO: ATTENTION !!! ici en dessous, la ligne commentée fait une copie du vector de components, c'est a dire que si le vector d'origine change (si une entity ou un component est ajouté ou supprimé, la liste peut changer et donc le vector de components aussi mais si on fait une copie, la copie reste inchangée. Donc il est obligatoire (et plus optimisé en plus) de copier la reference et non pas le vector lui meme (et en const ainsi on ne peut pas modifier le vector d'origine qui est dans l'entity list, ce serait de toute facon interdit par le compilateur) !!!
    
    // Get BodyBox2DComponent list
    //std::vector<MemoryHandle<BodyBox2DComponent>> bodyBox2DComponents(_entityList->getComponentsForClass<BodyBox2DComponent>());
    std::vector<ComponentLinkType<BodyBox2DComponent>> const &bodyBox2DComponents(_entityList->getComponentsForClass<BodyBox2DComponent>());
    
    // Get TransformComponent list
    //std::vector<MemoryHandle<TransformComponent>> transformComponents(_entityList->getComponentsForClass<TransformComponent>());
    std::vector<ComponentLinkType<TransformComponent>> const &transformComponents(_entityList->getComponentsForClass<TransformComponent>());
    
    // Repeat times loop
    for (int n = 0; n < 1000; ++n) {
        // Browse entity id list
        for (size_t x = 0, s = _entityList->getCount(); x < s; ++x) {
            // Get bodyBox2D component pointer
            BodyBox2DComponent *bodyBox2DComponent = bodyBox2DComponents[x].get();
            
            // Get transform component pointer
            TransformComponent *transformComponent = transformComponents[x].get();
            
            // Set data
            transformComponent->x = bodyBox2DComponent->x;
            transformComponent->y = bodyBox2DComponent->y;
            
            // Log
            //std::cout << x << ") body x = " << bodyBox2DComponent->x << ", y = " << bodyBox2DComponent->y << std::endl;
            //std::cout << x << ") transform x = " << transformComponent->x << ", y = " << transformComponent->y << std::endl;
        }
    }
    
    // Log
    std::cout << "Entity list size : " << _entityList->getCount() << std::endl;
}
