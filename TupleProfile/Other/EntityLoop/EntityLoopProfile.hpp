//
//  EntityLoopProfile.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 1/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityLoopProfile__
#define __Tuple__EntityLoopProfile__

#include "ProfileBase.hpp"
#include "EntityListManager.hpp"
#include "TransformComponent.hpp"
#include "BodyBox2DComponent.hpp"
#include "SpriteCocos2DComponent.hpp"
#include "ComponentSharingPolicyOptimizedSizeShare.hpp"
#include "ComponentSharingPolicyOptimizedSpeedShare.hpp"
#include "ComponentSharingPolicySmartLinkShare.hpp"
#include "ComponentSharingPolicyNoShare.hpp"


using namespace Tuple;

class EntityLoopProfile : public ProfileBase {
    //template <class TType> using MySmartLink = SmartLink<TType>;//TODO: ca ne compile pas si je passe directement SmartLink a l'entity manager
    //using MyEntityManager = EntityManager<MySmartLink>;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
    template <class TType> using MyMemoryLink = MemoryPointer<TType>;//MemoryHandle<TType>;
    template <template <class> class TComponentLinkType> using MyComponentSharingPolicy = ComponentSharingPolicySmartLinkShare<TComponentLinkType, OwnershipPolicySimpleCount>;
    
    using MyEntityManager = EntityManager<MyMemoryLink, MyComponentSharingPolicy/*ComponentSharingPolicyOptimizedSpeedShare*/>;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
    using MyEntityListManager = EntityListManager<MyEntityManager>;
    using EntityID = typename MyEntityManager::EntityID;
    //typedef typename MyEntityManager::Components Components;
    template <class TType> using ComponentLinkType = typename MyEntityManager::template ComponentLinkType<TType>;
    
    // Members
    std::shared_ptr<MemoryManager> _memoryManager;
    std::shared_ptr<MyEntityManager> _entityManager;
    std::shared_ptr<MyEntityListManager> _entityListManager;
    std::shared_ptr<EntityList<MyEntityManager, BodyBox2DComponent, TransformComponent> const> _entityList;
    
    // Methods
    void startProfile();
    
public:
    // Constructor
    EntityLoopProfile();
    
    // Destructor
    ~EntityLoopProfile();
    
    // Methods
};

#endif /* defined(__Tuple__EntityLoopProfile__) */
