//
//  EntityManagerWithMap.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 2/04/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#include "EntityManagerWithMap.hpp"
#include "TypeManager.hpp"
#include "TransformComponent.hpp"
#include "BodyBox2DComponent.hpp"
#include "SpriteCocos2DComponent.hpp"


/*
 
 Ca prend 160ms -> Beaucoup plus lent qu'avec l'EntityManager actuel
 De plus il y a un probleme pour désallouer les unordered_map de components car on ne connait pas leur vrai type a la déallocation.
 
 */


EntityManagerWithMapProfile::EntityManagerWithMapProfile() {
    Components<BodyBox2DComponent> *bodyBox2DComponentMap(new Components<BodyBox2DComponent>);
    Components<TransformComponent> *transformComponentMap(new Components<TransformComponent>);
    Components<SpriteCocos2DComponent> *spriteCocos2DComponentMap(new Components<SpriteCocos2DComponent>);
    
    size_t bodyType = TypeManager::getTypeIDForType<BodyBox2DComponent>();
    size_t transformType = TypeManager::getTypeIDForType<TransformComponent>();
    size_t spriteType = TypeManager::getTypeIDForType<SpriteCocos2DComponent>();
    
    _entities[bodyType] = bodyBox2DComponentMap;
    _entities[transformType] = transformComponentMap;
    _entities[spriteType] = spriteCocos2DComponentMap;
    
    // Entities loop
    for (int x = 0; x < 10000; ++x) {
        // Create entity
        EntityID eid = x;
        
        if (x & 0x1) {
            // Create transform component
            TransformComponent transformComponent;
            transformComponent.x = 0;
            transformComponent.y = 0;
            (*transformComponentMap)[eid] = std::move(transformComponent);
        }
        
        if (x & 0x2) {
            // Create body box2D component
            BodyBox2DComponent bodyBox2DComponent;
            bodyBox2DComponent.x = x;
            bodyBox2DComponent.y = x * 2;
            
            (*bodyBox2DComponentMap)[eid] = std::move(bodyBox2DComponent);
        }
        
        if (x & 0x4) {
            // Create sprite cocos2D component
            (*spriteCocos2DComponentMap)[eid];
        }
        
        if ((x & 0x3) == 0x3) {
            // Add to entity list (transform et body box2D)
            _entityList.push_back(eid);
        }
    }
}

EntityManagerWithMapProfile::~EntityManagerWithMapProfile() {
    for (auto componentMap : _entities) {
        // TODO: probleme ici: on delete sur un void * et pas moyen de caster puisqu'on a pas le type, juste une image du type en size_t
        delete componentMap.second;
    }
}

void EntityManagerWithMapProfile::startProfile() {
    size_t bodyType = TypeManager::getTypeIDForType<BodyBox2DComponent>();
    size_t transformType = TypeManager::getTypeIDForType<TransformComponent>();
    
    // Get BodyBox2DComponent list
    Components<BodyBox2DComponent> &bodyBox2DComponents(*reinterpret_cast<Components<BodyBox2DComponent> *>(_entities[bodyType]));
    
    // Get TransformComponent list
    Components<TransformComponent> &transformComponents(*reinterpret_cast<Components<TransformComponent> *>(_entities[transformType]));
    
    // Repeat times loop
    for (int n = 0; n < 1000; ++n) {
        // Browse entity id list
        for (size_t x = 0, s = _entityList.size(); x < s; ++x) {
            // Get eid
            EntityID eid = _entityList[x];
            
            // Get bodyBox2D component pointer
            BodyBox2DComponent *bodyBox2DComponent = &bodyBox2DComponents[eid];
            
            // Get transform component pointer
            TransformComponent *transformComponent = &transformComponents[eid];
            
            // Set data
            transformComponent->x = bodyBox2DComponent->x;
            transformComponent->y = bodyBox2DComponent->y;
            
            // Log
            //std::cout << x << ") body x = " << bodyBox2DComponent->x << ", y = " << bodyBox2DComponent->y << std::endl;
            //std::cout << x << ") transform x = " << transformComponent->x << ", y = " << transformComponent->y << std::endl;
        }
    }
    
    // Log
    std::cout << "Entity list size : " << _entityList.size() << std::endl;
}
