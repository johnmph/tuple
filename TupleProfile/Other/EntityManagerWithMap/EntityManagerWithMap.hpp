//
//  EntityManagerWithMap.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 2/04/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityManagerWithMap__
#define __Tuple__EntityManagerWithMap__

#include "ProfileBase.hpp"
#include "Component.hpp"
#include <unordered_map>
#include <vector>


using namespace Tuple;

class EntityManagerWithMapProfile : public ProfileBase {
    /*! Entity ID type */
    using EntityID = unsigned int;
    
    /*! Components */
    template <class TComponentType> using Components = std::unordered_map<EntityID, TComponentType>;
    
    // Members
    std::unordered_map<size_t, void *> _entities;
    std::vector<EntityID> _entityList;
    
    // Methods
    void startProfile();
    
public:
    // Constructor
    EntityManagerWithMapProfile();
    
    // Destructor
    ~EntityManagerWithMapProfile();
    
    // Methods
};

#endif /* defined(__Tuple__EntityManagerWithMap__) */
