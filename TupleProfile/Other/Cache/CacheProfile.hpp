//
//  CacheProfile.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__CacheProfile__
#define __Tuple__CacheProfile__

#include "ProfileBase.hpp"
#include <vector>


class CacheProfile : public ProfileBase {
    // Members
    unsigned int _arrayXSize, _arrayYSize;
    int *_intCArraySource;
    int *_intCArrayDest, *_intCArrayDest2;
    std::vector<int> _intVectorSource;
    std::vector<int> _intVectorDest;
    
    // Methods
    void startProfile();
    
public:
    // Constructor
    CacheProfile(unsigned int const arrayXSize, unsigned int const arrayYSize);
    
    // Destructor
    ~CacheProfile();
    
    // Methods
    void profileCArray();
    void profileCArray(int *__restrict__ source, int *__restrict__ dest, int *__restrict__ dest2);
    void profileVector();
};

#endif /* defined(__Tuple__CacheProfile__) */
