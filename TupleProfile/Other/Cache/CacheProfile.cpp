//
//  CacheProfile.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "CacheProfile.hpp"
#include <emmintrin.h>

/*
 
 - profileVector est aussi rapide que profileCArray
 - Un accès sequentiel en lecture et en ecriture est le plus rapide
 - Un accès sequentiel en lecture et random en ecriture est environ 4x plus lent (pourquoi ? car normalement le write est delayed ?)
 - Un accès random en lecture et sequentiel en ecriture est aussi environ 4x plus lent
 - Un accès random en ecriture et pas d'accès en lecture est un leger plus rapide que celui avec la lecture sequentielle et l'ecriture random
 - _mm_stream_si32 pour ecrire sans polluer le cache est enormement plus lent !!!
 - Je gagne +- 500ms en utilisant _mm_prefetch pour prefetcher le 64eme element à l'avance
 - Avoir un array avec en x la valeur a lire et en x+1 la valeur a ecrire n'est pas plus rapide que d'avoir 2 array séparés, c'est meme plus lent de +- 50ms
 - 1907 Mo/s de transfert ? (random), en sequentiel, c'est 4x plus rapide -> plutot 2680 Mo/s en sequentiel et 380 Mo/s en random ?
 - Il n'y a pas de possibilités d'ameliorer le random write, alors pour le game engine, soit laisser la loop ainsi (array sequentiel d'entity id, 1 random acces avec indirection MemoryHandle par component) soit avoir pour les components en read dans les loops une pool dédiée avec seulement les components qui seront présent (associé aux write components), avec les write components accédés toujours via le MemoryHandle et la liste d'entity dans ce style :
 
 int count = _entityList.getCount();
 BodyBox2DComponent *bodyBox2DComponents = _entityList.getComponentArrayForClass<BodyBox2DComponent>();//Read component
 EntityID *entityIDs = _entityList.getEntityIDs();
 for (int x = 0; x < count; ++x) {
   // Get transform component (Write component)
   MemoryHandle<TransformComponent> transformComponent = _entityManager->getComponentWithClassFromEntity<TransformComponent>(entityIDs[x]);
 
   // Set coordinates
   transformComponent->x = bodyBox2DComponents[x].x;
   transformComponent->y = bodyBox2DComponents[x].y;
 }
 
*/


CacheProfile::CacheProfile(unsigned int const arrayXSize, unsigned int const arrayYSize) : _arrayXSize(arrayXSize), _arrayYSize(arrayYSize) {
    // Calculate array size
    unsigned int arraySize = _arrayXSize * _arrayYSize;
    
    // Create C-Array
/*    _intCArraySource = (int *) malloc(sizeof(int) * arraySize);
    _intCArrayDest = (int *) malloc(sizeof(int) * arraySize);
    _intCArrayDest2 = (int *) malloc(sizeof(int) * arraySize);*/
    posix_memalign((void **) &_intCArraySource, 64, sizeof(int) * arraySize);
    posix_memalign((void **) &_intCArrayDest, 64, sizeof(int) * arraySize);
    posix_memalign((void **) &_intCArrayDest2, 64, sizeof(int) * arraySize);
    
    // Create vector
    _intVectorSource = std::vector<int>(arraySize);
    _intVectorDest = std::vector<int>(arraySize);
    
    // Browse array and vector
    for (int x = 0; x < arraySize; ++x) {
        // Set current int
        _intCArraySource[x] = x;
        _intVectorSource[x] = x;
    }
    
    //std::cout << "Source size = " << _intVectorSource.size() << std::endl;
    //std::cout << "Dest size = " << _intVectorDest.size() << std::endl;
}

CacheProfile::~CacheProfile() {
    // Free C-Array
    free(_intCArraySource);
    free(_intCArrayDest);
    free(_intCArrayDest2);
}

void CacheProfile::startProfile() {
    // Repeat times loop
    for (int x = 0; x < 500; ++x) {
        profileCArray(_intCArraySource, _intCArrayDest, _intCArrayDest2);
        //profileVector();
    }
    /*
    int y = 0;
    for (int x = 0; x < 100000000; ++x) {
        y += (x / 12) * (x % 2);
    }
    std::cout << y << std::endl;*/
}
/*
void CacheProfile::profileCArray() {
    // Calculate array size
    unsigned int arraySize = _arrayXSize * _arrayYSize;
    
    // Browse C-Array
    for (int x = 0; x < arraySize; ++x) {
        // Calculate index of dest
        int index = (x / _arrayXSize) + ((x % _arrayXSize) * _arrayXSize);
        
        // Read current int in source
        int value = _intCArraySource[x];
        
        // Set value in dest
        _intCArrayDest[index] = value;
//        _mm_stream_si32(&_intCArrayDest[index], value);//TODO: beaucoup plus lent si random, le meme si sequential
    }
}*/
/*
void CacheProfile::profileCArray() {
    int z = 0;
    for (int y = 0; y < _arrayYSize; ++y) {
        for (int x = 0; x < _arrayXSize; ++x) {
            // Calculate index of next dest
            _mm_prefetch(&_intCArraySource[((x + 32) * _arrayXSize) + y], _MM_HINT_T0);
            
            // Read current int in source
            int value = _intCArraySource[(x * _arrayXSize) + y];
            
            // Set value in dest
            _intCArrayDest[z++] = value;
//            _mm_stream_si32(&_intCArrayDest[(x * _arrayXSize) + y], value);//TODO: beaucoup plus lent si random, le meme si sequential
        }
    }
}*/

/*
void CacheProfile::profileCArray() {
    // Calculate array size
    unsigned int arraySize = _arrayXSize * _arrayYSize;
    
    // Browse C-Array
    for (int x = 0; x < arraySize; ++x) {
        // Calculate index of next dest
        int nextIndex = ((x + 64) / _arrayXSize) + (((x + 64) % _arrayXSize) * _arrayXSize);
        _mm_prefetch(&_intCArraySource[nextIndex], _MM_HINT_T0);
        
        // Calculate index of next dest
        int index = (x / _arrayXSize) + ((x % _arrayXSize) * _arrayXSize);
        
        // Read current int in source
        int value = _intCArraySource[index];
        
        // Set value in dest
        _intCArrayDest[x] = value;
    }
}*/

void CacheProfile::profileCArray(int *__restrict__ source, int *__restrict__ dest, int *__restrict__ dest2) {
    // Calculate array size
    int arraySize = _arrayXSize * _arrayYSize;
    
    // Browse C-Array
    for (int x = 0; x < arraySize; ++x) {
        // Read current int in source
        int value = source[x];
        
        // Calculate index of next dest
        int index = (x / _arrayXSize) + ((x % _arrayXSize) * _arrayXSize);
        
        // Set value in dest
//        _mm_stream_si32(&dest[index], value);
        dest[index] = value;
        
        // Set value in dest2
//        _mm_stream_si32(&dest2[index], value);
//        dest2[index] = value;
    }
    /*
    // Browse C-Array
    for (int x = 0; x < arraySize; ++x) {//4670ms
        // Read current int in source
        int value = dest[x];
        
        dest2[x] = value;
//        _mm_stream_si32(&dest2[index], value);
    }*/
}

void CacheProfile::profileVector() {
    // Calculate array size
    unsigned int arraySize = _arrayXSize * _arrayYSize;
    
    // Browse C-Array
    for (int x = 0; x < arraySize; ++x) {
        // Calculate index of dest
        int index = (x / _arrayXSize) + ((x % _arrayXSize) * _arrayXSize);
        
        // Read current int in source
        int value = _intVectorSource[x];
        
        // Set value in dest
        _intVectorDest[index] = value;
    }
}
