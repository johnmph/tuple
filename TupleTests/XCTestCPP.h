//
//  XCTestCPP.h
//  Tuple
//
//  Created by Jonathan Baliko on 20/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef Tuple_XCTestCPP_h
#define Tuple_XCTestCPP_h

#include <XCTest/XCTest.h>


#define _XCTPrimitiveAssertThrowsCPP(expression, format...) \
({ \
    bool __caughtException = false; \
    try { \
        (expression); \
    } \
    catch (...) { \
        __caughtException = true; \
    }\
    if (!__caughtException) { \
        _XCTRegisterFailure(self, _XCTFailureDescription(_XCTAssertion_Throws, 0, @#expression),format); \
    } \
})

#define _XCTPrimitiveAssertNoThrowCPP(expression, format...) \
({ \
    try { \
        (expression); \
    } \
    catch (...) { \
        _XCTRegisterFailure(self, _XCTFailureDescription(_XCTAssertion_NoThrow, 0, @#expression),format); \
    }\
})

/// \def XCTAssertThrowsCPP(expression, format...)
/// \brief Generates a failure when expression does not throw an exception.
/// \param expression The expression that is evaluated.
/// \param format An NSString object that contains a printf-style string containing an error message describing the failure condition and placeholders for the arguments.
/// \param ... The arguments displayed in the format string.
#define XCTAssertThrowsCPP(expression, format...) \
    _XCTPrimitiveAssertThrowsCPP(expression, ## format)


/// \def XCTAssertNoThrowCPP(expression, format...)
/// \brief Generates a failure when expression does throw an exception.
/// \param expression The expression that is evaluated.
/// \param format An NSString object that contains a printf-style string containing an error message describing the failure condition and placeholders for the arguments.
/// \param ... The arguments displayed in the format string.
#define XCTAssertNoThrowCPP(expression, format...) \
_XCTPrimitiveAssertNoThrowCPP(expression, ## format)

#endif
