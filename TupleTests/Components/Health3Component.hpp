//
//  Health3Component.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Health3Component__
#define __Tuple__Health3Component__

#include "Component.hpp"


class Health3Component : public Tuple::Component {
    int _health;
    
public:
    // Constructor
    Health3Component(int health) : _health(health) {};
    
    // Destructor
    ~Health3Component() {};
    
    // Methods
    int getHealth();
};

#endif /* defined(__Tuple__Health3Component__) */
