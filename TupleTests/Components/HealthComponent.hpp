//
//  HealthComponent.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__HealthComponent__
#define __Tuple__HealthComponent__

#include "Component.hpp"


class HealthComponent : public Tuple::Component {
    int _health;
    
public:
    // Constructor
    HealthComponent() = default;//TODO: pour faire de HealthComponent un POD (constructor par défaut en 1er et pas de destructor ni de methode virtuelle)
    HealthComponent(int health) : _health(health) {};
    
    // Destructor
    //~HealthComponent() {};
    
    // Methods
    int getHealth();
};

#endif /* defined(__Tuple__HealthComponent__) */
