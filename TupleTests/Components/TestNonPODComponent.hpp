//
//  TestNonPODComponent.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__TestNonPODComponent__
#define __Tuple__TestNonPODComponent__

#include "Component.hpp"


class TestNonPODComponent : public Tuple::Component {
    // Counter
    static int _count;
    
public:
    // Constructor
    TestNonPODComponent();
    
    // Destructor
    ~TestNonPODComponent();
    
    // Methods
    static void resetCount();
    static int getCount();
};

#endif /* defined(__Tuple__TestNonPODComponent__) */
