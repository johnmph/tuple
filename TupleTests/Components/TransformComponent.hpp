//
//  TransformComponent.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__TransformComponent__
#define __Tuple__TransformComponent__

#include "Component.hpp"


class TransformComponent : public Tuple::Component {
public:
    float x, y;
    
    // Constructor
//    TransformComponent() {};
    
    // Destructor
//    ~TransformComponent() {};
    
    // Methods
};

#endif /* defined(__Tuple__TransformComponent__) */
