//
//  BodyBox2DComponent.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__BodyBox2DComponent__
#define __Tuple__BodyBox2DComponent__

#include "Component.hpp"


class BodyBox2DComponent : public Tuple::Component {
public:
    float x, y;
    
    // Constructor
    //BodyBox2DComponent() {};
    
    // Destructor
    //~BodyBox2DComponent() {};
    
    // Methods
};

#endif /* defined(__Tuple__BodyBox2DComponent__) */
