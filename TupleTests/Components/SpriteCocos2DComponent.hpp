//
//  SpriteCocos2DComponent.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SpriteCocos2DComponent__
#define __Tuple__SpriteCocos2DComponent__

#include "Component.hpp"


class SpriteCocos2DComponent : public Tuple::Component {
public:
    float x, y;
    
    // Constructor
    //SpriteCocos2DComponent() {};
    
    // Destructor
    //~SpriteCocos2DComponent() {};
    
    // Methods
};

#endif /* defined(__Tuple__SpriteCocos2DComponent__) */
