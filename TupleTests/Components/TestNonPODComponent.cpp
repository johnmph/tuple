//
//  TestNonPODComponent.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "TestNonPODComponent.hpp"


int TestNonPODComponent::_count = 0;


TestNonPODComponent::TestNonPODComponent() {
    ++/*TestNonPODComponent::*/_count;
}

TestNonPODComponent::~TestNonPODComponent() {
    --_count;
}

void TestNonPODComponent::resetCount() {
    _count = 0;
}

int TestNonPODComponent::getCount() {
    return _count;
}
