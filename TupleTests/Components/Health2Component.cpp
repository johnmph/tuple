//
//  Health2Component.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "Health2Component.hpp"


int Health2Component::getHealth() {
    return _health;
}

void Health2Component::setHealth(int health) {
    _health = health;
}
