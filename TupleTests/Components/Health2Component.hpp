//
//  Health2Component.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Health2Component__
#define __Tuple__Health2Component__

#include "Component.hpp"


class Health2Component : public Tuple::Component {
    int _health;
    
public:
    // Constructor
    Health2Component() {};
    
    // Destructor
    ~Health2Component() {};
    
    // Methods
    int getHealth();
    void setHealth(int health);
};

#endif /* defined(__Tuple__Health2Component__) */
