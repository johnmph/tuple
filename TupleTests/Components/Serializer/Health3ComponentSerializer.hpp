//
//  Health3ComponentSerializer.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Health3ComponentSerializer__
#define __Tuple__Health3ComponentSerializer__

#include "ComponentLoader.hpp"
#include "ComponentSaver.hpp"
#include "Health3ComponentBuilder.hpp"
#include "Health3Component.hpp"


template <template <class> class TComponentLinkType> class Health3ComponentSerializer : public Tuple::ComponentLoader<TComponentLinkType>, public Tuple::ComponentSaver<TComponentLinkType> {
public:
    // Constructor
    Health3ComponentSerializer();
    
    // Destructor
    ~Health3ComponentSerializer();
    
    // Component loader methods
    TComponentLinkType<Tuple::Component> loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) override;
    
    // Component saver methods
    size_t getManagedClassType() override;
    void saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) override;
};


template <template <class> class TComponentLinkType> Health3ComponentSerializer<TComponentLinkType>::Health3ComponentSerializer() {
    std::cout << "Health3ComponentSerializer::Constructor (" << this << ")" << std::endl;
}

template <template <class> class TComponentLinkType> Health3ComponentSerializer<TComponentLinkType>::~Health3ComponentSerializer() {
    std::cout << "Health3ComponentSerializer::Destructor (" << this << ")" << std::endl;
}

template <template <class> class TComponentLinkType> TComponentLinkType<Tuple::Component> Health3ComponentSerializer<TComponentLinkType>::loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) {
    // TODO: exemple avec un builder externe qui fait office de memory manager constructor delegate
    // TODO: ca n'a plus vraiment d'interet d'avoir ca depuis la suppression du MemoryManagerConstructorDelegate !!!
    Health3ComponentBuilder builder;//TODO: a moins d'avoir plutot Health3ComponentBuilder builder(memoryManager); et c'est lui qui se charge d'appeler le newDataWithType avec les bons parametres dans l'ordre quand on appelle builder.getObject(); (apres avoir setté ses propriétés comme setHealth)
    
    builder.setHealth(dataDecoder.readInt("Health"));
    
    // Get new Health3Component from memory manager
    TComponentLinkType<Health3Component> health3Component(memoryManager.newDataWithType<Health3Component>(builder.getHealth()));
    
    // Return Health3Component
    return health3Component;
}

template <template <class> class TComponentLinkType> size_t Health3ComponentSerializer<TComponentLinkType>::getManagedClassType() {
    return Tuple::TypeManager::getTypeIDForType<Health3Component>();
}

template <template <class> class TComponentLinkType> void Health3ComponentSerializer<TComponentLinkType>::saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) {
    // Downcast component to Health3Component
    TComponentLinkType<Health3Component> health3Component(Tuple::downcastTo<Health3Component>(std::move(TComponentLinkType<Tuple::Component>(component))));
    
    // Write health
    dataEncoder.writeInt("Health", health3Component->getHealth());
}

#endif /* defined(__Tuple__Health3ComponentSerializer__) */
