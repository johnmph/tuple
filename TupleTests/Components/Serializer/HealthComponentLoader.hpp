//
//  HealthComponentLoader.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 9/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__HealthComponentLoader__
#define __Tuple__HealthComponentLoader__

#include "ComponentLoader.hpp"
#include "HealthComponent.hpp"


template <template <class> class TComponentLinkType> class HealthComponentLoader : public Tuple::ComponentLoader<TComponentLinkType> {
public:
    // Constructor
    HealthComponentLoader();
    
    // Destructor
    ~HealthComponentLoader();
    
    // Component loader methods
    TComponentLinkType<Tuple::Component> loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) override;
};


template <template <class> class TComponentLinkType> HealthComponentLoader<TComponentLinkType>::HealthComponentLoader() {
    std::cout << "HealthComponentLoader::Constructor (" << this << ")" << std::endl;
};

template <template <class> class TComponentLinkType> HealthComponentLoader<TComponentLinkType>::~HealthComponentLoader() {
    std::cout << "HealthComponentLoader::Destructor (" << this << ")" << std::endl;
};

template <template <class> class TComponentLinkType> TComponentLinkType<Tuple::Component> HealthComponentLoader<TComponentLinkType>::loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) {
    // Get new HealthComponent from memory manager
    TComponentLinkType<HealthComponent> healthComponent(memoryManager.newDataWithType<HealthComponent>(dataDecoder.readInt("Health")));
    
    // Return HealthComponent
    return healthComponent;
}

#endif /* defined(__Tuple__HealthComponentLoader__) */
