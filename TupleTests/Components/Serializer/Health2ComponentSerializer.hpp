//
//  Health2ComponentSerializer.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 3/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Health2ComponentSerializer__
#define __Tuple__Health2ComponentSerializer__

#include "ComponentLoader.hpp"
#include "ComponentSaver.hpp"
#include "Health2Component.hpp"


template <template <class> class TComponentLinkType> class Health2ComponentSerializer : public Tuple::ComponentLoader<TComponentLinkType>, public Tuple::ComponentSaver<TComponentLinkType> {
public:
    // Constructor
    Health2ComponentSerializer();
    
    // Destructor
    ~Health2ComponentSerializer();
    
    // Component loader methods
    TComponentLinkType<Tuple::Component> loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) override;
    
    // Component saver methods
    size_t getManagedClassType() override;
    void saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) override;
};


template <template <class> class TComponentLinkType> Health2ComponentSerializer<TComponentLinkType>::Health2ComponentSerializer() {
    std::cout << "Health2ComponentSerializer::Constructor (" << this << ")" << std::endl;
}

template <template <class> class TComponentLinkType> Health2ComponentSerializer<TComponentLinkType>::~Health2ComponentSerializer() {
    std::cout << "Health2ComponentSerializer::Destructor (" << this << ")" << std::endl;
}

template <template <class> class TComponentLinkType> TComponentLinkType<Tuple::Component> Health2ComponentSerializer<TComponentLinkType>::loadComponent(Tuple::MemoryManager &memoryManager, Tuple::DataDecoder &dataDecoder) {
    // Get new Health2Component from memory manager
    TComponentLinkType<Health2Component> health2Component(memoryManager.newDataWithType<Health2Component>());
    
    // TODO: exemple pour une création de component sans builder parce que default constructor dans le component, on sette les valeurs par apres (comme si le component etait un builder !)
    health2Component->setHealth(dataDecoder.readInt("Health"));
    
    // Return Health2Component
    return health2Component;
}

template <template <class> class TComponentLinkType> size_t Health2ComponentSerializer<TComponentLinkType>::getManagedClassType() {
    return Tuple::TypeManager::getTypeIDForType<Health2Component>();
}

template <template <class> class TComponentLinkType> void Health2ComponentSerializer<TComponentLinkType>::saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) {
    // Downcast component to Health2Component
    TComponentLinkType<Health2Component> health2Component(Tuple::downcastTo<Health2Component>(std::move(TComponentLinkType<Tuple::Component>(component))));
    
    // Write health
    dataEncoder.writeInt("Health", health2Component->getHealth());
}

#endif /* defined(__Tuple__Health2ComponentSerializer__) */
