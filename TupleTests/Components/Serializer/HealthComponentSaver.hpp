//
//  HealthComponentSaver.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 9/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__HealthComponentSaver__
#define __Tuple__HealthComponentSaver__

#include "ComponentSaver.hpp"
#include "HealthComponent.hpp"


template <template <class> class TComponentLinkType> class HealthComponentSaver : public Tuple::ComponentSaver<TComponentLinkType> {
public:
    // Constructor
    HealthComponentSaver();
    
    // Destructor
    ~HealthComponentSaver();
    
    // Component saver methods
    size_t getManagedClassType() override;
    void saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) override;
};


template <template <class> class TComponentLinkType> HealthComponentSaver<TComponentLinkType>::HealthComponentSaver() {
    std::cout << "HealthComponentSaver::Constructor (" << this << ")" << std::endl;
};

template <template <class> class TComponentLinkType> HealthComponentSaver<TComponentLinkType>::~HealthComponentSaver() {
    std::cout << "HealthComponentSaver::Destructor (" << this << ")" << std::endl;
};

template <template <class> class TComponentLinkType> size_t HealthComponentSaver<TComponentLinkType>::getManagedClassType() {
    return Tuple::TypeManager::getTypeIDForType<HealthComponent>();
};

template <template <class> class TComponentLinkType> void HealthComponentSaver<TComponentLinkType>::saveComponent(TComponentLinkType<Tuple::Component> const &component, Tuple::DataEncoder &dataEncoder) {
    // Downcast component to HealthComponent
    TComponentLinkType<HealthComponent> healthComponent = Tuple::downcastTo<HealthComponent>(std::move(TComponentLinkType<Tuple::Component>(component)));
    
    // Write health
    dataEncoder.writeInt("Health", healthComponent->getHealth());
}

#endif /* defined(__Tuple__HealthComponentSaver__) */
