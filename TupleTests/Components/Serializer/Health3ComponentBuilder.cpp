//
//  Health3ComponentBuilder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 3/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "Health3ComponentBuilder.hpp"
#include "Health3Component.hpp"


Health3ComponentBuilder::Health3ComponentBuilder() {
    std::cout << "Health3ComponentBuilder::Constructor (" << this << ")" << std::endl;
}

Health3ComponentBuilder::~Health3ComponentBuilder() {
    std::cout << "Health3ComponentBuilder::Destructor (" << this << ")" << std::endl;
}

int Health3ComponentBuilder::getHealth() {
    return _health;
}

void Health3ComponentBuilder::setHealth(int health) {
    _health = health;
}
