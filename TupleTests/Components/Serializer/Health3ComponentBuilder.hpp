//
//  Health3ComponentBuilder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 3/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Health3ComponentBuilder__
#define __Tuple__Health3ComponentBuilder__

#include <iostream>


class Health3ComponentBuilder {
    int _health;
    
public:
    // Constructor
    Health3ComponentBuilder();
    
    // Destructor
    ~Health3ComponentBuilder();
    
    // Methods
    int getHealth();
    void setHealth(int health);
};

#endif /* defined(__Tuple__Health3ComponentBuilder__) */
