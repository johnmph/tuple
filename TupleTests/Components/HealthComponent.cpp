//
//  HealthComponent.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "HealthComponent.hpp"


int HealthComponent::getHealth() {
    return _health;
}
