//
//  TestNotificationManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 19/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "NotificationManager.hpp"
#include "SenderNotificationManagerMock.hpp"
#include "TargetNotificationManagerMock.hpp"

//TODO: il reste des shared_ptr a transformer en make_shared ici
using namespace Tuple;

@interface TestNotificationManager : XCTestCase {
    // Notification manager
    std::shared_ptr<NotificationManager> _notificationManager;
}

@end

@implementation TestNotificationManager

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create notification manager
    _notificationManager = std::make_shared<NotificationManager>();
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    // Call parent method
    [super tearDown];
}

- (void)testRegisterAndUnregisterOneListenerToOneNotification {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener = std::make_shared<NotificationListener<TargetNotificationManagerMock, Notification1>>(target, &TargetNotificationManagerMock::Notification1Callback);
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener), @"Failed to register listener");
    
    // Unregister it
    _notificationManager->unregisterListener(listener);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::make_shared<NotificationListener<TargetNotificationManagerMock, Notification1>>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener), @"Failed to unregister listener");
}

- (void)testRegisterAndUnregisterTwoListenersOfSameTargetToTwoNotifications {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification2>(target, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister first listener
    _notificationManager->unregisterListener(listener1);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Unregister second listener
    _notificationManager->unregisterListener(listener2);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification2>(target, &TargetNotificationManagerMock::Notification2Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
}

- (void)testRegisterAndUnregisterTwoListenersOfDifferentTargetsToOneSameNotification {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification1>(target2, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister first listener
    _notificationManager->unregisterListener(listener1);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Unregister second listener
    _notificationManager->unregisterListener(listener2);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target2, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
}

- (void)testRegisterAndUnregisterTwoListenersOfDifferentTargetsToOneDifferentNotification {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification2>(target2, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister first listener
    _notificationManager->unregisterListener(listener1);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Unregister second listener
    _notificationManager->unregisterListener(listener2);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification2>(target2, &TargetNotificationManagerMock::Notification2Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
}

- (void)testRegisterAndUnregisterTwoListenersOfDifferentTargetsToTwoSameNotifications {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification2>(target1, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener3(new NotificationListener<TargetNotificationManagerMock, Notification1>(target2, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener3);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener3), @"Failed to register listener");
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener4(new NotificationListener<TargetNotificationManagerMock, Notification2>(target2, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener4);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener4), @"Failed to register listener");
    
    // Unregister first listener
    _notificationManager->unregisterListener(listener1);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Unregister second listener
    _notificationManager->unregisterListener(listener2);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification2>(target1, &TargetNotificationManagerMock::Notification2Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Unregister third listener
    _notificationManager->unregisterListener(listener3);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener3), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener3);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener3), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target2, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener3), @"Failed to unregister listener");
    
    // Unregister fourth listener
    _notificationManager->unregisterListener(listener4);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener4), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener4);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener4), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification2>(target2, &TargetNotificationManagerMock::Notification2Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener4), @"Failed to unregister listener");
}

- (void)testRegisterAndUnregisterTwoListenersOfDifferentTargetsToTwoDifferentNotifications {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification2>(target1, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener3(new NotificationListener<TargetNotificationManagerMock, Notification3>(target2, &TargetNotificationManagerMock::Notification3Callback));
    
    // Register it
    _notificationManager->registerListener(listener3);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener3), @"Failed to register listener");
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener4(new NotificationListener<TargetNotificationManagerMock, Notification4>(target2, &TargetNotificationManagerMock::Notification4Callback));
    
    // Register it
    _notificationManager->registerListener(listener4);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener4), @"Failed to register listener");
    
    // Unregister first listener
    _notificationManager->unregisterListener(listener1);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener1);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener1), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener1), @"Failed to unregister listener");
    
    // Unregister second listener
    _notificationManager->unregisterListener(listener2);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener2);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener2), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification2>(target1, &TargetNotificationManagerMock::Notification2Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener2), @"Failed to unregister listener");
    
    // Unregister third listener
    _notificationManager->unregisterListener(listener3);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener3), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener3);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener3), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification3>(target2, &TargetNotificationManagerMock::Notification3Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener3), @"Failed to unregister listener");
    
    // Unregister fourth listener
    _notificationManager->unregisterListener(listener4);
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener4), @"Failed to unregister listener");
    
    // Register again (to check register a listener object already used before)
    _notificationManager->registerListener(listener4);
    
    // Check if registered
    XCTAssertTrue(_notificationManager->isListenerRegistered(listener4), @"Failed to register listener");
    
    // Unregister it with another listener equals to registered listener
    _notificationManager->unregisterListener(std::shared_ptr<NotificationListenerBase>(new NotificationListener<TargetNotificationManagerMock, Notification4>(target2, &TargetNotificationManagerMock::Notification4Callback)));
    
    // Check if unregistered
    XCTAssertFalse(_notificationManager->isListenerRegistered(listener4), @"Failed to unregister listener");
}

- (void)testRegisterAlreadyRegisteredListener {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Register it again
    XCTAssertThrowsCPP(_notificationManager->registerListener(listener), @"Register an already registered listener must throw an exception");
}

- (void)testUnregisterNotRegisteredListener {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Unregister it
    XCTAssertThrowsCPP(_notificationManager->unregisterListener(listener), @"Unregister a no registered listener must throw an exception");
}

- (void)testSendNotificationForOneRegisteredListenerOfSameNotification {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notification that listener listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    
    // Check if notification received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification that it was registered for");
    target.reset();
    
    // Unregister it
    _notificationManager->unregisterListener(listener);
    
    // Send notification that listener listened before it was unregistered
    sender.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification that it was not registered for");
}

- (void)testSendNotificationForOneRegisteredListenerOfDifferentNotification {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notification that listener doesn't listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification2();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification2, @"Target received notification that it was not registered for");
}

- (void)testSendTwoNotificationsForTwoRegisteredListenersOfSameNotificationsWithSameTarget {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification2>(target, &TargetNotificationManagerMock::Notification2Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Send notification that listeners listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notification received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification that it was registered for");
    XCTAssertTrue(target.receivedNotification2, @"Target failed to receive notification that it was registered for");
    target.reset();
    
    // Unregister them
    _notificationManager->unregisterListener(listener1);
    _notificationManager->unregisterListener(listener2);
    
    // Send notification that listeners listened before it was unregistered
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification that it was not registered for");
    XCTAssertFalse(target.receivedNotification2, @"Target received notification that it was not registered for");
}

- (void)testSendTwoNotificationsForTwoRegisteredListenersOfDifferentNotificationsWithSameTarget {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification3>(target, &TargetNotificationManagerMock::Notification3Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification4>(target, &TargetNotificationManagerMock::Notification4Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Send notification that listeners listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification that it was not registered for");
    XCTAssertFalse(target.receivedNotification2, @"Target received notification that it was not registered for");
}

- (void)testSendOneNotificationForTwoRegisteredListenersOfSameNotificationsWithDifferentTargets {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification1>(target1, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification1>(target2, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Send notification that listeners listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    
    // Check if notification received
    XCTAssertTrue(target1.receivedNotification1, @"Target failed to receive notification that it was registered for");
    XCTAssertTrue(target2.receivedNotification1, @"Target failed to receive notification that it was registered for");
    target1.reset();
    target2.reset();
    
    // Unregister them
    _notificationManager->unregisterListener(listener1);
    _notificationManager->unregisterListener(listener2);
    
    // Send notification that listeners listened before it was unregistered
    sender.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target1.receivedNotification1, @"Target received notification that it was not registered for");
    XCTAssertFalse(target2.receivedNotification1, @"Target received notification that it was not registered for");
}

- (void)testSendTwoNotificationsForTwoRegisteredListenersOfDifferentNotificationsWithDifferentTargets {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification3>(target1, &TargetNotificationManagerMock::Notification3Callback));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification4>(target2, &TargetNotificationManagerMock::Notification4Callback));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    // Send notification that listeners listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notification not received
    XCTAssertFalse(target1.receivedNotification1, @"Target received notification that it was not registered for");
    XCTAssertFalse(target2.receivedNotification2, @"Target received notification that it was not registered for");
}

- (void)testListenersPriorities {
    TargetNotificationManagerMock target1;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener1(new NotificationListener<TargetNotificationManagerMock, Notification3>(target1, &TargetNotificationManagerMock::Notification3Callback, 1));
    
    // Register it
    _notificationManager->registerListener(listener1);
    
    TargetNotificationManagerMock target2;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener2(new NotificationListener<TargetNotificationManagerMock, Notification3>(target2, &TargetNotificationManagerMock::Notification3Callback, -1));
    
    // Register it
    _notificationManager->registerListener(listener2);
    
    TargetNotificationManagerMock target3;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener3(new NotificationListener<TargetNotificationManagerMock, Notification3>(target3, &TargetNotificationManagerMock::Notification3Callback, 3));
    
    // Register it
    _notificationManager->registerListener(listener3);
    
    // Send notification that listeners listen
    SenderNotificationManagerMock sender(_notificationManager);
    Notification3 notification3;
    sender.methodWithNotification3(notification3);
    
    // Check that priorities are ok
    XCTAssertEqual(notification3.targets[0], &target3, @"target3 must be the first target that receives the notification");
    XCTAssertEqual(notification3.targets[1], &target1, @"target1 must be the second target that receives the notification");
    XCTAssertEqual(notification3.targets[2], &target2, @"target2 must be the third target that receives the notification");
}

- (void)testListenerFilterSenderClass {
    SenderNotificationManagerMock sender1(_notificationManager);
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Set sender class
    listener->setSenderType<SenderNotificationManagerMock>();
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notification that listeners listen with bad sender
    Sender2NotificationManagerMock sender2(_notificationManager);
    sender2.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification from a sender that it was not registered for");
    
    // Send notification that listeners listen with good sender
    sender1.methodWithNotification1();
    
    // Check if notification received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification from a sender that it was registered for");
}

- (void)testListenerFilterSenderObject {
    SenderNotificationManagerMock sender1(_notificationManager);
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback, &sender1));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notification that listeners listen with bad sender
    SenderNotificationManagerMock sender2(_notificationManager);
    sender2.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification from a sender that it was not registered for");
    
    // Send notification that listeners listen with good sender
    sender1.methodWithNotification1();
    
    // Check if notification received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification from a sender that it was registered for");
}
/*
- (void)testListenerFilterSenderClassAndObject {//TODO: Can't be tested because it's an assert
    SenderNotificationManagerMock sender(_notificationManager);
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback, &sender));
    
    // Set sender class
    listener->setSenderClass<TestNotificationManagerSender>();
}*/
/*
- (void)testSendSubNotification {//TODO: avec unordered_map pour NotificationManager (pour optimisation), on ne peut pas avoir ce systeme de hierarchie entre les notifications classes
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification1>(target, &TargetNotificationManagerMock::Notification1Callback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notification with parent notification class that listener listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithSubNotification();
    
    // Check if notification received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification that it was registered for");
    target.reset();
    
    // Unregister it
    _notificationManager->unregisterListener(listener);
    
    // Send notification that listener listened before it was unregistered
    sender.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification that it was not registered for");
}*/

- (void)testSendBaseNotification {
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, SubNotification>(target, &TargetNotificationManagerMock::SubNotificationCallback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send parent notification class that listener listen
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    
    // Check if notification not received
    XCTAssertFalse(target.receivedSubNotification, @"Target received notification that it was not registered for");
}
/*
- (void)testRegisterAllNotifications {//TODO: j'ai desactivé ce systeme dans NotificationManager (voir le hpp) donc je desactive le test aussi
    TargetNotificationManagerMock target;
    
    // Create notification listener
    std::shared_ptr<NotificationListenerBase> listener(new NotificationListener<TargetNotificationManagerMock, Notification>(target, &TargetNotificationManagerMock::AllNotificationsCallback));
    
    // Register it
    _notificationManager->registerListener(listener);
    
    // Send notifications
    SenderNotificationManagerMock sender(_notificationManager);
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notifications received
    XCTAssertTrue(target.receivedNotification1, @"Target failed to receive notification that it was registered for");
    XCTAssertTrue(target.receivedNotification2, @"Target failed to receive notification that it was registered for");
    target.reset();
    
    // Unregister it
    _notificationManager->unregisterListener(listener);
    
    // Send notifications
    sender.methodWithNotification1();
    sender.methodWithNotification2();
    
    // Check if notifications not received
    XCTAssertFalse(target.receivedNotification1, @"Target received notification that it was not registered for");
    XCTAssertFalse(target.receivedNotification2, @"Target received notification that it was not registered for");
}*/

@end
