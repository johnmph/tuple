//
//  ReceiverEventManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 2/07/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#include "ReceiverEventManagerMock.hpp"


void ReceiverEventManagerMock::eventReceived(TestEvent /*const*/ &event) {
    std::cout << "eventReceived(TestEvent)" << std::endl;
}

void ReceiverEventManagerMock::eventReceived(TestEvent2 /*const*/ &event) {
    std::cout << "eventReceived(TestEvent2)" << std::endl;
}
