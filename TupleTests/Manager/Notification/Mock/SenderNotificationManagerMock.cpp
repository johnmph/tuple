//
//  SenderNotificationManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "SenderNotificationManagerMock.hpp"


SenderNotificationManagerMock::SenderNotificationManagerMock(std::shared_ptr<Tuple::NotificationManager> notificationManager) : _notificationManager(notificationManager) {
}

void SenderNotificationManagerMock::methodWithNotification1() {
    Notification1 notification1;
    
    _notificationManager->sendNotification(this, notification1);
}

void SenderNotificationManagerMock::methodWithNotification2() {
    Notification2 notification2;
    
    _notificationManager->sendNotification(this, notification2);
}

void SenderNotificationManagerMock::methodWithNotification3(Notification3 &notification3) {
    _notificationManager->sendNotification(this, notification3);
}

void SenderNotificationManagerMock::methodWithSubNotification() {
    SubNotification subNotification;
    
    _notificationManager->sendNotification(this, subNotification);
}

Sender2NotificationManagerMock::Sender2NotificationManagerMock(std::shared_ptr<Tuple::NotificationManager> notificationManager) : _notificationManager(notificationManager) {
}

void Sender2NotificationManagerMock::methodWithNotification1() {
    Notification1 notification1;
    
    _notificationManager->sendNotification(this, notification1);
}
