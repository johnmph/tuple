//
//  TargetNotificationManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__TargetNotificationManagerMock__
#define __Tuple__TargetNotificationManagerMock__

#include "NotificationManager.hpp"


class TargetNotificationManagerMock;

// Mock notifications
class Notification1 : public Tuple::Notification {
public:
    /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
    virtual std::string toStr() const;
};

class Notification2 : public Tuple::Notification {
public:
    std::string toStr() const;
};

class Notification3 : public Tuple::Notification {
public:
    std::vector<TargetNotificationManagerMock *> targets;
    std::string toStr() const;
};

class Notification4 : public Tuple::Notification {
public:
    std::string toStr() const;
};

class SubNotification : public Notification1 {
public:
    std::string toStr() const;
};

// Mock target
class TargetNotificationManagerMock {
public:
    bool receivedNotification1;
    bool receivedNotification2;
    bool receivedSubNotification;
    
    // Constructor
    TargetNotificationManagerMock();
    
    // Methods
    void reset();
    void Notification1Callback(Notification1 &notification1);
    void Notification2Callback(Notification2 &notification2);
    void Notification3Callback(Notification3 &notification3);
    void Notification4Callback(Notification4 &notification4);
    void SubNotificationCallback(SubNotification &subNotification);
    void AllNotificationsCallback(Tuple::Notification &notification);
};

#endif /* defined(__Tuple__TargetNotificationManagerMock__) */
