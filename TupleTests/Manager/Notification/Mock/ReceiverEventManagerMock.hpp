//
//  ReceiverEventManagerMock.h
//  Tuple
//
//  Created by Jonathan Baliko on 2/07/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ReceiverEventManagerMock__
#define __Tuple__ReceiverEventManagerMock__

#include "EventManager.hpp"


class TestEvent : public Tuple::Event {
};

class TestEvent2 : public Tuple::Event {
};

class ReceiverEventManagerMock {
public:
    void eventReceived(TestEvent /*const*/ &event);
    void eventReceived(TestEvent2 /*const*/ &event);
};

#endif /* defined(__Tuple__ReceiverEventManagerMock__) */
