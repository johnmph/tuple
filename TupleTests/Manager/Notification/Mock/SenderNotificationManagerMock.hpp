//
//  SenderNotificationManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SenderNotificationManagerMock__
#define __Tuple__SenderNotificationManagerMock__

#include "TargetNotificationManagerMock.hpp"


// Mock sender
class SenderNotificationManagerMock {
    std::shared_ptr<Tuple::NotificationManager> _notificationManager;
    
public:
    // Constructor
    explicit SenderNotificationManagerMock(std::shared_ptr<Tuple::NotificationManager> notificationManager);
    
    // Methods
    void methodWithNotification1();
    void methodWithNotification2();
    void methodWithNotification3(Notification3 &notification3);
    void methodWithSubNotification();
};

// Another mock sender to check sender filter
class Sender2NotificationManagerMock {
    std::shared_ptr<Tuple::NotificationManager> _notificationManager;
    
public:
    // Constructor
    explicit Sender2NotificationManagerMock(std::shared_ptr<Tuple::NotificationManager> notificationManager);
    
    // Methods
    void methodWithNotification1();
};

#endif /* defined(__Tuple__SenderNotificationManagerMock__) */
