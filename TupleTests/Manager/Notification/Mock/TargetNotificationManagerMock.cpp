//
//  TargetNotificationManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "TargetNotificationManagerMock.hpp"


std::string Notification1::toStr() const {
    return "Notification1 object";
}

std::string Notification2::toStr() const {
    return "Notification2 object";
}

std::string Notification3::toStr() const {
    return "Notification3 object";
}

std::string Notification4::toStr() const {
    return "Notification4 object";
}

std::string SubNotification::toStr() const {
    return "SubNotification object";
}


TargetNotificationManagerMock::TargetNotificationManagerMock() : receivedNotification1(false), receivedNotification2(false) {
}

void TargetNotificationManagerMock::reset() {
    // Reset flags
    receivedNotification1 = false;
    receivedNotification2 = false;
    receivedSubNotification = false;
}

void TargetNotificationManagerMock::Notification1Callback(Notification1 &notification1) {
    //std::cout << "Notification1Callback: " << notification1.toStr() << std::endl;
    // Set received notification1 flag
    receivedNotification1 = true;
}

void TargetNotificationManagerMock::Notification2Callback(Notification2 &notification2) {
    //std::cout << "Notification2Callback: " << notification2.toStr() << std::endl;
    // Set received notification2 flag
    receivedNotification2 = true;
}

void TargetNotificationManagerMock::Notification3Callback(Notification3 &notification3) {
    notification3.targets.push_back(this);
}

void TargetNotificationManagerMock::Notification4Callback(Notification4 &notification4) {
}

void TargetNotificationManagerMock::SubNotificationCallback(SubNotification &subNotification) {
    receivedSubNotification = true;
}

void TargetNotificationManagerMock::AllNotificationsCallback(Tuple::Notification &notification) {
    // Try to get notification as notification1
    Notification1 *notification1 = dynamic_cast<Notification1 *>(&notification);
    
    // If ok
    if (notification1) {
        // Manage it as Notification1
        Notification1Callback(*notification1);
    } else {
        // Try to get notification as notification2
        Notification2 *notification2 = dynamic_cast<Notification2 *>(&notification);
        
        // If ok
        if (notification2) {
            // Manage it as Notification2
            Notification2Callback(*notification2);
        }
    }
}
