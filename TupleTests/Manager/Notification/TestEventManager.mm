//
//  TestEventManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 2/07/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "ReceiverEventManagerMock.hpp"


using namespace Tuple;

@interface TestEventManager : XCTestCase {
    // Event manager
    std::shared_ptr<EventManager> _eventManager;
    
    ReceiverEventManagerMock _receiver;
}

@end

@implementation TestEventManager

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create event manager
    _eventManager = std::make_shared<EventManager>();
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    // Call parent method
    [super tearDown];
}

- (void)testRegisterAndUnregisterOneReceiverToOneEvent {
    assert(sizeof(std::unique_ptr<std::vector<void *>>) == sizeof(std::unique_ptr<std::vector<ReceiverEventManagerMock>>));
    
    _eventManager->registerListener<TestEvent>(_receiver);
    _eventManager->registerListener<TestEvent2>(_receiver);
    
    TestEvent testEvent;
    _eventManager->sendEvent(_eventManager.get(), testEvent);
    
    TestEvent2 testEvent2;
    _eventManager->sendEvent(_eventManager.get(), testEvent2);
    
    // TODO: probleme : on doit specifier le listener ET le type de l'event alors que chaque listener a son event type (mais pas dans la classe) + on a un listener par type d'event enregistré, pas bon
    //_eventManager.reset();
    _eventManager->unregisterListener<TestEvent>(_receiver);
    _eventManager->unregisterListener<TestEvent2>(_receiver);
}

@end
