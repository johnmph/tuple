//
//  TestSmartLink.mm
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "MemoryManager.hpp"
#include "SmartLink.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "ChildObjectMemoryManagerMock.hpp"
#include "ReferenceCountObjectMemoryManagerMock.hpp"


using namespace Tuple;

@interface TestSmartLink : XCTestCase {
    // Memory manager
    std::shared_ptr<MemoryManager> _memoryManager;
}

@end

@implementation TestSmartLink

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    _memoryManager = std::make_shared<MemoryManager>();
}

- (void)tearDown {
    
    // Call parent method
    [super tearDown];
}

- (void)testStructure {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a SmartLink with its default constructor
    SmartLink<ChildObjectMemoryManagerMock> smartLinkDefault;
    
    // Create a SmartLink with its empty constructor
    SmartLink<ChildObjectMemoryManagerMock> smartLinkEmpty(nullptr);
    
    // Create a SmartLink with its parametrized constructor
    SmartLink<ChildObjectMemoryManagerMock> smartLink(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Copy construct a SmartLink
    SmartLink<ChildObjectMemoryManagerMock> smartLinkCopyConstruct(smartLink);
    
    // Copy construct an empty SmartLink
    SmartLink<ChildObjectMemoryManagerMock> smartLinkCopyConstructEmpty(smartLinkEmpty);
    
    // Copy assign a SmartLink
    smartLinkCopyConstruct = smartLink;
    
    // Copy assign a SmartLink to himself
    smartLinkCopyConstruct = smartLinkCopyConstruct;
    
    // Copy assign a SmartLink to nullptr
    smartLinkCopyConstruct = nullptr;
    
    // Copy assign an empty SmartLink
    smartLinkCopyConstructEmpty = smartLinkEmpty;
    
    // Move construct a SmartLink
    SmartLink<ChildObjectMemoryManagerMock> smartLinkMoveConstruct(std::move(smartLink));
    
    // Move construct an empty SmartLink
    SmartLink<ChildObjectMemoryManagerMock> smartLinkMoveConstructEmpty(std::move(smartLinkEmpty));
    
    // Move assign a SmartLink
    smartLinkMoveConstruct = std::move(smartLink);
    
    // Move assign a SmartLink to himself
    smartLinkMoveConstruct = std::move(smartLinkMoveConstruct);
    
    // Move assign an empty SmartLink
    smartLinkMoveConstructEmpty = std::move(smartLinkEmpty);
    
    /*//TODO: unit test compile-time et meme syntax time !!!
     // Forbidden coercion copy construct a SmartLink
     SmartLink<BaseObjectMemoryManagerMock> smartLinkForbiddenCoercionCopy(smartLink);
     
     // Forbidden coercion copy assign a SmartLink
     smartLinkForbiddenCoercionCopy = smartLink;
     
     // Forbidden coercion move construct a SmartLink
     SmartLink<BaseObjectMemoryManagerMock> smartLinkForbiddenCoercionMove(std::move(smartLink));
     
     // Forbidden coercion move assign a SmartLink
     smartLinkForbiddenCoercionMove = std::move(smartLink);*/
}

- (void)testGet {
}

- (void)testEqualityOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    SmartLink<ChildObjectMemoryManagerMock> smartLink(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Create another ChildObjectMemoryManagerMock object
    SmartLink<ChildObjectMemoryManagerMock> smartLink2(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that smart links are not equal
    XCTAssertFalse(smartLink == smartLink2, @"Smart links must not be equal but they are");
    XCTAssertTrue(smartLink != smartLink2, @"Smart links must not be equal but they are");
    
    // Copy smartLink to smartLink2
    smartLink2 = smartLink;
    
    // Test that smart links are equal
    XCTAssertTrue(smartLink == smartLink2, @"Smart links must be equal but they are not");
    XCTAssertFalse(smartLink != smartLink2, @"Smart links must be equal but they are not");
    
    // Get smartLink as BaseObjectMemoryManagerMock SmartLink
    SmartLink<BaseObjectMemoryManagerMock> smartLinkBase(smartLink);//TODO: ca ne marche pas car on veut convertir le smartLink et pas son MemoryHandle mais on est obligé d'utiliser ce constructor car si on fait smartLinkBase(smartLink.getMemoryHandle()), on va avoir 2 smart links differents pointant vers le meme MemoryHandle et ce n'est pas bon car des qu'un va etre supprimé ca va supprimer l'objet sans se soucier de l'autre smart link
    
    // Test that smart links are equal
    XCTAssertTrue(smartLink == smartLinkBase, @"Smart links must be equal but they are not");
    XCTAssertFalse(smartLink != smartLinkBase, @"Smart links must be equal but they are not");
    
    // Test that smartLink is not nullptr with equality operators
    XCTAssertFalse(smartLink == nullptr, @"Smart link must not be nullptr");
    XCTAssertTrue(smartLink != nullptr, @"Smart link must not be nullptr");
    
    // Reset smartLink
    smartLink = nullptr;
    
    // Test that smartLink is nullptr with equality operators
    XCTAssertTrue(nullptr == smartLink, @"Smart link must be nullptr");
    XCTAssertFalse(nullptr != smartLink, @"Smart link must be nullptr");
    
    // Test that smart links are not equal
    XCTAssertFalse(smartLink == smartLink2, @"Smart links must not be equal but they are");
    XCTAssertTrue(smartLink != smartLink2, @"Smart links must not be equal but they are");
    
    // Reset memoryHandle2
    smartLink2 = nullptr;
    
    // Test that smart links are equal
    XCTAssertTrue(smartLink == smartLink2, @"Smart links must be equal but they are not");
    XCTAssertFalse(smartLink != smartLink2, @"Smart links must be equal but they are not");
}

- (void)testDowncastTo {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    SmartLink<ChildObjectMemoryManagerMock> smartLink(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Copy smartLink to a SmartLink with the base class as type
    SmartLink<BaseObjectMemoryManagerMock> baseSmartLink(smartLink);
    
    // Test that smart links are equal
    XCTAssertTrue(smartLink == baseSmartLink, @"Smart links must be equal but they are not");
    
    // Downcast to ChildObjectMemoryManagerMock type
    SmartLink<ChildObjectMemoryManagerMock> downcastedSmartLink = downcastTo<ChildObjectMemoryManagerMock>(std::move(baseSmartLink));//TODO: ici et dans les autres tests : le downcastTo des smartlink copie l'objet dans un autre type, il ne le move pas, ce qui fait que si on fait un downcastTo pour remettre le bon type et qu'on supprime cet objet, le destructor va etre appelé et comme il a le bon type c'est ok, mais si on fait ca avant de supprimer l'upcasted smartptr, le downcasted ne liberera pas l'objet car il reste la reference de l'upcasted puis dans le destructor de l'upcasted, il n'a pas le bon type et donc probleme !!! il faut donc que le downcastTo fasse un move et pas un copy !!! on peut avoir aussi un policy dans smartlink pour sauver le type ou non et c'est ce qu'on utiliserai pour "downcaster" un smartlink quand on ne connaitrai pas le type statique mais qu'on connaitrai le type sous forme size_t (comme dans l'entity manager)
    
    // Test that smart links are equal
    XCTAssertTrue(smartLink == downcastedSmartLink, @"Smart links must be equal but they are not");
    
    // Downcast to Child2ObjectMemoryManagerMock type
    //downcastedSmartLink = baseSmartLink.downcastTo<Child2ObjectMemoryManagerMock>();//TODO: il faut tester ca en unit test compile-time !!
    
    //TODO: tester plusieurs niveaux de hierarchie ?
}

- (void)testReferenceCount {//TODO: separer ca en petits tests
    // Reset count of ReferenceCountObjectMemoryManagerMock
    ReferenceCountObjectMemoryManagerMock::count = 0;
    
    // Register a memory allocator for ReferenceCountObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ReferenceCountObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ReferenceCountObjectMemoryManagerMock, count must be 1
    MemoryHandle<ReferenceCountObjectMemoryManagerMock> referenceCountObjectMemoryManagerMock = _memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>();
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create another ReferenceCountObjectMemoryManagerMock, count must be 2
    MemoryHandle<ReferenceCountObjectMemoryManagerMock> referenceCountObjectMemoryManagerMock2 = _memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>();
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create local scope to delete first ReferenceCountObjectMemoryManagerMock object by having a temporary SmartLink holding it
    {
        // Create SmartLink to hold first ReferenceCountObjectMemoryManagerMock object
        SmartLink<ReferenceCountObjectMemoryManagerMock> smartLink(referenceCountObjectMemoryManagerMock);
    }
    
    // Test that we deleted the object
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create SmartLink to hold second ReferenceCountObjectMemoryManagerMock object
    SmartLink<ReferenceCountObjectMemoryManagerMock> smartLink(referenceCountObjectMemoryManagerMock2);
    
    // Copy construct a SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink and smartLinkCopyConstruct
    SmartLink<ReferenceCountObjectMemoryManagerMock> smartLinkCopyConstruct(smartLink);
    
    // Reset smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstruct
    smartLink = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Copy assign a SmartLink to a empty SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstruct and smartLinkEmpty
    SmartLink<ReferenceCountObjectMemoryManagerMock> smartLinkEmpty;
    smartLinkEmpty = smartLinkCopyConstruct;
    
    // Reset smartLinkCopyConstruct : referenceCountObjectMemoryManagerMock2 is managed by smartLinkEmpty
    smartLinkCopyConstruct = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Set smartLink
    smartLink = smartLinkEmpty;
    
    // Reset smartLinkEmpty : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLinkEmpty = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create a SmartLink which contains another ReferenceCountObjectMemoryManagerMock
    SmartLink<ReferenceCountObjectMemoryManagerMock> smartLinkOther(_memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>());
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Copy assign a SmartLink to a SmartLink which contains different data : referenceCountObjectMemoryManagerMock2 is managed by smartLink and smartLinkOther
    smartLinkOther = smartLink;
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Reset smartLinkOther : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLinkOther = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Set smartLinkCopyConstruct
    smartLinkCopyConstruct = smartLink;
    
    // Copy assign a SmartLink to a SmartLink which contains the same data : referenceCountObjectMemoryManagerMock2 is managed by smartLink and smartLinkCopyConstruct
    smartLinkCopyConstruct = smartLink;
    
    // Reset smartLinkCopyConstruct
    smartLinkCopyConstruct = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move construct a SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkMoveConstruct
    SmartLink<ReferenceCountObjectMemoryManagerMock> smartLinkMoveConstruct(std::move(smartLink));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move assign a SmartLink to a empty SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkEmpty
    smartLinkEmpty = std::move(smartLinkMoveConstruct);
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Reset smartLinkEmpty and smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = std::move(smartLinkEmpty);
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create a SmartLink which contains another ReferenceCountObjectMemoryManagerMock
    smartLinkOther = SmartLink<ReferenceCountObjectMemoryManagerMock>(_memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>());
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move assign a SmartLink to a SmartLink which contains different data : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther
    smartLinkOther = std::move(smartLink);
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Reset smartLinkOther and smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = std::move(smartLinkOther);
    
    // Set smartLinkCopyConstruct
    smartLinkCopyConstruct = smartLink;
    
    // Move assign a SmartLink to a SmartLink which contains same data : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstruct
    smartLinkCopyConstruct = std::move(smartLink);
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Reset smartLinkCopyConstruct and smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = std::move(smartLinkCopyConstruct);
    
    // Copy construct (with conversion) a SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink and smartLinkCopyConstructConverted
    SmartLink<BaseReferenceCountObjectMemoryManagerMock> smartLinkCopyConstructConverted(smartLink);
    
    // Reset smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstructConvert
    smartLink = nullptr;
    
    // Downcast smartLinkCopyConstructConvert in smartLinkOther : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther
    smartLinkOther = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkCopyConstructConverted));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Copy assign a SmartLink to a empty SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther and smartLinkEmptyConverted
    SmartLink<BaseReferenceCountObjectMemoryManagerMock> smartLinkEmptyConverted;
    smartLinkEmptyConverted = smartLinkOther;
    
    // Reset smartLinkOther : referenceCountObjectMemoryManagerMock2 is managed by smartLinkEmptyConverted
    smartLinkOther = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkEmptyConverted in smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkEmptyConverted));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create a SmartLink which contains another ReferenceCountObjectMemoryManagerMock
    SmartLink<BaseReferenceCountObjectMemoryManagerMock> smartLinkOtherConverted(_memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>());
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Copy assign a SmartLink to a SmartLink which contains different data : referenceCountObjectMemoryManagerMock2 is managed by smartLink and smartLinkOtherConverted
    smartLinkOtherConverted = smartLink;
    
    // Reset smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOtherConverted
    smartLink = nullptr;
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkOtherConverted in smartLinkOther : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther
    smartLinkOther = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkOtherConverted));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Set smartLinkCopyConstructConverted : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther and smartLinkCopyConstructConverted
    smartLinkCopyConstructConverted = smartLinkOther;
    
    // Copy assign a SmartLink to a SmartLink which contains the same data : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOther and smartLinkCopyConstructConverted
    smartLinkCopyConstructConverted = smartLinkOther;
    
    // Reset smartLinkOther : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstructConverted
    smartLinkOther = nullptr;
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkCopyConstructConverted in smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkCopyConstructConverted));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move construct a SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkMoveConstructConverted
    SmartLink<BaseReferenceCountObjectMemoryManagerMock> smartLinkMoveConstructConverted(std::move(smartLink));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move assign a SmartLink to a empty SmartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLinkEmptyConverted
    smartLinkEmptyConverted = std::move(smartLinkMoveConstructConverted);
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkEmptyConverted in smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkEmptyConverted));
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Create a SmartLink which contains another ReferenceCountObjectMemoryManagerMock
    smartLinkOtherConverted = SmartLink<BaseReferenceCountObjectMemoryManagerMock>(_memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>());
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Move assign a SmartLink to a SmartLink which contains different data : referenceCountObjectMemoryManagerMock2 is managed by smartLinkOtherConverted
    smartLinkOtherConverted = std::move(smartLink);
    
    // Test that reference count is correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkOtherConverted in smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkOtherConverted));
    
    // Set smartLinkCopyConstructConverted
    smartLinkCopyConstructConverted = smartLink;
    
    // Move assign a SmartLink to a SmartLink which contains same data : referenceCountObjectMemoryManagerMock2 is managed by smartLinkCopyConstructConverted
    smartLinkCopyConstructConverted = std::move(smartLink);
    
    // Test that reference count is always correct
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Downcast smartLinkCopyConstructConverted in smartLink : referenceCountObjectMemoryManagerMock2 is managed by smartLink
    smartLink = downcastTo<ReferenceCountObjectMemoryManagerMock>(std::move(smartLinkCopyConstructConverted));
    
    // Reset smartLink : referenceCountObjectMemoryManagerMock2 is not managed anymore
    smartLink = nullptr;
    
    // Test that we deleted the object
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 0, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 0 but is %d", ReferenceCountObjectMemoryManagerMock::count);
}

@end
