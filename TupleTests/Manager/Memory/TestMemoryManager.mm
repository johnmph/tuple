//
//  TestMemoryManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 22/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "MemoryManager.hpp"
#include "SmartLink.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "ReferenceCountObjectMemoryManagerMock.hpp"
#include "HealthComponent.hpp"
#include "Health2Component.hpp"


using namespace Tuple;

@interface TestMemoryManager : XCTestCase {
    // Memory manager
    std::shared_ptr<MemoryManager> _memoryManager;
}

@end

@implementation TestMemoryManager

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    _memoryManager = std::make_shared<MemoryManager>();
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    // Call parent method
    [super tearDown];
}
/*
- (void)testClassType {//TODO: a mettre dans un TestTypeManager.mm
    // Test that class type is same between class and instance of the class
    HealthComponent healthComponent(100);
    size_t classType = _memoryManager->getClassTypeForClass<HealthComponent>();
    size_t classInstanceType = _memoryManager->getClassTypeForObject(&healthComponent);
    STAssertEquals(classType, classInstanceType, @"Class have %d type but one instance of this class have %d type", classType, classInstanceType);
    
    // Test that class type is same between class and instance of the class with base pointer
    Component *component = &healthComponent;
    classInstanceType = _memoryManager->getClassTypeForObject(component);
    STAssertEquals(classType, classInstanceType, @"Class have %d type but one instance of this class have %d type", classType, classInstanceType);
    
    // Test that class type is different between base class and instance of the subclass
    classType = _memoryManager->getClassTypeForClass<Component>();
    classInstanceType = _memoryManager->getClassTypeForObject(&healthComponent);
    STAssertFalse(classType == classInstanceType, @"Both base class and derived class instance have %d type", classType, classInstanceType);
}*/
//TODO: ecrire les tests pour fixed et moveable memory allocators !!!
- (void)testRegisterMemoryAllocator {
    // Create a memory allocator unique ptr with a default memory allocator
    std::unique_ptr<MoveableMemoryAllocator> defaultMemoryAllocatorUPtr(new DefaultMemoryAllocator());
    
    // Register it in memory manager
    _memoryManager->registerMemoryAllocatorForClass<HealthComponent>(std::move(defaultMemoryAllocatorUPtr));
    
    // Test that memory manager owns memory allocator by asking hit to have a HealthComponent object
    int health = 50;
    //HealthComponent *healthComponent = _memoryManager->newDataWithType<HealthComponent>(health);
    MemoryHandle<HealthComponent> healthComponentHandle = _memoryManager->newDataWithType<HealthComponent>(health);
    HealthComponent *healthComponent = healthComponentHandle.get();
    
    XCTAssertTrue(healthComponent != nullptr, @"healthComponent must not be nullptr");
    //_memoryManager->deleteObject(healthComponentHandle);
    
    // Test that memory allocator unique ptr loses ownership of default memory allocator
    XCTAssertTrue(defaultMemoryAllocatorUPtr.get() == nullptr, @"Memory allocator unique ptr must be nullptr, current value is 0x%p", defaultMemoryAllocatorUPtr.get());
}

- (void)testRegisterMemoryAllocatorsSameType {
    // Register a memory allocator for HealthComponent
    _memoryManager->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Test that trying to register a memory allocator for HealthComponent again throws an exception
    XCTAssertThrowsCPP(_memoryManager->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator())), @"Register memory allocator for a class which has already a memory allocator registered must throw a std::logic_error exception");
}

- (void)testNoRegisteredMemoryAllocator {
    // Register a memory allocator for HealthComponent
    _memoryManager->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    try {
        // Try to get a HealthComponent2
        //Health2Component *health2Component = _memoryManager->newDataWithType<Health2Component>();
        MemoryHandle<Health2Component> health2ComponentHandle = _memoryManager->newDataWithType<Health2Component>();
        
        // If no exception caught, it is an error
        XCTFail(@"Get an object from memory manager for a class which has not a memory allocator registered must throw a std::logic_error exception");
        
        // Delete object
        MemoryManager::deleteData(health2ComponentHandle);
    }
    catch(std::logic_error const &e) {
        // Ok, caught the exception
    }
}

- (void)testGetObject {
    // Reset count of ReferenceCountObjectMemoryManagerMock
    ReferenceCountObjectMemoryManagerMock::count = 0;
    
    // Register a memory allocator for ReferenceCountObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ReferenceCountObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Get an object
    MemoryHandle<ReferenceCountObjectMemoryManagerMock> referenceCountObjectMemoryManagerMockMemoryHandle = _memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>();
    ReferenceCountObjectMemoryManagerMock *referenceCountObjectMemoryManagerMock = referenceCountObjectMemoryManagerMockMemoryHandle.get();
    
    // Test that we got the object
    XCTAssertTrue(referenceCountObjectMemoryManagerMock != nullptr, @"ReferenceCountObjectMemoryManagerMock must not be nullptr");
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Get another object
    MemoryHandle<ReferenceCountObjectMemoryManagerMock> referenceCountObjectMemoryManagerMockMemoryHandle2 = _memoryManager->newDataWithType<ReferenceCountObjectMemoryManagerMock>();
    ReferenceCountObjectMemoryManagerMock *referenceCountObjectMemoryManagerMock2 = referenceCountObjectMemoryManagerMockMemoryHandle2.get();
    
    // Test that we got the object
    XCTAssertTrue(referenceCountObjectMemoryManagerMock2 != nullptr, @"referenceCountObjectMemoryManagerMock2 must not be nullptr");
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 2, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 2 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Delete first object
    {
        SmartLink<ReferenceCountObjectMemoryManagerMock> temp(referenceCountObjectMemoryManagerMockMemoryHandle);
    }
    
    // Test that we free the object
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 1, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 1 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    
    // Test that trying to delete first object again throws an exception
    //XCTAssertThrowsCPP(_memoryManager->deleteObject(referenceCountObjectMemoryManagerMockMemoryHandle), @"Free an object not allocated must throw a std::logic_error exception");//TODO: a voir
    
    // Delete second object
    {
        //SmartLink<ReferenceCountObjectMemoryManagerMock> temp = referenceCountObjectMemoryManagerMockMemoryHandle2; // TODO: ne marche pas si je met explicit ce qui est normal mais est ce que je dois autoriser ce genre d'ecriture ?
        SmartLink<ReferenceCountObjectMemoryManagerMock> temp(referenceCountObjectMemoryManagerMockMemoryHandle2);
    }
    
    // Test that we free the object
    XCTAssertEqual(ReferenceCountObjectMemoryManagerMock::count, 0, @"ReferenceCountObjectMemoryManagerMock::count must be equal to 0 but is %d", ReferenceCountObjectMemoryManagerMock::count);
    /*
    try {
        // Create (local) health component
        HealthComponent healthComponent(100);
        
        // Try to delete an object of unregistered memory allocator class
        _memoryManager->deleteObject(&healthComponent);
        
        // If no exception caught, it is an error
        STFail(@"Free an object not allocated must throw a std::logic_error exception");
    }
    catch(std::invalid_argument const &e) {
        // Ok, caught the exception
    }*/
}

@end
