//
//  TestMemoryPointer.mm
//  Tuple
//
//  Created by Jonathan Baliko on 28/06/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "MemoryManager.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "ChildObjectMemoryManagerMock.hpp"
#include "Child2ObjectMemoryManagerMock.hpp"


using namespace Tuple;

@interface TestMemoryPointer : XCTestCase {
    // Memory manager
    std::shared_ptr<MemoryManager> _memoryManager;
}

@end

@implementation TestMemoryPointer

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    _memoryManager = std::make_shared<MemoryManager>();
}

- (void)tearDown {
    
    // Call parent method
    [super tearDown];
}

- (void)testStructure {
    // Create a MemoryPointer with its parametrized constructor
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(nullptr, nullptr);
    
    // Copy construct a MemoryPointer
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointerCopy(memoryPointer);
    
    // Copy assign a MemoryPointer
    memoryPointerCopy = memoryPointer;
    
    // Copy assign a MemoryPointer to himself
    memoryPointerCopy = memoryPointerCopy;
    
    // Move construct a MemoryPointer
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointerMove(std::move(memoryPointer));
    
    // Move assign a MemoryPointer
    memoryPointerMove = std::move(memoryPointer);
    
    // Move assign a MemoryPointer to himself
    memoryPointerMove = std::move(memoryPointerMove);
    
    // Authorized coercion copy construct a MemoryPointer
    MemoryPointer<BaseObjectMemoryManagerMock> memoryPointerAuthorizedCoercionCopy(memoryPointer);
    
    // Authorized coercion copy assign a MemoryPointer
    memoryPointerAuthorizedCoercionCopy = memoryPointer;
    
    // Authorized coercion move construct a MemoryPointer
    MemoryPointer<BaseObjectMemoryManagerMock> memoryPointerAuthorizedCoercionMove(std::move(memoryPointer));
    
    // Authorized coercion move assign a MemoryPointer
    memoryPointerAuthorizedCoercionMove = std::move(memoryPointer);
    /*//TODO: unit test compile-time : doit etre dans un autre fichier, voir ca : http://stackoverflow.com/questions/8870381/how-to-expect-a-static-assert-failure-and-deal-with-it-using-boost-test-framewor et http://stackoverflow.com/questions/605915/unit-test-compile-time-error
     // Forbidden coercion copy construct a MemoryPointer
     MemoryPointer<Child2ObjectMockMemoryManager> memoryPointerForbiddenCoercionCopy(memoryPointer);
     
     // Forbidden coercion copy assign a MemoryPointer
     memoryPointerForbiddenCoercionCopy = memoryPointer;
     
     // Forbidden coercion move construct a MemoryPointer
     MemoryPointer<Child2ObjectMockMemoryManager> memoryPointerForbiddenCoercionMove(std::move(memoryPointer));
     
     // Forbidden coercion move assign a MemoryPointer
     memoryPointerForbiddenCoercionMove = std::move(memoryPointer);
     */
    
    
    //MemoryPointer memoryPointer(nullptr, 0, Tuple::Pointer{ 1, 2 });//TODO: est ce qu'on peut faire ca
    //    MemoryPointer memoryPointer2; // TODO: ok, vide on est obligé de le garder car il vaut = nullptr et on l'utilise pour reseter le Pointer
    // TODO: est ce qu'on peut instancier un MemoryPointer ? ou abstract class et on serait obligé d'instancier un RawDataPointer ou un TypePointer ?
    //  TypePointer<int> typePointer(memoryPointer2);
    //    TypePointer<int> typePointer(memoryPointer);
    //    std::cout << typePointer << std::endl;
    /*
     TypePointer<Tuple::Component> component(nullptr);
     TypePointer<HealthComponent> healthComponent(component);
     TypePointer<HealthComponent> healthComponent2(TypePointer<Tuple::Component>(nullptr));
     healthComponent2 = component;*/
    /*
     TypePointer<HealthComponent> healthComponent(nullptr);
     TypePointer<Tuple::Component> component(healthComponent);
     TypePointer<Tuple::Component> component2(TypePointer<HealthComponent>(nullptr));
     component2 = healthComponent;*/
}

- (void)testGet {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that get method is ok
    //XCTAssertTrue(memoryPointer.get() == memoryPointer.getMemoryAllocator()->getMemoryForPointer(memoryPointer.getPointer()), @"get must be correct but it is not");
    XCTAssertTrue(memoryPointer.get() != nullptr, @"get must be correct but it is not");
    
    // Delete memoryPointer data
    MemoryManager::deleteData(memoryPointer);
}

- (void)testGetters {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    FixedMemoryAllocator *registeredMemoryAllocator = new DefaultMemoryAllocator();
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(registeredMemoryAllocator));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    /*
    // Get memory allocator of memoryPointer
    FixedMemoryAllocator *memoryAllocator = memoryPointer.getMemoryAllocator();
    
    // Test that memory allocator is ok
    XCTAssertTrue(memoryAllocator == registeredMemoryAllocator, @"Memory allocator must be correct but it is not");*/
    /*
    // Get pointer of memoryPointer
    void *pointer = memoryPointer.getPointer();
    
    // Test that pointer is ok
    XCTAssertFalse(pointer == nullptr, @"Pointer must not be an invalid pointer");
    XCTAssertTrue(registeredMemoryAllocator->getMemoryForPointer(pointer) == memoryPointer.get(), @"Pointer must be correct but it is not");*/
    XCTAssertFalse(memoryPointer.get() == nullptr, @"Pointer must not be an invalid pointer");
    /*
    // Get type of memoryPointer data
    size_t type = memoryPointer.getType();
    
    // Test that type is ok
    XCTAssertFalse(type == 0, @"Type must not be null");
    XCTAssertTrue(type == TypeManager::getTypeIDForType<ChildObjectMemoryManagerMock>(), @"Type must be correct but it is not");
    */
    // Delete memoryPointer data
    MemoryManager::deleteData(memoryPointer);
}

- (void)testEqualityOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Create another ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer2(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that memory pointers are not equal
    XCTAssertFalse(memoryPointer == memoryPointer2, @"Memory pointers must not be equal but they are");
    XCTAssertTrue(memoryPointer != memoryPointer2, @"Memory pointers must not be equal but they are");
    
    // Delete memoryPointer2 data
    MemoryManager::deleteData(memoryPointer2);
    
    // Copy memoryPointer to memoryPointer2
    memoryPointer2 = memoryPointer;
    
    // Test that memory pointers are equal
    XCTAssertTrue(memoryPointer == memoryPointer2, @"Memory pointers must be equal but they are not");
    XCTAssertFalse(memoryPointer != memoryPointer2, @"Memory pointers must be equal but they are not");
    
    // Get memoryPointer as BaseObjectMemoryManagerMock MemoryPointer
    MemoryPointer<BaseObjectMemoryManagerMock> memoryPointerBase(memoryPointer);
    
    // Test that memory pointers are equal
    XCTAssertTrue(memoryPointer == memoryPointerBase, @"Memory pointers must be equal but they are not");
    XCTAssertFalse(memoryPointer != memoryPointerBase, @"Memory pointers must be equal but they are not");
    
    // Test that memoryPointer is not nullptr with equality operators
    XCTAssertFalse(memoryPointer == nullptr, @"Memory pointer must not be nullptr");
    XCTAssertTrue(memoryPointer != nullptr, @"Memory pointer must not be nullptr");
    
    // Reset memoryPointer
    memoryPointer = MemoryPointer<ChildObjectMemoryManagerMock>(nullptr, nullptr);
    
    // Test that memoryPointer is nullptr with equality operators
    XCTAssertTrue(nullptr == memoryPointer, @"Memory pointer must be nullptr");
    XCTAssertFalse(nullptr != memoryPointer, @"Memory pointer must be nullptr");
    
    // Test that memory pointers are not equal
    XCTAssertFalse(memoryPointer == memoryPointer2, @"Memory pointers must not be equal but they are");
    XCTAssertTrue(memoryPointer != memoryPointer2, @"Memory pointers must not be equal but they are");
    
    // Delete memoryPointer2 data
    MemoryManager::deleteData(memoryPointer2);
    
    // Reset memoryPointer2
    memoryPointer2 = MemoryPointer<ChildObjectMemoryManagerMock>(nullptr, nullptr);
    
    // Test that memory pointers are equal
    XCTAssertTrue(memoryPointer == memoryPointer2, @"Memory pointers must be equal but they are not");
    XCTAssertFalse(memoryPointer != memoryPointer2, @"Memory pointers must be equal but they are not");
}

- (void)testDowncastTo {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Copy memoryPointer to a MemoryPointer with the base class as type
    MemoryPointer<BaseObjectMemoryManagerMock> baseMemoryPointer(memoryPointer);
    
    // Test that memory pointers are equal
    XCTAssertTrue(memoryPointer == baseMemoryPointer, @"Memory pointers must be equal but they are not");
    
    // Downcast to ChildObjectMemoryManagerMock type
    MemoryPointer<ChildObjectMemoryManagerMock> downcastedMemoryPointer = downcastTo<ChildObjectMemoryManagerMock>(std::move(baseMemoryPointer));
    
    // Test that memory pointers are equal
    XCTAssertTrue(memoryPointer == downcastedMemoryPointer, @"Memory pointers must be equal but they are not");
    
    // Downcast to Child2ObjectMemoryManagerMock type
    //downcastedMemoryPointer = baseMemoryPointer.downcastTo<Child2ObjectMemoryManagerMock>();//TODO: il faut tester ca en unit test compile-time !!
    
    // Delete memoryPointer data
    MemoryManager::deleteData(memoryPointer);
    
    //TODO: tester plusieurs niveaux de hierarchie ?
}

- (void)testDereferenceOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryPointer<ChildObjectMemoryManagerMock> memoryPointer(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that * operator is ok
    XCTAssertTrue(&(*memoryPointer) == memoryPointer.get(), @"Operator * must be correct but it is not");
    
    // TODO: il faut tester -> aussi et il faut tester les 2 avec un type qui ne le permet pas comme void (tester ca au compile time)
    
    // Delete memoryPointer data
    MemoryManager::deleteData(memoryPointer);
}

@end
