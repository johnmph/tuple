//
//  TestMemoryHandle.mm
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "MemoryManager.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "ChildObjectMemoryManagerMock.hpp"
#include "Child2ObjectMemoryManagerMock.hpp"


using namespace Tuple;

@interface TestMemoryHandle : XCTestCase {
    // Memory manager
    std::shared_ptr<MemoryManager> _memoryManager;
}

@end

@implementation TestMemoryHandle

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    _memoryManager = std::make_shared<MemoryManager>();
}

- (void)tearDown {
    
    // Call parent method
    [super tearDown];
}

- (void)testStructure {
    // Create a MemoryHandle with its parametrized constructor
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(nullptr, InvalidHandle);
    
    // Copy construct a MemoryHandle
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandleCopy(memoryHandle);
    
    // Copy assign a MemoryHandle
    memoryHandleCopy = memoryHandle;
    
    // Copy assign a MemoryHandle to himself
    memoryHandleCopy = memoryHandleCopy;
    
    // Move construct a MemoryHandle
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandleMove(std::move(memoryHandle));
    
    // Move assign a MemoryHandle
    memoryHandleMove = std::move(memoryHandle);
    
    // Move assign a MemoryHandle to himself
    memoryHandleMove = std::move(memoryHandleMove);
    
    // Authorized coercion copy construct a MemoryHandle
    MemoryHandle<BaseObjectMemoryManagerMock> memoryHandleAuthorizedCoercionCopy(memoryHandle);
    
    // Authorized coercion copy assign a MemoryHandle
    memoryHandleAuthorizedCoercionCopy = memoryHandle;
    
    // Authorized coercion move construct a MemoryHandle
    MemoryHandle<BaseObjectMemoryManagerMock> memoryHandleAuthorizedCoercionMove(std::move(memoryHandle));
    
    // Authorized coercion move assign a MemoryHandle
    memoryHandleAuthorizedCoercionMove = std::move(memoryHandle);
    /*//TODO: unit test compile-time : doit etre dans un autre fichier, voir ca : http://stackoverflow.com/questions/8870381/how-to-expect-a-static-assert-failure-and-deal-with-it-using-boost-test-framewor et http://stackoverflow.com/questions/605915/unit-test-compile-time-error
     // Forbidden coercion copy construct a MemoryHandle
     MemoryHandle<Child2ObjectMockMemoryManager> memoryHandleForbiddenCoercionCopy(memoryHandle);
     
     // Forbidden coercion copy assign a MemoryHandle
     memoryHandleForbiddenCoercionCopy = memoryHandle;
     
     // Forbidden coercion move construct a MemoryHandle
     MemoryHandle<Child2ObjectMockMemoryManager> memoryHandleForbiddenCoercionMove(std::move(memoryHandle));
     
     // Forbidden coercion move assign a MemoryHandle
     memoryHandleForbiddenCoercionMove = std::move(memoryHandle);
     */
    
    
    //MemoryHandle memoryHandle(nullptr, 0, Tuple::Handle{ 1, 2 });//TODO: est ce qu'on peut faire ca
    //    MemoryHandle memoryHandle2; // TODO: ok, vide on est obligé de le garder car il vaut = nullptr et on l'utilise pour reseter le handle
    // TODO: est ce qu'on peut instancier un MemoryHandle ? ou abstract class et on serait obligé d'instancier un RawDataHandle ou un MemoryHandle ?
    //  MemoryHandle<int> MemoryHandle(memoryHandle2);
    //    MemoryHandle<int> MemoryHandle(memoryHandle);
    //    std::cout << MemoryHandle << std::endl;
    /*
     MemoryHandle<Tuple::Component> component(nullptr);
     MemoryHandle<HealthComponent> healthComponent(component);
     MemoryHandle<HealthComponent> healthComponent2(MemoryHandle<Tuple::Component>(nullptr));
     healthComponent2 = component;*/
    /*
     MemoryHandle<HealthComponent> healthComponent(nullptr);
     MemoryHandle<Tuple::Component> component(healthComponent);
     MemoryHandle<Tuple::Component> component2(MemoryHandle<HealthComponent>(nullptr));
     component2 = healthComponent;*/
}

- (void)testGet {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that get method is ok
    //XCTAssertTrue(memoryHandle.get() == memoryHandle.getMemoryAllocator()->getMemoryForHandle(memoryHandle.getHandle()), @"get must be correct but it is not");
    XCTAssertTrue(memoryHandle.get() != nullptr, @"get must be correct but it is not");
    
    // Delete memoryHandle data
    MemoryManager::deleteData(memoryHandle);
}

- (void)testGetters {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    MoveableMemoryAllocator *registeredMemoryAllocator = new DefaultMemoryAllocator();
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(registeredMemoryAllocator));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    /*
    // Get memory allocator of memoryHandle
    MoveableMemoryAllocator *memoryAllocator = memoryHandle.getMemoryAllocator();
    
    // Test that memory allocator is ok
    XCTAssertTrue(memoryAllocator == registeredMemoryAllocator, @"Memory allocator must be correct but it is not");
    
    // Get handle of memoryHandle
    Tuple::Handle handle = memoryHandle.getHandle();
    
    // Test that handle is ok
    XCTAssertFalse(handle == InvalidHandle, @"Handle must not be an invalid handle");
    XCTAssertTrue(registeredMemoryAllocator->getMemoryForHandle(handle) == memoryHandle.get(), @"Handle must be correct but it is not");
    *//*
    // Get type of memoryHandle data
    size_t type = memoryHandle.getType();
    
    // Test that type is ok
    XCTAssertFalse(type == 0, @"Type must not be null");
    XCTAssertTrue(type == TypeManager::getTypeIDForType<ChildObjectMemoryManagerMock>(), @"Type must be correct but it is not");
    */
    // Delete memoryHandle data
    MemoryManager::deleteData(memoryHandle);
}

- (void)testEqualityOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Create another ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle2(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that memory handles are not equal
    XCTAssertFalse(memoryHandle == memoryHandle2, @"Memory handles must not be equal but they are");
    XCTAssertTrue(memoryHandle != memoryHandle2, @"Memory handles must not be equal but they are");
    
    // Delete memoryHandle2 data
    MemoryManager::deleteData(memoryHandle2);
    
    // Copy memoryHandle to memoryHandle2
    memoryHandle2 = memoryHandle;
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryHandle == memoryHandle2, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryHandle != memoryHandle2, @"Memory handles must be equal but they are not");
    
    // Get memoryHandle as BaseObjectMemoryManagerMock MemoryHandle
    MemoryHandle<BaseObjectMemoryManagerMock> memoryHandleBase(memoryHandle);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryHandle == memoryHandleBase, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryHandle != memoryHandleBase, @"Memory handles must be equal but they are not");
    
    // Test that memoryHandle is not nullptr with equality operators
    XCTAssertFalse(memoryHandle == nullptr, @"Memory handle must not be nullptr");
    XCTAssertTrue(memoryHandle != nullptr, @"Memory handle must not be nullptr");
    
    // Reset memoryHandle
    memoryHandle = MemoryHandle<ChildObjectMemoryManagerMock>(nullptr, InvalidHandle);
    
    // Test that memoryHandle is nullptr with equality operators
    XCTAssertTrue(nullptr == memoryHandle, @"Memory handle must be nullptr");
    XCTAssertFalse(nullptr != memoryHandle, @"Memory handle must be nullptr");
    
    // Test that memory handles are not equal
    XCTAssertFalse(memoryHandle == memoryHandle2, @"Memory handles must not be equal but they are");
    XCTAssertTrue(memoryHandle != memoryHandle2, @"Memory handles must not be equal but they are");
    
    // Delete memoryHandle2 data
    MemoryManager::deleteData(memoryHandle2);
    
    // Reset memoryHandle2
    memoryHandle2 = MemoryHandle<ChildObjectMemoryManagerMock>(nullptr, InvalidHandle);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryHandle == memoryHandle2, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryHandle != memoryHandle2, @"Memory handles must be equal but they are not");
}

- (void)testDowncastTo {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Copy memoryHandle to a MemoryHandle with the base class as type
    MemoryHandle<BaseObjectMemoryManagerMock> baseMemoryHandle(memoryHandle);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryHandle == baseMemoryHandle, @"Memory handles must be equal but they are not");
    
    // Downcast to ChildObjectMemoryManagerMock type
    MemoryHandle<ChildObjectMemoryManagerMock> downcastedMemoryHandle = downcastTo<ChildObjectMemoryManagerMock>(std::move(baseMemoryHandle));
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryHandle == downcastedMemoryHandle, @"Memory handles must be equal but they are not");
    
    // Downcast to Child2ObjectMemoryManagerMock type
    //downcastedMemoryHandle = baseMemoryHandle.downcastTo<Child2ObjectMemoryManagerMock>();//TODO: il faut tester ca en unit test compile-time !!
    
    // Delete memoryHandle data
    MemoryManager::deleteData(memoryHandle);
    
    //TODO: tester plusieurs niveaux de hierarchie ?
}

- (void)testDereferenceOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    MemoryHandle<ChildObjectMemoryManagerMock> memoryHandle(_memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    
    // Test that * operator is ok
    XCTAssertTrue(&(*memoryHandle) == memoryHandle.get(), @"Operator * must be correct but it is not");
    
    // TODO: il faut tester -> aussi et il faut tester les 2 avec un type qui ne le permet pas comme void (tester ca au compile time)
    
    // Delete memoryHandle data
    MemoryManager::deleteData(memoryHandle);
}

@end
