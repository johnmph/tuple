//
//  ReferenceCountObjectMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ReferenceCountObjectMemoryManagerMock__
#define __Tuple__ReferenceCountObjectMemoryManagerMock__

#include <iostream>


class BaseReferenceCountObjectMemoryManagerMock {
protected:
    // Constructor
    BaseReferenceCountObjectMemoryManagerMock() = default;
    
    // Destructor
    ~BaseReferenceCountObjectMemoryManagerMock() = default;
};

class ReferenceCountObjectMemoryManagerMock : public BaseReferenceCountObjectMemoryManagerMock {
    
public:
    // Counter
    static int count;
    
    // Constructor
    ReferenceCountObjectMemoryManagerMock();
    
    // Destructor
    ~ReferenceCountObjectMemoryManagerMock();
};

#endif /* defined(__Tuple__ReferenceCountObjectMemoryManagerMock__) */
