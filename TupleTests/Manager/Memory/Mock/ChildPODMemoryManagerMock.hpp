//
//  ChildPODMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ChildPODMemoryManagerMock__
#define __Tuple__ChildPODMemoryManagerMock__

#include "BaseObjectMemoryManagerMock.hpp"


class ChildPODMemoryManagerMock : public BaseObjectMemoryManagerMock {
    // Members
    int _intValue;
    
public:
    // Default constructor (let this class be a POD by having a default constructor first and no destructor nor virtual methods)
    ChildPODMemoryManagerMock() = default;
    
    // Parametrized constructor
    ChildPODMemoryManagerMock(int intValue);
    
    // Methods
    int getIntValue() const;
};

#endif /* defined(__Tuple__ChildPODMemoryManagerMock__) */
