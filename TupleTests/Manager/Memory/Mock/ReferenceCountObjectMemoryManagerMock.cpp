//
//  ReferenceCountObjectMemoryManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "ReferenceCountObjectMemoryManagerMock.hpp"


int ReferenceCountObjectMemoryManagerMock::count = 0;

ReferenceCountObjectMemoryManagerMock::ReferenceCountObjectMemoryManagerMock() {
    ++ReferenceCountObjectMemoryManagerMock::count;
}

ReferenceCountObjectMemoryManagerMock::~ReferenceCountObjectMemoryManagerMock() {
    --ReferenceCountObjectMemoryManagerMock::count;
}
