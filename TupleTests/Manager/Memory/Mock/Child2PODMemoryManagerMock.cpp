//
//  Child2PODMemoryManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "Child2PODMemoryManagerMock.hpp"


Child2PODMemoryManagerMock::Child2PODMemoryManagerMock(int intValue) : _intValue(intValue) {
}

int Child2PODMemoryManagerMock::getIntValue() const {
    // Return int value
    return _intValue;
}
