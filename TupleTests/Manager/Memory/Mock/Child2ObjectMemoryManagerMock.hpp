//
//  Child2ObjectMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Child2ObjectMemoryManagerMock__
#define __Tuple__Child2ObjectMemoryManagerMock__

#include "BaseObjectMemoryManagerMock.hpp"


class Child2ObjectMemoryManagerMock : public BaseObjectMemoryManagerMock {
    
public:
    // Constructor
    Child2ObjectMemoryManagerMock();
    
    // Destructor
    ~Child2ObjectMemoryManagerMock();
};

#endif /* defined(__Tuple__Child2ObjectMemoryManagerMock__) */
