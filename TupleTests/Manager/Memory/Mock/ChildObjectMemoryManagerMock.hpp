//
//  ChildObjectMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 13/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ChildObjectMemoryManagerMock__
#define __Tuple__ChildObjectMemoryManagerMock__

#include "BaseObjectMemoryManagerMock.hpp"


class ChildObjectMemoryManagerMock : public BaseObjectMemoryManagerMock {
    
public:
    // Constructor
    ChildObjectMemoryManagerMock();
    
    // Destructor
    ~ChildObjectMemoryManagerMock();
};

#endif /* defined(__Tuple__ChildObjectMemoryManagerMock__) */
