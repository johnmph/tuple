//
//  ChildPODMemoryManagerMock.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "ChildPODMemoryManagerMock.hpp"


ChildPODMemoryManagerMock::ChildPODMemoryManagerMock(int intValue) : _intValue(intValue) {
}

int ChildPODMemoryManagerMock::getIntValue() const {
    // Return int value
    return _intValue;
}
