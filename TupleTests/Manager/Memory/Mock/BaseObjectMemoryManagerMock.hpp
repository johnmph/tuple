//
//  BaseObjectMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 2/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__BaseObjectMemoryManagerMock__
#define __Tuple__BaseObjectMemoryManagerMock__

#include <iostream>


class BaseObjectMemoryManagerMock {
protected:
    // Constructor
    BaseObjectMemoryManagerMock() = default;
    
    // Destructor
    ~BaseObjectMemoryManagerMock() = default;
};

#endif /* defined(__Tuple__BaseObjectMemoryManagerMock__) */
