//
//  Child2PODMemoryManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 11/05/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Child2PODMemoryManagerMock__
#define __Tuple__Child2PODMemoryManagerMock__

#include "BaseObjectMemoryManagerMock.hpp"


class Child2PODMemoryManagerMock : public BaseObjectMemoryManagerMock {
    // Members
    int _intValue;
    
public:
    // Default constructor (let this class be a POD by having a default constructor first and no destructor nor virtual methods)
    Child2PODMemoryManagerMock() = default;
    
    // Parametrized constructor
    Child2PODMemoryManagerMock(int intValue);
    
    // Methods
    int getIntValue() const;
};

#endif /* defined(__Tuple__Child2PODMemoryManagerMock__) */
