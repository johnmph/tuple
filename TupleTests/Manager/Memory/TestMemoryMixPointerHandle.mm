//
//  TestMemoryMixPointerHandle.mm
//  Tuple
//
//  Created by Jonathan Baliko on 28/06/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "MemoryManager.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "ChildObjectMemoryManagerMock.hpp"
#include "Child2ObjectMemoryManagerMock.hpp"


using namespace Tuple;

@interface TestMemoryMixPointerHandle : XCTestCase {
    // Memory manager
    std::shared_ptr<MemoryManager> _memoryManager;
}

@property (nonatomic, readonly) std::shared_ptr<MemoryManager> memoryManager;

@end

@implementation TestMemoryMixPointerHandle

@synthesize memoryManager = _memoryManager;


- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    _memoryManager = std::make_shared<MemoryManager>();
}

- (void)tearDown {
    
    // Call parent method
    [super tearDown];
}

template <template <class> class TLink> void doTestStructure() {//TODO: tester d'assigner un Mix avec pointeur sur un mix avec handle et vice-versa
    // Create a MemoryMixPointerHandle with its parametrized constructor
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(TLink<ChildObjectMemoryManagerMock>(nullptr));
    
    // Copy construct a MemoryMixPointerHandle
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandleCopy(memoryMixPointerHandle);
    
    // Copy assign a MemoryMixPointerHandle
    memoryMixPointerHandleCopy = memoryMixPointerHandle;
    
    // Copy assign a MemoryMixPointerHandle to himself
    memoryMixPointerHandleCopy = memoryMixPointerHandleCopy;
    
    // Move construct a MemoryMixPointerHandle
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandleMove(std::move(memoryMixPointerHandle));
    
    // Move assign a MemoryMixPointerHandle
    memoryMixPointerHandleMove = std::move(memoryMixPointerHandle);
    
    // Move assign a MemoryMixPointerHandle to himself
    memoryMixPointerHandleMove = std::move(memoryMixPointerHandleMove);
    
    // Authorized coercion copy construct a MemoryMixPointerHandle
    MemoryMixPointerHandle<BaseObjectMemoryManagerMock> memoryMixPointerHandleAuthorizedCoercionCopy(memoryMixPointerHandle);
    
    // Authorized coercion copy assign a MemoryMixPointerHandle
    memoryMixPointerHandleAuthorizedCoercionCopy = memoryMixPointerHandle;
    
    // Authorized coercion move construct a MemoryMixPointerHandle
    MemoryMixPointerHandle<BaseObjectMemoryManagerMock> memoryMixPointerHandleAuthorizedCoercionMove(std::move(memoryMixPointerHandle));
    
    // Authorized coercion move assign a MemoryMixPointerHandle
    memoryMixPointerHandleAuthorizedCoercionMove = std::move(memoryMixPointerHandle);
    /*//TODO: unit test compile-time : doit etre dans un autre fichier, voir ca : http://stackoverflow.com/questions/8870381/how-to-expect-a-static-assert-failure-and-deal-with-it-using-boost-test-framewor et http://stackoverflow.com/questions/605915/unit-test-compile-time-error
     // Forbidden coercion copy construct a MemoryMixPointerHandle
     MemoryMixPointerHandle<Child2ObjectMockMemoryManager> memoryMixPointerHandleForbiddenCoercionCopy(memoryMixPointerHandle);
     
     // Forbidden coercion copy assign a MemoryMixPointerHandle
     memoryMixPointerHandleForbiddenCoercionCopy = memoryMixPointerHandle;
     
     // Forbidden coercion move construct a MemoryMixPointerHandle
     MemoryMixPointerHandle<Child2ObjectMockMemoryManager> memoryMixPointerHandleForbiddenCoercionMove(std::move(memoryMixPointerHandle));
     
     // Forbidden coercion move assign a MemoryMixPointerHandle
     memoryMixPointerHandleForbiddenCoercionMove = std::move(memoryMixPointerHandle);
     */
    
    
    //MemoryMixPointerHandle memoryMixPointerHandle(nullptr, 0, Tuple::Handle{ 1, 2 });//TODO: est ce qu'on peut faire ca
    //    MemoryMixPointerHandle memoryMixPointerHandle2; // TODO: ok, vide on est obligé de le garder car il vaut = nullptr et on l'utilise pour reseter le handle
    // TODO: est ce qu'on peut instancier un MemoryMixPointerHandle ? ou abstract class et on serait obligé d'instancier un RawDataHandle ou un MemoryHandle ?
    //  MemoryHandle<int> MemoryHandle(memoryMixPointerHandle2);
    //    MemoryHandle<int> MemoryHandle(memoryMixPointerHandle);
    //    std::cout << MemoryHandle << std::endl;
    /*
     MemoryHandle<Tuple::Component> component(nullptr);
     MemoryHandle<HealthComponent> healthComponent(component);
     MemoryHandle<HealthComponent> healthComponent2(MemoryHandle<Tuple::Component>(nullptr));
     healthComponent2 = component;*/
    /*
     MemoryHandle<HealthComponent> healthComponent(nullptr);
     MemoryHandle<Tuple::Component> component(healthComponent);
     MemoryHandle<Tuple::Component> component2(MemoryHandle<HealthComponent>(nullptr));
     component2 = healthComponent;*/
}

- (void)testStructure {
    // Test with MemoryPointer
    doTestStructure<MemoryPointer>();
    
    // Test with MemoryHandle
    doTestStructure<MemoryHandle>();
}

template <class TMemoryAllocator, template <class> class TLink> void doTestGet(id self) {
    // Get memory manager
    std::shared_ptr<MemoryManager> memoryManager = [self memoryManager];
    
    // Register a memory allocator for ChildObjectMemoryManagerMock
    memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<TMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    TLink<ChildObjectMemoryManagerMock> link(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(link);
    
    // Test that get method is ok
    //XCTAssertTrue(memoryMixPointerHandle.get() == memoryMixPointerHandle.getMemoryAllocator()->getMemoryForHandle(memoryMixPointerHandle.getHandle()), @"get must be correct but it is not");
    XCTAssertTrue(memoryMixPointerHandle.get() != nullptr, @"get must be correct but it is not");
    
    // Delete memoryMixPointerHandle data
    MemoryManager::deleteData(memoryMixPointerHandle);
}

- (void)testGet {
    // Test with MemoryPointer
    doTestGet<FixedMemoryAllocator, MemoryPointer>(self);
    
    // Test with MemoryHandle
    doTestGet<MoveableMemoryAllocator, MemoryHandle>(self);
}

template <class TMemoryAllocator, template <class> class TLink> void doTestGetters(id self) {
    // Get memory manager
    std::shared_ptr<MemoryManager> memoryManager = [self memoryManager];
    
    // Register a memory allocator for ChildObjectMemoryManagerMock
    TMemoryAllocator *registeredMemoryAllocator = new DefaultMemoryAllocator();
    memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<TMemoryAllocator>(registeredMemoryAllocator));
    
    // Create a ChildObjectMemoryManagerMock object
    TLink<ChildObjectMemoryManagerMock> link(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(link);
    /*
    // Get memory allocator of memoryMixPointerHandle
    TMemoryAllocator *memoryAllocator = memoryMixPointerHandle.getMemoryAllocator();
    
    // Test that memory allocator is ok
    XCTAssertTrue(memoryAllocator == registeredMemoryAllocator, @"Memory allocator must be correct but it is not");
    
    // Get handle of memoryMixPointerHandle
    Tuple::Handle handle = memoryMixPointerHandle.getHandle();
    
    // Test that handle is ok
    XCTAssertFalse(handle == InvalidHandle, @"Handle must not be an invalid handle");
    XCTAssertTrue(registeredMemoryAllocator->getMemoryForHandle(handle) == memoryMixPointerHandle.get(), @"Handle must be correct but it is not");
    *//*
    // Get type of memoryMixPointerHandle data
    size_t type = memoryMixPointerHandle.getType();
    
    // Test that type is ok
    XCTAssertFalse(type == 0, @"Type must not be null");
    XCTAssertTrue(type == TypeManager::getTypeIDForType<ChildObjectMemoryManagerMock>(), @"Type must be correct but it is not");
    */
    // Delete memoryMixPointerHandle data
    MemoryManager::deleteData(memoryMixPointerHandle);
}

- (void)testGetters {
    // Test with MemoryPointer
    doTestGetters<FixedMemoryAllocator, MemoryPointer>(self);
    
    // Test with MemoryHandle
    doTestGetters<MoveableMemoryAllocator, MemoryHandle>(self);
}

template <template <class> class TLink, template <class> class TLink2> void doTestEqualityOperators(id self) {
    // Get memory manager
    std::shared_ptr<MemoryManager> memoryManager = [self memoryManager];
    
    // Create a ChildObjectMemoryManagerMock object
    TLink<ChildObjectMemoryManagerMock> link(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(link);
    
    // Create another ChildObjectMemoryManagerMock object
    TLink2<ChildObjectMemoryManagerMock> link2(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle2(link2);
    
    // Test that memory handles are not equal
    XCTAssertFalse(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    XCTAssertTrue(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    
    // Delete memoryMixPointerHandle2 data
    MemoryManager::deleteData(memoryMixPointerHandle2);
    
    // Copy memoryMixPointerHandle to memoryMixPointerHandle2
    memoryMixPointerHandle2 = memoryMixPointerHandle;
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
    
    // Get memoryMixPointerHandle as BaseObjectMemoryManagerMock MemoryMixPointerHandle
    MemoryMixPointerHandle<BaseObjectMemoryManagerMock> memoryMixPointerHandleBase(memoryMixPointerHandle);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == memoryMixPointerHandleBase, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryMixPointerHandle != memoryMixPointerHandleBase, @"Memory handles must be equal but they are not");
    
    // Test that memoryMixPointerHandle is not nullptr with equality operators
    XCTAssertFalse(memoryMixPointerHandle == nullptr, @"Memory handle must not be nullptr");
    XCTAssertTrue(memoryMixPointerHandle != nullptr, @"Memory handle must not be nullptr");
    
    // Reset memoryMixPointerHandle
    memoryMixPointerHandle = MemoryMixPointerHandle<ChildObjectMemoryManagerMock>(nullptr);
    
    // Test that memoryMixPointerHandle is nullptr with equality operators
    XCTAssertTrue(nullptr == memoryMixPointerHandle, @"Memory handle must be nullptr");
    XCTAssertFalse(nullptr != memoryMixPointerHandle, @"Memory handle must be nullptr");
    
    // Test that memory handles are not equal
    XCTAssertFalse(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    XCTAssertTrue(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    
    // Reset memoryMixPointerHandle with a TLink null object
    memoryMixPointerHandle = MemoryMixPointerHandle<ChildObjectMemoryManagerMock>(TLink<ChildObjectMemoryManagerMock>(nullptr));
    
    // Test that memoryMixPointerHandle is nullptr with equality operators
    XCTAssertTrue(nullptr == memoryMixPointerHandle, @"Memory handle must be nullptr");
    XCTAssertFalse(nullptr != memoryMixPointerHandle, @"Memory handle must be nullptr");
    
    // Test that memory handles are not equal
    XCTAssertFalse(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    XCTAssertTrue(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must not be equal but they are");
    
    // Delete memoryMixPointerHandle2 data
    MemoryManager::deleteData(memoryMixPointerHandle2);
    
    // Reset memoryMixPointerHandle2
    memoryMixPointerHandle2 = MemoryMixPointerHandle<ChildObjectMemoryManagerMock>(nullptr);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
    
    // Reset memoryMixPointerHandle2 with a TLink2 null object
    memoryMixPointerHandle2 = MemoryMixPointerHandle<ChildObjectMemoryManagerMock>(TLink2<ChildObjectMemoryManagerMock>(nullptr));
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
    XCTAssertFalse(memoryMixPointerHandle != memoryMixPointerHandle2, @"Memory handles must be equal but they are not");
}

- (void)testEqualityOperators {
    // Register a memory allocator for ChildObjectMemoryManagerMock
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Test with MemoryPointer
    doTestEqualityOperators<MemoryPointer, MemoryPointer>(self);
    
    // Test with MemoryHandle
    doTestEqualityOperators<MemoryHandle, MemoryHandle>(self);
    
    // Mixed test
    doTestEqualityOperators<MemoryPointer, MemoryHandle>(self);
}

template <class TMemoryAllocator, template <class> class TLink> void doTestDowncastTo(id self) {
    // Get memory manager
    std::shared_ptr<MemoryManager> memoryManager = [self memoryManager];
    
    // Register a memory allocator for ChildObjectMemoryManagerMock
    memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<TMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    TLink<ChildObjectMemoryManagerMock> link(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(link);
    
    // Copy memoryMixPointerHandle to a MemoryMixPointerHandle with the base class as type
    MemoryMixPointerHandle<BaseObjectMemoryManagerMock> baseMemoryMixPointerHandle(memoryMixPointerHandle);
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == baseMemoryMixPointerHandle, @"Memory handles must be equal but they are not");
    
    // Downcast to ChildObjectMemoryManagerMock type
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> downcastedMemoryMixPointerHandle = downcastTo<ChildObjectMemoryManagerMock>(std::move(baseMemoryMixPointerHandle));
    
    // Test that memory handles are equal
    XCTAssertTrue(memoryMixPointerHandle == downcastedMemoryMixPointerHandle, @"Memory handles must be equal but they are not");
    
    // Downcast to Child2ObjectMemoryManagerMock type
    //downcastedMemoryMixPointerHandle = baseMemoryMixPointerHandle.downcastTo<Child2ObjectMemoryManagerMock>();//TODO: il faut tester ca en unit test compile-time !!
    
    // Delete memoryMixPointerHandle data
    MemoryManager::deleteData(memoryMixPointerHandle);
    
    //TODO: tester plusieurs niveaux de hierarchie ?
}

- (void)testDowncastTo {
    // Test with MemoryPointer
    doTestDowncastTo<FixedMemoryAllocator, MemoryPointer>(self);
    
    // Test with MemoryHandle
    doTestDowncastTo<MoveableMemoryAllocator, MemoryHandle>(self);
}

template <class TMemoryAllocator, template <class> class TLink> void doTestDeferenceOperators(id self) {
    // Get memory manager
    std::shared_ptr<MemoryManager> memoryManager = [self memoryManager];
    
    // Register a memory allocator for ChildObjectMemoryManagerMock
    memoryManager->registerMemoryAllocatorForClass<ChildObjectMemoryManagerMock>(std::unique_ptr<TMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a ChildObjectMemoryManagerMock object
    TLink<ChildObjectMemoryManagerMock> link(memoryManager->newDataWithType<ChildObjectMemoryManagerMock>());
    MemoryMixPointerHandle<ChildObjectMemoryManagerMock> memoryMixPointerHandle(link);
    
    // Test that * operator is ok
    XCTAssertTrue(&(*memoryMixPointerHandle) == memoryMixPointerHandle.get(), @"Operator * must be correct but it is not");
    
    // TODO: il faut tester -> aussi et il faut tester les 2 avec un type qui ne le permet pas comme void (tester ca au compile time)
    
    // Delete memoryMixPointerHandle data
    MemoryManager::deleteData(memoryMixPointerHandle);
}

- (void)testDereferenceOperators {
    // Test with MemoryPointer
    doTestDeferenceOperators<FixedMemoryAllocator, MemoryPointer>(self);
    
    // Test with MemoryHandle
    doTestDeferenceOperators<MoveableMemoryAllocator, MemoryHandle>(self);
}

@end
