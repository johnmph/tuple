//
//  TestHandleManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 22/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "HandleManager.hpp"


using namespace Tuple;

@interface TestHandleManager : XCTestCase {
    // Handle manager
    std::shared_ptr<HandleManager> _handleManager;
    
    // Data
    std::vector<int> _data;
}

@end

@implementation TestHandleManager

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create handle manager
    _handleManager = std::make_shared<HandleManager>();
    
    // Create data
    _data = std::vector<int>(10, 0);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    // Call parent method
    [super tearDown];
}

- (void)testCreateAndDestroyHandle {
    // Create an handle for first element
    Tuple::Handle handle = _handleManager->createHandle(&_data[0]);
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 1, @"handle count must be equal to 1 but is %d", _handleManager->getHandleCount());
    
    // Destroy the handle
    _handleManager->destroyHandle(handle);
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 0, @"handle count must be equal to 0 but is %d", _handleManager->getHandleCount());
}

- (void)testCreateAndDestroyTwoHandles {
    // Create an handle for first element
    Tuple::Handle handle = _handleManager->createHandle(&_data[0]);
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 1, @"handle count must be equal to 1 but is %d", _handleManager->getHandleCount());
    
    // Create an handle for second element
    Tuple::Handle handle2 = _handleManager->createHandle(&_data[1]);
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 2, @"handle count must be equal to 2 but is %d", _handleManager->getHandleCount());
    
    // Destroy the handles
    _handleManager->destroyHandle(handle);
    _handleManager->destroyHandle(handle2);
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 0, @"handle count must be equal to 0 but is %d", _handleManager->getHandleCount());
}

- (void)testDestroyAllHandles {
    // Create an handle for each element
    for (int x = 0; x < _data.size(); ++x) {
        _handleManager->createHandle(&_data[x]);
    }
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 10, @"handle count must be equal to 10 but is %d", _handleManager->getHandleCount());
    
    // Destroy all handles
    _handleManager->destroyAllHandles();
    
    // Test that count is ok
    XCTAssertEqual(_handleManager->getHandleCount(), 0, @"handle count must be equal to 0 but is %d", _handleManager->getHandleCount());
}

- (void)testHandleGetData {
    // Create an handle for first element
    Tuple::Handle handle = _handleManager->createHandle(&_data[0]);
    
    // Test that handle points to good data
    void *dataPtr = _handleManager->getDataPointer(handle);
    XCTAssertEqual(dataPtr, (void *) &_data[0], @"handle pointer must be equal to 0x%p but is 0x%p", &_data[0], dataPtr);
    
    // Get data pointer as int and test it
    int *dataPtrAsInt = _handleManager->getDataPointerAs<int>(handle);
    XCTAssertEqual(dataPtrAsInt, &_data[0], @"handle pointer must be equal to 0x%p but is 0x%p", &_data[0], dataPtr);
    
    // Destroy the handle
    _handleManager->destroyHandle(handle);
    
    // Test that handle points to nullptr
    dataPtr = _handleManager->getDataPointer(handle);
    XCTAssertTrue(dataPtr == nullptr, @"handle pointer must be equal to nullptr but is 0x%p", dataPtr);
}

- (void)testCreateExistingHandle {
    // Create an handle for first element
    _handleManager->createHandle(&_data[0]);
    
    // Test that trying to create an handle for first element again throws an exception
    XCTAssertThrowsCPP(_handleManager->createHandle(&_data[0]), @"Create an already created handle must throw a std::logic_error exception");
}

- (void)testDestroyAlreadyDestroyedHandle {
    // Create an handle for first element
    Tuple::Handle handle = _handleManager->createHandle(&_data[0]);
    
    // Destroy the handle
    _handleManager->destroyHandle(handle);
    
    // Test that trying to destroy the handle again throws an exception
    XCTAssertThrowsCPP(_handleManager->destroyHandle(handle), @"Release an already released handle must throw a std::logic_error exception");
}

- (void)testHandleExpiration {
    // Create an handle for first element
    Tuple::Handle handle = _handleManager->createHandle(&_data[0]);
    
    // Destroy the handle
    _handleManager->destroyHandle(handle);
    
    // Save old invalidated handle
    Tuple::Handle oldHandle = handle;
    
    // Create an handle for second element
    handle = _handleManager->createHandle(&_data[1]);
    
    // Test that old invalidated handle points to nullptr
    void *dataPtr = _handleManager->getDataPointer(oldHandle);
    XCTAssertTrue(dataPtr == nullptr, @"old handle pointer must be equal to nullptr but is 0x%p", dataPtr);
}

- (void)testUpdateHandleEntry {
    // Create an handle for second element
    Tuple::Handle handle = _handleManager->createHandle(&_data[1]);
    
    // Create an handle for 8th element
    Tuple::Handle handle2 = _handleManager->createHandle(&_data[8]);
    
    // Move data in handle manager
    _handleManager->updateHandleEntry(&_data[1], &_data[2]);
    
    // Test that handle points to good data
    void *dataPtr = _handleManager->getDataPointer(handle);
    XCTAssertEqual(dataPtr, (void *) &_data[2], @"handle pointer must be equal to 0x%p but is 0x%p", &_data[2], dataPtr);
    
    // Test that handle2 has not moved
    dataPtr = _handleManager->getDataPointer(handle2);
    XCTAssertEqual(dataPtr, (void *) &_data[8], @"handle2 pointer must be equal to 0x%p but is 0x%p", &_data[8], dataPtr);
}

- (void)testUpdateHandleEntries {
    // Create an handle for 3th element
    Tuple::Handle handle = _handleManager->createHandle(&_data[2]);
    
    // Create an handle for 9th element
    Tuple::Handle handle2 = _handleManager->createHandle(&_data[8]);
    
    // Calculate pointer offset between _data[3] and _data[1] in bytes
    //size_t offset = (&_data[1] - &_data[3]) * sizeof(int); // OU :
    size_t offset = static_cast<unsigned char *>(static_cast<void *>(&_data[1])) - static_cast<unsigned char *>(static_cast<void *>(&_data[3]));
    
    // Move all data in handle manager
    _handleManager->updateHandleEntries(offset);
    
    // Test that handle points to good data
    void *dataPtr = _handleManager->getDataPointer(handle);
    XCTAssertEqual(dataPtr, (void *) &_data[0], @"handle pointer must be equal to 0x%p but is 0x%p", &_data[0], dataPtr);
    dataPtr = _handleManager->getDataPointer(handle2);
    XCTAssertEqual(dataPtr, (void *) &_data[6], @"handle2 pointer must be equal to 0x%p but is 0x%p", &_data[6], dataPtr);
}

@end
