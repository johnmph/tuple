//
//  TestEntityManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "EntityManager.hpp"
#include "TargetNotificationEntityManagerMock.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "HealthComponent.hpp"
#include "Health2Component.hpp"
#include "Health3Component.hpp"
#include "TestNonPODComponent.hpp"
#include "ComponentSharingPolicyOptimizedSizeShare.hpp"
#include "ComponentSharingPolicyOptimizedSpeedShare.hpp"
#include "ComponentSharingPolicySmartLinkShare.hpp"


using namespace Tuple;

//template <class TType> using MySmartLink = SmartLink<TType>;//TODO: ca ne compile pas si je passe directement SmartLink a l'entity manager
//typedef EntityManager<MySmartLink> MyEntityManager;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
template <class TType> using MyMemoryLink = MemoryHandle<TType>;
template <template <class> class TComponentLinkType> using MyComponentSharingPolicy = ComponentSharingPolicySmartLinkShare<TComponentLinkType, OwnershipPolicySimpleCount>;
typedef EntityManager<MyMemoryLink, /*MyComponentSharingPolicy*/ComponentSharingPolicyOptimizedSizeShare> MyEntityManager;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
typedef typename MyEntityManager::EntityID EntityID;
typedef typename MyEntityManager::Components Components;
template <class TType> using ComponentLinkType = typename MyEntityManager::template ComponentLinkType<TType>;

@interface TestEntityManager : XCTestCase {
    // Entity manager
    std::shared_ptr<MyEntityManager> _entityManager;
    
    // Notification target
    std::shared_ptr<TargetNotificationEntityManagerMock<MyEntityManager>> _targetNotification;
}

@end

@implementation TestEntityManager

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    std::shared_ptr<MemoryManager> memoryManager = std::make_shared<MemoryManager>();
    
    // Create notification manager
    std::shared_ptr<NotificationManager> notificationManager = std::make_shared<NotificationManager>();
    
    // Create entity manager
    _entityManager = std::make_shared<MyEntityManager>(memoryManager, notificationManager);
    
    // Create target notification
    _targetNotification = std::make_shared<TargetNotificationEntityManagerMock<MyEntityManager>>(_entityManager);
}

- (void)tearDown {
    // Reset entity manager and target notification to let their destructors called TODO: je ne sais pas pourquoi le destructor n'est pas appelé si on ne fait pas ca, peut etre que chaque test est dans une sorte de sandbox et que toute sa mémoire est supprimée avant le test d'apres un peu comme dans les app de l'iOS sinon ca devrait appeler le destructor a la fin de cette methode ou dans le setUp suivant quand _entityManager et _targetNotification est re-setté
    _entityManager = nullptr;
    _targetNotification = nullptr;
    
    // Call parent method
    [super tearDown];
}

- (void)testCreateEntityWithoutSpecificID {
    EntityID eid;
    
    try {
        // Create entity
        eid = _entityManager->createEntity();
    }
    catch (...) {
        // It can't throw an exception
        XCTFail("createEntity throwed an exception");
    }
    
    // Test if eid is valid
    XCTAssertTrue(eid != 0, "eid is invalid");
    XCTAssertFalse(_entityManager->isFreeEntityID(eid), "eid must not be a free entity ID");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->entityRemovedEID && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}

- (void)testCreateEntityWithSpecificID {
    EntityID eid = 1;
    
    // Test if create entity with specific ID doesn't throw an exception
    XCTAssertNoThrowCPP(_entityManager->createEntityWithID(eid), "createEntityWithID throwed an exception");
    
    // Test if eid is valid
    XCTAssertFalse(_entityManager->isFreeEntityID(eid), "eid must not be a free entity ID");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->entityRemovedEID && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}
/*
- (void)testCreateEntityWithBadID {
    // Test if create entity with bad ID throw an exception
    XCTAssertThrowsCPP(_entityManager->createEntityWithID(0), "createEntityWithID with bad ID must throw an exception");//TODO: pour tester les asserts il faut qu'on compile la librairie Tuple avec un assert modifié
}*/

- (void)testCreateEntityWithExistingID {
    // Create an entity
    _entityManager->createEntityWithID(1);
    
    // Reset target notification
    _targetNotification->reset();
    
    // Test if create entity with existing ID throw an exception
    XCTAssertThrowsCPP(_entityManager->createEntityWithID(1), "createEntityWithID with existing ID must throw an exception");
    
    // Test if notification is ok
    XCTAssertTrue(!_targetNotification->entityCreatedEID && !_targetNotification->entityRemovedEID && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}

- (void)testCreateTwoEntitiesWithoutSpecificID {
    EntityID eid, eid2;
    
    try {
        // Create entity
        eid = _entityManager->createEntity();
        
        // Create another entity
        eid2 = _entityManager->createEntity();
    }
    catch (...) {
        // It can't throw an exception
        XCTFail("createEntity throwed an exception");
    }
    
    // Test if eid is valid
    XCTAssertTrue(eid != 0, "eid is invalid");
    XCTAssertTrue(eid2 != 0, "eid2 is invalid");
    XCTAssertTrue(eid != eid2, "eid must be different than eid2");
    XCTAssertFalse(_entityManager->isFreeEntityID(eid), "eid must not be a free entity ID");
    XCTAssertFalse(_entityManager->isFreeEntityID(eid2), "eid2 must not be a free entity ID");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid2 && !_targetNotification->entityRemovedEID && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}

- (void)testRemoveEntity {
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Test if remove eid entity doesn't throw an exception
    XCTAssertNoThrowCPP(_entityManager->removeEntity(eid), "removeEntity throwed an exception");
    
    // Test if eid entity was removed
    XCTAssertTrue(_entityManager->isFreeEntityID(eid), "eid must be a free entity ID");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && _targetNotification->entityRemovedEID == eid && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}

/*
- (void)testRemoveBadEntity {
}*/

- (void)testRemoveNonExistingEntity {
    // Test if remove non existing entity throw an exception
    XCTAssertThrowsCPP(_entityManager->removeEntity(1), "removeEntity with non existing entity must throw an exception");
    
    // Test if notification is ok
    XCTAssertTrue(!_targetNotification->entityCreatedEID && !_targetNotification->entityRemovedEID && !_targetNotification->componentAddedEID && !_targetNotification->componentRemovedEID, "Notification error");
}

- (void)testIsFreeEntityID {
    // Random eid
    EntityID eid = 12;
    
    // Test if eid is a free entity ID
    XCTAssertTrue(_entityManager->isFreeEntityID(eid), "eid must be a free entity ID");
    
    // Create entity
    _entityManager->createEntityWithID(eid);
    
    // Test if eid is no more a free entity ID
    XCTAssertFalse(_entityManager->isFreeEntityID(eid), "eid must not be a free entity ID");
    
    // Remove entity
    _entityManager->removeEntity(eid);
    
    // Test if eid is a free entity ID
    XCTAssertTrue(_entityManager->isFreeEntityID(eid), "eid must be a free entity ID");
}

- (void)testGetAllEntityIDs {
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create another entity
    EntityID eid2 = 5;
    _entityManager->createEntityWithID(eid2);
    
    // Get all entity IDs
    std::vector<EntityID> entityIds = _entityManager->getAllEntityIDs();
    
    // Test that we have two entities in entityIDs
    XCTAssertTrue(entityIds.size() == 2, "getAllEntityIDs must returns two entity IDs");
    XCTAssertTrue(((entityIds[0] == eid) && (entityIds[1] == eid2)) || ((entityIds[0] == eid2) && (entityIds[1] == eid)), "getAllEntityIDs must returns eid and eid2");
    
    // Remove eid entity
    _entityManager->removeEntity(eid);
    
    // Get all entity IDs
    entityIds = _entityManager->getAllEntityIDs();
    
    // Test that we have one entity in entityIDs
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDs must returns one entity IDs");
    XCTAssertTrue(entityIds[0] == eid2, "getAllEntityIDs must returns eid2");
    
    // Remove eid2 entity
    _entityManager->removeEntity(eid2);
    
    // Get all entity IDs
    entityIds = _entityManager->getAllEntityIDs();
    
    // Test that we have no entity in entityIDs
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDs must returns no entity IDs");
}

- (void)testAddComponentToOneEntity {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Test if component is in eid
    XCTAssertTrue(_entityManager->getComponentInEntity<HealthComponent>(eid) == healthComponent, "Component was not added to entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->entityRemovedEID && _targetNotification->componentAddedEID == eid && *(_targetNotification->componentAddedComponent) == healthComponent && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testAddTwoComponentsOfDifferentsClassesToOneEntity {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Test if components are in eid
    XCTAssertTrue(_entityManager->getComponentInEntity<HealthComponent>(eid) == healthComponent, "Component was not added to entity");
    XCTAssertTrue(_entityManager->getComponentInEntity<Health2Component>(eid) == health2Component, "Component was not added to entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->entityRemovedEID && _targetNotification->componentAddedEID == eid && *(_targetNotification->componentAddedComponent) == health2Component && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testAddTwoComponentsOfSameClassToOneEntity {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<HealthComponent> healthComponent2(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Test if add component of same class throw an exception
    XCTAssertThrowsCPP(_entityManager->addComponentInEntity(eid, healthComponent2), "Add a component with same class of an already added component must throw an exception");
    
    // Test if only first component is in eid
    XCTAssertTrue(_entityManager->getComponentInEntity<HealthComponent>(eid) == healthComponent, "Component was not added to entity");
    XCTAssertFalse(_entityManager->getComponentInEntity<HealthComponent>(eid) == healthComponent2, "Component was added to entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && _targetNotification->componentAddedEID == eid && *(_targetNotification->componentAddedComponent) == healthComponent && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testAddSameComponentToTwoEntities {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Add component to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Add component to another entity
    //_entityManager->addComponentInEntity(eid2, healthComponent);
    _entityManager->addSharedComponentInEntity<HealthComponent>(eid2, eid);//TODO: shared, voir si ok
    
    // Test if components are in eid
    XCTAssertTrue(_entityManager->getComponentInEntity<HealthComponent>(eid) == healthComponent, "Component was not added to entity");
    XCTAssertTrue(_entityManager->getComponentInEntity<HealthComponent>(eid2) == healthComponent, "Component was not added to entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid2 && _targetNotification->componentAddedEID == eid2 && *(_targetNotification->componentAddedComponent) == healthComponent && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testAddComponentOfNonExistingEntity {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Test that adding a component to a non existing eid throw an exception
    EntityID eid = 1;
    XCTAssertThrowsCPP(_entityManager->addComponentInEntity(eid, healthComponent), "Adding a component to a non existing entity must throw an exception");
    
    // Test that getting a component from a non existing eid throw an exception
    XCTAssertThrowsCPP(_entityManager->getComponentInEntity<HealthComponent>(eid), "Get a component from a non existing entity must throw an exception");
    
    // Test if notification is ok
    XCTAssertTrue(!_targetNotification->entityCreatedEID && !_targetNotification->componentAddedEID && _targetNotification->componentAddedComponent == nullptr && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testRemoveComponentWithObject {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Remove component from entity
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<HealthComponent>());
    
    // Test if getting a non existing component throw an exception
    XCTAssertThrowsCPP(_entityManager->getComponentInEntity<HealthComponent>(eid), "Component must not be anymore in entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && _targetNotification->componentAddedEID == eid && *(_targetNotification->componentAddedComponent) == healthComponent && _targetNotification->componentRemovedEID == eid && *(_targetNotification->componentRemovedComponent) == healthComponent, "Notification error");
}

- (void)testRemoveComponentWithClass {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Remove component from entity
    _entityManager->removeComponentInEntity<HealthComponent>(eid);
    
    // Test if getting a non existing component throw an exception
    XCTAssertThrowsCPP(_entityManager->getComponentInEntity<HealthComponent>(eid), "Component must not be anymore in entity");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && _targetNotification->componentAddedEID == eid && *(_targetNotification->componentAddedComponent) == healthComponent && _targetNotification->componentRemovedEID == eid && *(_targetNotification->componentRemovedComponent) == healthComponent, "Notification error");
}

- (void)testRemoveNonExistingComponentWithObject {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Test that remove a non added component throws an exception
    XCTAssertThrowsCPP(_entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<HealthComponent>()), "Remove a non added component must throw an exception");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->componentAddedEID && _targetNotification->componentAddedComponent == nullptr && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testRemoveNonExistingComponentWithClass {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    _entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100);
    
    // Test that remove a non added component throws an exception
    XCTAssertThrowsCPP(_entityManager->removeComponentInEntity<HealthComponent>(eid), "Remove a non added component must throw an exception");
    
    // Test if notification is ok
    XCTAssertTrue(_targetNotification->entityCreatedEID == eid && !_targetNotification->componentAddedEID && _targetNotification->componentAddedComponent == nullptr && !_targetNotification->componentRemovedEID && _targetNotification->componentRemovedComponent == nullptr, "Notification error");
}

- (void)testGetAllComponentsOfOneEntity {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Get all components of this entity
    Components components = _entityManager->getAllComponentsInEntity(eid);
    
    // Test that components is empty
    XCTAssertTrue(components.size() == 0, "Entity must have no components");
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Get all components of this entity
    components = _entityManager->getAllComponentsInEntity(eid);
    
    // Test that components contains components of entity
    XCTAssertTrue(components.size() == 2, "Entity must have 2 components");
    XCTAssertTrue(components.at(TypeManager::getTypeIDForType<HealthComponent>()) == healthComponent, "Can't find an added component");
    XCTAssertTrue(components.at(TypeManager::getTypeIDForType<Health2Component>()) == health2Component, "Can't find an added component");
    
    // Remove a component
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<HealthComponent>());
    
    // Get all components of this entity
    components = _entityManager->getAllComponentsInEntity(eid);
    
    // Test that components contains components of entity
    XCTAssertTrue(components.size() == 1, "Entity must have 1 component");
    XCTAssertTrue(components.at(TypeManager::getTypeIDForType<Health2Component>()) == health2Component, "Can't find an added component");
}

- (void)testGetAllComponentsOfNonExistingEntity {
    // Test that getting all components of a non existing entity throws an exception
    EntityID eid = 1;
    XCTAssertThrowsCPP(_entityManager->getAllComponentsInEntity(eid), "Get all components of a non existing entity must throw an exception");
}

- (void)testGetAllEntityIDsForComponentClass {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Add same component to another entity
    //_entityManager->addComponentInEntity(eid2, healthComponent);
    _entityManager->addSharedComponentInEntity<HealthComponent>(eid2, eid);//TODO: shared, voir si ok
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Get all entity IDs for HealthComponent class
    std::vector<EntityID> entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent>();
    
    // Test that we have the two entities in entityIds
    XCTAssertTrue(entityIds.size() == 2, "getAllEntityIDsForComponentClass must returns two entities");
    XCTAssertTrue(((entityIds[0] == eid) && (entityIds[1] == eid2)) || ((entityIds[0] == eid2) && (entityIds[1] == eid)), "getAllEntityIDsForComponentClass must returns eid and eid2");
    
    // Get all entity IDs for Health2Component class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<Health2Component>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClass must returns one entity");
    XCTAssertTrue(entityIds[0] == eid2, "getAllEntityIDsForComponentClass must returns eid2");
    
    // Remove eid entity
    _entityManager->removeEntity(eid);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClass must returns one entity");
    XCTAssertTrue(entityIds[0] == eid2, "getAllEntityIDsForComponentClass must returns eid2");
    
    // Remove eid2 entity
    _entityManager->removeEntity(eid2);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent>();
    
    // Test that we have no entity in entityIds
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDsForComponentClass must returns no entity");
    
    // Get all entity IDs for Health2Component class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<Health2Component>();
    
    // Test that we have no entity in entityIds
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDsForComponentClass must returns no entity");
}

- (void)testGetAllEntityIDsForComponentClasses {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health3Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health3Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Add same component to another entity
    //_entityManager->addComponentInEntity(eid2, healthComponent);
    _entityManager->addSharedComponentInEntity<HealthComponent>(eid2, eid);//TODO: shared, voir si ok
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Create another entity
    EntityID eid3 = _entityManager->createEntity();
    
    // Add same components to another entity
    //_entityManager->addComponentInEntity(eid3, healthComponent);
    //_entityManager->addComponentInEntity(eid3, health2Component);
    _entityManager->addSharedComponentInEntity<HealthComponent>(eid3, eid);//TODO: shared, voir si ok
    _entityManager->addSharedComponentInEntity<Health2Component>(eid3, eid2);//TODO: shared, voir si ok
    
    // Create a component
    ComponentLinkType<Health3Component> health3Component(_entityManager->getMemoryManager()->newDataWithType<Health3Component>(100));
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid3, health3Component);
    
    // Get all entity IDs for Health3Component class
    std::vector<EntityID> entityIds = _entityManager->getAllEntityIDsForComponentClasses<Health3Component>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClasses must returns one entity");
    XCTAssertTrue(entityIds[0] == eid3, "getAllEntityIDsForComponentClasses must returns eid3");
    
    // Get all entity IDs for HealthComponent and Health2Component classes
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, Health2Component>();
    
    // Test that we have two entities in entityIds
    XCTAssertTrue(entityIds.size() == 2, "getAllEntityIDsForComponentClasses must returns two entities");
    XCTAssertTrue(((entityIds[0] == eid2) && (entityIds[1] == eid3)) || ((entityIds[0] == eid3) && (entityIds[1] == eid2)), "getAllEntityIDsForComponentClasses must returns eid2 and eid3");
    
    // Get all entity IDs for HealthComponent, Health2Component and Health3Component classes
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, Health2Component, Health3Component>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClasses must returns one entity");
    XCTAssertTrue(entityIds[0] == eid3, "getAllEntityIDsForComponentClasses must returns eid3");
    
    // Remove HealthComponent from eid3 entity
    _entityManager->removeComponentInEntity<HealthComponent>(eid3);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent>();
    
    // Test that we have two entities in entityIds
    XCTAssertTrue(entityIds.size() == 2, "getAllEntityIDsForComponentClasses must returns two entities");
    XCTAssertTrue(((entityIds[0] == eid) && (entityIds[1] == eid2)) || ((entityIds[0] == eid2) && (entityIds[1] == eid)), "getAllEntityIDsForComponentClasses must returns eid and eid2");
    
    // Remove eid2 entity
    _entityManager->removeEntity(eid2);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, Health2Component>();
    
    // Test that we have no entity in entityIds
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDsForComponentClass must returns no entity");
}

- (void)testGetAllEntityIDsForComponentClassesWithSameClasses {
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Add same component to another entity
    //_entityManager->addComponentInEntity(eid2, healthComponent);
    _entityManager->addSharedComponentInEntity<HealthComponent>(eid2, eid);//TODO: shared, voir si ok
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Get all entity IDs for HealthComponent class
    std::vector<EntityID> entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, HealthComponent>();
    
    // Test that we have the two entities in entityIds
    XCTAssertTrue(entityIds.size() == 2, "getAllEntityIDsForComponentClass must returns two entities");
    XCTAssertTrue(((entityIds[0] == eid) && (entityIds[1] == eid2)) || ((entityIds[0] == eid2) && (entityIds[1] == eid)), "getAllEntityIDsForComponentClass must returns eid and eid2");
    
    // Get all entity IDs for Health2Component class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<Health2Component, Health2Component>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClass must returns one entity");
    XCTAssertTrue(entityIds[0] == eid2, "getAllEntityIDsForComponentClass must returns eid2");
    
    // Remove eid entity
    _entityManager->removeEntity(eid);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, HealthComponent>();
    
    // Test that we have one entity in entityIds
    XCTAssertTrue(entityIds.size() == 1, "getAllEntityIDsForComponentClass must returns one entity");
    XCTAssertTrue(entityIds[0] == eid2, "getAllEntityIDsForComponentClass must returns eid2");
    
    // Remove eid2 entity
    _entityManager->removeEntity(eid2);
    
    // Get all entity IDs for HealthComponent class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<HealthComponent, HealthComponent>();
    
    // Test that we have no entity in entityIds
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDsForComponentClass must returns no entity");
    
    // Get all entity IDs for Health2Component class
    entityIds = _entityManager->getAllEntityIDsForComponentClasses<Health2Component, Health2Component>();
    
    // Test that we have no entity in entityIds
    XCTAssertTrue(entityIds.size() == 0, "getAllEntityIDsForComponentClass must returns no entity");
}

- (void)testNonPODComponentHaveCorrectDestructorCalledWithStaticType {//TODO: ecrire de meilleurs tests et pour chaque choix de policy pour EntityManager, ...
    // Disable target notification
    _targetNotification = nullptr;
    
    // Reset TestNonPODComponent count
    TestNonPODComponent::resetCount();
    
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<TestNonPODComponent> testNonPODComponent(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as value
    _entityManager->addComponentInEntity(eid, testNonPODComponent);
    
    // Reset variable
    testNonPODComponent = nullptr;
    
    // Remove it from entity
    _entityManager->removeComponentInEntity<TestNonPODComponent>(eid);
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");
    /*
    // Create a component
    testNonPODComponent = ComponentLinkType<TestNonPODComponent>(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as rvalue
    _entityManager->addComponentInEntity(eid, std::move(testNonPODComponent));
    
    // Remove it from entity
    _entityManager->removeComponentInEntity<TestNonPODComponent>(eid);
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");
    */
    // TODO: ca ne passe pas le test car quand on passe dans la methode addComponentInEntity, meme si on lui passe une rvalue, comme dans cette methode on utilise un std::make_pair et on crée une notification dont le listener (TargetNotificationEntityManagerMock) sauve le component, donc on se retrouve avec un count = 2 une fois le component removed (gardée par la variable ici et par TargetNotificationEntityManagerMock), j'ai mis targetNotification reset pour annuler pour ca mais il reste la variable (qui meme si elle est passée en rvalue, est copiée via std::make_pair) -> je met un std::move dans make_pair ou bien il faudrait transformer la signature de addComponentInEntity pour lui passer une rvalue ? : ca ne marche pas en passant en rvalue dans std::make_pair
    // TODO: ca ne marche pas parce qu'on sauve dans targetNotification les components et il faut qu'il soit supprimé par removeComponentToEntity, si on en a encore ailleurs, le compteur n'est pas a 0 quand removeComponentToEntity et donc pas le bon destructor appelé apres.
    // TODO: et faire attention a un grand probleme : je peux mettre en const les SmartLink et iterator de SmartLink avec les ownership qui changent les variables grace au mot clé mutable mais le probleme est que si le compteur est a 0 on libere et on reset le SmartLink donc il n'est plus const et ca fout la merde !!! -> CA NE FOUT PAS LA MERDE, ca appelle le constructor avec const & a la place de celui avec && et donc ca incremente le compteur de reference plutot que de déplacer donc moins efficace
}

- (void)testNonPODComponentHaveCorrectDestructorCalledWithDynamicType {
    // Disable target notification
    _targetNotification = nullptr;
    
    // Reset TestNonPODComponent count
    TestNonPODComponent::resetCount();
    
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<TestNonPODComponent> testNonPODComponent(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as value
    _entityManager->addComponentInEntity(eid, testNonPODComponent);
    
    // Reset variable
    testNonPODComponent = nullptr;
    
    // Remove it from entity
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<TestNonPODComponent>());
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");
    /*
    // Create a component
    testNonPODComponent = ComponentLinkType<TestNonPODComponent>(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as rvalue
    _entityManager->addComponentInEntity(eid, std::move(testNonPODComponent));
    
    // Remove it from entity
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<TestNonPODComponent>());
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");*/
}

- (void)testNonPODComponentHaveCorrectDestructorCalledWhenRemoveEntity {
    // Disable target notification
    _targetNotification = nullptr;
    
    // Reset TestNonPODComponent count
    TestNonPODComponent::resetCount();
    
    // Register pool allocator for components
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<TestNonPODComponent>(std::unique_ptr<MoveableMemoryAllocator>(new DefaultMemoryAllocator()));
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<TestNonPODComponent> testNonPODComponent(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as value
    _entityManager->addComponentInEntity(eid, testNonPODComponent);
    
    // Reset variable
    testNonPODComponent = nullptr;
    
    // Remove entity
    _entityManager->removeEntity(eid);
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");
    /*
    // Create entity
    eid = _entityManager->createEntity();
    
    // Create a component
    testNonPODComponent = ComponentLinkType<TestNonPODComponent>(_entityManager->getMemoryManager()->newDataWithType<TestNonPODComponent>());
    
    // Add it to entity as rvalue
    _entityManager->addComponentInEntity(eid, std::move(testNonPODComponent));
    
    // Remove entity
    _entityManager->removeEntity(eid);
    
    // Test that correct destructor is called (by checking counter incremented in constructor and decremented in destructor)
    XCTAssertTrue(TestNonPODComponent::getCount() == 0, "The correct destructor of TestNonPODComponent is not called");*/
}

@end
