//
//  TargetNotificationEntityManagerMock.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 26/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__TargetNotificationEntityManagerMock__
#define __Tuple__TargetNotificationEntityManagerMock__

#include "EntityManager.hpp"


template <class TEntityManagerType> class TargetNotificationEntityManagerMock {
    using EntityID = typename TEntityManagerType::EntityID;
    template <class TType> using ComponentLinkType = typename TEntityManagerType::template ComponentLinkType<TType>;
    
    std::shared_ptr<TEntityManagerType> _entityManager;
    
    // Notifications
    void entityCreated(Tuple::EntityCreatedNotification<TEntityManagerType> &notification);
    void entityRemoved(Tuple::EntityRemovedNotification<TEntityManagerType> &notification);
    void componentAdded(Tuple::ComponentAddedNotification<TEntityManagerType> &notification);
    void componentRemoved(Tuple::ComponentRemovedNotification<TEntityManagerType> &notification);
    
public:
    EntityID entityCreatedEID;
    EntityID entityRemovedEID;
    EntityID componentAddedEID;
    std::unique_ptr<ComponentLinkType<Tuple::Component>> componentAddedComponent;
    EntityID componentRemovedEID;
    std::unique_ptr<ComponentLinkType<Tuple::Component>> componentRemovedComponent;
    
    // Constructor
    explicit TargetNotificationEntityManagerMock(std::shared_ptr<TEntityManagerType> entityManager);
    
    // Destructor
    ~TargetNotificationEntityManagerMock();
    
    // Methods
    void reset();
};


template <class TEntityManagerType> void TargetNotificationEntityManagerMock<TEntityManagerType>::entityCreated(Tuple::EntityCreatedNotification<TEntityManagerType> &notification) {
    entityCreatedEID = notification.getEntityID();
}

template <class TEntityManagerType> void TargetNotificationEntityManagerMock<TEntityManagerType>::entityRemoved(Tuple::EntityRemovedNotification<TEntityManagerType> &notification) {
    entityRemovedEID = notification.getEntityID();
}

template <class TEntityManagerType> void TargetNotificationEntityManagerMock<TEntityManagerType>::componentAdded(Tuple::ComponentAddedNotification<TEntityManagerType> &notification) {
    componentAddedEID = notification.getEntityID();
    componentAddedComponent = std::unique_ptr<ComponentLinkType<Tuple::Component>>(new ComponentLinkType<Tuple::Component>(notification.getComponent()));
}

template <class TEntityManagerType> void TargetNotificationEntityManagerMock<TEntityManagerType>::componentRemoved(Tuple::ComponentRemovedNotification<TEntityManagerType> &notification) {
    componentRemovedEID = notification.getEntityID();
    componentRemovedComponent = std::unique_ptr<ComponentLinkType<Tuple::Component>>(new ComponentLinkType<Tuple::Component>(notification.getComponent()));
}

template <class TEntityManagerType> TargetNotificationEntityManagerMock<TEntityManagerType>::TargetNotificationEntityManagerMock(std::shared_ptr<TEntityManagerType> entityManager) : _entityManager(entityManager), entityCreatedEID(0), entityRemovedEID(0), componentAddedEID(0), componentRemovedEID(0) {
    // Register entityCreated listener
    _entityManager->getNotificationManager()->registerListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::EntityCreatedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::entityCreated, _entityManager.get()));
    
    // Register entityRemoved listener
    _entityManager->getNotificationManager()->registerListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::EntityRemovedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::entityRemoved, _entityManager.get()));
    
    // Register componentAdded listener
    _entityManager->getNotificationManager()->registerListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::ComponentAddedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::componentAdded, _entityManager.get()));
    
    // Register componentRemoved listener
    _entityManager->getNotificationManager()->registerListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::ComponentRemovedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::componentRemoved, _entityManager.get()));
}

template <class TEntityManagerType> TargetNotificationEntityManagerMock<TEntityManagerType>::~TargetNotificationEntityManagerMock() {
    // Unregister listeners
    _entityManager->getNotificationManager()->unregisterListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::EntityCreatedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::entityCreated, _entityManager.get()));
    _entityManager->getNotificationManager()->unregisterListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::EntityRemovedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::entityRemoved, _entityManager.get()));
    _entityManager->getNotificationManager()->unregisterListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::ComponentAddedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::componentAdded, _entityManager.get()));
    _entityManager->getNotificationManager()->unregisterListener(std::make_shared<Tuple::NotificationListener<TargetNotificationEntityManagerMock, Tuple::ComponentRemovedNotification<TEntityManagerType>>>(*this, &TargetNotificationEntityManagerMock::componentRemoved, _entityManager.get()));//TODO: utiliser des members plutot que recréer des shared_ptr juste pour unregisterListener
}

template <class TEntityManagerType> void TargetNotificationEntityManagerMock<TEntityManagerType>::reset() {
    // Reset flags
    entityCreatedEID = 0;
    entityRemovedEID = 0;
    componentAddedEID = 0;
    componentAddedComponent = std::unique_ptr<ComponentLinkType<Tuple::Component>>();
    componentRemovedEID = 0;
    componentRemovedComponent = std::unique_ptr<ComponentLinkType<Tuple::Component>>();
}

#endif /* defined(__Tuple__TargetNotificationEntityManagerMock__) */
