//
//  TestEntityListManager.mm
//  Tuple
//
//  Created by Jonathan Baliko on 27/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "EntityListManager.hpp"
#include "DefaultMemoryAllocator.hpp"
#include "PoolMemoryAllocator.hpp"
#include "HealthComponent.hpp"
#include "Health2Component.hpp"
#include "Health3Component.hpp"
#include "ComponentSharingPolicyOptimizedSizeShare.hpp"
#include "ComponentSharingPolicyOptimizedSpeedShare.hpp"
#include "ComponentSharingPolicySmartLinkShare.hpp"


using namespace Tuple;

//template <class TType> using MySmartLink = SmartLink<TType>;//TODO: ca ne compile pas si je passe directement SmartLink a l'entity manager
//typedef EntityManager<MySmartLink> MyEntityManager;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
template <class TType> using MyMemoryLink = MemoryHandle<TType>;
template <template <class> class TComponentLinkType> using MyComponentSharingPolicy = ComponentSharingPolicySmartLinkShare<TComponentLinkType, OwnershipPolicySimpleCount>;
typedef EntityManager<MyMemoryLink, /*MyComponentSharingPolicy*/ComponentSharingPolicyOptimizedSizeShare> MyEntityManager;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca
typedef EntityListManager<MyEntityManager> MyEntityListManager;
typedef typename MyEntityManager::EntityID EntityID;
//typedef typename MyEntityManager::Components Components;
template <class TType> using ComponentLinkType = typename MyEntityManager::template ComponentLinkType<TType>;

@interface TestEntityListManager : XCTestCase {
    // Entity manager
    std::shared_ptr<MyEntityManager> _entityManager;
    
    // Entity list manager
    std::shared_ptr<MyEntityListManager> _entityListManager;
    
    // Notification target
//    std::shared_ptr<TargetNotificationEntityManagerMock> _targetNotification;
}

@end
//TODO: dans ces tests, je presume que je recois les entity ids dans l'ordre de creation (pour les tests sur les components), si j'attrape des erreurs de tests par la suite, regarder a ca en 1er et peut etre reecrire les tests pour prendre en compte un ordre aleatoire possible (du moment que le bon component est associé a la bonne entity, il n'y a pas d'importance pour l'ordre des entities)
@implementation TestEntityListManager//TODO: il reste les notifications a tester (si on les laisse pour les entity list, voir si vraiment besoin de ces notifs)

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create memory manager
    std::shared_ptr<MemoryManager> memoryManager = std::make_shared<MemoryManager>();
    
    // Create notification manager
    std::shared_ptr<NotificationManager> notificationManager = std::make_shared<NotificationManager>();
    
    // Create entity manager
    _entityManager = std::make_shared<MyEntityManager>(memoryManager, notificationManager);
    
    // Create entity list manager
    _entityListManager = std::make_shared<MyEntityListManager>(_entityManager);
    
    // Create target notification
//    _targetNotification = std::make_shared<TargetNotificationEntityManagerMock>(_entityManager);
    
    // Register allocator for components
    /*
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health3Component>(std::unique_ptr<MemoryAllocator>(new DefaultMemoryAllocator()));
     */
    
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health3Component>(std::unique_ptr<FixedMemoryAllocator>(new DefaultMemoryAllocator()));
    
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(HealthComponent), 100)));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health2Component>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(Health2Component), 100)));
    _entityManager->getMemoryManager()->registerMemoryAllocatorForClass<Health3Component>(std::unique_ptr<MoveableMemoryAllocator>(new PoolMemoryAllocator(sizeof(Health3Component), 100)));
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    // Call parent method
    [super tearDown];
}

- (void)testGetEntityListForComponentClassAfterAddedEntity {
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, /*std::move(*/healthComponent/*)*/);//TODO: j'ai mis move pour tester, normalement le dernier test ici ne devrait pas etre bon mais ca passe, voir dans le constructor move de MemoryHandle pourquoi
    
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    
    //MemoryHandle<HealthComponent> healthComponent2 = nullptr;//TODO : a retirer apres, pour tester le swap avec template <TSubType>
    //healthComponent2 = healthComponent;
}

- (void)testGetEntityListForComponentClassAndAddEntity {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    
    // Create entity
    _entityManager->createEntity();
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassAndAddGoodComponent {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
}

- (void)testGetEntityListForComponentClassAndAddBadComponent {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity ID");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassAndRemoveGoodComponent {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Remove component
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<HealthComponent>());
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassAndRemoveBadComponent {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Remove component
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<Health2Component>());
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
}

- (void)testGetEntityListForComponentClassAndRemoveGoodEntity {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Remove entity
    _entityManager->removeEntity(eid);
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassAndRemoveBadEntity {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Remove entity
    _entityManager->removeEntity(eid2);
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
}

- (void)testGetEntityListForComponentClassWithTwoGoodEntities {
    // Get entity list for HealthComponent
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Add same component to another entity
    _entityManager->addComponentInEntity(eid2, healthComponent);
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Test that there is eid and eid2 entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 2, "entityListHealthComponent must have two entity IDs");
    XCTAssertTrue(((entityListHealthComponent->getEntityIDs()[0] == eid) && (entityListHealthComponent->getEntityIDs()[1] == eid2)) || ((entityListHealthComponent->getEntityIDs()[0] == eid2) && (entityListHealthComponent->getEntityIDs()[1] == eid)), "entityListHealthComponent must have eid and eid2 entity IDs");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[1].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    
    // Get entity list for Health2Component
    auto entityListHealth2Component = _entityListManager->getEntityListForComponentClasses<Health2Component>();
    
    // Test that there is eid2 entity in entity list
    XCTAssertTrue(entityListHealth2Component->getCount() == 1, "entityListHealth2Component must have one entity IDs");
    XCTAssertEqual(entityListHealth2Component->getEntityIDs()[0], eid2, "entityListHealth2Component must have eid2 entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealth2Component->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealth2Component must have health2Component component");
}

- (void)testGetEntityListForComponentClassesAfterAddedEntity {
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
}

- (void)testGetEntityListForComponentClassesAndAddEntity {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
    
    // Create entity
    _entityManager->createEntity();
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassesAndAddGoodComponent {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
}

- (void)testGetEntityListForComponentClassesAndAddBadComponent {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create a component
    ComponentLinkType<Health3Component> health3Component(_entityManager->getMemoryManager()->newDataWithType<Health3Component>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health3Component);
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassesAndRemoveGoodComponent {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create a component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Remove component
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<Health2Component>());
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassesAndRemoveBadComponent {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Create another component
    ComponentLinkType<Health3Component> health3Component(_entityManager->getMemoryManager()->newDataWithType<Health3Component>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health3Component);
    
    // Remove component
    _entityManager->removeComponentInEntity(eid, TypeManager::getTypeIDForType<Health3Component>());
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
}

- (void)testGetEntityListForComponentClassesAndRemoveGoodEntity {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Remove entity
    _entityManager->removeEntity(eid);
    
    // Test that there is no entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 0, "entityListHealthComponent must have no entity IDs");
    
    // Test that there is no components in entity list
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<HealthComponent>().size() == 0, "entityListHealthComponent must have no component");
    XCTAssertTrue(entityListHealthComponent->getComponentsForClass<Health2Component>().size() == 0, "entityListHealthComponent must have no component");
}

- (void)testGetEntityListForComponentClassesAndRemoveBadEntity {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Remove entity
    _entityManager->removeEntity(eid2);
    
    // Test that there is eid entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 1, "entityListHealthComponent must have one entity IDs");
    XCTAssertEqual(entityListHealthComponent->getEntityIDs()[0], eid, "entityListHealthComponent must have eid entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
}

- (void)testGetEntityListForComponentClassesWithTwoGoodEntities {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent2(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add components to another entity
    _entityManager->addComponentInEntity(eid2, healthComponent2);
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Create a component
    ComponentLinkType<Health3Component> health3Component(_entityManager->getMemoryManager()->newDataWithType<Health3Component>(100));
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health3Component);
    
    // Test that there is eid and eid2 entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 2, "entityListHealthComponent must have two entity IDs");
    XCTAssertTrue(((entityListHealthComponent->getEntityIDs()[0] == eid) && (entityListHealthComponent->getEntityIDs()[1] == eid2)) || ((entityListHealthComponent->getEntityIDs()[0] == eid2) && (entityListHealthComponent->getEntityIDs()[1] == eid)), "entityListHealthComponent must have eid and eid2 entity IDs");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[1].get(), healthComponent2.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[1].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
    
    // Get entity list for Health2Component and Health3Component
    auto entityListHealth2Component = _entityListManager->getEntityListForComponentClasses<Health2Component, Health3Component>();
    
    // Test that there is eid2 entity in entity list
    XCTAssertTrue(entityListHealth2Component->getCount() == 1, "entityListHealth2Component must have one entity IDs");
    XCTAssertEqual(entityListHealth2Component->getEntityIDs()[0], eid2, "entityListHealth2Component must have eid2 entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealth2Component->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealth2Component must have healthComponent component");
    XCTAssertEqual(entityListHealth2Component->getComponentsForClass<Health3Component>()[0].get(), health3Component.get(), "entityListHealth2Component must have health2Component component");
}

- (void)testGetTwoEntityLists {
    // Get entity list for HealthComponent and Health2Component
    auto entityListHealthComponent = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component>();
    
    // Get entity list for Health2Component and Health3Component
    auto entityListHealth2Component = _entityListManager->getEntityListForComponentClasses<Health2Component, Health3Component>();
    
    // Create entity
    EntityID eid = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, healthComponent);
    
    // Create another component
    ComponentLinkType<Health2Component> health2Component(_entityManager->getMemoryManager()->newDataWithType<Health2Component>());
    
    // Add it to entity
    _entityManager->addComponentInEntity(eid, health2Component);
    
    // Create another entity
    EntityID eid2 = _entityManager->createEntity();
    
    // Create a component
    ComponentLinkType<HealthComponent> healthComponent2(_entityManager->getMemoryManager()->newDataWithType<HealthComponent>(100));
    
    // Add components to another entity
    _entityManager->addComponentInEntity(eid2, healthComponent2);
    _entityManager->addComponentInEntity(eid2, health2Component);
    
    // Create a component
    ComponentLinkType<Health3Component> health3Component(_entityManager->getMemoryManager()->newDataWithType<Health3Component>(100));
    
    // Add it to another entity
    _entityManager->addComponentInEntity(eid2, health3Component);
    
    // Test that there is eid and eid2 entity in entity list
    XCTAssertTrue(entityListHealthComponent->getCount() == 2, "entityListHealthComponent must have two entity IDs");
    XCTAssertTrue(((entityListHealthComponent->getEntityIDs()[0] == eid) && (entityListHealthComponent->getEntityIDs()[1] == eid2)) || ((entityListHealthComponent->getEntityIDs()[0] == eid2) && (entityListHealthComponent->getEntityIDs()[1] == eid)), "entityListHealthComponent must have eid and eid2 entity IDs");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[0].get(), healthComponent.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<HealthComponent>()[1].get(), healthComponent2.get(), "entityListHealthComponent must have healthComponent component");
    XCTAssertEqual(entityListHealthComponent->getComponentsForClass<Health2Component>()[1].get(), health2Component.get(), "entityListHealthComponent must have health2Component component");
    
    // Test that there is eid2 entity in entity list
    XCTAssertTrue(entityListHealth2Component->getCount() == 1, "entityListHealth2Component must have one entity IDs");
    XCTAssertEqual(entityListHealth2Component->getEntityIDs()[0], eid2, "entityListHealth2Component must have eid2 entity ID");
    
    // Test that component is in entity list
    XCTAssertEqual(entityListHealth2Component->getComponentsForClass<Health2Component>()[0].get(), health2Component.get(), "entityListHealth2Component must have healthComponent component");
    XCTAssertEqual(entityListHealth2Component->getComponentsForClass<Health3Component>()[0].get(), health3Component.get(), "entityListHealth2Component must have health2Component component");
}

- (void)testGetSameEntityList {//TODO: j'ai retiré les entity list de int, char, float car ca ne compile plus avec le nouveau systeme dans EntityList, les types doivent dériver de Component
    // Create entity list for int class
    auto entityList = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Get another entity list for same class
    auto entityList2 = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Test that is the same list
    XCTAssertEqual(entityList.get(), entityList2.get(), "getEntityListForComponentClasses must returns same list if class is the same");
    
    // Create entity list for int and float class
    auto entityList3 = _entityListManager->getEntityListForComponentClasses<HealthComponent, Health2Component, Health3Component>();
    
    // Get another entity list for same class
    auto entityList4 = _entityListManager->getEntityListForComponentClasses<Health3Component, Health2Component, HealthComponent>();
    
    // Test that is the same list
    XCTAssertEqual((void *) entityList3.get(), (void *) entityList4.get(), "getEntityListForComponentClasses must returns same list if classes are the same");
    
    // Create entity list for int class with getEntityListForComponentClasses
    auto entityList5 = _entityListManager->getEntityListForComponentClasses<HealthComponent>();
    
    // Test that is the same list
    XCTAssertEqual(entityList.get(), entityList5.get(), "getEntityListForComponentClass and getEntityListForComponentClasses with one class must returns same list if class is the same");
}

@end
