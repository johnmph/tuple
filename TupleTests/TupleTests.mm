//
//  TupleTests.mm
//  TupleTests
//
//  Created by Jonathan Baliko on 9/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <fstream>
#include "GameEngine.hpp"
#include "Health3Component.hpp"
#include "HealthComponentLoader.hpp"
#include "Health2ComponentSerializer.hpp"
#include "GameEngineLoaderV1.hpp"
#include "BinaryDataDecoder.hpp"
//#include "EntityIDListManager.hpp"


@interface TupleTests : XCTestCase

@end

@implementation TupleTests

- (void)setUp {
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    
    [super tearDown];
}
/*
 - (void)testGameEngineConstructorParameters {
 // Create empty memory manager unique ptr
 std::unique_ptr<Tuple::MemoryManager> memoryManagerUPtr;
 
 try {
 // Create game engine
 Tuple::GameEngine gameEngine(std::move(memoryManagerUPtr));//TODO: ca ne marche pas car assert() en C++ appelle abort() et ne throw pas d'exception !!!
 
 // If no exception caught, it is an error
 STFail(@"We can't have an nullptr memory manager unique ptr as constructor parameter of game engine");
 }
 catch (...) {
 // Ok, caught the error
 }
 }*/
/*
 - (void)testGameEngineUniquePtr {
 // Create memory manager
 Tuple::MemoryManager *memoryManager = new Tuple::MemoryManager();
 
 // Create memory manager unique ptr
 std::unique_ptr<Tuple::MemoryManager> memoryManagerUPtr(memoryManager);
 
 // Create game engine
 Tuple::GameEngine gameEngine(std::move(memoryManagerUPtr));
 
 // Test that game engine owns memory manager
 STAssertEquals(memoryManager, &gameEngine.getMemoryManager(), @"Memory manager 0x%X in game engine must be 0x%X", &gameEngine.getMemoryManager(), memoryManager);
 
 // Test that memory manager unique ptr loses ownership of memory manager
 STAssertTrue(memoryManagerUPtr.get() == nullptr, @"Memory manager unique ptr must be nullptr, current value is 0x%X", memoryManagerUPtr.get());
 }
 */

/*
 - (void)testGameEngineLoader {
 // Create game engine
 auto gameEngine = std::make_shared<Tuple::GameEngine>();
 //    std::shared_ptr<Tuple::GameEngine> gameEngine(new Tuple::GameEngine());
 
 // Register HealthComponent class default memory allocator
 gameEngine->getMemoryManager().registerMemoryAllocatorForClass<HealthComponent>(std::unique_ptr<Tuple::MemoryAllocator>(new Tuple::DefaultMemoryAllocator()));
 
 // Create (local) game engine loader
 Tuple::GameEngineLoaderV1 gameEngineLoaderV1(gameEngine);
 
 // Register loader for HealthComponent class
 gameEngineLoaderV1.registerComponentLoader("HealthComponentGUID", std::shared_ptr<Tuple::ComponentLoader>(new HealthComponentLoader()));//TODO: attention, ici j'utilise un shared_ptr temporaire dans une methode a plusieurs arguments mais comme l'autre argument ("HealthComponentGUID") ne throw pas d'exception, c'est ok (voir http://www.boost.org/doc/libs/1_53_0/libs/smart_ptr/shared_ptr.htm best practices)
 
 // Get a game level file stream
 std::ifstream gameLevelFStream;
 NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"gameLevel1" ofType:@"txt"];
 gameLevelFStream.open([path cStringUsingEncoding:NSASCIIStringEncoding], std::ios::in | std::ios::binary);//TODO: voir le mode binary
 
 // Check if file stream opened
 if (!gameLevelFStream.is_open()) {//TODO: ici ou check dans loadFromStream ? (ou dans les 2, un check ici qui affiche un alert si pas ok mais un throw dans loadFromStream)
 // It is an error
 STFail(@"Can't open gameLevel1.txt");
 
 // Exit
 return;
 }
 
 // Load game level
 gameEngineLoaderV1.loadFromStream(gameLevelFStream);//TODO: peut etre le passer en unique_ptr et le recuperer en unique_ptr ainsi si on en a plus besoin comme on ne recupere pas l'unique_ptr, il sera automatiquement detruit et donc le close sera appelé (a tester) et si encore besoin, il suffit de le recuperer, ca evite d'avoir des lectures entre le fichier si multithread ? mais ca ne sert a rien car si on est en multithread, on pourra revenir ici avant la fin de la lecture du loadFromStream et ca va poser des problemes et si on bloque (synchronize), on perd l'avantage du multithread !!!
 
 // Close game level file stream
 gameLevelFStream.close();
 }*/

@end
