//
//  TestGameEngine.mm
//  Tuple
//
//  Created by Jonathan Baliko on 25/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "XCTestCPP.h"
#include "GameEngine.hpp"
#include "ComponentSharingPolicySmartLinkShare.hpp"
#include "ComponentSharingPolicyOptimizedSizeShare.hpp"


using namespace Tuple;

template <class TType> using MyMemoryLink = MemoryHandle<TType>;
template <template <class> class TComponentLinkType> using MyComponentSharingPolicy = ComponentSharingPolicySmartLinkShare<TComponentLinkType, OwnershipPolicySimpleCount>;
typedef EntityManager<MyMemoryLink, /*MyComponentSharingPolicy*/ComponentSharingPolicyOptimizedSizeShare> MyEntityManager;// TODO: on ne peut pas le declarer dans TestEntityListManager, voir pq en objective c on ne peut pas faire ca

@interface TestGameEngine : XCTestCase {
    // Game engine
    std::shared_ptr<GameEngine<MyEntityManager>> _gameEngine;
}

@end

@implementation TestGameEngine

- (void)setUp {
    // Call parent method
    [super setUp];
    
    // Create game engine
    _gameEngine = std::make_shared<GameEngine<MyEntityManager>>();
}

- (void)tearDown {
    _gameEngine = nullptr;
    
    // Call parent method
    [super tearDown];
}

- (void)testGameEngine {
}

@end
