//
//  System.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__System__
#define __Tuple__System__

#include <iostream>


namespace Tuple {
    
    class System {
    public:
        // Destructor
        virtual ~System() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
    };
    
}

#endif /* defined(__Tuple__System__) */
