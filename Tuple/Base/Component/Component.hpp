//
//  Component.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__Component__
#define __Tuple__Component__

#include <iostream>


namespace Tuple {
    
    /*!
     * Base class for component classes, made as a Plain Old Data
     * to optimize processing and memory.
     * Subclass can always be non POD classes.
     * Optimized also with the Empty Base Class Optimization, size of this class is 1 byte
     * but when subclassed this class take 0 byte on the subclass.
     * @see http://stackoverflow.com/questions/4178175/what-are-aggregates-and-pods-and-how-why-are-they-special
     * @see https://www.informit.com/guides/content.aspx?g=cplusplus&seqNum=319
     */
    class Component {/*! @todo a voir, voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private */
    protected:
        // Protected constructor to avoid creating a Component base object and default to let this class be a POD
        Component() = default;
        
        // Non-virtual protected destructor (avoid virtual methods to let childs of Component be a POD class and avoid possibility of deletion through a Component base pointer)
        // See rule 50 of "C++ Coding Standards - 101 Rules, Guidelines, And Best Practices"
        ~Component() = default;
    };
    
}

#endif /* defined(__Tuple__Component__) */
