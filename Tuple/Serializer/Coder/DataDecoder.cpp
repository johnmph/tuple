//
//  DataDecoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "DataDecoder.hpp"


namespace Tuple {
    
    std::istream &DataDecoder::getInputStream() const {
        return _inputStream;
    }
    
    DataDecoder::DataDecoder(std::istream &inputStream) : _inputStream(inputStream) {
    }
    
    DataDecoder::~DataDecoder() {
    }
    
}
