//
//  DataEncoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "DataEncoder.hpp"


namespace Tuple {
    
    std::ostream &DataEncoder::getOutputStream() const {
        return _outputStream;
    }
    
    DataEncoder::DataEncoder(std::ostream &outputStream) : _outputStream(outputStream) {
    }
    
    DataEncoder::~DataEncoder() {
    }
    
}
