//
//  DataEncoder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__DataEncoder__
#define __Tuple__DataEncoder__

#include <iostream>


namespace Tuple {
    
    class DataEncoder {
    public:
        // Constructor
        explicit DataEncoder(std::ostream &outputStream);
        
        // Destructor
        virtual ~DataEncoder() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual void writeBool(std::string const &name, bool data) = 0;
        virtual void writeInt(std::string const &name, int data) = 0;
        virtual void writeFloat(std::string const &name, float data) = 0;
        virtual void writeString(std::string const &name, std::string const &data) = 0;
        
    protected:
        // Methods
        std::ostream &getOutputStream() const;
        
    private:
        std::ostream &_outputStream;//TODO: voir si ca ou pointer ou smart ptr (un shared surement) ? : Pour eviter les close et les mouvement dans le fichier c'est preferable un unique_ptr !!!
    };
    
}

#endif /* defined(__Tuple__DataEncoder__) */
