//
//  DataDecoder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__DataDecoder__
#define __Tuple__DataDecoder__

#include <iostream>


namespace Tuple {
    
    class DataDecoder {
    public:
        // Constructor
        explicit DataDecoder(std::istream &inputStream);
        
        // Destructor
        virtual ~DataDecoder() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual bool readBool(std::string const &name) = 0;
        virtual int readInt(std::string const &name) = 0;
        virtual float readFloat(std::string const &name) = 0;
        virtual std::string readString(std::string const &name) = 0;
        
    protected:
        // Methods
        std::istream &getInputStream() const;
        
    private:
        std::istream &_inputStream;//TODO: voir si ca ou pointer ou smart ptr (un shared surement) ? : Pour eviter les close et les mouvement dans le fichier c'est preferable un unique_ptr !!!
    };
    
}

#endif /* defined(__Tuple__DataDecoder__) */
