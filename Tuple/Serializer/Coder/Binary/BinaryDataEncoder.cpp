//
//  BinaryDataEncoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "BinaryDataEncoder.hpp"

// static_cast is safer than reinterpret_cast for the writedXXX methods
namespace Tuple {
    
    BinaryDataEncoder::BinaryDataEncoder(std::ostream &outputStream) : DataEncoder(outputStream) {
    }
    /*
    BinaryDataEncoder::~BinaryDataEncoder() {
    }*/
    
    void BinaryDataEncoder::writeBool(std::string const &name, bool data) {//TODO: si tjs le meme, changer avec une methode template template <class T> void writeData(T const &data) et pareil pour le decoder ? : normalement pas possible car methodes virtuelles dans la classe de base et pas possible d'avoir une methode template virtual
        // Write bool
        void *dataPtr = &data;
        getOutputStream().write(static_cast<char *>(dataPtr), sizeof(data));
    }
    
    void BinaryDataEncoder::writeInt(std::string const &name, int data) {
        // Write int
        void *dataPtr = &data;
        getOutputStream().write(reinterpret_cast<char *>(dataPtr), sizeof(data));
    }
    
    void BinaryDataEncoder::writeFloat(std::string const &name, float data) {
        // Write float
        void *dataPtr = &data;
        getOutputStream().write(reinterpret_cast<char *>(dataPtr), sizeof(data));
    }
    
    void BinaryDataEncoder::writeString(std::string const &name, std::string const &data) {
        // Write string
        getOutputStream() << data;
    }
    
}
