//
//  BinaryDataEncoder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__BinaryDataEncoder__
#define __Tuple__BinaryDataEncoder__

#include "DataEncoder.hpp"


namespace Tuple {
    
    class BinaryDataEncoder : public DataEncoder {
    public:
        // Constructor
        explicit BinaryDataEncoder(std::ostream &outputStream);
        
        // Destructor
        //~BinaryDataEncoder();
        
        // Methods
        void writeBool(std::string const &name, bool data) override;
        void writeInt(std::string const &name, int data) override;
        void writeFloat(std::string const &name, float data) override;
        void writeString(std::string const &name, std::string const &data) override;
    };
    
}

#endif /* defined(__Tuple__BinaryDataEncoder__) */
