//
//  BinaryDataDecoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "BinaryDataDecoder.hpp"

// static_cast is safer than reinterpret_cast for the readXXX methods
namespace Tuple {
    
    BinaryDataDecoder::BinaryDataDecoder(std::istream &inputStream) : DataDecoder(inputStream) {
    }
    /*
    BinaryDataDecoder::~BinaryDataDecoder() {
    }*/
    
    bool BinaryDataDecoder::readBool(std::string const &name) {
        // Read bool
        bool data = false;
        void *dataPtr = &data;
        getInputStream().read(static_cast<char *>(dataPtr), sizeof(data));
        
        // Return it
        return data;
    }
    
    int BinaryDataDecoder::readInt(std::string const &name) {
        // Read int
        int data = 0;
        void *dataPtr = &data;
        getInputStream().read(static_cast<char *>(dataPtr), sizeof(data));
        
        // Return it
        return data;
    }
    
    float BinaryDataDecoder::readFloat(std::string const &name) {
        // Read float
        float data = 0.0f;
        void *dataPtr = &data;
        getInputStream().read(static_cast<char *>(dataPtr), sizeof(data));
        
        // Return it
        return data;
    }
    
    std::string BinaryDataDecoder::readString(std::string const &name) {
        // Read string
        std::string data("");
        std::getline(getInputStream(), data, '\0');
        
        // Return it
        return data;
    }
    
}
