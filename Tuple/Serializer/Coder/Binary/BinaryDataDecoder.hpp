//
//  BinaryDataDecoder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__BinaryDataDecoder__
#define __Tuple__BinaryDataDecoder__

#include "DataDecoder.hpp"


namespace Tuple {
    
    class BinaryDataDecoder : public DataDecoder {
    public:
        // Constructor
        explicit BinaryDataDecoder(std::istream &inputStream);
        
        // Destructor
        //~BinaryDataDecoder();
        
        // Methods
        bool readBool(std::string const &name) override;
        int readInt(std::string const &name) override;
        float readFloat(std::string const &name) override;
        std::string readString(std::string const &name) override;
    };
    
}

#endif /* defined(__Tuple__BinaryDataDecoder__) */
