//
//  XMLDataDecoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "XMLDataDecoder.hpp"


namespace Tuple {
    
    XMLDataDecoder::XMLDataDecoder(std::istream &inputStream) : DataDecoder(inputStream) {
    }
    /*
    XMLDataDecoder::~XMLDataDecoder() {
    }*/
    
    bool XMLDataDecoder::readBool(std::string const &name) {
        //TODO: que je le fasse moi meme ou que je passe par une lib XML, il faut bien faire attention a lire dans le noeud courant car sinon on pourrait lire un bool avec le nom name dans un autre component / system / object
        return false;
    }
    
    int XMLDataDecoder::readInt(std::string const &name) {
        return 0;
    }
    
    float XMLDataDecoder::readFloat(std::string const &name) {
        return 0.0f;
    }
    
    std::string XMLDataDecoder::readString(std::string const &name) {
        return std::string("");
    }
    
}
