//
//  XMLDataDecoder.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__XMLDataDecoder__
#define __Tuple__XMLDataDecoder__

#include "DataDecoder.hpp"

// TODO: est ce qu'il faut vraiment mettre les fonctions read... dans la classe de base ? car peut etre que XMLDataDecoder::readBool aura besoin d'un contexte courant (pour savoir dans quel noeud il est) ? et comme on connait la classe exacte du decoder/encoder dans les loader/saver, peut etre pas besoin : NON dans les componentLoader / Saver, on ne connait pas le type exact !!! Trouver une autre solution
namespace Tuple {
    
    class XMLDataDecoder : public DataDecoder {//TODO: bridge pour une lib xml (SAX) <-> DataDecoder ?, mais ne pas l'implementer pour l'instant car il sera mieux de garder un format binaire et a la limite dans l'editeur, avoir un format xml mais qui compile en binaire (mais on peut garder la conception modulaire des data decoder dans le cas ou dans le futur, je change de format
    public:
        // Constructor
        explicit XMLDataDecoder(std::istream &inputStream);
        
        // Destructor
        //~XMLDataDecoder();
        
        // Methods
        bool readBool(std::string const &name) override;
        int readInt(std::string const &name) override;
        float readFloat(std::string const &name) override;
        std::string readString(std::string const &name) override;
    };
    
}

#endif /* defined(__Tuple__XMLDataDecoder__) */
