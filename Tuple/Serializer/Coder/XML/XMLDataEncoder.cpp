//
//  XMLDataEncoder.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 22/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "XMLDataEncoder.hpp"


namespace Tuple {
    
    XMLDataEncoder::XMLDataEncoder(std::ostream &outputStream) : DataEncoder(outputStream) {
    }
    /*
    XMLDataEncoder::~XMLDataEncoder() {
    }*/
    
    void XMLDataEncoder::writeBool(std::string const &name, bool data) {//TODO: si tjs le meme, changer avec une methode template template <class T> void writeData(T const &data) et pareil pour le decoder ? : normalement pas possible car methodes virtuelles dans la classe de base et pas possible d'avoir une methode template virtual
        // Write bool
        getOutputStream() << "<" << name << ">" << data << "</" << name << ">" << std::endl;
    }
    
    void XMLDataEncoder::writeInt(std::string const &name, int data) {
        // Write int
        getOutputStream() << "<" << name << ">" << data << "</" << name << ">" << std::endl;
    }
    
    void XMLDataEncoder::writeFloat(std::string const &name, float data) {
        // Write float
        getOutputStream() << "<" << name << ">" << data << "</" << name << ">" << std::endl;
    }
    
    void XMLDataEncoder::writeString(std::string const &name, std::string const &data) {
        // Write string
        getOutputStream() << "<" << name << ">" << data << "</" << name << ">" << std::endl;
    }
    
}
