//
//  ComponentSaver.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentSaver__
#define __Tuple__ComponentSaver__

#include "Component.hpp"
#include "MemoryManager.hpp"
#include "DataEncoder.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType> class ComponentSaver {//TODO: voir si dériver d'une classe de base ObjectSaver mais normalement non
    public:
        // Pure virtual destructor (this class is like an interface, virtual destructor is needed to call the correct destructor when object is referenced with a pointer of this class) TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        virtual ~ComponentSaver() = 0;
        
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual void saveComponent(TComponentLinkType<Component> const &component, DataEncoder &dataEncoder) = 0;
        virtual size_t getManagedClassType() const = 0;
    };
    
    
    template <template <class> class TComponentLinkType> ComponentSaver<TComponentLinkType>::~ComponentSaver() {
    }
    
}

#endif /* defined(__Tuple__ComponentSaver__) */
