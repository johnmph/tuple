//
//  ComponentLoader.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentLoader__
#define __Tuple__ComponentLoader__

#include "Component.hpp"
#include "MemoryManager.hpp"
#include "DataDecoder.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType> class ComponentLoader {//TODO: voir si dériver d'une classe de base ObjectLoader mais normalement non
    public:
        // Pure virtual destructor (this class is like an interface, virtual destructor is needed to call the correct destructor when object is referenced with a pointer of this class) TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        virtual ~ComponentLoader() = 0;
        
        // Methods
        /*! @todo mettre cette methode virtual private et créer une methode publique non virtual qui appelle cette methode, ainsi je n'expose pas une methode virtual */
        virtual TComponentLinkType<Component> loadComponent(MemoryManager &memoryManager, DataDecoder &dataDecoder) = 0;
    };
    
    
    template <template <class> class TComponentLinkType> ComponentLoader<TComponentLinkType>::~ComponentLoader() {
    }
    
}

#endif /* defined(__Tuple__ComponentLoader__) */
