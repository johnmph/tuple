//
//  SystemLoader.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SystemLoader__
#define __Tuple__SystemLoader__

#include "System.hpp"
#include "MemoryManager.hpp"
#include "DataDecoder.hpp"


namespace Tuple {
    
    class SystemLoader {//TODO: voir si dériver d'une classe de base ObjectLoader mais normalement non
    public:
        // Pure virtual destructor (this class is like an interface, virtual destructor is needed to call the correct destructor when object is referenced with a pointer of this class)
        virtual ~SystemLoader() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual System *loadSystem(MemoryManager &memoryManager, DataDecoder &dataDecoder) = 0;//TODO: voir si System * ou MemoryHandle<System> ca depend si ca sert a quelque chose de les mettre dans des handle
    };
    
}

#endif /* defined(__Tuple__SystemLoader__) */
