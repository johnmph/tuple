//
//  SystemSaver.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SystemSaver__
#define __Tuple__SystemSaver__

#include "System.hpp"
#include "MemoryManager.hpp"
#include "DataEncoder.hpp"


namespace Tuple {
    
    class SystemSaver {//TODO: voir si dériver d'une classe de base ObjectSaver mais normalement non
    public:
        // Pure virtual destructor (this class is like an interface, virtual destructor is needed to call the correct destructor when object is referenced with a pointer of this class)
        virtual ~SystemSaver() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual void saveSystem(System *system, DataEncoder &dataEncoder) = 0;//TODO: voir si system ou handle (voir le comm dans SystemLoader.hpp)
        virtual size_t getManagedClassType() const = 0;
    };
    
}

#endif /* defined(__Tuple__SystemSaver__) */
