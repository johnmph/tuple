//
//  GameEngineLoaderV1.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__GameEngineLoaderV1__
#define __Tuple__GameEngineLoaderV1__

#include "GameEngineLoader.hpp"
#include "BinaryDataDecoder.hpp"


namespace Tuple {
    
    template <class TGameEngineType> class GameEngineLoaderV1 : public GameEngineLoader<TGameEngineType> {
    public:
        // Constructor
        explicit GameEngineLoaderV1(std::shared_ptr<TGameEngineType> const &gameEngine);
        
        // Methods
        
    private:
        // Types aliases
        using EntityID = typename GameEngineLoader<TGameEngineType>::EntityID;
        using Components = typename GameEngineLoader<TGameEngineType>::Components;
        template <class TType> using ComponentLinkType = typename GameEngineLoader<TGameEngineType>::template ComponentLinkType<TType>;
        // TODO: peut etre mettre le type ComponentLoader dans le GameEngine aussi ?
        
        // Methods
        std::unique_ptr<DataDecoder> dataDecoderWithInputStream(std::istream &inputStream) override;
        void decodeData(DataDecoder &dataDecoder) override;
    };
    
    
    template <class TGameEngineType> std::unique_ptr<DataDecoder> GameEngineLoaderV1<TGameEngineType>::dataDecoderWithInputStream(std::istream &inputStream) {
        // Return a Binary data decoder
        return std::unique_ptr<DataDecoder>(new BinaryDataDecoder(inputStream));
    }
    
    template <class TGameEngineType> void GameEngineLoaderV1<TGameEngineType>::decodeData(DataDecoder &dataDecoder) {
        // Read version
        int version = dataDecoder.readInt("Version");
        
        // Check version
        if (version != 0x31524556) {
            // Throw an exception
            throw std::exception();//TODO: changer
        }
        // TODO: rajouter un hash pour le contenu du fichier ?
        
        // Read count of component GUIDs
        int componentGuidsCount = dataDecoder.readInt("ComponentGuidsCount");
        //TODO: tester le count
        
        // Create components map
        std::unordered_map<int, ComponentLinkType<Component>> components;//TODO: voir pourquoi ca ne va pas car le type est defini dans protected de la classe parente
        
        // Browse all component GUIDs
        for (int x = 0; x < componentGuidsCount; ++x) {
            // Read component GUID
            std::string componentGuid = dataDecoder.readString("ComponentGuid");
            
            // Get component loader for this GUID
            ComponentLoader<ComponentLinkType> &componentLoader = this->getComponentLoaderForGuid(componentGuid);
            // TODO: tester dans un try/catch le loader ?
            
            // Read count of components of this GUID
            int componentsCount = dataDecoder.readInt("ComponentsCount");
            //TODO: tester le count
            
            // Browse all components of this GUID
            for (int x2 = 0; x2 < componentsCount; ++x2) {
                // Read component ID
                int componentId = dataDecoder.readInt("ComponentId");
                
                // Load component
                ComponentLinkType<Component> component = componentLoader.loadComponent(this->getGameEngine().getMemoryManager(), dataDecoder);//TODO: si plus assez de données ds le fichier pour lire les données du component?
                
                // Try to insert component to components map
                /*std::pair<typename std::unordered_map<int, ComponentLinkType<Component>>::const_iterator, bool>*/auto ret = components.insert(std::make_pair(componentId, component));
                
                // If failed (because component id key already exists)
                if (ret.second == false) {
                    // Throw an exception
                    //throw std::logic_error("Already registered a component with this ID");//TODO: il ne faut pas thrower une exception mais enregistrer dans un log
                }
            }
        }
        
        // Read count of entities
        int entitiesCount = dataDecoder.readInt("EntitiesCount");
        //TODO: tester le count
        
        // Browse all entities
        for (int x = 0; x < entitiesCount; ++x) {
            // Read entity ID
            EntityID entityId = dataDecoder.readInt("EntityID");//dataDecoder.readString("EntityId");
            
            // Read count of components of this entity
            int componentsCount = dataDecoder.readInt("ComponentsCount");
            //TODO: tester le count
            
            // Browse all components of this entity
            for (int x2 = 0; x2 < componentsCount; ++x2) {
                // Read component ID
                int componentId = dataDecoder.readInt("ComponentId");
                
                // Try to find component with ID in components map
                /*typename std::unordered_map<int, ComponentLinkType<Component>>::const_iterator*/auto itComponent = components.find(componentId);
                
                // If found
                if (itComponent != std::end(components)) {
                    // Add component to entity
                    this->getGameEngine().getEntityManager().addComponentToEntity(entityId, itComponent->second);//TODO: checker l'ajout car il peut foirer (try/catch) si component avec meme class deja ajouté
                } else {
                    // Throw an exception
                    //throw std::logic_error("Component not found");//TODO: il ne faut pas thrower une exception mais enregistrer dans un log
                }
            }
        }
        
        // Read count of system GUIDs
        int systemGuidsCount = dataDecoder.readInt("SystemGuidsCount");
        //TODO: tester le count
        
        // Browse all system GUIDs
        for (int x = 0; x < systemGuidsCount; ++x) {
            // Read system GUID
            std::string systemGuid = dataDecoder.readString("SystemGuid");
            
            // Get system loader for this GUID
            SystemLoader &systemLoader = this->getSystemLoaderForGuid(systemGuid);
            // TODO: tester dans un try/catch le loader ?
            
            // Load system
            System *system = systemLoader.loadSystem(this->getGameEngine().getMemoryManager(), dataDecoder);//TODO: si plus assez de données ds le fichier pour lire les données du system?
            
            // Add system to game engine
            this->getGameEngine().getSystemManager().addSystem(system);//TODO: checker l'ajout car il peut foirer (try/catch) si system avec meme class deja ajouté
        }
        
        // TODO: par après les assets
        
        
        
        // TODO: + voir ou remplacer les char * par des std::string
        std::cout << "Version = " << version << std::endl;
    }
    
    template <class TGameEngineType> GameEngineLoaderV1<TGameEngineType>::GameEngineLoaderV1(std::shared_ptr<TGameEngineType> const &gameEngine) : GameEngineLoader<TGameEngineType>(gameEngine) {
    }
    
}

#endif /* defined(__Tuple__GameEngineLoaderV1__) */
