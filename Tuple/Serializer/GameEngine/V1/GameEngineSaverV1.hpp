//
//  GameEngineSaverV1.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__GameEngineSaverV1__
#define __Tuple__GameEngineSaverV1__

#include "GameEngineSaver.hpp"
#include "BinaryDataEncoder.hpp"


namespace Tuple {
    
    template <class TGameEngineType> class GameEngineSaverV1 : public GameEngineSaver<TGameEngineType> {
    public:
        // Constructor
        explicit GameEngineSaverV1(std::shared_ptr<TGameEngineType> const &gameEngine);
        
        // Methods
        
    private:
        // Methods
        std::unique_ptr<DataEncoder> dataEncoderWithOutputStream(std::ostream &outputStream) override;
        void encodeData(DataEncoder &dataEncoder) override;
    };
    
    
    template <class TGameEngineType> std::unique_ptr<DataEncoder> GameEngineSaverV1<TGameEngineType>::dataEncoderWithOutputStream(std::ostream &outputStream) {
        // Return a Binary data encoder
        return std::unique_ptr<DataEncoder>(new BinaryDataEncoder(outputStream));
    }
    
    template <class TGameEngineType> void GameEngineSaverV1<TGameEngineType>::encodeData(DataEncoder &dataEncoder) {
        // Write version
        dataEncoder.writeInt("Version", 0x31524556);
        /*
         
         // Read component GUID
         std::string componentGuid = dataDecoder.readString("ComponentGUID");
         
         // Get component loader for this GUID
         ComponentLoader &componentLoader = getComponentLoaderForGuid(componentGuid.c_str());
         
         //
         Component *component = componentLoader.loadComponent(getGameEngine().getMemoryManager(), dataDecoder);
         // TODO: + voir ou remplacer les char * par des std::string
         
         std::cout << "Version = " << version << ", guid = " << componentGuid << std::endl;*/
    }
    
    template <class TGameEngineType> GameEngineSaverV1<TGameEngineType>::GameEngineSaverV1(std::shared_ptr<TGameEngineType> const &gameEngine) : GameEngineSaver<TGameEngineType>(gameEngine) {
    }
    
}

#endif /* defined(__Tuple__GameEngineSaverV1__) */
