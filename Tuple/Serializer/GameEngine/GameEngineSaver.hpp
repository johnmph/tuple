//
//  GameEngineSaver.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__GameEngineSaver__
#define __Tuple__GameEngineSaver__

#include "GameEngine.hpp"
#include "ComponentSaver.hpp"
#include "SystemSaver.hpp"


namespace Tuple {
    
    template <class TGameEngineType> class GameEngineSaver {
    public:
        /*! Types aliases */
        using EntityID = typename TGameEngineType::EntityID;
        using Components = typename TGameEngineType::Components;
        template <class TType> using ComponentLinkType = typename TGameEngineType::template ComponentLinkType<TType>;
        
        // Constructor
        explicit GameEngineSaver(std::shared_ptr<TGameEngineType> const &gameEngine);
        
        // Destructor
        virtual ~GameEngineSaver();//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        void registerComponentSaver(std::string const &guid, std::shared_ptr<ComponentSaver<ComponentLinkType>> const &componentSaver);
        void registerSystemSaver(std::string const &guid, std::shared_ptr<SystemSaver> const &systemSaver);
        
        void saveToStream(std::ostream &outputStream);//TODO: renommer surement
        
    protected:
        // Methods
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual std::unique_ptr<DataEncoder> dataEncoderWithOutputStream(std::ostream &outputStream) = 0;
        virtual void encodeData(DataEncoder &dataEncoder) = 0;//TODO: renommer surement
        
    private:
        // Methods
        template <class TSaver> void registerSaver(std::unordered_map<size_t, std::pair<std::string, std::shared_ptr<TSaver>>> &savers, std::string const &guid, std::shared_ptr<TSaver> const &saver);
        
        // Game engine
        std::shared_ptr<TGameEngineType> _gameEngine;
        
        // Class type / GUID and Component saver map, needed class type because we get base class pointer and we need to know exact type to get good component saver
        std::unordered_map<size_t, std::pair<std::string, std::shared_ptr<ComponentSaver<ComponentLinkType>>>> _componentSavers;
        
        // Class type / GUID and System saver map, needed class type because we get base class pointer and we need to know exact type to get good system saver
        std::unordered_map<size_t, std::pair<std::string, std::shared_ptr<SystemSaver>>> _systemSavers;
    };
    
    template <class TGameEngineType> template <class TSaver> void GameEngineSaver<TGameEngineType>::registerSaver(std::unordered_map<size_t, std::pair<std::string, std::shared_ptr<TSaver>>> &savers, std::string const &guid, std::shared_ptr<TSaver> const &saver) {
        // Get saver managed class type
        size_t classType = saver->getManagedClassType();
        
        // Try to insert GUID / saver pair to savers map
        /*std::pair<typename std::unordered_map<size_t, std::pair<std::string, std::shared_ptr<TSaver>>>::const_iterator, bool>*/auto ret = savers.insert(std::make_pair(classType, std::make_pair(guid, saver)));
        
        // If failed (because class type key already exists)
        if (ret.second == false) {
            // Throw an exception
            throw std::logic_error("Already registered a saver for this class type");//TODO: voir si suffisant ou si passer aussi la chaine en parametre pour specifier si component ou system ?
        }
    }
    
    
    template <class TGameEngineType> GameEngineSaver<TGameEngineType>::GameEngineSaver(std::shared_ptr<TGameEngineType> const &gameEngine) : _gameEngine(gameEngine) {
        // Check parameters
        assert(gameEngine);
    }
    
    template <class TGameEngineType> GameEngineSaver<TGameEngineType>::~GameEngineSaver() {
    }
    
    template <class TGameEngineType> void GameEngineSaver<TGameEngineType>::registerComponentSaver(std::string const &guid, std::shared_ptr<ComponentSaver<ComponentLinkType>> const &componentSaver) {
        // Check parameters
        assert(componentSaver);
        
        // Register component saver
        registerSaver(_componentSavers, guid, componentSaver);
    }
    
    template <class TGameEngineType> void GameEngineSaver<TGameEngineType>::registerSystemSaver(std::string const &guid, std::shared_ptr<SystemSaver> const &systemSaver) {
        // Check parameters
        assert(systemSaver);
        
        // Register system saver
        registerSaver(_systemSavers, guid, systemSaver);
    }
    
    template <class TGameEngineType> void GameEngineSaver<TGameEngineType>::saveToStream(std::ostream &outputStream) {
        // Save to stream with specific data encoder from subclass initialized with outputStream
        encodeData(*dataEncoderWithOutputStream(outputStream));
    }
    
}

#endif /* defined(__Tuple__GameEngineSaver__) */
