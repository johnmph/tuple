//
//  GameEngineLoader.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 6/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__GameEngineLoader__
#define __Tuple__GameEngineLoader__

#include "GameEngine.hpp"
#include "DataDecoder.hpp"
#include "ComponentLoader.hpp"
#include "SystemLoader.hpp"

//TODO: si je met les key des unordered_map en const, ca me fait un drole de bug avec une error sur le destructor de GameEngineLoader qui dit qu'il est different de la definition du destructor !! + un bug dans le GameEngineSaver aussi
namespace Tuple {
    
    template <class TGameEngineType> class GameEngineLoader {
    public:
        /*! Types aliases */
        using EntityID = typename TGameEngineType::EntityID;
        using Components = typename TGameEngineType::Components;
        template <class TType> using ComponentLinkType = typename TGameEngineType::template ComponentLinkType<TType>;
        
        // Constructor
        explicit GameEngineLoader(std::shared_ptr<TGameEngineType> const &gameEngine);
        
        // Destructor
        virtual ~GameEngineLoader();//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private + regarder si les virtual destructor empty (et = 0) ne doivent pas etre declarés = default plutot que de mettre leur declaration, voir http://www.codesynthesis.com/~boris/blog/2012/04/04/when-provide-empty-destructor/ )
        
        // Methods
        void registerComponentLoader(std::string const &guid, std::shared_ptr<ComponentLoader<ComponentLinkType>> const &componentLoader);
        void registerSystemLoader(std::string const &guid, std::shared_ptr<SystemLoader> const &systemLoader);
        
        void loadFromStream(std::istream &inputStream);//TODO: renommer surement
        //TODO: ne pas oublier le systeme ou on peut charger une partie d'un fichier ou bien tout le fichier mais sans supprimer les anciennes données dans le game engine, a voir (ce que j'ai ecrit dans mon carnet)
        
    protected:
        // Methods
        TGameEngineType &getGameEngine();
        ComponentLoader<ComponentLinkType> &getComponentLoaderForGuid(std::string const &guid);
        SystemLoader &getSystemLoaderForGuid(std::string const &guid);
        
        /*! @todo mettre ces methodes virtual private et créer des methodes publique non virtual qui appelle ces methodes, ainsi je n'expose pas une methode virtual */
        virtual std::unique_ptr<DataDecoder> dataDecoderWithInputStream(std::istream &inputStream) = 0;
        virtual void decodeData(DataDecoder &dataDecoder) = 0;//TODO: renommer surement
        
    private:
        // Methods
        template <class TLoader> void registerLoader(std::unordered_map<std::string, std::shared_ptr<TLoader>> &loaders, std::string const &guid, std::shared_ptr<TLoader> const &loader);
        
        // Game engine
        std::shared_ptr<TGameEngineType> _gameEngine;
        
        // GUID / Component loader map
        std::unordered_map<std::string, std::shared_ptr<ComponentLoader<ComponentLinkType>>> _componentLoaders;
        
        // GUID / System loader map
        std::unordered_map<std::string, std::shared_ptr<SystemLoader>> _systemLoaders;
    };
    
    
    template <class TGameEngineType> template <class TLoader> void GameEngineLoader<TGameEngineType>::registerLoader(std::unordered_map<std::string, std::shared_ptr<TLoader>> &loaders, std::string const &guid, std::shared_ptr<TLoader> const &loader) {
        // Try to insert loader to loaders map
        /*std::pair<typename std::unordered_map<std::string, std::shared_ptr<TLoader>>::const_iterator, bool>*/auto ret = loaders.insert(std::make_pair(guid, loader));
        
        // If failed (because GUID key already exists)
        if (ret.second == false) {
            // Throw an exception
            throw std::logic_error("Already registered a loader for this GUID");//TODO: voir si suffisant ou si passer aussi la chaine en parametre pour specifier si component ou system ?
        }
    }
    
    
    template <class TGameEngineType> TGameEngineType &GameEngineLoader<TGameEngineType>::getGameEngine() {
        return *_gameEngine;
    }
    
    template <class TGameEngineType> ComponentLoader<GameEngineLoader<TGameEngineType>::template ComponentLinkType> &GameEngineLoader<TGameEngineType>::getComponentLoaderForGuid(std::string const &guid) {//TODO: peut etre une methode plus generique pour eviter le copier coller avec getSystemLoaderForGuid pour avoir un truc du style T &GameEngineLoader::getLoaderForGuid<T>(std::unordered_map &loader<std::string, std::shared_ptr<T>>, std::string const &guid) (en private) et les getComponent/System qui appelle cette methode avec les bons parametres
        // Try to find component loader with guid
        /*typename std::unordered_map<std::string, std::shared_ptr<ComponentLoader<ComponentLinkType>>>::iterator*/auto itComponentLoader = _componentLoaders.find(guid);
        
        // If not found
        if (itComponentLoader == std::end(_componentLoaders)) {
            // Throw an exception
            throw std::logic_error("Component loader with GUID not registered");//TODO: est ce qu'il faut vraiment faire avec une exception ou retourner un pointeur de componentloader et retourner null ici ? car un fichier peut avoir un guid qui n'est pas enregistré ici et ca ne dois pas etre considéré comme une erreur avec exception normalement ou peut etre que c bien ainsi
        }
        
        // Return component loader
        return *itComponentLoader->second;
    }
    
    template <class TGameEngineType> SystemLoader &GameEngineLoader<TGameEngineType>::getSystemLoaderForGuid(std::string const &guid) {
        // Try to find system loader with guid
        /*std::unordered_map<std::string, std::shared_ptr<SystemLoader>>::iterator*/auto itSystemLoader = _systemLoaders.find(guid);
        
        // If not found
        if (itSystemLoader == std::end(_systemLoaders)) {
            // Throw an exception
            throw std::logic_error("System loader with GUID not registered");//TODO: est ce qu'il faut vraiment faire avec une exception ou retourner un pointeur de systemLoader et retourner null ici ? car un fichier peut avoir un guid qui n'est pas enregistré ici et ca ne dois pas etre considéré comme une erreur avec exception normalement ou peut etre que c bien ainsi
        }
        
        // Return system loader
        return *itSystemLoader->second;
    }
    
    template <class TGameEngineType> GameEngineLoader<TGameEngineType>::GameEngineLoader(std::shared_ptr<TGameEngineType> const &gameEngine) : _gameEngine(gameEngine) {
        // Check parameters
        assert(gameEngine);
    }
    
    template <class TGameEngineType> GameEngineLoader<TGameEngineType>::~GameEngineLoader() {
    }
    
    template <class TGameEngineType> void GameEngineLoader<TGameEngineType>::registerComponentLoader(std::string const &guid, std::shared_ptr<ComponentLoader<ComponentLinkType>> const &componentLoader) {
        // Check parameters
        assert(componentLoader);
        
        // Register component loader
        registerLoader(_componentLoaders, guid, componentLoader);
    }
    
    template <class TGameEngineType> void GameEngineLoader<TGameEngineType>::registerSystemLoader(std::string const &guid, std::shared_ptr<SystemLoader> const &systemLoader) {
        // Check parameters
        assert(systemLoader);
        
        // Register system loader
        registerLoader(_systemLoaders, guid, systemLoader);
    }
    
    template <class TGameEngineType> void GameEngineLoader<TGameEngineType>::loadFromStream(std::istream &inputStream) {
        // Load from stream with specific data decoder from subclass initialized with inputStream
        decodeData(*dataDecoderWithInputStream(inputStream));
    }
    
}

#endif /* defined(__Tuple__GameEngineLoader__) */
