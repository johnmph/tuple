//
//  GameEngine.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 17/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__GameEngine__
#define __Tuple__GameEngine__

#include "MemoryManager.hpp"//TODO: voir pour les headers inclus dans tous les hpp/cpp
#include "NotificationManager.hpp"
#include "EntityManager.hpp"
#include "SystemManager.hpp"

//TODO: instruction cache optimization -> Locality : Reorder functions, manually within file
//TODO: une fois le plus gros fini, regarder les optimisations de copy des classes variables : http://stackoverflow.com/questions/3106110/what-is-move-semantics/11540204#11540204
//TODO: pour toutes les classes, renommer les parametre de template avec cette convention : TParameterName (voir ici) + verifier aussi que tous les membres privés commencent par _ + faire les units tests de toutes les classes
//TODO: verifier dans toutes les creations d'objets direct (en declarant la variable direct plutot qu'un pointeur + new) de voir si c'est pas mieux justement de créer dynamiquement les objets : surtout pour ce qui est contenu dans des containers (ainsi quand ils sont redimensionnés, on copie des pointeurs plutot que tous les objets)
//TODO: http://stackoverflow.com/questions/7823845/disable-compiler-generated-copy-assignment-operator : desactiver les assignements et copie dans les classes qui n'en n'ont pas besoin (comme les managers)
namespace Tuple {
    
    template </*class TMemoryManager, class TNotificationManager, */class TEntityManagerType/*, class TSystemManager, class TLoopManager*/> class GameEngine {//TODO: faire ce systeme de policies pour choisir quel types on veut sans les hardcoder dans le game engine
    public:
        // Constructor
        GameEngine();//TODO: pour l'instant je ne passe rien mais peut etre passer le memory manager, l'entityManager et le systemManager si famille de classes
        
        // Destructor
        //~GameEngine();
        
        // Methods
        MemoryManager &getMemoryManager() const;//TODO: soit ainsi, soit ne pas donner ces membres mais avoir les memes methodes que ces membres dans cette classe qui delegue les appels a ces membres
        NotificationManager &getNotificationManager() const;//TODO: et plutot retourner des shared_ptr que les objets eux memes
        TEntityManagerType &getEntityManager() const;
        SystemManager &getSystemManager() const;
        
    private:
        // Memory manager
        std::shared_ptr<MemoryManager> _memoryManager;
        
        // Notification manager
        std::shared_ptr<NotificationManager> _notificationManager;
        
        // Entity manager
        std::shared_ptr<TEntityManagerType> _entityManager;
        
        // System manager
        std::shared_ptr<SystemManager> _systemManager;
    };
    
    
    template <class TEntityManagerType> GameEngine<TEntityManagerType>::GameEngine() {
        // Create memory manager
        _memoryManager = std::make_shared<MemoryManager>();
        
        // Create notification manager
        _notificationManager = std::make_shared<NotificationManager>();
        
        // Create entity manager
        _entityManager = std::make_shared<TEntityManagerType>(_memoryManager, _notificationManager);
        
        // Create system manager
        _systemManager = std::make_shared<SystemManager>();
    }
    /*
    template <class TEntityManagerType> GameEngine<TEntityManagerType>::~GameEngine() {
    }*/
    
    template <class TEntityManagerType> MemoryManager &GameEngine<TEntityManagerType>::getMemoryManager() const {
        return *_memoryManager;
    }
    
    template <class TEntityManagerType> NotificationManager &GameEngine<TEntityManagerType>::getNotificationManager() const {
        return *_notificationManager;
    }
    
    template <class TEntityManagerType> TEntityManagerType &GameEngine<TEntityManagerType>::getEntityManager() const {
        return *_entityManager;
    }
    
    template <class TEntityManagerType> SystemManager &GameEngine<TEntityManagerType>::getSystemManager() const {
        return *_systemManager;
    }
    
}

#endif /* defined(__Tuple__GameEngine__) */
