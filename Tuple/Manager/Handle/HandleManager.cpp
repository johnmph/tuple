//
//  HandleManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 3/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "HandleManager.hpp"
#include <assert.h>


namespace Tuple {
    
    HandleEntry *HandleManager::getHandleEntryForDataPointer(void *dataPointer) {
        /*! @todo tres lent, trouver une autre solution avec une map par exemple pour faire la conversion dataPointer -> handle index */
        // Browse all handle entries
        for (/*std::vector<HandleEntry>::iterator*/auto itHandleEntry = std::begin(_handleEntries); itHandleEntry != std::end(_handleEntries); ++itHandleEntry) {
            // Get current handle entry
            /*HandleEntry &*/auto &handleEntry = *itHandleEntry;
            
            // If handle entry is used and data pointer found
            if ((handleEntry.used) && (handleEntry.dataPointer == dataPointer)) {
                // Return handle entry
                return &handleEntry;
            }
        }
        
        // Not found
        return nullptr;
    }
    
    HandleManager::HandleManager() : _handleCount(0), _firstFreeHandleEntryIndex(0) {
    }
    
    HandleManager::HandleManager(unsigned int size) : HandleManager() {
        // Reserve entries in vector
        _handleEntries.reserve(size);
    }
    
    Handle HandleManager::createHandle(void *dataPointer) {
        // Check parameter
        assert(dataPointer != nullptr);
        
        /*! @todo vraiment faire ce test? ca doit bouffer des ressources : voir si ok de le mettre qu'en debug */
#ifdef DEBUG
        // Check if data pointer has already an handle entry
        if (getHandleEntryForDataPointer(dataPointer) != nullptr) {
            // Throw an exception
            /*! @todo changer */
            throw std::logic_error("dataPointer has already acquired an handle");
        }
#endif
        
        Handle handle { _firstFreeHandleEntryIndex, 0 };
        
        // If has free handle entry
        if (_firstFreeHandleEntryIndex < _handleEntries.size()) {
            // Get free handle entry
            /*HandleEntry &*/auto &handleEntry = _handleEntries[_firstFreeHandleEntryIndex];
            
            // Set first free handle entry
            _firstFreeHandleEntryIndex = handleEntry.nextFreeIndex;
            
            // Set used flag
            handleEntry.used = 1;
            
            // Set data pointer
            handleEntry.dataPointer = dataPointer;
            
            // Update magic
            ++handleEntry.magic;
            
            // magic = -1 used for invalid handle
            if (handleEntry.magic == static_cast<decltype(handleEntry.magic)>(-1)) {
                handleEntry.magic = 0;
            }
            
            // Set handle magic
            handle.magic = handleEntry.magic;
        } else {
            // Push a new handle entry
            // Use push_back to optimize insertion (See rule 80 of "C++ Coding Standards - 101 Rules, Guidelines, And Best Practices")
            _handleEntries.push_back({ dataPointer, 0, 1 });
            
            // Set first free handle entry
            ++_firstFreeHandleEntryIndex;
        }
        
        // Increment handle count
        ++_handleCount;
        
        // Return handle
        return handle;
    }
    
    void HandleManager::destroyHandle(Handle const &handle) {
        // Get handle entry with bounds check
        /*HandleEntry &*/auto &handleEntry = _handleEntries.at(handle.index);
        
        // Check if handle entry is not used
        if (handleEntry.used == 0) {
            // Throw an exception
            /*! @todo changer */
            throw std::logic_error("Handle was already released");
        }
        
        // Reset used flag
        handleEntry.used = 0;
        
        // Set next free index of handle entry
        handleEntry.nextFreeIndex = _firstFreeHandleEntryIndex;
        
        // Set first free handle entry index
        _firstFreeHandleEntryIndex = handle.index;
        
        // Decrement handle count
        --_handleCount;
    }
    
    void HandleManager::destroyAllHandles() {
        // Remove all handles
        /*! @todo a voir et a tester, ca ne change pas forcement la taille mais si on a alloué beaucoup, il vaut peut etre mieux diminuer la taille avec size (du constructor, donc le sauver : http://stackoverflow.com/questions/8102198/how-to-free-the-memory-of-unordered-set ) */
        _handleEntries.clear();
        
        // Reset first free handle entry
        _firstFreeHandleEntryIndex = 0;
        
        // Reset handle count
        _handleCount = 0;
    }
    
    void *HandleManager::getDataPointer(Handle const &handle) const {
        // Get handle entry with bounds check
        /*HandleEntry const &*/auto const &handleEntry = _handleEntries.at(handle.index);
        
        // Return pointer if handle still valid else nullptr pointer
        return ((handleEntry.used) && (handle.magic == handleEntry.magic)) ? handleEntry.dataPointer : nullptr;
    }
    
    void HandleManager::updateHandleEntry(void *oldDataPointer, void *newDataPointer) {
        // Check parameters
        assert(oldDataPointer != nullptr);
        assert(newDataPointer != nullptr);
        
        // Get handle entry for old data pointer
        /*HandleEntry **/auto handleEntryPtr = getHandleEntryForDataPointer(oldDataPointer);
        
        // If no handle entry found
        if (handleEntryPtr == nullptr) {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("No handle for oldDataPointer");
        }
        
        /*! @todo vraiment faire ce test? ca doit bouffer des ressources : voir si ok de le mettre qu'en debug */
#ifdef DEBUG
        // Check if new data pointer has already an handle entry
        if (getHandleEntryForDataPointer(newDataPointer) != nullptr) {
            // Throw an exception
            /*! @todo changer */
            throw std::logic_error("newDataPointer has already acquired an handle");
        }
#endif
        
        // Set new data pointer in handle entry
        handleEntryPtr->dataPointer = newDataPointer;
    }
    
    void HandleManager::updateHandleEntries(size_t dataPointerByteOffset) {
        // Check parameters
        assert(dataPointerByteOffset != 0);
        
        // Browse all handle entries
        for (/*std::vector<HandleEntry>::iterator*/auto itHandleEntry = std::begin(_handleEntries); itHandleEntry != std::end(_handleEntries); ++itHandleEntry) {
            // Get current handle entry
            /*HandleEntry &*/auto &handleEntry = *itHandleEntry;
            
            // If handle entry is used
            if (handleEntry.used) {
                // Add data pointer byte offset to data pointer of current handle entry
                handleEntry.dataPointer = static_cast<unsigned char *>(handleEntry.dataPointer) + dataPointerByteOffset;
            }
        }
    }
    
    int HandleManager::getHandleCount() const {
        // Return handle count
        return _handleCount;
    }
    
}
