//
//  HandleManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 3/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__HandleManager__
#define __Tuple__HandleManager__

#include <vector>


namespace Tuple {
    
    /*!
     * This structure is a Handle for 32 bits CPU.
     * A handle is an index of a HandleEntry in the HandleManager,
     * it saves a magic number to let HandleManager knows if it is
     * always valid.
     */
    struct Handle32Bits {
        /*! Index in handle manager */
        unsigned short index;
        
        /*! Magic number, used to check if handle is still valid (points to the same data that was used when acquired) */
        unsigned short magic;
    };
    
    /*!
     * This structure is a Handle for 64 bits CPU.
     * A handle is an index of a HandleEntry in the HandleManager,
     * it saves a magic number to let HandleManager knows if it is
     * always valid.
     */
    struct Handle64Bits {
        /*! Index in handle manager */
        unsigned int index;
        
        /*! Magic number, used to check if handle is still valid (points to the same data that was used when acquired) */
        unsigned int magic;
    };
    
    /*! @todo check preprocessor pour verifier si CPU 32 / 64 bits, ok ici, a voir dans d'autres compilateurs *///TODO: changer et utiliser std::enable_if (ou un truc dans le genre) pour choisir la bonne structure (n'avoir plus qu'un Handle struct selon le processor ou un Handle struct et un typedef pour unsigned int ou unsigned short pour les members de Handle)
#if UINT_MAX == ULONG_MAX
    using Handle = Handle32Bits;
#else
    using Handle = Handle64Bits;
#endif
    
    /*
     * Equality operator overload, same handles will point to same
     * data if they are used in same HandleManager.
     * @param lhs First handle to compare
     * @param rhs Second handle to compare
     * @return true if lhs handle is equal to rhs handle else false
     */
    inline bool operator ==(Handle const &lhs, Handle const &rhs) {
        // Two handles are same if both their index and magic are same
        return (lhs.index == rhs.index) && (lhs.magic == rhs.magic);
    }
    
    /*!
     * Inequality operator overload.
     * @param lhs First handle to compare
     * @param rhs Second handle to compare
     * @return true if lhs handle is not equal to rhs handle else false
     */
    inline bool operator !=(Handle const &lhs, Handle const &rhs) {
        // Inequality is inverse of equality
        return !(lhs == rhs);
    }
    
    /*!
     * This structure saves the data pointer and saves other
     * informations to know if Handle that uses it is always valid.
     */
    struct HandleEntry {
        union {
            /*! Data pointer */
            void *dataPointer;
            
            /*! Index to next free handle entry */
            unsigned int nextFreeIndex;
        };
        
        /*! Magic number, used to check if handle is still valid (points to the same data that was used when acquired) */
        unsigned short magic;//TODO: ici tjs short ca sert a quoi de l'avoir en int dans le handle 64 bits ?
        
        /*! Used flag (only need a byte (even a bit) for flag but short for alignment) */
        unsigned short used;
    };
    
    /*! Invalid handle */
    static constexpr Handle InvalidHandle { 0, static_cast<decltype(Handle::magic)>(-1) };//TODO: voir si c'est une declaration correcte pour un null object accessible de partout
    
    /*!
     * This class manages Handle objects, it saves HandleEntry in
     * an array used as a pool to optimize creation of Handle.
     * An handle is used instead of a pointer to follow the object
     * even if it moves in memory. The handle will be always valid
     * unless it will be removed from the manager, in this case it will
     * returns nullptr.
     */
    class HandleManager {
    public:
        /*!
         * Constructor.
         */
        HandleManager();
        
        /*!
         * Constructor.
         * @param size Default number of handles that HandleManager will have
         */
        explicit HandleManager(unsigned int size);
        
        /*!
         * Create an handle from a data pointer.
         * @param dataPointer The data pointer that Handle will manage
         * @return The created handle
         */
        Handle createHandle(void *dataPointer);
        
        /*!
         * Destroy an existing handle.
         * @param handle The handle to destroy
         */
        void destroyHandle(Handle const &handle);
        
        /*!
         * Destroy all handles.
         */
        void destroyAllHandles();
        
        /*!
         * Get data pointer from an handle.
         * @param handle The handle which contains the data pointer
         * @return The data pointer of the handle or nullptr if handle is not valid anymore
         */
        void *getDataPointer(Handle const &handle) const;
        
        /*!
         * Get data pointer from an handle as TType.
         * @param TType The type of the data pointer
         * @param handle The handle which contains the data pointer
         * @return The data pointer of the handle as TType or nullptr if handle is not valid anymore
         */
        template <typename TType> TType *getDataPointerAs(Handle const &handle) const;
        
        /*!
         * Update an handle entry with new data pointer.
         * @param oldDataPointer The old data pointer that handle had
         * @param newDataPointer The new data pointer that handle will have
         */
        void updateHandleEntry(void *oldDataPointer, void *newDataPointer);
        
        /*!
         * Update all handle entries.
         * @param dataPointerByteOffset The offset to apply to all data pointers of the existing handles
         */
        void updateHandleEntries(size_t dataPointerByteOffset);
        
        /*!
         * Get count of managed handles.
         * @return The number of handles managed by this HandleManager
         */
        int getHandleCount() const;
        
    private:
        /*!
         * Get the handle entry for a data pointer.
         * @param dataPointer The data pointer that handle entry uses
         * @return HandleEntry corresponding to dataPointer or nullptr if not found
         */
        HandleEntry *getHandleEntryForDataPointer(void *dataPointer);
        
        /*! Handle count */
        unsigned int _handleCount;
        
        /*! Handle entry vector */
        std::vector<HandleEntry> _handleEntries;
        
        /*! First free handle entry index in handle vector */
        /*! @todo check preprocessor pour verifier si CPU 32 / 64 bits, ok ici, a voir dans d'autres compilateurs */
#if UINT_MAX == ULONG_MAX
        unsigned short _firstFreeHandleEntryIndex;
#else
        unsigned int _firstFreeHandleEntryIndex;
#endif
    };
    
    template <typename TType> TType *HandleManager::getDataPointerAs(Handle const &handle) const {
        // Get data pointer
        void *dataPointer = getDataPointer(handle);
        
        // Cast to TType
        return static_cast<TType *>(dataPointer);
    }
    
}

#endif /* defined(__Tuple__HandleManager__) */
