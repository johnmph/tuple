//
//  ComponentSharingPolicyNoShare.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentSharingPolicyNoShare__
#define __Tuple__ComponentSharingPolicyNoShare__

#include "EntityManagerBase.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType> class ComponentSharingPolicyNoShare : protected EntityManagerBase {
    protected:
        template <class TType> using ComponentLinkType = TComponentLinkType<TType>;
        enum { AllowSharing = false };
        
        static bool removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID);
    };
    
    
    template <template <class> class TComponentLinkType> bool ComponentSharingPolicyNoShare<TComponentLinkType>::removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID) {
        // SmartLink can always be deleted
        return true;
    }
    
}

#endif /* defined(__Tuple__ComponentSharingPolicyNoShare__) */
