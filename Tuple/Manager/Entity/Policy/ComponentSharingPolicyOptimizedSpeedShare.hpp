//
//  ComponentSharingPolicyOptimizedSpeedShare.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentSharingPolicyOptimizedSpeedShare__
#define __Tuple__ComponentSharingPolicyOptimizedSpeedShare__

#include "EntityManagerBase.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType> class ComponentSharingPolicyOptimizedSpeedShare : protected EntityManagerBase {
    protected:
        template <class TType> using ComponentLinkType = TComponentLinkType<TType>;
        enum { AllowSharing = true };
        
        ~ComponentSharingPolicyOptimizedSpeedShare() {//TODO: a retirer par apres, pour les tests (voir si les components shared sont bien tous retirés)
            std::cout << "MPH : " << _sharedComponentsAllTypes.size() << std::endl;
        };
        
        void addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID);
        bool removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID);
        
    private:
        std::unordered_map<TypeManager::TypeID, std::unordered_map<EntityID, std::unordered_set<EntityID>>> _sharedComponentsAllTypes;
    };
    
    
    template <template <class> class TComponentLinkType> void ComponentSharingPolicyOptimizedSpeedShare<TComponentLinkType>::addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID) {
        // Get (and create if not exist) shared components map for this component type
        /*std::unordered_map<EntityID, std::unordered_set<EntityID>> &*/auto &sharedComponents = _sharedComponentsAllTypes[componentTypeID];
        
        // Get (and create if not exist) map of shared entities of containingComponentEid
        /*std::unordered_set<EntityID> &*/auto &sharedEids = sharedComponents[containingComponentEid];
        
        // Copy shared entities of containingComponentEid to eid
        sharedComponents[eid] = sharedEids;
        
        // Add containingComponentEid to shared eids of eid
        sharedComponents[eid].insert(containingComponentEid);
        
        // Browse shared entities of containingComponentEid
        for (/*std::unordered_set<EntityID>::iterator*/auto it = std::begin(sharedEids); it != std::end(sharedEids); ++it) {
            // Add eid to shared entities of current entity
            sharedComponents[*it].insert(eid);
        }
        
        // Add eid to sharedEids of containingComponentEid
        sharedEids.insert(eid);
    }
    
    template <template <class> class TComponentLinkType> bool ComponentSharingPolicyOptimizedSpeedShare<TComponentLinkType>::removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID) {
        // Try to find map of shared components for this component type
        /*std::unordered_map<TypeManager::TypeID, std::unordered_map<EntityID, std::unordered_set<EntityID>>>::iterator*/auto itSharedComponents = _sharedComponentsAllTypes.find(componentTypeID);
        
        // If not found
        if (itSharedComponents == std::end(_sharedComponentsAllTypes)) {
            // Component not shared, can be deleted
            return true;
        }
        
        // Get map of shared components for this component type
        /*std::unordered_map<EntityID, std::unordered_set<EntityID>> &*/auto &sharedComponents = itSharedComponents->second;
        
        // Try to find map of shared eids for eid
        /*std::unordered_map<EntityID, std::unordered_set<EntityID>>::iterator*/auto itSharedEids = sharedComponents.find(eid);
        
        // If not found
        if (itSharedEids == std::end(sharedComponents)) {
            // Component not shared, can be deleted
            return true;
        }
        
        // Get set of shared eids for eid
        /*std::unordered_set<EntityID> &*/auto &sharedEids = itSharedEids->second;
        
        // If the component is not shared anymore
        if (sharedEids.size() == 1) {
            // Remove last shared eid of eid from sharedComponents
            sharedComponents.erase(*(std::begin(sharedEids)));
            
            // Remove eid from sharedComponents
            sharedComponents.erase(itSharedEids);
            
            // If sharedComponents is empty
            if (sharedComponents.empty()) {
                // Remove sharedComponents from sharedComponentsAllTypes
                _sharedComponentsAllTypes.erase(itSharedComponents);
            }
        } else {
            // Browse shared entities of eid
            for (/*std::unordered_set<EntityID>::iterator*/auto it = std::begin(sharedEids); it != std::end(sharedEids); ++it) {
                // Remove eid from shared eids of current entity
                sharedComponents.at(*it).erase(eid);
            }
            
            // Remove eid from sharedComponents
            sharedComponents.erase(itSharedEids);
        }
        
        // Component shared, can not be deleted
        return false;
    }
    
}

#endif /* defined(__Tuple__ComponentSharingPolicyOptimizedSpeedShare__) */
