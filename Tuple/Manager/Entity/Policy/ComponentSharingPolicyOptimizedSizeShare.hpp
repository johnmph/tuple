//
//  ComponentSharingPolicyOptimizedSizeShare.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentSharingPolicyOptimizedSizeShare__
#define __Tuple__ComponentSharingPolicyOptimizedSizeShare__

#include "EntityManagerBase.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType> class ComponentSharingPolicyOptimizedSizeShare : protected EntityManagerBase {
    protected:
        template <class TType> using ComponentLinkType = TComponentLinkType<TType>;
        enum { AllowSharing = true };
        
        ~ComponentSharingPolicyOptimizedSizeShare() {//TODO: a retirer par apres, pour les tests (voir si les components shared sont bien tous retirés)
            std::cout << "MPH : " << _sharedComponentsAllTypes.size() << std::endl;
        };
        
        void addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID);
        bool removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID);
        
    private:
        std::unordered_map<TypeManager::TypeID, std::vector<std::unordered_set<EntityID>>> _sharedComponentsAllTypes;
    };
    
    
    template <template <class> class TComponentLinkType> void ComponentSharingPolicyOptimizedSizeShare<TComponentLinkType>::addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID) {
        // Get (and create if not exist) shared components vector for this component type
        /*std::vector<std::unordered_set<EntityID>> &*/auto &sharedComponents = _sharedComponentsAllTypes[componentTypeID];
        
        // Browse shared components
        for (/*std::vector<std::unordered_set<EntityID>>::iterator*/auto it = std::begin(sharedComponents); it != std::end(sharedComponents); ++it) {
            // Get current shared eids
            /*std::unordered_set<EntityID> &*/auto &sharedEids = *it;
            
            // If we found containingComponentEid in shared eids
            if (sharedEids.find(containingComponentEid) != std::end(sharedEids)) {
                // Add eid in sharedEids
                sharedEids.insert(eid);
                
                // Exit
                return;
            }
        }
        
        // If not found, add both entities in a new set
        sharedComponents.push_back(std::unordered_set<EntityID>({ containingComponentEid, eid }));
    }
    
    template <template <class> class TComponentLinkType> bool ComponentSharingPolicyOptimizedSizeShare<TComponentLinkType>::removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID) {
        // Try to find vector of shared components for this component type
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::unordered_set<EntityID>>>::iterator*/auto itSharedComponents = _sharedComponentsAllTypes.find(componentTypeID);
        
        // If not found
        if (itSharedComponents == std::end(_sharedComponentsAllTypes)) {
            // Component not shared, can be deleted
            return true;
        }
        
        // Get vector of shared components for this component type
        /*std::vector<std::unordered_set<EntityID>> &*/auto &sharedComponents = itSharedComponents->second;
        
        // Browse shared components
        for (/*std::vector<std::unordered_set<EntityID>>::iterator*/auto it = std::begin(sharedComponents); it != std::end(sharedComponents); ++it) {
            // Get current shared eids
            /*std::unordered_set<EntityID> &*/auto &sharedEids = *it;
            
            // If we found eid in shared eids
            if (sharedEids.find(eid) != std::end(sharedEids)) {
                // If the component is not shared anymore
                if (sharedEids.size() <= 2) {
                    // Remove sharedEids from components
                    sharedComponents.erase(it);
                    
                    // If sharedComponents is empty
                    if (sharedComponents.empty()) {
                        // Remove sharedComponents from sharedComponentsAllTypes
                        _sharedComponentsAllTypes.erase(itSharedComponents);
                    }
                } else {
                    // Remove eid from sharedEids
                    sharedEids.erase(eid);
                }
                
                // Component shared, can not be deleted
                return false;
            }
        }
        
        // Component not shared, can be deleted
        return true;
    }
    
}

#endif /* defined(__Tuple__ComponentSharingPolicyOptimizedSizeShare__) */
