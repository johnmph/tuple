//
//  ComponentSharingPolicySmartLinkShare.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__ComponentSharingPolicySmartLinkShare__
#define __Tuple__ComponentSharingPolicySmartLinkShare__

#include "EntityManagerBase.hpp"
#include "SmartLink.hpp"


namespace Tuple {
    
    template <template <class> class TComponentLinkType, class TOwnershipPolicy> class ComponentSharingPolicySmartLinkShare : protected EntityManagerBase {
    protected:
        template <class TType> using ComponentLinkType = SmartLink<TType, TComponentLinkType, TOwnershipPolicy>;
        enum { AllowSharing = true };
        
        static void addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID);
        static bool removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID);
    };
    
    
    template <template <class> class TComponentLinkType, class TOwnershipPolicy> void ComponentSharingPolicySmartLinkShare<TComponentLinkType, TOwnershipPolicy>::addedSharedComponentInEntity(EntityID eid, EntityID containingComponentEid, TypeManager::TypeID componentTypeID) {
    }
    
    template <template <class> class TComponentLinkType, class TOwnershipPolicy> bool ComponentSharingPolicySmartLinkShare<TComponentLinkType, TOwnershipPolicy>::removedComponentInEntityCanBeDeleted(EntityID eid, TypeManager::TypeID componentTypeID) {
        // SmartLink can always be deleted
        return true;
    }
    
}

#endif /* defined(__Tuple__ComponentSharingPolicySmartLinkShare__) */
