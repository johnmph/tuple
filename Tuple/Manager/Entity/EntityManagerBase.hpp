//
//  EntityManagerBase.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 1/11/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityManagerBase__
#define __Tuple__EntityManagerBase__

#include <stdio.h>


namespace Tuple {
    
    class EntityManagerBase {
    protected:
        /*! Entity ID type */
        using EntityID = unsigned int;
    };
    
}

#endif /* defined(__Tuple__EntityManagerBase__) */
