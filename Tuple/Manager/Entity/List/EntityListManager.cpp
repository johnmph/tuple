//
//  EntityListManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 7/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "EntityListManager.hpp"
#include <set>


namespace Tuple {
    
    void EntityListKey::updateHashValue() {
        // Get hash function of keys
        /*std::unordered_set<TypeManager::TypeID>::hasher*/auto hashFunction = _keys.hash_function();
        
        // Get ordered set from keys unordered set (need a specific order because of the combine hash formula, see below)
        std::set<TypeManager::TypeID> orderedKeys;
        
        for (/*std::unordered_set<TypeManager::TypeID>::const_iterator*/auto itKey = _keys.cbegin(); itKey != _keys.cend(); ++itKey) {
            orderedKeys.insert(*itKey);
        }
        
        // Reset hash value
        _hashValue = 0;
        
        // Browse all ordered keys
        for (/*std::set<TypeManager::TypeID>::const_iterator*/auto itOrderedKey = orderedKeys.cbegin(); itOrderedKey != orderedKeys.cend(); ++itOrderedKey) {
            // XOR for best distribution ( see http://stackoverflow.com/questions/5889238/why-is-xor-the-default-way-to-combine-hashes?lq=1 )
            // The formula is taken from Boost ( see http://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x )
            _hashValue ^= hashFunction(*itOrderedKey) + 0x9e3779b9 + (_hashValue << 6) + (_hashValue >> 2);
        }
    }
    
    EntityListKey::EntityListKey(std::unordered_set<TypeManager::TypeID> const &keys) : _keys(keys) {
        // Check parameters
        assert(!_keys.empty());
        
        // Update hash value
        updateHashValue();
    }
    
    std::unordered_set<TypeManager::TypeID> const &EntityListKey::getKeys() const {
        // Return keys
        return _keys;
    }
    
    TypeManager::TypeID EntityListKey::getHashValue() const {
        // Return hash value
        return _hashValue;
    }
    
}
