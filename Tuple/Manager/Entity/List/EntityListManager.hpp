//
//  EntityListManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 7/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityListManager__
#define __Tuple__EntityListManager__

#include "EntityList.hpp"


namespace Tuple {
    
    /*!
     * This class is needed to save EntityList objects in a map container, it is used as
     * the key, it calculates a hash and store it for optimization.
     */
    class EntityListKey {
    public:
        /*!
         * Constructor.
         * @param keys keys which are types of component classes
         * @todo voir si copy ou pas le keys: normalement non car ca passe par le move constructor du set ?
         */
        explicit EntityListKey(std::unordered_set<TypeManager::TypeID> const &keys);
        
        /*!
         * Give the keys which are types of component classes.
         * @return The keys array
         * @todo voir si retourner un const & ou une valeur (move semantics pas applicable ici car membre de la classe que l'on retourne ?) TODO: peut etre plus besoin de sauver ca si je met les notifs dans EntityList car il a aussi son unordered_set de types !!!: je ne peux pas car _keys est utilisé aussi pour l'operateur == de cette classe, c'est la merde car ca fait une duplication de données pour presque rien (car le meme set est dans EntityList !!!) : peut etre utiliser un shared_ptr<std::unordered_set<TypeManager::TypeID>> dans EntityList et dans EntityListKey, un le crée et l'autre le recoit en parametre de son constructor !!!
         */
        std::unordered_set<TypeManager::TypeID> const &getKeys() const;
        
        /*!
         * Give the hashed value calculated with the keys array, used
         * essentially in containers.
         * @return The hashed value
         */
        TypeManager::TypeID getHashValue() const;
        
    private:
        /*! Allows access of private members from operator free functions */
        friend bool operator ==(EntityListKey const &lhs, EntityListKey const &rhs);
        
        /*!
         * Calculate the hash value from keys and cache it.
         */
        void updateHashValue();
        
        /*! Unordered set which contains keys */
        std::unordered_set<TypeManager::TypeID> _keys;
        
        /*! Cached hash value */
        TypeManager::TypeID _hashValue;
    };
    
    /*!
     * Equality operator overload, used essentially in containers.
     * @param lhs Object to compare
     * @param rhs Object to compare
     * @return true if lhs and rhs are equal else false
     * @todo ecrire aussi l'inequality operator ?
     */
    inline bool operator ==(EntityListKey const &lhs, EntityListKey const &rhs) {
        // Delegate to unordered_set
        return lhs._keys == rhs._keys;
    }
    
}

namespace std {
    
    /*!
     * Specialisation of hash function for EntityListKey class.
     * ( see http://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key )
     */
    template <> class hash<Tuple::EntityListKey> {
        public :
        /*!
         * () operator overload for EntityListKey class.
         * @param obj EntityListKey object to check for hash
         * @return Calculated hash value from keys
         */
        Tuple::TypeManager::TypeID operator()(Tuple::EntityListKey const &obj) const {
            // Return its hash value
            return obj.getHashValue();
        }
    };
    
}

namespace Tuple {
    
    /*!
     * This class manages EntityList objects and store them in a map container,
     * it gives same EntityList object if called with same components classes (shared).
     * @todo peut etre que je pourrai retirer le systeme de weak_ptr / shared_ptr pour les listes et avoir un delegate dans ces listes qui est notifié quand on delete un de ces listes (pour la virer de la map de liste ici. (ou a la place du delegate, un systeme de notif ?) // TODO: + peut etre un (unordered_)set a la place d'un vector car beaucoup de recherche d'eid dans EntityList ici !!!
     */
    template <class TEntityManagerType> class EntityListManager {
    public:
        /*!
         * Constructor.
         * @param entityManager entity manager in a shared pointer
         */
        explicit EntityListManager(std::shared_ptr<TEntityManagerType> entityManager);
        
        /*!
         * Give the entity manager.
         * @return The entity manager
         * @todo voir si shared_ptr ou pointeur ou reference direct
         */
        std::shared_ptr<TEntityManagerType> getEntityManager() const;
        
        /*!
         * Give the EntityList object that correspond to component classes.
         * Calling this method with same component classes will return
         * same list object (shared).
         * @param TComponents Component classes (template parameter)
         * @return A shared ptr of EntityList
         * @todo le seul probleme de ce systeme est que quand une liste n'est plus pointée par aucun shared_ptr, l'entrée de la liste dans la map n'est pas supprimée, seulement le weak_ptr est expired mais du coup on ne supprime pas l'entrée dans la map
         */
        template <class... TComponents> std::shared_ptr<EntityList<TEntityManagerType, TComponents ...> const> getEntityListForComponentClasses();
        
    private:
        /*!
         * Get an EntityListKey from component classes.
         * @param TComponents The components classes to get key for
         * @return The EntityListKey associated with component classes
         */
        template <class... TComponents> EntityListKey getKeyFromComponentClasses();
        
        /*! Entity manager */
        std::shared_ptr<TEntityManagerType> _entityManager;
        
        /*! Entity lists map */
        std::unordered_map<EntityListKey, std::weak_ptr<EntityListBase>> _entityLists;
    };
    
    
    template <class TEntityManagerType> EntityListManager<TEntityManagerType>::EntityListManager(std::shared_ptr<TEntityManagerType> entityManager) : _entityManager(entityManager) {
        // Check parameters
        assert(entityManager);
    }
    
    template <class TEntityManagerType> std::shared_ptr<TEntityManagerType> EntityListManager<TEntityManagerType>::getEntityManager() const {
        // Return entity manager
        return _entityManager;
    }
    
    template <class TEntityManagerType> template <class... TComponents> EntityListKey EntityListManager<TEntityManagerType>::getKeyFromComponentClasses() {
        // Create unordered set of types and return it as EntityListKey
        return EntityListKey(std::unordered_set<TypeManager::TypeID>({ TypeManager::getTypeIDForType<TComponents>()... }));
    }
    
    template <class TEntityManagerType> template <class... TComponents> std::shared_ptr<EntityList<TEntityManagerType, TComponents ...> const> EntityListManager<TEntityManagerType>::getEntityListForComponentClasses() {
        // Get entity list key from component classes
        EntityListKey key = getKeyFromComponentClasses<TComponents ...>();
        
        // Try to get entity list for key (can't use const_iterator because lock a weak ptr (to get shared_ptr) will modify it)
        /*std::unordered_map<EntityListKey, std::weak_ptr<EntityListBase>>::iterator*/auto itEntityList = _entityLists.find(key);
        
        // If entity list found
        if ((itEntityList != std::end(_entityLists)) && (!(itEntityList->second.expired()))) {
            // Get entity list in void pointer format (to be able to static cast them)
            void *entityListPtr = &(itEntityList->second);
            
            // Return entity list casted to correct templated EntityList class
            return static_cast<std::weak_ptr<EntityList<TEntityManagerType, TComponents ...>> *>(entityListPtr)->lock();
        }
        
        // Create a new entity list with correct templated class in a shared_ptr
        /*std::shared_ptr<EntityList<TEntityManagerType, TComponents ...>>*/auto entityList = std::make_shared<EntityList<TEntityManagerType, TComponents ...>>(_entityManager);
        
        // Get entity list in void pointer format (to be able to static cast them)
        void *entityListPtr = &entityList;
        
        // Add it casted to correct EntityListBase class to entity lists map with a weak_ptr (can't use insert here because an entry with this key can exist if its weak_ptr has expired in case of all users of this list have no more need of this list)
        _entityLists[key] = std::weak_ptr<EntityListBase>(*static_cast<std::shared_ptr<EntityListBase> *>(entityListPtr));
        
        // Return entity list
        return entityList;
    }
    
}

#endif /* defined(__Tuple__EntityListManager__) */
