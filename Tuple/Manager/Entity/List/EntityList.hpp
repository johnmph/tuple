//
//  EntityList.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 5/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityList__
#define __Tuple__EntityList__

#include "EntityManager.hpp"


namespace Tuple {
    
    // TODO: a mettre dans un header séparé utility (provient de http://stackoverflow.com/questions/16594002/for-stdtuple-how-to-get-data-by-type-and-how-to-get-type-by-index ) :
    template <int Index, class Search, class First, class... Types> struct GetInternal {
        using type = typename GetInternal<Index + 1, Search, Types...>::type;
        static constexpr int index = Index;//TODO: peut etre remplacer par enum
    };
    
    template <int Index, class Search, class... Types> struct GetInternal<Index, Search, Search, Types...> {
        using type = GetInternal;
        static constexpr int index = Index;//TODO: peut etre remplacer par enum
    };
    
    template <class T, class... Types> constexpr T& get(std::tuple<Types...> &tuple) {
        return std::get<GetInternal<0, T, Types...>::type::index>(tuple);
    }
    
    template <class T, class... Types> constexpr T&& get(std::tuple<Types...> &&tuple) {
        return std::get<GetInternal<0, T, Types...>::type::index>(tuple);
    }
    
    template <class T, class... Types> constexpr T const& get(std::tuple<Types...> const &tuple) {
        return std::get<GetInternal<0, T, Types...>::type::index>(tuple);
    }
    
    /*!
     * This class is only needed for saving EntityList in smart ptr and containers,
     * EntityList class can not be saved directly because it is a templated class
     * (without specifying the template parameters).
     */
    class EntityListBase {
    public:
        /*!
         * Destructor.
         */
        virtual ~EntityListBase() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
    };
    
    /*!
     * This templated class give the list of entity IDs and their components with
     * specified classes, only entities that have all components of these classes
     * are listed. This class automatically update the lists when a entity / component
     * is added / removed.
     * @param TComponents List of component classes
     */
    template <class TEntityManagerType, class... TComponents> class EntityList : public EntityListBase {
    public:
        /*! Types aliases */
        using EntityID = typename TEntityManagerType::EntityID;
        using Components = typename TEntityManagerType::Components;
        template <class TType> using ComponentLinkType = typename TEntityManagerType::template ComponentLinkType<TType>;
        
        /*!
         * Constructor.
         * @param entityManager entity manager in a shared pointer
         */
        explicit EntityList(std::shared_ptr<TEntityManagerType> entityManager);
        
        /*!
         * Destructor.
         */
        ~EntityList();
        
        /*!
         * Give the number of entities / components by class that are found
         * with specified component classes in the entity manager.
         * @return The number of entities / components by class
         */
        size_t getCount() const;
        
        /*!
         * Give the list of entity IDs of entities that are found with
         * specified component classes in the entity manager.
         * @return An array of entity IDs that have all required component classes
         */
        std::vector<EntityID> const &getEntityIDs() const;
        
        /*!
         * Give the list of MemoryHandle that contains components of entities
         * with specified component classes in the entity manager.
         * These lists have the same order than entity IDs list, by example
         * the components at index x in the components lists are the components
         * of entities at index x in the entity IDs list
         * @param TComponent Component class (template parameter)
         * @return An array of MemoryHandle of components of entities that have
         * all required component classes
         */
        template <class TComponent> std::vector<ComponentLinkType<TComponent>> const &getComponentsForClass() const;
        
    private:
        /*!
         * Remove a possible existing entity and its components from lists.
         * @param eid The entity ID of entity to remove
         */
        void removePossibleEntity(EntityID eid);
        
        /*!
         * Notification method called when an entity is removed from
         * entity manager.
         * @param notification The entity removed notification
         */
        void entityRemoved(EntityRemovedNotification<TEntityManagerType> &notification);
        
        /*!
         * Notification method called when a component is added to an
         * entity from entity manager.
         * @param notification The component added notification
         */
        void componentAdded(ComponentAddedNotification<TEntityManagerType> &notification);
        
        /*!
         * Notification method called when a component is removed from
         * an entity from entity manager.
         * @param notification The component removed notification
         */
        void componentRemoved(ComponentRemovedNotification<TEntityManagerType> &notification);
        
        /*! Types corresponding to TComponents classes */
        std::unordered_set<TypeManager::TypeID> _typeIDs;
        
        /*! Entity manager */
        std::shared_ptr<TEntityManagerType> _entityManager;
        
        /*! Entity IDs */
        std::vector<EntityID> _entityIDs;
        
        /*! Components of all types for entities in _entityIDs in the same order */
        std::tuple<std::vector<ComponentLinkType<TComponents>>...> _componentsAllTypes;
        
        // Need these nested classes to expand variadic template parameters pack, this could be avoided if lambda expression could be generic (added in C++14)
        template <typename TFirstComponent, typename... TNextComponents> struct AddComponentOfEntityVT {
            static void process(EntityList &entityList, EntityID eid) {
                // Call for first type
                AddComponentOfEntityVT<TFirstComponent>::process(entityList, eid);
                
                // Recurse for next types
                AddComponentOfEntityVT<TNextComponents...>::process(entityList, eid);
            };
        };
        
        template <typename TComponent> struct AddComponentOfEntityVT<TComponent> {
            static void process(EntityList &entityList, EntityID eid) {
                // Real function body
                // Add component
                get<std::vector<ComponentLinkType<TComponent>>>(entityList._componentsAllTypes).push_back(entityList._entityManager->template getComponentInEntity<TComponent>(eid));
            };
        };
        
        template <typename TFirstComponent, typename... TNextComponents> struct RemoveComponentFromIndexVT {
            static void process(EntityList &entityList, typename std::iterator_traits<typename std::vector<ComponentLinkType<TFirstComponent>>::iterator>::difference_type index) {
                // Call for first type
                RemoveComponentFromIndexVT<TFirstComponent>::process(entityList, index);
                
                // Recurse for next types
                RemoveComponentFromIndexVT<TNextComponents...>::process(entityList, index);
            };
        };
        
        template <typename TComponent> struct RemoveComponentFromIndexVT<TComponent> {
            static void process(EntityList &entityList, typename std::iterator_traits<typename std::vector<ComponentLinkType<TComponent>>::iterator>::difference_type index) {
                // Real function body
                // Get component vector
                auto &componentVector = get<std::vector<ComponentLinkType<TComponent>>>(entityList._componentsAllTypes);
                
                // Calculate iterator
                auto itComponent = std::begin(componentVector);
                std::advance(itComponent, index);
                
                // Remove component
                componentVector.erase(itComponent);
            };
        };
        
    };
    
    template <class TEntityManagerType, class... TComponents> EntityList<TEntityManagerType, TComponents ...>::EntityList(std::shared_ptr<TEntityManagerType> entityManager) : _typeIDs({ TypeManager::getTypeIDForType<TComponents>()... }), _entityManager(entityManager) {
        // Check parameters
        assert(entityManager);
        
        // Get entity IDs for component classes
        _entityIDs = _entityManager->template getAllEntityIDsForComponentClasses<TComponents ...>();//TODO: verifier si c'est ok ainsi (avec template pour eviter des erreurs de compilation quand on utilise des variadic template pour une methode d'une classe d'un type recu en template parameter)
        
        // Entity IDs loop
        /*! @todo Il faut que dans une EntityList on puisse déclarer le type qui sera le type de lecture, c'est a dire le type qui va définir l'ordre de l'entity list (L'entity list sera dans le meme ordre que les components de ce type) ainsi on a un cache hit en lecture, on s'en fout en ecriture mais pour la lecture c'est tres important !!! ainsi on peut avoir juste ici la liste des entities selon l'ordre du component de ce type et grace a cette liste on peut recuperer les autres types de components (mais voir si appeler des methodes pour récuperer les components pour ecrire dedans n'a pas un cout aussi sinon il faudra avoir aussi le vector _componentsAllTypes) et il faudra garder cette liste en ordre (l'ordre du type de component read). Ca implique forcement que dans une entity list il ne peut y avoir qu'un seul type lu (mais plusieurs peuvent etre ecrit) */
        for (auto itEntityID = _entityIDs.cbegin(); itEntityID != _entityIDs.cend(); ++itEntityID) {
            // Add components of entity to components all types vectors
            AddComponentOfEntityVT<TComponents...>::process(*this, *itEntityID);
        }
        
        //TODO: OPTIMISATION !!! : Une fois ca fait, on peut reorganiser _entityIDs et les std::vector dans _componentAllTypes par pointeur croissant, autrement dit on optimise le cache hit, on prend comme pointeur ceux des components du premier type (donc le premier type dans _types, on recupere le vector de component via _componentAllTypes[premier type] et on regarde les MemoryHandle dedans, on recupere les pointeurs et on reorganise par ordre croissant (ce qui permet d'avoir la boucle rapide pour les listes pour un type de component).
        // Le seul probleme est quand un memory allocator reorganise sa mémoire (mais seulement quand il déplace un seul objet, pas tous), ca n'arrive que quand on supprime un component (et ca bouge le dernier a l'endroit du component supprimé), donc on peut le voir avec les notifs de component delete et on peut corriger l'ordre des listes a ce moment la !!!
        
        // Register to entity manager notifications
        _entityManager->getNotificationManager()->registerListener(std::make_shared<NotificationListener<EntityList, EntityRemovedNotification<TEntityManagerType>>>(*this, &EntityList::entityRemoved, _entityManager.get()));
        _entityManager->getNotificationManager()->registerListener(std::make_shared<NotificationListener<EntityList, ComponentAddedNotification<TEntityManagerType>>>(*this, &EntityList::componentAdded, _entityManager.get()));
        _entityManager->getNotificationManager()->registerListener(std::make_shared<NotificationListener<EntityList, ComponentRemovedNotification<TEntityManagerType>>>(*this, &EntityList::componentRemoved, _entityManager.get()));
    }
    
    template <class TEntityManagerType, class... TComponents> EntityList<TEntityManagerType, TComponents ...>::~EntityList() {
        // Unregister to entity manager notifications
        _entityManager->getNotificationManager()->unregisterListener(std::make_shared<NotificationListener<EntityList, EntityRemovedNotification<TEntityManagerType>>>(*this, &EntityList::entityRemoved, _entityManager.get()));
        _entityManager->getNotificationManager()->unregisterListener(std::make_shared<NotificationListener<EntityList, ComponentAddedNotification<TEntityManagerType>>>(*this, &EntityList::componentAdded, _entityManager.get()));
        _entityManager->getNotificationManager()->unregisterListener(std::make_shared<NotificationListener<EntityList, ComponentRemovedNotification<TEntityManagerType>>>(*this, &EntityList::componentRemoved, _entityManager.get()));//TODO: a la place de recréer des shared_ptr pour juste retirer les listeners, surement les sauver dans des membres dans le constructor et les utiliser ici
    }
    
    template <class TEntityManagerType, class... TComponents> size_t EntityList<TEntityManagerType, TComponents ...>::getCount() const {
        // Return size of entity IDs vector
        return _entityIDs.size();
    }
    
    template <class TEntityManagerType, class... TComponents> auto EntityList<TEntityManagerType, TComponents ...>::getEntityIDs() const -> std::vector<EntityID> const & {
        // Return entity IDs vector
        return _entityIDs;
    }
    
    template <class TEntityManagerType, class... TComponents> template <class TComponent> auto EntityList<TEntityManagerType, TComponents ...>::getComponentsForClass() const -> std::vector<ComponentLinkType<TComponent>> const & {
        // Return components vector for this type
        return get<std::vector<ComponentLinkType<TComponent>>>(_componentsAllTypes);
    }
    
    template <class TEntityManagerType, class... TComponents> void EntityList<TEntityManagerType, TComponents ...>::removePossibleEntity(EntityID eid) {
        // Check eid parameter
        assert(eid != 0);
        
        // Try to find entity removed in entity list
        /*! @todo peut etre lent le std::find si beaucoup d'entities */
        auto itEntityID = std::find(std::begin(_entityIDs), std::end(_entityIDs), eid);
        
        // If not found
        if (itEntityID == std::end(_entityIDs)) {
            // Exit
            return;
        }
        
        // Calculate index
        /*! @todo Moyen d'etre plus rapide (pour std::advance aussi) : http://stackoverflow.com/questions/2152986/best-way-to-get-the-index-of-an-iterator */
        auto index = std::distance(std::begin(_entityIDs), itEntityID);
        
        // Remove entity
        _entityIDs.erase(itEntityID);
        
        // Remove components of entity from components all types vectors
        RemoveComponentFromIndexVT<TComponents...>::process(*this, index);
        
        // Notify
        /*! @todo a faire */
    }
    
    template <class TEntityManagerType, class... TComponents> void EntityList<TEntityManagerType, TComponents ...>::entityRemoved(EntityRemovedNotification<TEntityManagerType> &notification) {
        // Remove possible entity
        removePossibleEntity(notification.getEntityID());
    }
    
    template <class TEntityManagerType, class... TComponents> void EntityList<TEntityManagerType, TComponents ...>::componentAdded(ComponentAddedNotification<TEntityManagerType> &notification) {
        // Check if we don't manage this type of component
        if (_typeIDs.find(notification.getComponentTypeID()) == std::end(_typeIDs)) {//TODO: peut etre le mettre dans une methode a part const ainsi on peut avoir un const_iterator et retirer la duplication de code avec la methode en bas qui utilise le meme
            // Exit
            return;
        }
        
        // Get entity ID from notification
        /*EntityID*/auto eid = notification.getEntityID();
        
        // Get all components from notification entity
        //Components const &components = _entityManager->getAllComponentsInEntity(eid);//TODO: pas tres optimisé, voir si possible de ne pas avoir a recharger tous les components de l'entity a chaque notification de component added, voir comment fonctionne les EntityList
        
        // Check if entity has all required components
        for (auto itType = _typeIDs.cbegin(); itType != _typeIDs.cend(); ++itType) {
            if (/*components.find(*itType) == std::end(components)*/!_entityManager->hasComponentInEntity(eid, *itType)) {
                // Exit
                return;
            }
        }
        
        // Add entity ID to entity IDs vector
        _entityIDs.push_back(eid);
        
        // Add components of entity to components all types vectors
        AddComponentOfEntityVT<TComponents...>::process(*this, eid);
        
        // Notify
        /*! @todo a faire */
    }
    
    template <class TEntityManagerType, class... TComponents> void EntityList<TEntityManagerType, TComponents ...>::componentRemoved(ComponentRemovedNotification<TEntityManagerType> &notification) {
        // Check if we don't manage this type of component
        if (_typeIDs.find(notification.getComponentTypeID()) == std::end(_typeIDs)) {
            // Exit
            return;
        }
        
        // Remove possible entity
        removePossibleEntity(notification.getEntityID());
    }
    
}

#endif /* defined(__Tuple__EntityList__) */
