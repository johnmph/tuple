//
//  EntityManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 20/10/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EntityManager__
#define __Tuple__EntityManager__

#include <unordered_map>
#include <vector>
#include <unordered_set>
#include "MemoryManager.hpp"
#include "NotificationManager.hpp"
#include "Component.hpp"
#include "SmartLink.hpp"//TODO: je l'inclus pour le enable_if du deleteComponent, peut etre plutot avoir un enum OwnMemory dans les ComponentSharingPolicy et enable_if d'apres ca ? avec false dans tous sauf celui avec SmartLink : ou passer par un trait style ComponentSharingPolicyMemoryOwnerTrait qui dans sa version generale retourne false et pour le type ComponentLinkType = SmartLink retourne true

//TODO: Voir pour faire pleins de tests pour entity manager (en écrire pour chaque type de sharing)
namespace Tuple {
    /*! @todo How to manage hierarchy (like composite pattern) in entity system ? */
    
    
    template <typename> struct IsSmartLink : std::false_type {
    };
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> struct IsSmartLink<SmartLink<TType, TStorageType, TOwnershipPolicy>> : std::true_type {
    };
    
    /*!
     * This class manages entities and components, an entity is
     * just a link between components object of different classes.
     * Components are objects or POD that compose together the
     * "game object".
     * @todo peut etre changer le type de l'entity : int a la place de std::string pour les performances + la possibilité d'ajouter un nom pour l'entity ? mais alors ce serait une classe et pas un int !!! -> et utiliser l'adresse de l'entity comme id ici ? (ne pas oublier qu'on peut ajouter des tags aussi aux entities (qui deviendraient de + en + des game objects, donc on aurait un entity system hybride)) : non surement utiliser des components pour ajouter des noms, tags, ... aux entities : Plutot avoir un tableau a part avec pour chaque entity un int qui serait le hash de son nom et un vector de hash pour ses tags. ainsi on ne sauve que ce qui est necessaire, c'est rapide pour la recherche d'une entity avec tel nom ou appartenant a tel tag et c'est optimisé pour le cache cpu car stocké a part. pour l'editeur, lui il sauverai a part le nom en string et les tags en strings, et ferai la conversion en hash pour appeler les methodes de l'entity manager.
     */
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> class EntityManager : private TComponentSharingPolicy<TComponentLinkType> {
    public:
        /*! Entity ID type */
        using EntityID = typename TComponentSharingPolicy<TComponentLinkType>::EntityID;
        
        /*! Component link type */
        template <class TType> using ComponentLinkType = typename TComponentSharingPolicy<TComponentLinkType>::template ComponentLinkType<TType>;
        
        /*! Components type */
        using Components = std::unordered_map<TypeManager::TypeID, ComponentLinkType<Component>>;
        
        /*!
         * Constructor.
         * @param memoryManager memory manager in a shared pointer
         * @param notificationManager notification manager in a shared pointer
         */
        EntityManager(std::shared_ptr<MemoryManager> memoryManager, std::shared_ptr<NotificationManager> notificationManager);
        
        /*!
         * Destructor.
         */
        ~EntityManager();//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        /*!
         * Give the memory manager.
         * @return The memory manager
         * @todo voir si shared_ptr ou pointeur ou reference direct
         */
        std::shared_ptr<MemoryManager> getMemoryManager() const;
        
        /*!
         * Give the notification manager.
         * @return The notification manager
         * @todo voir si shared_ptr ou pointeur ou reference direct
         */
        std::shared_ptr<NotificationManager> getNotificationManager() const;
        
        /*!
         * Check if a entity ID is not used.
         * @param eid The entity ID to check
         * @return true if entity ID is free or false if an entity with this ID already exist
         */
        bool isFreeEntityID(EntityID eid) const;
        
        /*!
         * Give all entities created.
         * @return An array of entity ID.
         */
        std::vector<EntityID> getAllEntityIDs() const;
        
        /*!
         * Create an entity with specified ID.
         * @param eid The entity ID that created entity will have
         * @todo est ce que cette methode est vraiment necessaire, a voir pour les fichier level qu'on lit, s'il faut garder les entities id exacts ou pas
         */
        void createEntityWithID(EntityID eid);
        
        /*!
         * Create an entity.
         * @return The ID of created entity
         */
        EntityID createEntity();
        
        /*!
         * Remove an existing entity.
         * @param eid The ID of entity to remove
         */
        void removeEntity(EntityID eid);
        
        /*!
         * Remove all existing entities.
         */
        void removeAllEntities();
        
        /*!
         * Add a component to an existing entity.
         * @param eid The ID of entity which will receive the component
         * @param component The component in a MemoryHandle to add
         */
        template <class TComponent> void addComponentInEntity(EntityID eid, ComponentLinkType<TComponent> const &component);//TODO: pour bien montrer que c'est lui qui recupere le ownership (pour les MemoryLink (pour les SmartLink un peu moins)), il faudrait le passer en rvalue plutot que const lvalue
        
        template <class TComponent, bool AllowSharing = TComponentSharingPolicy<TComponentLinkType>::AllowSharing, typename std::enable_if<AllowSharing>::type* = nullptr> void addSharedComponentInEntity(EntityID eid, EntityID containingComponentEid);
        
        /*!
         * Remove an existing component with specified class from an existing entity.
         * @param TComponent The class of the component to remove
         * @param eid The ID of entity on which the component will be removed
         */
        template <class TComponent> void removeComponentInEntity(EntityID eid);
        
        /*!
         * Remove an existing component from an existing entity.
         * @param eid The ID of entity on which the component will be removed
         * @param component The component in a MemoryHandle
         */
        void removeComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID);//TODO: peut etre avoir une methode qui donne le type d'un component: getTypeForComponent(EntityID eid, TComponentLinkType<Component> const &component), voir si necessaire
        
        /*!
         * Get an existing component with specified class of an existing entity.
         * @param TComponent The class of the component to get
         * @param eid The ID of entity containing the component
         * @return The component in a MemoryHandle
         */
        template <class TComponent> ComponentLinkType<TComponent> getComponentInEntity(EntityID eid) const;
        
        ComponentLinkType<Component> /*const &*/getComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID) const;//TODO: voir avec SmartLink si le fait de le retourner en const & ne pose pas de probleme si le code client utilise la variable de retour pour la mettre dans downcastTo<X>(std::move(getComponentInEntity(...))), voir si la variable n'est pas resetée a null !!! : je ne peux pas convertir une const lvalue en rvalue donc ok / Je peux soit le retourner en const & ou en valeur, ca ne change pas grand chose point de vue optimisation (un peu mieux en const ref quand meme si je l'utilise pour modifier un component) et de tt facons si je supprime le component dans l'entity manager, la reference constante ne sera plus valide mais si je l'avais fait par valeur, la valeur serait bonne mais le component pointé par cette valeur aurait quand meme été supprimé par l'entity manager donc ca aurait quand meme foiré : mais a voir car si je delay la suppression en copiant dans un autre tableau et en le supprimant dans le tableau principal alors la const ref ne va pas et voir aussi avec les SmartLink (pour les SmartLink aucun autre probleme que pour les MemoryLink, ca revient au meme) : on peut toujours sauver dans une variable pour eviter le probleme quand on supprime l'objet et qu'il est deplacé dans un autre tableau. Mais a quoi sert de le recuperer ici en const & ? car on recoit un Component et pas la bonne classe TComponent donc il faut quand meme soit le caster soit le mettre dans une autre variable : Je pense qu'il est mieux de ne pas le retourner en const & et pareil pour getAllComponentsInEntity : si je le met en valeur il faut créer une methode private getAllComponentsInEntityConst ; c'est fait, voir si ok
        
        /*!
         * Get all components of an existing entity.
         * @param eid The ID of entity containing the components
         * @return A map of components in a MemoryHandle with their class type as key
         */
        Components /*const &*/getAllComponentsInEntity(EntityID eid) const;
        
        /*!
         * Get all entities that have components with specified classes.
         * @param TComponents The component classes that entities need to have
         * @return An array of entity ID
         */
        template <class... TComponents> std::vector<EntityID> getAllEntityIDsForComponentClasses() const;
        
        template <class TComponent> bool hasComponentInEntity(EntityID eid) const;
        bool hasComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID) const;
        
        /*! @todo rajouter une methode getAllComponentsWithClass<>() pour recuperer un iterator via le memory allocator qui gere cette classe de components (pour la boucle rapide sur un type de component) et peut etre meme verifier que getAllEntityIDsForComponentClasses a au moins 2 variadic parameters ainsi on est sur qu'on utilise la boucle rapide pour les liste d'un seul type de component */
        
    private:
        /*!
         * Get a new entity ID.
         * @return A new entity ID
         */
        EntityID freeEntityID();
        
        /*!
         * Create a new entity with specified ID.
         * @param eid a entity ID which doesn't already exist
         * @return ID of created entity which is eid
         */
        EntityID createEntityWithFreeID(EntityID eid);
        
        //template <class TComponent> static void deleteComponent(/*TComponentLinkType<Component>/*typename std::enable_if<std::is_same<SmartLink<Component>, TComponentLinkType<Component>>::value, TComponentLinkType<Component>>::type/ &component*/void *object);
        //template <class TComponent> void deleteComponent(TComponentLinkType<Component>/*typename std::enable_if<!std::is_same<SmartLink<Component>, TComponentLinkType<Component>>::value, TComponentLinkType<Component>>::type*/ &component);
        // TODO: ATTENTION il y a un bug avec les signatures de ces methodes (trouvé le 31/03/2015) : SmartLink a 3 parametres templates et defini 2 parametres templates par défaut si un seul passé, avant je testais is_same entre ComponentLinkType<TComponent> et SmartLink<TComponent>, le probleme est que ComponentLinkType est un type créé par la policy TComponentSharingPolicy et si le type créé est un SmartLink, le code client peut specifier les 2 parametres restants du SmartLink dans cette policy donc le test is_same va etre fait entre un SmartLink avec ses 3 parametres définis et un SmartLink avec 2 parametres par défaut donc il sera false si les parametres ne correspondent pas (ici on s'en fout, on veut juste savoir si c'est un SmartLink) et donc ca va appeler la methode qui croit que ce n'est pas un SmartLink et donc ca va libérer 2x la mémoire et crasher. Ici j'ai ajouté un des 2 parametres mais il reste l'autre a ajouter mais je n'ai pas un acces direct ici (il est dans la policy TComponentSharingPolicy et il n'est pas exporté dedans), voir quel est le meilleur moyen de faire ca (si j'exporte, je dois aussi exporter dans les autres policy meme si elle n'ont pas ce type donc faire attention. ET voir aussi si d'autres méthodes peuvent avoir ce probleme (peut etre en virant temporairement les parametres par défaut du SmartLink et voir ou ca ne compile plus !!!!
        // OK pour ce bug, changé std::is_same par IsSmartLink, voir : http://stackoverflow.com/questions/16905359/c11-is-same-type-trait-for-templates
        template <class TComponent> static void deleteComponent(typename std::enable_if<!IsSmartLink<ComponentLinkType<TComponent>>::value, void *>::type component);
        template <class TComponent> static void deleteComponent(typename std::enable_if<IsSmartLink<ComponentLinkType<TComponent>>::value, void *>::type component);
        
        Components const &getAllComponentsInEntityConst(EntityID eid) const;
        Components &getAllComponentsInEntityNonConst(EntityID eid);
        
        /*! Entities type */
        /*! @todo voir si plus rapide d'avoir un unordered_map d'EntityID, std::shared_ptr<Components>, ainsi quand le unordered_map est redimensionné il copie un shared_ptr plutot qu'un Components (unordered_map) complet ? on aurait plus les Components contigus en mémoire mais on n'en a pas vraiment besoin ici */
        using Entities = std::unordered_map<EntityID, Components>;
        
        // TODO : Voir ca http://stackoverflow.com/questions/892133/should-i-prefer-pointers-or-references-in-member-data (ce que je vais faire dans le new tuple a la place d'avoir des shared_ptr) :
        /*! Memory manager */
        std::shared_ptr<MemoryManager> _memoryManager;//TODO: peut etre ne plus l'avoir puisque je ne l'utilise pas ici (pour les component en SmartLink mais a voir avec les MemoryHandle/Pointer)
        
        /*! Notification manager */
        std::shared_ptr<NotificationManager> _notificationManager;
        
        /*! Last free entity ID */
        EntityID _lastFreeEntityID;
        
        /*! EID / Component map */
        Entities _entities;
    };
    
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on entity.
     */
    template <class TEntityManagerType> class EntityNotification : public Notification {
    public:
        /*! Types aliases */
        using EntityID = typename TEntityManagerType::EntityID;
        
        /*!
         * Constructor.
         * @param entityID ID of entity that notification is based for
         */
        explicit EntityNotification(EntityID entityID);
        
        /*!
         * Give the entity ID of entity notification.
         * @return The entity ID
         */
        EntityID getEntityID() const;
        
    private:
        /*! Entity ID */
        EntityID _entityID;
    };
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on entity creation.
     */
    template <class TEntityManagerType> class EntityCreatedNotification : public EntityNotification<TEntityManagerType> {
    public:
        /*!
         * Constructor.
         */
        using EntityNotification<TEntityManagerType>::EntityNotification;
    };
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on entity destruction.
     */
    template <class TEntityManagerType> class EntityRemovedNotification : public EntityNotification<TEntityManagerType> {
    public:
        /*!
         * Constructor.
         */
        using EntityNotification<TEntityManagerType>::EntityNotification;
    };
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on component.
     */
    template <class TEntityManagerType> class ComponentNotification : public EntityNotification<TEntityManagerType> {//TODO: je peux peut etre avoir un template <class TComponent> class ComponentNotification pour avoir le vrai type du component directement dedans mais a voir avant de faire ca si aucun probleme !!! un probleme est que si on a la methode removeComponent avec le type dynamique, elle doit aussi envoyer une notif mais elle ne possede pas le type statique : on sait resoudre ca en ayant dans la methode deleteComponent l'envoi de la notification mais il y a un autre probleme : on doit s'enregistrer pour la notification avec le type de la notification ce qui veut dire que si je fais la classe ComponentNotification en template, il faudra s'enregistrer pour tous les types de components ce qui est impossible (impossible de savoir a l'avance)
    public:
        /*! Types aliases */
        using EntityID = typename TEntityManagerType::EntityID;
        template <class TType> using ComponentLinkType = typename TEntityManagerType::template ComponentLinkType<TType>;
        
        /*!
         * Constructor.
         * @param entityID ID of entity which is related to component
         * @param component component that notification is based for
         */
        ComponentNotification(EntityID entityID, ComponentLinkType<Component> const &component, TypeManager::TypeID componentTypeID);
        
        /*!
         * Give the component of component notification.
         * @return The component in a MemoryHandle
         */
        ComponentLinkType<Component> getComponent() const;
        
        /*!
         * Give the component type of component notification.
         * @return The component type in a TypeManager::TypeID
         */
        TypeManager::TypeID getComponentTypeID() const;
        
    private:
        /*! Component */
        ComponentLinkType<Component> _component;
        
        /*! Component type */
        TypeManager::TypeID _componentTypeID;
    };
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on component adding.
     */
    template <class TEntityManagerType> class ComponentAddedNotification : public ComponentNotification<TEntityManagerType> {
    public:
        /*!
         * Parent constructor.
         */
        using ComponentNotification<TEntityManagerType>::ComponentNotification;
    };
    
    /*!
     * This class is used when we need to send / receive a notification
     * based on component removing.
     */
    template <class TEntityManagerType> class ComponentRemovedNotification : public ComponentNotification<TEntityManagerType> {
    public:
        /*!
         * Parent constructor.
         */
        using ComponentNotification<TEntityManagerType>::ComponentNotification;
    };
    
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::addComponentInEntity(EntityID eid, ComponentLinkType<TComponent> const &component) {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        
        // Check parameters
        assert(component.get() != nullptr);
        
        // Get components map of entity
        Components &components = getAllComponentsInEntityNonConst(eid);
        
        // Get type from class
        /*TypeManager::TypeID*/auto componentTypeID = TypeManager::getTypeIDForType<TComponent>();
        
        // Try to add component to components map, only work if not already another component with same type
        /*! @todo si on veut ajouter plusieurs components de la meme classe comment on fait ? normalement on ne peut pas le faire dans un entity game engine, il faut créer une entity qui englobe plusieurs autres entities pour faire ca */
        auto ret = components.insert(std::make_pair(componentTypeID, component));
        
        // If already added a component with this type before
        if (ret.second == false) {
            // Debug log
            //EFDebugLog(@"(%s:%d:%s [%@]) Entity with ID '%@' has already a component with class %@", __FILE__, __LINE__, __FUNCTION__, self, eid, [component class]);
            
            // Throw an exception
            /*! @todo changer */
            throw std::logic_error("Component with same class already inserted in entity eid");
        }
        
        // Add deleteComponent method for this type
        TypeManager::addMethodCallAddressForType<TComponent>(&deleteComponent<TComponent>);
        
        // Notify that a component is added to an entity
        _notificationManager->sendNotification(this, ComponentAddedNotification<EntityManager<TComponentLinkType, TComponentSharingPolicy>>(eid, component, componentTypeID));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent, bool AllowSharing, typename std::enable_if<AllowSharing>::type*> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::addSharedComponentInEntity(EntityID eid, EntityID containingComponentEid) {
        // Get type from class
        /*TypeManager::TypeID*/auto componentTypeID = TypeManager::getTypeIDForType<TComponent>();
        
        // Get component from containingComponentEid
        ComponentLinkType<TComponent> component = getComponentInEntity<TComponent>(containingComponentEid);
        
        // Add it to eid
        addComponentInEntity(eid, component);
        
        // Manage sharing of component
        this->addedSharedComponentInEntity(eid, containingComponentEid, componentTypeID);
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::removeComponentInEntity(EntityID eid) {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        /*! @todo voir si la notification est envoyée avant la suppression ou apres */
        
        // Get components map of entity
        Components &components = getAllComponentsInEntityNonConst(eid);
        
        // Get type from class
        /*TypeManager::TypeID*/auto componentTypeID = TypeManager::getTypeIDForType<TComponent>();
        
        // Try to find component of same type in components map
        /*typename Components::const_iterator*/auto itComponent = components.find(componentTypeID);//TODO: ici je n'ai plus le const a cause du move du SmartLink
        
        // If found
        if (itComponent != std::end(components)) {
            // Save component because we need it for notification and to create downcasted version
            ComponentLinkType<Component> component(/*std::move(*/itComponent->second/*)*/);
            
            // Remove component with type from components map
            components.erase(componentTypeID);
            
            // Notify that a component is removed from an entity
            _notificationManager->sendNotification(this, ComponentRemovedNotification<EntityManager<TComponentLinkType, TComponentSharingPolicy>>(eid, component, componentTypeID));//TODO: voir dans les components si pas avoir le vrai type des components plutot que d'avoir des components de classe de base Component, ce qui permettrai de ne plus devoir passer le type et d'avoir le vrai component dans les notifs, ce qui eviterai des erreurs si certaines notifs sauverait le component et le supprimerai apres sans le downcaster
            
            // Check if we can delete this component
            if (this->removedComponentInEntityCanBeDeleted(eid, componentTypeID)) {
                // Create downcasted version of component with its true type to delete it correctly
                deleteComponent<TComponent>(&component);
            }
        } else {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("Component not found");
        }
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getComponentInEntity(EntityID eid) const -> ComponentLinkType<TComponent> /*const */ {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        
        // Get components map of entity
        Components const &components = getAllComponentsInEntityConst(eid);
        
        // Get type from class
        /*TypeManager::TypeID*/auto componentTypeID = TypeManager::getTypeIDForType<TComponent>();
        
        // Try to find component of same type in components map
        /*typename Components::const_iterator*/auto itComponent = components.find(componentTypeID);
        
        // If not found
        /*! @todo vraiment envoyer une exception ou plutot retourner un MemoryHandle invalide ? */
        if (itComponent == std::end(components)) {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("Component of this class not found in entity");
        }
        
        // Return founded component downcasted to TComponent
        return downcastTo<TComponent>(std::move(ComponentLinkType<Component>(itComponent->second)));//TODO: Pour SmartLink on copie d'abord pour qu'on ne perde pas l'ownership ici quand on passe en rvalue il faut voir si on peut eviter ca en ayant par exemple un downcastTo avec un lvalue aussi mais voir car ca laisse le code client pouvoir avoir des problemes s'il n'utilise pas la bonne version, a voir
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID) const -> ComponentLinkType<Component> /*const &*/ {
        // Get components map of entity
        Components const &components = getAllComponentsInEntityConst(eid);
        
        // Try to find component of same type in components map
        /*typename Components::const_iterator*/auto itComponent = components.find(componentTypeID);
        
        // If not found
        /*! @todo vraiment envoyer une exception ou plutot retourner un MemoryHandle invalide ? */
        if (itComponent == std::end(components)) {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("Component of this class not found in entity");
        }
        
        // Return found component
        return itComponent->second;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class... TComponents> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getAllEntityIDsForComponentClasses() const -> std::vector<EntityID> {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        
        // Get types from classes
        std::unordered_set<TypeManager::TypeID> const typeIDs({ TypeManager::getTypeIDForType<TComponents>()... });
        
        // Create eid vector
        std::vector<EntityID> eids;
        
        // Browse all entities
        for (auto itEntity = std::begin(_entities); itEntity != std::end(_entities); ++itEntity) {
            // Get components map of current entity
            Components const &components = itEntity->second;
            
            // Browse all component types
            bool entityOK = true;
            
            for (auto itType = std::begin(typeIDs); itType != std::end(typeIDs); ++itType) {
                // If component of current type not found in entity
                if (components.find(*itType) == std::end(components)) {
                    // Reset entity ok flag
                    entityOK = false;
                    
                    // Exit loop
                    break;
                }
            }
            
            // If entity has all components
            if (entityOK) {
                // Add entity id to eids
                eids.push_back(itEntity->first);
            }
        }
        
        // Return eids
        return eids;
    }
    
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::deleteComponent(typename std::enable_if<!IsSmartLink<ComponentLinkType<TComponent>>::value, void */*ComponentLinkType<Component> &*/>::type component) {
        // Convert to a true type MemoryLink
        ComponentLinkType<TComponent> downcastedComponent(downcastTo<TComponent>(std::move(*static_cast<ComponentLinkType<Component> *>(component))));
        //ComponentLinkType<TComponent> downcastedComponent(downcastTo<TComponent>(std::move(component)));
        
        // Delete it
        MemoryManager::deleteData(downcastedComponent);
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::deleteComponent(typename std::enable_if<IsSmartLink<ComponentLinkType<TComponent>>::value, void */*ComponentLinkType<Component> &*/>::type component) {
        // Convert to a true type SmartLink, it will manage itself the ownership
        downcastTo<TComponent>(std::move(*static_cast<ComponentLinkType<Component> *>(component)));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::freeEntityID() -> EntityID {
        /*! @todo a la place d'avoir ce systeme, avoir un membre int qui s'incremente (comme lastFreeEntityID) mais sauver uniquement les indexs que l'on libere (removeEntity) et on donne ceux la en priorité avant de donner lastFreeEntityID, si lastFreeEntityID arrive a intmax alors on throw une exception pour dire qu'il n'y a plus de eid de libre */
        // Increment last free entity ID
        ++_lastFreeEntityID;
        
        // Browse all entity id range
        for (EntityID eid = _lastFreeEntityID - 1; _lastFreeEntityID != eid; ++_lastFreeEntityID) {
            // Don't use 0 as valid eid
            if (_lastFreeEntityID != 0) {
                // Check if free eid
                if (isFreeEntityID(_lastFreeEntityID)) {
                    // Return it
                    return _lastFreeEntityID;
                }
            }
        }
        
        // Throw an exception
        /*! @todo changer */
        throw std::logic_error("No more free eid");
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::createEntityWithFreeID(EntityID eid) -> EntityID {
        // Add it to entities map
        /*! @todo peut etre optimiser en reservant une taille de X components dans la map avec X = le nombre de types de components enregistrés */
        _entities[eid] = Components();
        
        // Notify that an entity is created
        _notificationManager->sendNotification(this, EntityCreatedNotification<EntityManager<TComponentLinkType, TComponentSharingPolicy>>(eid));
        
        // Return eid
        return eid;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> EntityManager<TComponentLinkType, TComponentSharingPolicy>::EntityManager(std::shared_ptr<MemoryManager> memoryManager, std::shared_ptr<NotificationManager> notificationManager) : _memoryManager(memoryManager), _notificationManager(notificationManager), _lastFreeEntityID(0) {
        // Check parameters
        assert(memoryManager);
        assert(notificationManager);
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> EntityManager<TComponentLinkType, TComponentSharingPolicy>::~EntityManager() {
        // Remove all entities
        removeAllEntities();
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> std::shared_ptr<MemoryManager> EntityManager<TComponentLinkType, TComponentSharingPolicy>::getMemoryManager() const {
        // Return memory manager
        return _memoryManager;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> std::shared_ptr<NotificationManager> EntityManager<TComponentLinkType, TComponentSharingPolicy>::getNotificationManager() const {
        // Return notification manager
        return _notificationManager;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> bool EntityManager<TComponentLinkType, TComponentSharingPolicy>::isFreeEntityID(EntityID eid) const {
        // Try to find eid entity
        auto itEntity = _entities.find(eid);
        
        // Return true is not found (so free) else false
        return (itEntity == std::end(_entities));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getAllEntityIDs() const -> std::vector<EntityID> {
        // Create eids vector
        std::vector<EntityID> eids;
        
        // Reserve number of entities
        eids.reserve(_entities.size());
        
        // Browse all entities
        for (auto itEntity = std::begin(_entities); itEntity != std::end(_entities); ++itEntity) {
            // Add key
            eids.push_back(itEntity->first);
        }
        
        // Return eids vector
        return eids;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::createEntityWithID(EntityID eid) {
        // Check parameters
        assert(eid != 0);
        
        // Check if eid is free
        if (!isFreeEntityID(eid)) {
            // Debug log
            //EFDebugLog(@"(%s:%d:%s [%@]) Entity with ID '%@' already exists", __FILE__, __LINE__, __FUNCTION__, self, eid);
            
            // Throw an exception
            /*! @todo changer */
            throw std::logic_error("eid entity already exists");
        }
        
        // Create new entity
        createEntityWithFreeID(eid);
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::createEntity() -> EntityID {
        // Create a new entity with new ID and return it
        return createEntityWithFreeID(freeEntityID());
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::removeEntity(EntityID eid) {
        /*! @todo voir si la notification est envoyée avant la suppression ou apres et si on sauve les components ou pas pour les envoyer a la notif */
        
        // Get components map of entity
        Components &components = getAllComponentsInEntityNonConst(eid);//TODO: faire une methode private a partir d'ici et appeler ca ici et dans la boucle de removeAllEntities pour eviter les checks dans cette methode inutile ici si appelé via removeAllEntities
        
        // Browse all components of entity
        for (auto itComponent = std::begin(components); itComponent != std::end(components); ++itComponent) {//TODO: ici l'iterator n'est pas const a cause de l'appel a callMethodForTypeID qui va appeler deleteComponent avec un SmartLink qui devra etre modifié
            // Get component type
            /*TypeManager::TypeID*/auto componentTypeID = itComponent->first;
            
            // Check if we can delete this component
            if (this->removedComponentInEntityCanBeDeleted(eid, componentTypeID)) {
                // Call method
                TypeManager::callMethodForTypeID(componentTypeID, &(itComponent->second));
            }
        }
        
        // Remove entity from entities map
        _entities.erase(eid);//TODO: GROS PROBLEME: comment savoir quel type avait le component (on connait son type via size_t mais pas en statique donc pas moyen ici de reconstruire un type pour appeler le bon destructor du component !!!, il faut refaire un systeme comme j'avais avant pour sauver les destructors dans une map d'apres les types size_t (l'avoir peut etre dans le MemoryManager comme avant avec une autre facon d'appeler le deleteData avec le type passé en size_t plutot qu'avec le template parameter comme mon ancienne solution OU ALORS ne pas avoir la possibilité de supprimer une entity qui a encore un component pour obliger le code client a supprimer lui meme les components en specifiant les types d'abord mais c'est une sale contrainte !!!
        //TODO: peut etre avoir une methode static callDestructorForDynamicType(size_t type, void *object) dans TypeManager ? ce serait elle qui serait chargée de sauver la map de destructor mais pour ca il faut que le code client appelle la methode static saveDestructorForType<TType>() a chaque fois qu'il aura besoin plus tard d'appeler ce destructor avec un type dynamique : pas sur que ce soit bon car il ne faut pas oublier qu'avec les SmartLink ce sont eux qui doivent appeler le destructor et pas nous ici car si shared il ne faut pas l'appeler s'il est toujours utilisé dans une autre entity !!!
        //TODO: et ce probleme est aussi bien pour les SmartLink que pour les MemoryLink !!!
        //TODO: peut etre avoir un removeComponentFromEntity(EntityID eid, size_t componentTypeID); pour retirer un component avec un type dynamique ?
        //TODO: mais ici ne pas appeler en boucle pour tous les components de l'entity removeComponentFromEntity car je ne dois pas envoyer une notif de ComponentRemoved pour chaque component de cette entity et en plus dans la methode removeComponentFromEntity il y a des checks que je ne dois pas faire pour ici car les components appartiennent bien a cette entity + faire attention aux boucles de components quand on les retire dedans !!!
        // TODO: il faut donc un SmartLink avec le size_t type comme je pensais au début pour qu'il appelle le bon destructor quand il est detruit, il faut aussi une methode pour détruire un MemoryLink en donnant soit aussi le type pour qu'il sache appeler le bon destructor soit en lui passant directement l'adresse du destructor a appeler. Si on lui passe le type alors c'est lui qui doit se charger de créer les destructors address sinon c'est ici que ca doit etre géré. Tout ca peut etre géré dans le TypeManager mais il faut toujours appeler les statics methods ici ou dans le MemoryManager
        //TODO: ou alors a la place de tout ca, avoir une policy dans EntityManager pour choisir le type de la classe de base Component (il y en aurai une avec le destructor virtual pour avoir des components polymorphiques et une en POD et selon le choix on appelle le destructor ou non mais plus besoin de se soucier du vrai type car si on appelle le destructor via le type de base ce n'est pas grave puisqu'il est virtual !!! Le seul probleme de cette technique est qu'on ne peut plus avoir une partie des Component POD et l'autre polymorphique, on est obligé d'avoir l'un ou l'autre mais pas les 2 en meme temps : Mais si on a la classe Component qui est POD et une classe PolymorphicComponent qui deriverait de Component et qui aurait un destructor virtual puis qu'on aurait une map qui ferait la liaison entre le size_t type et un bool pour savoir si ce type est polymorphique ou non et selon cette info on appelerait ou non le destructor et si on doit l'appeler on appelerai celui de PolymorphicComponent et comme il est virtuel il appelerait automatiquement le bon de la classe derivée. Ca permettrai d'avoir des Components POD et non POD en meme temps, de plus on gagnerai de la place car ce tableau remplacerait celui des destructor address et il faudrait sauver un bool par type a la place d'un pointeur par type donc le tableau serait plus petit en mémoire !!!
        
        // Notify that an entity is removed
        // TODO: voir car ici j'envoie la notif EntityRemoved apres avoir supprimé ses components !!!
        _notificationManager->sendNotification(this, EntityRemovedNotification<EntityManager<TComponentLinkType, TComponentSharingPolicy>>(eid));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::removeAllEntities() {
        // Get all eids (remove entities one by one to have the same behaviour than if we remove one entity instead of removing all data with clear)
        std::vector<EntityID> const eids = getAllEntityIDs();
        
        // Browse all eids
        for (auto itEid = std::begin(eids); itEid != std::end(eids); ++itEid) {
            // Remove entity with eid
            removeEntity(*itEid);
        }
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> void EntityManager<TComponentLinkType, TComponentSharingPolicy>::removeComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID) {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        
        /*! @todo voir si la notification est envoyée avant la suppression ou apres */
        // Check parameters
        assert(componentTypeID != 0);
        
        // Get components map of entity
        Components &components = getAllComponentsInEntityNonConst(eid);
        
        // Try to find component of same type in components map
        /*typename Components::const_iterator*/auto itComponent = components.find(componentTypeID);//TODO: ici pas de const a cause du move du SmartLink en dessous
        
        // If component found in components map
        if (itComponent != std::end(components)) {
            // Save component because we need it for notification
            ComponentLinkType<Component> component(/*std::move(*/itComponent->second/*)*/);//TODO: avant de le supprimer, il faut le convertir en un smart link qui contient le type, peut etre utiliser celui la dans les notifications aussi ainsi pas besoin de passer le type dans les notifications component ? a voir
            
            // Remove component from components map
            components.erase(componentTypeID);
            
            // Notify that a component is removed from an entity
            _notificationManager->sendNotification(this, ComponentRemovedNotification<EntityManager<TComponentLinkType, TComponentSharingPolicy>>(eid, component, componentTypeID));//TODO: voir dans les components si pas avoir le vrai type des components plutot que d'avoir des components de classe de base Component, ce qui permettrai de ne plus devoir passer le type et d'avoir le vrai component dans les notifs, ce qui eviterai des erreurs si certaines notifs sauverait le component et le supprimerai apres sans le downcaster
            
            // Check if we can delete this component
            if (this->removedComponentInEntityCanBeDeleted(eid, componentTypeID)) {
                // Call method
                TypeManager::callMethodForTypeID(componentTypeID, &component);
            }
        } else {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("Component not found");
        }
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getAllComponentsInEntityConst(EntityID eid) const -> Components const & {
        // Check parameters
        assert(eid != 0);
        
        // Try to find eid entity
        auto itEntity = _entities.find(eid);
        
        // If not found
        if (itEntity == std::end(_entities)) {
            // Debug log
            //EFDebugLog(@"(%s:%d:%s [%@]) Entity with ID '%@' not found", __FILE__, __LINE__, __FUNCTION__, self, eid);
            
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("eid entity not found");
        }
        
        // Return components map of entity
        return itEntity->second;
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getAllComponentsInEntityNonConst(EntityID eid) -> Components & {
        /*! Call const version of getAllComponentsFromEntity (@see http://stackoverflow.com/questions/123758/how-do-i-remove-code-duplication-between-similar-const-and-non-const-member-func ) */
        return const_cast<typename EntityManager<TComponentLinkType, TComponentSharingPolicy>::Components &>(static_cast<const EntityManager<TComponentLinkType, TComponentSharingPolicy> &>(*this).getAllComponentsInEntityConst(eid));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> auto EntityManager<TComponentLinkType, TComponentSharingPolicy>::getAllComponentsInEntity(EntityID eid) const -> Components {
        return getAllComponentsInEntityConst(eid);
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> template <class TComponent> bool EntityManager<TComponentLinkType, TComponentSharingPolicy>::hasComponentInEntity(EntityID eid) const {
        // TODO: il faut rajouter un static_assert sur TComponent pour voir s'il est une classe derivee de Component
        
        // Get components map of entity
        Components const &components = getAllComponentsInEntityConst(eid);
        
        // Get type from class
        /*TypeManager::TypeID*/auto componentTypeID = TypeManager::getTypeIDForType<TComponent>();
        
        // Try to find component of same type in components map
        auto itComponent = components.find(componentTypeID);
        
        // Check if component found
        return (itComponent != std::end(components));
    }
    
    template <template <class> class TComponentLinkType, template <template <class> class> class TComponentSharingPolicy> bool EntityManager<TComponentLinkType, TComponentSharingPolicy>::hasComponentInEntity(EntityID eid, TypeManager::TypeID componentTypeID) const {
        // Get components map of entity
        Components const &components = getAllComponentsInEntityConst(eid);
        
        // Try to find component of same type in components map
        auto itComponent = components.find(componentTypeID);
        
        // Check if component found
        return (itComponent != std::end(components));
    }
    
    
    template <class TEntityManagerType> EntityNotification<TEntityManagerType>::EntityNotification(EntityID entityID) : _entityID(entityID) {
        // Check parameters
        assert(entityID != 0);
    }
    
    template <class TEntityManagerType> auto EntityNotification<TEntityManagerType>::getEntityID() const -> EntityID {
        // Return entity ID
        return _entityID;
    }
    
    template <class TEntityManagerType> ComponentNotification<TEntityManagerType>::ComponentNotification(EntityID entityID, ComponentLinkType<Component> const &component, TypeManager::TypeID componentTypeID) : EntityNotification<TEntityManagerType>(entityID), _component(component), _componentTypeID(componentTypeID) {
        // Check parameters
        assert(component.get() != nullptr);
        assert(componentTypeID != 0);//TODO: a voir
    }
    
    template <class TEntityManagerType> auto ComponentNotification<TEntityManagerType>::getComponent() const -> ComponentLinkType<Component> {
        // Get component
        return _component;
    }
    
    template <class TEntityManagerType> TypeManager::TypeID ComponentNotification<TEntityManagerType>::getComponentTypeID() const {
        // Get component type
        return _componentTypeID;
    }
    
}

#endif /* defined(__Tuple__EntityManager__) */
