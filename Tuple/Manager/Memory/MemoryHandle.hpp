//
//  MemoryHandle.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 24/08/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__MemoryHandle__
#define __Tuple__MemoryHandle__

#include "MoveableMemoryAllocator.hpp"
#include "MemoryLink.hpp"


namespace Tuple {
    
    template <class TType, class TDerived> class MemoryHandleTrait {
    public:
        /*!
         * Get the pointer managed by the pointer.
         * @return Pointer to data pointed by Pointer
         */
        TType *get() const;
        
        void freeMemory();//todo:surement private avec friend sur MemoryManager
        
        /*!
         * Type of data getter, it's the true type of the data,
         * even if it's a POD and if it's a upcasted MemoryPointer.
         * @return Type of data managed by this MemoryPointer
         */
        //TypeManager::TypeID getType() const;
        
    protected:
        /*! Declare type of MemoryAllocator and Link */
        using TMemoryAllocator = MoveableMemoryAllocator;
        using TLink = Handle;
        
        /*! Define constants for members initialization in derived class */
        // TODO: voir ca pour les constantes : http://codewrangler.home.comcast.net/~codewrangler/tech_info/cpp_const.html
        //static constexpr TMemoryAllocator *MemoryAllocatorNull = nullptr;
        //static constexpr TLink LinkNull = InvalidHandle;
        static constexpr TMemoryAllocator *MemoryAllocatorNull() { return nullptr; };
        static constexpr TLink LinkNull() { return InvalidHandle; };// TODO: si je declare en constante comme juste au dessus, j'ai des erreurs de link, surement parce qu'il est déclaré mais pas défini mais comment définir une constante qui est dans une template class et pourquoi je dois le définir si je n'utilise pas son adresse (je ne l'utilise que pour initialiser le membre _link dans la classe dérivée !!! a voir)
        
        /*! Protected non-virtual destructor to avoid create and delete this class, we must pass by its child classes */
        /*! @todo voir si suffisant de faire ca pour ne pas instancier la classe de base (celle ci), voir le paragraphe 1.7 du pdf d'andrei alexandrescu */
        ~MemoryHandleTrait() = default;
    };
    
    /*!
     * Equality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is equal to rhs object else false
     */
    template <class TType, class TDerived, class TOtherType, class TOtherDerived> inline bool operator ==(MemoryHandleTrait<TType, TDerived> const &lhs, MemoryHandleTrait<TOtherType, TOtherDerived> const &rhs) {
        // If MemoryAllocator (and so HandleManager) are same and link also, so they points to the same memory, no need to compare memory with get because that is not optimized
        //return (lhs.getMemoryAllocator() == rhs.getMemoryAllocator()) && (lhs.getLink() == rhs.getLink());
        return (static_cast<TDerived const *>(&lhs)->_memoryAllocator == static_cast<TOtherDerived const *>(&rhs)->_memoryAllocator) && (static_cast<TDerived const *>(&lhs)->_link == static_cast<TOtherDerived const *>(&rhs)->_link);
    }
    
    template <class TType, class TDerived> inline bool operator ==(MemoryHandleTrait<TType, TDerived> const &lhs, std::nullptr_t rhs) {
        // MemoryHandle is nullptr if its memory allocator is nullptr or if its link is invalid
        //return (lhs.getMemoryAllocator() == nullptr) || (lhs.getLink() == InvalidHandle);
        return (static_cast<TDerived const *>(&lhs)->_memoryAllocator == nullptr) || (static_cast<TDerived const *>(&lhs)->_link == InvalidHandle);
    }
    
    
    template <class TType, class TDerived> TType *MemoryHandleTrait<TType, TDerived>::get() const {
        //TODO: assert pour voir si *this != nullptr ?
        // Get this as MemoryLink
        /*TDerived const **/auto memoryLink = static_cast<TDerived const *>(this);
        
        // Get memory address of link
        /*void **/auto memoryAddr = memoryLink->_memoryAllocator->getMemoryForHandle(memoryLink->_link);
        
        // Cast to TType
        return static_cast<TType *>(memoryAddr);
    }
    /*
    template <class TType, class TDerived> TypeManager::TypeID MemoryHandleTrait<TType, TDerived>::getType() const {
        //TODO: assert pour voir si *this != nullptr ?
        // Get type from memory allocator
        return static_cast<TDerived const *>(this)->_memoryAllocator->getTypeForHandle(static_cast<TDerived const *>(this)->_link);
    }*/
    
    template <class TType, class TDerived> void MemoryHandleTrait<TType, TDerived>::freeMemory() {
        // Get this as MemoryLink
        /*TDerived const **/auto memoryLink = static_cast<TDerived *>(this);
        
        // Free link moveable memory from its memory allocator
        memoryLink->_memoryAllocator->freeMoveableMemory(memoryLink->_link);
        
        // TODO: reset le memory allocator et le link ?
    }
    
    
    // Define MemoryHandle
    template <typename TType> using MemoryHandle = MemoryLink<TType, MemoryHandleTrait>;
    
}

#endif /* defined(__Tuple__MemoryHandle__) */
