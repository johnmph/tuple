//
//  SmartLink.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 21/09/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SmartLink__
#define __Tuple__SmartLink__

#include "MemoryManager.hpp"
#include "OwnershipPolicySimpleCount.hpp"


namespace Tuple {
    
    /*!
     * This class manages a MemoryLink to automatically delete it
     * when it is not referenced by a SmartLink anymore.
     * It acts as a std::unique_ptr when there is only one SmartLink
     * which reference the MemoryHandle, so no count is allocated and
     * it acts as a std::shared_ptr when there are more than one
     * SmartLink which reference the MemoryHandle.
     * @todo voir pour les performances
     */
    // TODO: maintenant que le SmartLink est une classe fille de MemoryPointer/Handle/MixPointerHandle, voir si on peut simplifier en virant des methodes ici, par exemple get ? : PROBLEME avec cette technique que SmartLink est une classe fille de MemoryLink : le downcastTo est accessible alors que normalement on doit passer par la free function downcastTo !!! peut etre utiliser aussi des free function downcastTo dans MemoryLink ? et comme la free function ici a un type plus specialisé (le SmartLink) c'est celle ci qui sera appelée quand on lui passera un SmartLink en parametre mais faire attention car parametre est rvalue ici et pas dans MemoryLink
    // TODO: surement ne plus heriter de TStorage mais composer plutot mais a reflechir
    template <class TType, template <class> class TStorageType = MemoryHandle, /*template <class>*/ class TOwnershipPolicy = OwnershipPolicySimpleCount> class SmartLink : public TStorageType<TType>, public TOwnershipPolicy/*<TType>*/ {//TODO: a terminer
    public:
        // Simplified types aliases
        using ST = TStorageType<TType>;//TODO: les laisser dans public ou les mettre dans private ?? a voir par rapport a loki
        using OP = TOwnershipPolicy/*<TType>*/;
        
        /*!
         * Default constructor.
         * @todo si je laisse constexpr il faut definir la methode ici car un constexpr est inline obligé et voir pour le mettre ailleurs
         */
        /*constexpr */SmartLink();
        
        /*!
         * Empty constructor, to allow to reset SmartLink.
         * @todo explicit et aussi pour les copy/move constructors ?
         */
        SmartLink(std::nullptr_t);
        
        /*!
         * Parametrized constructor.
         * @param storage Storage which will be managed by this smart ptr
         */
        explicit SmartLink(ST const &storage);//TODO: si on le met explicit on ne peux plus convertir un MemoryLink en SmartLink via un assignment de MemoryLink sur un SmartLink, il faudra l'englober d'un constructor de SmartLink
        
        /*!
         * Copy constructor.
         * @param smartLink SmartLink to copy
         * @todo attention donc tout ce qui est construit par défaut par le compilateur est utilisé a la place des constructors et assignments (et peut etre meme les operators !!!) qui sont avec un autre parametre (TSubType) !!!, il faut voir si c'est possible de forcer l'utilisation de mes constructors, assignements et operators ou bien de créer ceux sans type mais de les faire appeler ceux avec type (pour ne pas avoir de duplication de code, voir si optimisé car encore un appel supplémentaire) !!!
         */
        SmartLink(SmartLink const &smartLink);//TODO: on ne peut pas mettre const a cause de OwnershipPolicyLazyCount ici et dans le coercion copy: TODO: maintenant si avec les mutable const members, a voir dans mes notes
        
        /*!
         * Move constructor.
         * @param smartLink SmartLink to move
         */
        SmartLink(SmartLink &&smartLink) noexcept;
        
        /*!
         * Coercion copying constructor, used for copying and converting a SmartLink if types are convertible.
         * @param smartLink Another SmartLink with TSubType to convert to SmartLink with TType type
         */
        template <class TSubType> SmartLink(SmartLink<TSubType, TStorageType, TOwnershipPolicy> const &smartLink);
        
        /*!
         * Coercion moving constructor, used for moving and converting a SmartLink if types are convertible.
         * @param smartLink Another SmartLink with TSubType to convert to SmartLink with TType type
         */
        template <class TSubType> SmartLink(SmartLink<TSubType, TStorageType, TOwnershipPolicy> &&smartLink) noexcept;
        
        /*!
         * Destructor.
         */
        ~SmartLink();
        
        /*!
         * Copy / move assignment using copy and swap technic.
         * @param smartLink SmartLink to copy / move
         * @return This object
         */
        SmartLink& operator =(SmartLink smartLink) noexcept;
        
        /*!
         * Coercion copying / moving assignment using copy and swap technic, used for copying / moving and converting a SmartLink if types are convertible.
         * @param smartLink Another SmartLink with TSubType to convert to SmartLink with TType type
         * @return This object
         * @todo attention avec le copy and swap car dans : http://en.cppreference.com/w/cpp/language/move_operator il est marqué que ca fait un appel au move constructor supplémentaire mais on a l'exception safety garanty mais ici normalement je n'en ai pas besoin car on ne fait que copier des pointeurs et des scalaires
         */
        template <class TSubType> SmartLink<TType, TStorageType, TOwnershipPolicy>& operator =(SmartLink<TSubType, TStorageType, TOwnershipPolicy> smartLink) noexcept;
        
        /*!
         * Get the pointer managed by the link.
         * @return Pointer to data pointed by Handle
         */
        //TType *get() const;
        
        /*!
         * Swap friend function used by coercion copying / moving assignment.
         * @param first First SmartLink with TFirstType to swap
         * @param second Second SmartLink with TSecondType to swap
         * @todo les fonctions friend peuvent etre definies en dehors de la classe, donc a regarder pour toutes les fonctions friend : ok voir pour les autres
         */
        template <class TFirstType, class TSecondType, template <class> class TOtherStorageType,/*template <class>*/ class TOtherOwnershipPolicy> friend void swap(SmartLink<TFirstType, TOtherStorageType, TOtherOwnershipPolicy> &first, SmartLink<TSecondType, TOtherStorageType, TOtherOwnershipPolicy> &second) noexcept;
        
        /*!
         * Swap friend function used when swap same types of SmartLink, needed to avoid ambiguous methods call between the template swap and std::swap (due to template parameter).
         * @param first First SmartLink with TSameType to swap
         * @param second Second SmartLink with TSameType to swap
         */
        template <class TSameType, template <class> class TOtherStorageType, /*template <class>*/ class TOtherOwnershipPolicy> friend void swap(SmartLink<TSameType, TOtherStorageType, TOtherOwnershipPolicy> &first, SmartLink<TSameType, TOtherStorageType, TOtherOwnershipPolicy> &second) noexcept;//TODO: pourquoi je n'en ai besoin que dans MemoryHandle ?
        
    private:
        /*! Allows access of private members from other SmartLink class (with other template parameter) */
        template<class TOtherType, template <class> class TOtherStorageType, /*template <class>*/ class TOtherOwnershipPolicy> friend class SmartLink;//TODO: comment faire pour forcer a avoir le meme policy?
        
        /*! Allows access of protected members from downcastTo function */
        template <class TSubType, class TOtherType, template <class> class TOtherStorageType, /*template <class>*/ class TOtherOwnershipPolicy> friend SmartLink<TSubType, TOtherStorageType, TOtherOwnershipPolicy> downcastTo(SmartLink<TOtherType, TOtherStorageType, TOtherOwnershipPolicy> &&smartLink);
        
        /*!
         * Swap method (to avoid duplicating code between swap friend functions).
         * Declared as method and not friend function to be able to hide it with private.
         * @param SmartLink SmartLink with TSubType to swap with
         */
        template <class TSubType> void swapWith(SmartLink<TSubType, TStorageType, TOwnershipPolicy> &smartLink);
    };
    
    /*!
     * Downcast a SmartLink to a subtype, the downcasted smartlink will be reset.
     * @param TSubType Type that downcasted SmartLink will have
     * @param smartLink SmartLink to downcast
     * @return The downcasted SmartLink
     */
    template <class TSubType, class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TSubType, TStorageType, TOwnershipPolicy> downcastTo(SmartLink<TType, TStorageType, TOwnershipPolicy> &&smartLink) {
        // Check types for downcast (need to use type instead TType because link is maybe an upcasted version of another link)
        //assert(_memoryHandle.getType() == TypeManager::getTypeIDForType<TSubType>());//TODO: pas bon, il faut checker qu'on peut convertir mais au runtime, car ici avec ce test si on a 3 niveau de hierarchie et qu'on a un vrai type de SubChild et que le type est Parent et qu'on veut downcaster en Child, ca ne marchera pas alors qu'on pourrai vu que Child est un parent de SubChild !!! avec dynamic_cast mais ca bouffe des ressources : on sait peut etre le faire avec une liste statique de tous les types de toutes les classes intermediaires et il faudra checker qu'il y en a au moins une que son type est ok, voir le livre d'andrei alexandrescu pour savoir comment faire OU renommer cette methode revertToTrueType ou encore gérer ca dans les copy/move et assignment (un OU logique dans le static_assert pour voir si on upcast ou si on downcast)
        
        //TODO : voir pour le static_cast des types ici pour autoriser le downcastTo : voir d'apres le downcastTo de MemoryLink
        
        // Create an empty SmartLink with downcasted type
        SmartLink<TSubType, TStorageType, TOwnershipPolicy> downcasted(nullptr);//TODO: remplacer tout ca par un constructor private qui prend un pointeur de SmartLink et qui va faire le meme que le constructor de coercion copy mais sans static_assert (plus optimisé que ca)
        /*
         // Swap storage between smartLink and downcasted
         static_cast<typename SmartLink<TType, TStorageType, TOwnershipPolicy>::ST &>(smartLink).swap(downcasted);
         
         // Swap ownership between smartLink and downcasted
         static_cast<typename SmartLink<TType, TStorageType, TOwnershipPolicy>::OP &>(smartLink).swap(downcasted);*/
        // Swap between smartLink and downcasted
        swap(smartLink, downcasted);//TODO: a voir pq ca marche : il n'y a pas de restriction de type sur le swap (pas de verification si le type est compatible) : c'est bon pour moi mais pour le code client ca permet de faire des choses pas normal, voir si laisser le swap ainsi ou pas
        
        // Return downcasted
        return downcasted;
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink() : ST(nullptr), OP(nullptr) {
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(std::nullptr_t) : SmartLink() {
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(ST const &storage) : ST(storage), OP() {
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(SmartLink const &smartLink) : ST(smartLink), OP(smartLink) {
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(SmartLink &&smartLink) noexcept : ST(std::move(smartLink)), OP(std::move(smartLink)) {
        // Reset ST part of smartLink to set it in null mode
        static_cast<ST &>(smartLink) = nullptr;//TODO: quand je fais un move du SmartLink::ST j'utilise le move par défaut et ca fait un copy des données donc meme s'il n'a plus le ownership, quand on fera un test d'egalité plus bas avec un autre qui contient la meme donnée, le test va dire qu'ils sont egaux, le swap ne se fera pas et ce sera la merdasse, donc voila pq je fais ca ici et dans le template coercion move constructor, voir si on peut simplifier ce systeme
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> template <class TSubType> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(SmartLink<TSubType, TStorageType, TOwnershipPolicy> const &smartLink) : ST(smartLink), OP(smartLink) {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> template <class TSubType> SmartLink<TType, TStorageType, TOwnershipPolicy>::SmartLink(SmartLink<TSubType, TStorageType, TOwnershipPolicy> &&smartLink) noexcept : ST(std::move(smartLink)), OP(std::move(smartLink)) {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        // Reset ST part of smartLink to set it in null mode
        static_cast<typename SmartLink<TSubType, TStorageType, TOwnershipPolicy>::ST &>(smartLink) = nullptr;
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>::~SmartLink() {//TODO: le probleme est qu'on ne peut pas reconvertir un shared en unique quand il ne reste qu'un SmartLink car je n'ai pas la reference sur le dernier SmartLink ici (il faudrait une liste doublement chainée ou une liste simplement chainée circulaire ? : la circulaire est trop lente car il faut parcourir toute la liste pour fermer la liste quand on supprime un element)
        // Release ownership and check if fully released
        if (OP::release()) {
            // Delete data of storage
            MemoryManager::deleteData(*this);
        }
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> SmartLink<TType, TStorageType, TOwnershipPolicy>& SmartLink<TType, TStorageType, TOwnershipPolicy>::operator =(SmartLink smartLink) noexcept {
        // Swap between this and smartLink
        swap(*this, smartLink);
        
        // Return itself
        return *this;
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> template <class TSubType> SmartLink<TType, TStorageType, TOwnershipPolicy>& SmartLink<TType, TStorageType, TOwnershipPolicy>::operator =(SmartLink<TSubType, TStorageType, TOwnershipPolicy> smartLink) noexcept {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        // Swap between this and smartLink
        swap(*this, smartLink);
        
        // Return itself
        return *this;
    }
    /*
    template <class TType, template <class> class TStorageType, /*template <class>/ class TOwnershipPolicy> TType *SmartLink<TType, TStorageType, TOwnershipPolicy>::get() const {
        // Return pointer of object managed by Handle
        return ST::get();//TODO: voir si optimisé (inline)
    }*/
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> template <class TSubType> inline void SmartLink<TType, TStorageType, TOwnershipPolicy>::swapWith(SmartLink<TSubType, TStorageType, TOwnershipPolicy> &smartLink) {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Optimization : Swap only if smart links doesn't point to the same data
        if (*this != smartLink) {
            // Swap storage policy
            //ST::swapWith(smartLink);//TODO: ainsi j'appelle la methode parente, mais peut etre en castant smartLink en StorageType et appeler la methode swap :
            swap(static_cast<ST &>(*this), static_cast<typename SmartLink<TSubType, TStorageType, TOwnershipPolicy>::ST &>(smartLink));
            
            // Swap ownership policy
            OP::swap(smartLink);
        }
    }
    
    template <class TFirstType, class TSecondType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> void swap(SmartLink<TFirstType, TStorageType, TOwnershipPolicy> &first, SmartLink<TSecondType, TStorageType, TOwnershipPolicy> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
    template <class TType, template <class> class TStorageType, /*template <class>*/ class TOwnershipPolicy> void swap(SmartLink<TType, TStorageType, TOwnershipPolicy> &first, SmartLink<TType, TStorageType, TOwnershipPolicy> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
}

#endif /* defined(__Tuple__SmartLink__) */
