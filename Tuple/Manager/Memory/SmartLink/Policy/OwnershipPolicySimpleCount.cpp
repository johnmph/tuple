//
//  OwnershipPolicySimpleCount.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "OwnershipPolicySimpleCount.hpp"


namespace Tuple {
    
    OwnershipPolicySimpleCount::OwnershipPolicySimpleCount() : _count(new int(1)) {
        //TODO: tester si new a foiré
    }
    
    OwnershipPolicySimpleCount::OwnershipPolicySimpleCount(std::nullptr_t) : _count(nullptr) {
    }
    
    OwnershipPolicySimpleCount::OwnershipPolicySimpleCount(OwnershipPolicySimpleCount const &other) : _count(other._count) {
        acquire();
    }
    
    OwnershipPolicySimpleCount::OwnershipPolicySimpleCount(OwnershipPolicySimpleCount &&other) noexcept : _count(std::move(other._count)) {
        transfert(other);
    }
    /*
    template <class TOtherType> OwnershipPolicySimpleCount::OwnershipPolicySimpleCount(OwnershipPolicySimpleCount<TOtherType> const &other) : _count(other._count) {
        acquire();
    }
    
    // Coercion moving constructor
    template <class TOtherType> OwnershipPolicySimpleCount::OwnershipPolicySimpleCount(OwnershipPolicySimpleCount<TOtherType> &&other) noexcept : _count(std::move(other._count)) {
        transfert(other);
    }*/
    
    OwnershipPolicySimpleCount::~OwnershipPolicySimpleCount() {
    }
    
    bool OwnershipPolicySimpleCount::release() {
        // If not in empty mode
        if (_count != nullptr) {
            // Decrement count and check if reaches 0
            if (--(*_count) == 0) {
                // Release count
                delete _count;
                
                // Reset count member
                _count = nullptr;
                
                // Fully released
                return true;
            }
        }
        
        // Not fully released or in empty mode
        return false;
    }
    
    void OwnershipPolicySimpleCount::swap(OwnershipPolicySimpleCount &other) noexcept {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Swap count
        swap(_count, other._count);
    }
    
    void OwnershipPolicySimpleCount::acquire() {
        // If it is in empty mode
        if (_count == nullptr) {
            // Exit
            return;
        }
        
        // Increment count
        ++(*_count);
    }
    
    void OwnershipPolicySimpleCount::transfert(OwnershipPolicySimpleCount &other) {
        // Reset count of other
        other._count = nullptr;
    }
    
}
