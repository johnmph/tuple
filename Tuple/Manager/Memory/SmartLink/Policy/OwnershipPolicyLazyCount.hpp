//
//  OwnershipPolicyLazyCount.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 21/09/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__OwnershipPolicyLazyCount__
#define __Tuple__OwnershipPolicyLazyCount__

#include <iostream>


namespace Tuple {
    
    /*template <class TType>*/ class OwnershipPolicyLazyCount {
    public:
        // Swap
        /*template <class TOtherType>*/ void swap(OwnershipPolicyLazyCount/*<TOtherType>*/ &other) noexcept;//TODO: voir si pas protected aussi
        
    protected:
        // Constructor
        OwnershipPolicyLazyCount();
        
        // Empty constructor
        OwnershipPolicyLazyCount(std::nullptr_t);
        
        // Copy constructor
        OwnershipPolicyLazyCount(OwnershipPolicyLazyCount const &other);
        
        // Move constructor
        OwnershipPolicyLazyCount(OwnershipPolicyLazyCount &&other) noexcept;
        /*
        // Coercion copying constructor
        template <class TOtherType> OwnershipPolicyLazyCount(OwnershipPolicyLazyCount<TOtherType> &other);
        
        // Coercion moving constructor
        template <class TOtherType> OwnershipPolicyLazyCount(OwnershipPolicyLazyCount<TOtherType> &&other) noexcept;*/
        
        // Non virtual protected destructor
        ~OwnershipPolicyLazyCount();
        
        // Release
        bool release();
        
    private:
        /*! Allows access of private members from other OwnershipPolicyLazyCount class (with other template parameter) */
        //template <class TOtherType> friend class OwnershipPolicyLazyCount;
        
        // Acquire
        /*template <class TOtherType>*/ void acquire(OwnershipPolicyLazyCount/*<TOtherType>*/ const &other);
        
        // Transfert
        /*template <class TOtherType> */void transfert(OwnershipPolicyLazyCount/*<TOtherType>*/ &other);
        
        /*! Reference counter */
        mutable int *_count;
    };
    
}

#endif /* defined(__Tuple__OwnershipPolicyLazyCount__) */
