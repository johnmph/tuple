//
//  OwnershipPolicyLazyCount.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "OwnershipPolicyLazyCount.hpp"


namespace Tuple {
    
    void OwnershipPolicyLazyCount::swap(OwnershipPolicyLazyCount &other) noexcept {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Swap count
        swap(_count, other._count);
    }
    
    OwnershipPolicyLazyCount::OwnershipPolicyLazyCount() : _count(reinterpret_cast<int *>(-1)) {
    }
    
    OwnershipPolicyLazyCount::OwnershipPolicyLazyCount(std::nullptr_t) : _count(nullptr) {
    }
    
    OwnershipPolicyLazyCount::OwnershipPolicyLazyCount(OwnershipPolicyLazyCount const &other) : _count(other._count) {
        acquire(other);
    }
    
    OwnershipPolicyLazyCount::OwnershipPolicyLazyCount(OwnershipPolicyLazyCount &&other) noexcept : _count(std::move(other._count)) {
        transfert(other);
    }
    /*
    template <class TOtherType> OwnershipPolicyLazyCount::OwnershipPolicyLazyCount(OwnershipPolicyLazyCount<TOtherType> &other) : _count(other._count) {
        acquire(other);
    }
    
    // Coercion moving constructor
    template <class TOtherType> OwnershipPolicyLazyCount::OwnershipPolicyLazyCount(OwnershipPolicyLazyCount<TOtherType> &&other) : _count(std::move(other._count)) {
        transfert(other);
    }*/
    
    OwnershipPolicyLazyCount::~OwnershipPolicyLazyCount() {
    }
    
    bool OwnershipPolicyLazyCount::release() {
        // If it is in unique mode
        if (_count == reinterpret_cast<int *>(-1)) {
            // Fully released
            return true;
        }
        
        // If it is in shared mode
        if (_count != nullptr) {
            // Decrement count and check if reaches 0
            if (--(*_count) == 0) {
                // Release count
                delete _count;
                
                // Reset count member
                _count = nullptr;
                
                // Fully released
                return true;
            }
        }
        
        // Not fully released
        return false;
    }
    
    void OwnershipPolicyLazyCount::acquire(OwnershipPolicyLazyCount const &other) {
        // If it is in empty mode
        if (other._count == nullptr) {
            // Exit
            return;
        }
        
        // If it is in unique mode
        if (other._count == reinterpret_cast<int *>(-1)) {
            // Create count with value of 2 (one for other and one for this object)
            other._count = new int(2);
            
            // TODO: tester si new a pas foiré
            
            // Set new count
            _count = other._count;
        } else {
            // Increment count
            ++(*_count);
        }
    }
    
    // Transfert
    void OwnershipPolicyLazyCount::transfert(OwnershipPolicyLazyCount &other) {
        // Reset count of other
        other._count = nullptr;
    }
    
}
