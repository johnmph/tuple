//
//  OwnershipPolicySimpleCount.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 21/09/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__OwnershipPolicySimpleCount__
#define __Tuple__OwnershipPolicySimpleCount__

#include <iostream>


namespace Tuple {
    
    /*template <class TType> */class OwnershipPolicySimpleCount {
    public:
        // Swap
        /*template <class TOtherType>*/ void swap(OwnershipPolicySimpleCount/*<TOtherType>*/ &other) noexcept;//TODO: voir si pas protected aussi
        
    protected:
        // Constructor
        OwnershipPolicySimpleCount();
        
        // Empty constructor
        OwnershipPolicySimpleCount(std::nullptr_t);
        
        // Copy constructor
        OwnershipPolicySimpleCount(OwnershipPolicySimpleCount const &other);
        
        // Move constructor
        OwnershipPolicySimpleCount(OwnershipPolicySimpleCount &&other) noexcept;
        /*
        // Coercion copying constructor
        template <class TOtherType> OwnershipPolicySimpleCount(OwnershipPolicySimpleCount<TOtherType> const &other);
        
        // Coercion moving constructor
        template <class TOtherType> OwnershipPolicySimpleCount(OwnershipPolicySimpleCount<TOtherType> &&other) noexcept;*/
        
        // Non virtual protected destructor
        ~OwnershipPolicySimpleCount();
        
        // Release
        bool release();
        
    private:
        /*! Allows access of private members from other OwnershipPolicySimpleCount class (with other template parameter) */
        //template <class TOtherType> friend class OwnershipPolicySimpleCount;
        
        // Acquire
        void acquire();
        
        // Transfert
        /*template <class TOtherType> */void transfert(OwnershipPolicySimpleCount/*<TOtherType>*/ &other);
        
        /*! Reference counter */
        int *_count;
        
    };
    
}

#endif /* defined(__Tuple__OwnershipPolicySimpleCount__) */
