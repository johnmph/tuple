//
//  OwnershipPolicyLinkedList.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "OwnershipPolicyLinkedList.hpp"


namespace Tuple {
    
    void OwnershipPolicyLinkedList::swap(OwnershipPolicyLinkedList &other) noexcept {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Swap linked list
        swap(_previous, other._previous);
        swap(_next, other._next);
        
        // Correct swap for this
        correctSwap(_previous, _previous->_next, &other);
        correctSwap(_next, _next->_previous, &other);
        
        // Correct swap for other
        other.correctSwap(other._previous, other._previous->_next, this);
        other.correctSwap(other._next, other._next->_previous, this);
    }
    
    OwnershipPolicyLinkedList::OwnershipPolicyLinkedList() : _previous(this), _next(this) {
    }
    
    OwnershipPolicyLinkedList::OwnershipPolicyLinkedList(std::nullptr_t) : _previous(nullptr), _next(nullptr) {
    }
    
    OwnershipPolicyLinkedList::OwnershipPolicyLinkedList(OwnershipPolicyLinkedList const &other) {
        acquire(other);
    }
    
    OwnershipPolicyLinkedList::OwnershipPolicyLinkedList(OwnershipPolicyLinkedList &&other) noexcept : _previous(std::move(other._previous)), _next(std::move(other._next)) {
        transfert(other);
    }
    /*
    template <class TOtherType> OwnershipPolicyLinkedList(OwnershipPolicyLinkedList<TOtherType> &other) {
        acquire(other);
    }
    
    template <class TOtherType> OwnershipPolicyLinkedList(OwnershipPolicyLinkedList<TOtherType> &&other) noexcept : _previous(std::move(other._previous)), _next(std::move(other._next)) {
        transfert(other);
    }*/
    
    OwnershipPolicyLinkedList::~OwnershipPolicyLinkedList() {
    }
    
    bool OwnershipPolicyLinkedList::release() {
        // If it is not in empty mode
        if (!isEmpty()) {
            // Check if it is the last item in the list
            if (isUnique()) {
                // Reset linked list member
                //_previous = nullptr;
                //_next = nullptr;
                
                // Fully released
                return true;
            } else {
                // Remove itself from list
                _previous->_next = _next;
                _next->_previous = _previous;
            }
        }
        
        // Not fully released
        return false;
    }
    
    bool OwnershipPolicyLinkedList::isEmpty() const {
        // No need to check _next also
        return _previous == nullptr;
    }
    
    bool OwnershipPolicyLinkedList::isUnique() const {
        // No need to check _next also
        return _previous == this;
    }
    
    void OwnershipPolicyLinkedList::acquire(OwnershipPolicyLinkedList const &other) {
        // If it is in empty mode
        if (other.isEmpty()) {
            // Reset linked list
            _previous = nullptr;
            _next = nullptr;
            
            // Exit
            return;
        }
        
        // If it is in unique mode
        if (other.isUnique()) {
            // Correct link between this and other (Circular reference)
            other._previous = this;
            _next = &other;
        } else {
            // Correct link between this and object next to other
            _next = other._next;
            other._next->_previous = this;
        }
        
        // Correct link between other and this
        _previous = &other;
        other._next = this;
    }
    
    void OwnershipPolicyLinkedList::transfert(OwnershipPolicyLinkedList &other) {
        // Correct linked list
        correctSwap(_previous, _previous->_next, &other);
        correctSwap(_next, _next->_previous, &other);
        
        // Reset linked list of other
        other._previous = nullptr;
        other._next = nullptr;
    }
    
    void OwnershipPolicyLinkedList::correctSwap(OwnershipPolicyLinkedList const *&link, OwnershipPolicyLinkedList const *&followingLink, OwnershipPolicyLinkedList *other) {
        // If it is in unique mode
        if (link == other) {
            // Correct link to set in unique mode
            link = this;
            // If it is in circular reference
        } else if (link == this) {
            // Correct link to set in circular reference
            link = other;
            // If it is in normal mode (not in empty mode)
        } else if (link != nullptr) {
            // Correct following link
            followingLink = this;
        }
    }
    
}
