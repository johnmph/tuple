//
//  OwnershipPolicyNoOwnership.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 21/09/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__OwnershipPolicyNoOwnership__
#define __Tuple__OwnershipPolicyNoOwnership__

#include <iostream>


namespace Tuple {
    
    // Policies for SmartLink TODO: voir pour les policies ou je ne declare pas les constructors si pas de probleme quand constructors appelés dans les constructors de smartLink (normalement pas de prob pour copy/move constructors car il y a un copy constructor créé par défaut mais pour le empty constructor et nullptr constructor ??) : A TESTER -> Il faut les mettre (au moins le empty mais en mettant le empty on desactive le constructor par defaut donc il faut le mettre aussi, le copy/move constructor n'est pas obligatoire car celui par défaut est créé) : ATTENTION avec cette policy les tests foirent, a verifier si normal !!!
    //TODO: voir car on n'interdit pas de copier alors qu'on devrait car on ne peut pas avoir 2 unique vers le meme objet mais le probleme est qu'on va avoir une interface differente pour un SmartLink
    /*template <class TType> */class OwnershipPolicyNoOwnership {
    public:
        // Swap
        /*template <class TOtherType>*/ void swap(OwnershipPolicyNoOwnership/*<TOtherType>*/ &other) noexcept;//TODO: voir si pas protected aussi
        
    protected:
        // Constructor
        OwnershipPolicyNoOwnership();
        
        // Empty constructor
        OwnershipPolicyNoOwnership(std::nullptr_t);
        
        // Non virtual protected destructor
        ~OwnershipPolicyNoOwnership();
        
        // Release
        static bool release();
    };
    
}

#endif /* defined(__Tuple__OwnershipPolicyNoOwnership__) */
