//
//  OwnershipPolicyNoOwnership.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 19/10/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "OwnershipPolicyNoOwnership.hpp"


namespace Tuple {
    
    void OwnershipPolicyNoOwnership::swap(OwnershipPolicyNoOwnership &other) noexcept {
    }
    
    OwnershipPolicyNoOwnership::OwnershipPolicyNoOwnership() {
    }
    
    OwnershipPolicyNoOwnership::OwnershipPolicyNoOwnership(std::nullptr_t) {
    }
    
    OwnershipPolicyNoOwnership::~OwnershipPolicyNoOwnership() {
    }
    
    bool OwnershipPolicyNoOwnership::release() {
        // Fully released
        return true;
    }
    
}
