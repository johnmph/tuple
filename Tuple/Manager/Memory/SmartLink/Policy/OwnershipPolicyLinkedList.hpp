//
//  OwnershipPolicyLinkedList.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 21/09/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__OwnershipPolicyLinkedList__
#define __Tuple__OwnershipPolicyLinkedList__

#include <iostream>


namespace Tuple {
    
    /*template <class TType>*/ class OwnershipPolicyLinkedList {
    public:
        // Swap
        /*template <class TOtherType>*/ void swap(OwnershipPolicyLinkedList/*<TOtherType>*/ &other) noexcept;//TODO: voir si pas protected aussi
        
    protected:
        // Constructor
        OwnershipPolicyLinkedList();
        
        // Empty constructor
        OwnershipPolicyLinkedList(std::nullptr_t);
        
        // Copy constructor
        OwnershipPolicyLinkedList(OwnershipPolicyLinkedList const &other);
        
        // Move constructor
        OwnershipPolicyLinkedList(OwnershipPolicyLinkedList &&other) noexcept;
        /*
        // Coercion copying constructor
        template <class TOtherType> OwnershipPolicyLinkedList(OwnershipPolicyLinkedList<TOtherType> &other);
        
        // Coercion moving constructor
        template <class TOtherType> OwnershipPolicyLinkedList(OwnershipPolicyLinkedList<TOtherType> &&other) noexcept;*/
        
        // Non virtual protected destructor
        ~OwnershipPolicyLinkedList();
        
        // Release
        bool release();
        
    private:
        /*! Allows access of private members from other OwnershipPolicyLinkedList class (with other template parameter) */
        //template <class TOtherType> friend class OwnershipPolicyLinkedList;
        
        // Check if it is in empty mode
        bool isEmpty() const;
        
        // Check if it is in unique mode
        bool isUnique() const;
        
        // Acquire
        /*template <class TOtherType>*/ void acquire(OwnershipPolicyLinkedList/*<TOtherType>*/ const &other);
        
        // Transfert
        /*template <class TOtherType>*/ void transfert(OwnershipPolicyLinkedList/*<TOtherType>*/ &other);
        
        void correctSwap(OwnershipPolicyLinkedList const *&link, OwnershipPolicyLinkedList const *&followingLink, OwnershipPolicyLinkedList *other);
        
        /*! Linked list */
        mutable const OwnershipPolicyLinkedList *_previous, *_next;
    };
    
}

#endif /* defined(__Tuple__OwnershipPolicyLinkedList__) */
