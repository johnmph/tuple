//
//  MemoryManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "MemoryManager.hpp"


namespace Tuple {
    
    FixedMemoryAllocator *MemoryManager::getFixedMemoryAllocatorForClassType(TypeManager::TypeID classType) const {
        // Get memory allocator from _fixedMemoryAllocators
        return getMemoryAllocatorForClassType(classType, _fixedMemoryAllocators);
    }
    
    MoveableMemoryAllocator *MemoryManager::getMoveableMemoryAllocatorForClassType(TypeManager::TypeID classType) const {
        // Get memory allocator from _moveableMemoryAllocators
        return getMemoryAllocatorForClassType(classType, _moveableMemoryAllocators);
    }
    
    /*RawDataHandle MemoryManager::newRawDataWithTag(std::string const &tag, size_t size) {
        return RawDataHandle(nullptr, InvalidHandle);//TODO: a changer
    }*/
    
}
