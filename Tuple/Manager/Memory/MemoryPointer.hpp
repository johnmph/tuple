//
//  MemoryPointer.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 24/08/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__MemoryPointer__
#define __Tuple__MemoryPointer__

#include "FixedMemoryAllocator.hpp"
#include "MemoryLink.hpp"


namespace Tuple {
    
    template <class TType, class TDerived> class MemoryPointerTrait {
    public:
        /*!
         * Get the pointer managed by the pointer.
         * @return Pointer to data pointed by Pointer
         */
        TType *get() const;
        
        void freeMemory();//todo:surement private avec friend sur MemoryManager
        
        /*!
         * Type of data getter, it's the true type of the data,
         * even if it's a POD and if it's a upcasted MemoryPointer.
         * @return Type of data managed by this MemoryPointer
         */
        //TypeManager::TypeID getType() const;
        
    protected:
        /*! Declare type of MemoryAllocator and Link */
        using TMemoryAllocator = FixedMemoryAllocator;
        using TLink = void *;
        
        /*! Define constants for members initialization in derived class */
        // TODO: voir ca pour les constantes : http://codewrangler.home.comcast.net/~codewrangler/tech_info/cpp_const.html
        //static constexpr TMemoryAllocator *MemoryAllocatorNull = nullptr;
        //static constexpr TLink LinkNull = nullptr;
        static constexpr TMemoryAllocator *MemoryAllocatorNull() { return nullptr; };
        static constexpr TLink LinkNull() { return nullptr; };
        
        /*! Protected non-virtual destructor to avoid create and delete this class, we must pass by its child classes */
        /*! @todo voir si suffisant de faire ca pour ne pas instancier la classe de base (celle ci), voir le paragraphe 1.7 du pdf d'andrei alexandrescu */
        ~MemoryPointerTrait() = default;
    };
    
    /*!
     * Equality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is equal to rhs object else false
     */
    template <class TType, class TDerived, class TOtherType, class TOtherDerived> inline bool operator ==(MemoryPointerTrait<TType, TDerived> const &lhs, MemoryPointerTrait<TOtherType, TOtherDerived> const &rhs) {
        // Check if they points to the same memory
        return static_cast<TDerived const *>(&lhs)->_link == static_cast<TOtherDerived const *>(&rhs)->_link;
    }
    
    template <class TType, class TDerived> inline bool operator ==(MemoryPointerTrait<TType, TDerived> const &lhs, std::nullptr_t rhs) {
        // MemoryLink is nullptr if its memory allocator is nullptr or if its link is nullptr
        //return (lhs.getMemoryAllocator() == nullptr) || (lhs.get() == nullptr);//TODO: a voir pour le getMemoryAllocator == nullptr
        return (static_cast<TDerived const *>(&lhs)->_memoryAllocator == nullptr) || (static_cast<TDerived const *>(&lhs)->_link == nullptr);//TODO: a voir pour le getMemoryAllocator == nullptr
    }
    
    
    template <class TType, class TDerived> TType *MemoryPointerTrait<TType, TDerived>::get() const {
        // Return casted link
        return static_cast<TType *>(static_cast<TDerived const *>(this)->_link);
    }
    /*
    template <class TType, class TDerived> TypeManager::TypeID MemoryPointerTrait<TType, TDerived>::getType() const {
        //TODO: assert pour voir si *this != nullptr ?
        // Get type from memory allocator
        return static_cast<TDerived const *>(this)->_memoryAllocator->getTypeForPointer(static_cast<TDerived const *>(this)->_link);
    }*/
    
    template <class TType, class TDerived> void MemoryPointerTrait<TType, TDerived>::freeMemory() {
        // Free link fixed memory from its memory allocator
        static_cast<TDerived *>(this)->_memoryAllocator->freeFixedMemory(static_cast<TDerived *>(this)->_link);
        
        // TODO: reset le memory allocator et le link ?
    }
    
    
    // Define MemoryPointer
    template <typename TType> using MemoryPointer = MemoryLink<TType, MemoryPointerTrait>;//TODO: pq j'ai mis typename et pas class ? et pareil pour le MemoryHandle
    
}

#endif /* defined(__Tuple__MemoryPointer__) */
