//
//  MemoryMixPointerHandle.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 24/08/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__MemoryMixPointerHandle__
#define __Tuple__MemoryMixPointerHandle__

#include "MemoryPointer.hpp"
#include "MemoryHandle.hpp"


namespace Tuple {
    
    // Un type unifié pour pouvoir avoir au runtime des pointeurs et des handles, j'utilise un union + type et pas une hierarchie (TestMixBase qui aurait 2 classes filles TestMixHandle et TestMixPointer et qui overriderait la methode virtuelle get pour retourner l'adresse de la mémoire) car il faudrait stocker les TestMixBase sous forme de pointeur et donc les allouer dynamiquement ce qui serait une grosse perte de performance !!!
    // TODO: trouver un moyen de factoriser avec MemoryLink
    template <class TType> class MemoryMixPointerHandle {
    public:
        MemoryMixPointerHandle(MemoryPointer<TType> const &pointer);
        MemoryMixPointerHandle(MemoryHandle<TType> const &handle);
        
        /*!
         * Empty constructor, to allow to reset MemoryMixPointerHandle.
         * @todo explicit et aussi pour les copy/move constructors ?
         */
        MemoryMixPointerHandle(std::nullptr_t);//TODO: tout mettre en dehors TODO: faut il un mode null ici ? car si je passe par le constructor avec pointer ou handle et que je lui passe un MemoryPointer null ou un MemoryHandle null ? + est ce qu'un null == mixpointerhandle avec pointer null ou handle null ? et est ce qu'un mixpointerhandle avec pointeur null == mixpointerhandle avec handle null ?
        
        /*!
         * Copy constructor (default is ok).
         * @param memoryMixPointerHandle MemoryMixPointerHandle to copy
         */
        MemoryMixPointerHandle(MemoryMixPointerHandle const &memoryMixPointerHandle) = default;
        
        /*!
         * Move constructor (default is ok).
         * @param memoryMixPointerHandle MemoryMixPointerHandle to move
         */
        MemoryMixPointerHandle(MemoryMixPointerHandle &&memoryMixPointerHandle) = default;
        
        /*!
         * Coercion copying constructor, used for copying and converting a MemoryMixPointerHandle if types are convertible.
         * @param memoryMixPointerHandle Another MemoryMixPointerHandle with TSubType to convert to MemoryMixPointerHandle with TType type
         */
        template <class TSubType> MemoryMixPointerHandle(MemoryMixPointerHandle<TSubType> const &memoryMixPointerHandle);
        
        /*!
         * Coercion moving constructor, used for moving and converting a MemoryMixPointerHandle if types are convertible.
         * @param memoryMixPointerHandle Another MemoryMixPointerHandle with TSubType to convert to MemoryMixPointerHandle with TType type
         */
        template <class TSubType> MemoryMixPointerHandle(MemoryMixPointerHandle<TSubType> &&memoryMixPointerHandle) noexcept;
        
        /*!
         * Copy / move assignment using copy and swap technic.
         * @param memoryMixPointerHandle MemoryMixPointerHandle to copy / move
         * @return This object
         */
        MemoryMixPointerHandle& operator =(MemoryMixPointerHandle memoryMixPointerHandle) noexcept;
        
        /*!
         * Coercion copying / moving assignment using copy and swap technic, used for copying / moving and converting a MemoryMixPointerHandle if types are convertible.
         * @param memoryMixPointerHandle Another MemoryMixPointerHandle with TSubType to convert to MemoryMixPointerHandle with TType type
         * @return This object
         * @todo attention avec le copy and swap car dans : http://en.cppreference.com/w/cpp/language/move_operator il est marqué que ca fait un appel au move constructor supplémentaire mais on a l'exception safety garanty mais ici normalement je n'en ai pas besoin car on ne fait que copier des pointeurs et des scalaires
         */
        template <class TSubType> MemoryMixPointerHandle<TType>& operator =(MemoryMixPointerHandle<TSubType> memoryMixPointerHandle) noexcept;
        
        /*!
         * Get the pointer managed by the handle.
         * @return Pointer to data pointed by Handle
         */
        TType *get() const;
        
        /*!
         * * operator overload, enabled only for types that can have a pointer to dereference (All types but void).
         * @return Reference of Object of TType class pointed by Handle
         */
        template <class TTypeCheck = TType> typename std::enable_if<!std::is_void<TTypeCheck>::value, TType>::type &operator *() const;
        
        
        bool isPointer() const { return _type == POINTER_TYPE; };
        bool isHandle() const { return _type == HANDLE_TYPE; };
        
        TType *operator ->() const;
        
        void freeMemory();//todo:surement private avec friend sur MemoryManager
        
        /*!
         * Type of data getter, it's the true type of the data,
         * even if it's a POD and if it's a upcasted MemoryPointer.
         * @return Type of data managed by this MemoryPointer
         */
        //TypeManager::TypeID getType() const;
        
        /*!
         * Swap friend function used by coercion copying / moving assignment.
         * @param first First MemoryHandle with TFirstType to swap
         * @param second Second MemoryHandle with TSecondType to swap
         * @todo les fonctions friend peuvent etre definies en dehors de la classe, donc a regarder pour toutes les fonctions friend : ok voir pour les autres
         */
        template <class TFirstType, class TSecondType> friend void swap(MemoryMixPointerHandle<TFirstType> &first, MemoryMixPointerHandle<TSecondType> &second) noexcept;
        
        /*!
         * Swap friend function used when swap same types of MemoryHandle, needed to avoid ambiguous methods call between the template swap and std::swap (due to template parameter).
         * @param first First MemoryHandle with TSameType to swap
         * @param second Second MemoryHandle with TSameType to swap
         */
        template <class TSameType> friend void swap(MemoryMixPointerHandle<TSameType> &first, MemoryMixPointerHandle<TSameType> &second) noexcept;
        
    private:
        /*! Allows access of private members from other MemoryMixPointerHandle class (with other template parameter) */
        template<class TOtherType> friend class MemoryMixPointerHandle;
        
        /*! Allows access of private members from downcastTo function */
        template <class TSubType, class TOtherType> friend MemoryMixPointerHandle<TSubType> downcastTo(MemoryMixPointerHandle<TOtherType> &&memoryMixPointerHandle);
        
        /*! Allows access of protected methods from operator functions */
        template <class TType2, class TOtherType> friend bool operator ==(MemoryMixPointerHandle<TType2> const &lhs, MemoryMixPointerHandle<TOtherType> const &rhs);
        template <class TType2> friend bool operator ==(MemoryMixPointerHandle<TType2> const &lhs, std::nullptr_t rhs);
        
        /*!
         * Swap method (to avoid duplicating code between swap friend functions).
         * Declared as method and not friend function to be able to hide it with private.
         * @param memoryHandle MemoryHandle with TSubType to swap with
         */
        template <class TSubType> void swapWith(MemoryMixPointerHandle<TSubType> &memoryMixPointerHandle);
        
        enum {
            POINTER_TYPE,
            HANDLE_TYPE,
        };
        
        union {
            MemoryPointer<TType> _pointer;
            MemoryHandle<TType> _handle;
        };
        int _type;
    };
    
    template <class TSubType, class TType> MemoryMixPointerHandle<TSubType> downcastTo(MemoryMixPointerHandle<TType> &&memoryMixPointerHandle) {
        // Check types for downcast (need to use type instead TType because handle is maybe an upcasted version of another handle)
        //assert(getType() == TypeManager::getTypeIDForType<TSubType>());//TODO: pas bon, il faut checker qu'on peut convertir mais au runtime, car ici avec ce test si on a 3 niveau de hierarchie et qu'on a un vrai type de SubChild et que le type est Parent et qu'on veut downcaster en Child, ca ne marchera pas alors qu'on pourrai vu que Child est un parent de SubChild !!! avec dynamic_cast mais ca bouffe des ressources : on sait peut etre le faire avec une liste statique de tous les types de toutes les classes intermediaires et il faudra checker qu'il y en a au moins une que son type est ok, voir le livre d'andrei alexandrescu pour savoir comment faire
        
        // Check types for downcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");// TODO: normalement pas besoin car ca va etre testé via l'appel ci dessous de soit le downcastTo du pointer ou du handle
        
        // Copy / Convert without check
        return (memoryMixPointerHandle.isPointer()) ? MemoryMixPointerHandle<TSubType>(downcastTo<TSubType>(std::move(memoryMixPointerHandle._pointer))) : MemoryMixPointerHandle<TSubType>(downcastTo<TSubType>(std::move(memoryMixPointerHandle._handle)));
    }
    
    /*!
     * Equality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is equal to rhs object else false
     */
    template <class TType, class TOtherType> inline bool operator ==(MemoryMixPointerHandle<TType> const &lhs, MemoryMixPointerHandle<TOtherType> const &rhs) {
        // If both are pointers
        if (lhs.isPointer() && rhs.isPointer()) {
            // Test equality on MemoryPointer
            return lhs._pointer == rhs._pointer;
        }
        
        // If both are handles
        if (lhs.isHandle() && rhs.isHandle()) {
            // Test equality on MemoryHandle
            return lhs._handle == rhs._handle;
        }
        
        // Check if they are both nullptr
        return ((lhs == nullptr) && (rhs == nullptr));
    }
    
    template <class TType> inline bool operator ==(MemoryMixPointerHandle<TType> const &lhs, std::nullptr_t rhs) {
        // Check if lhs is null
        return (lhs.isPointer()) ? (lhs._pointer == nullptr) : (lhs._handle == nullptr);
    }
    
    template <class TType> inline bool operator ==(std::nullptr_t lhs, MemoryMixPointerHandle<TType> const &rhs) {
        // Check equality with above function by inverting parameters
        return rhs == lhs;
    }
    
    /*!
     * Inequality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is not equal to rhs object else false
     */
    template <class TType, class TOtherType> inline bool operator !=(MemoryMixPointerHandle<TType> const &lhs, MemoryMixPointerHandle<TOtherType> const &rhs) {
        // Inequality is inverse of equality
        return !(lhs == rhs);
    }
    
    template <class TType> inline bool operator !=(MemoryMixPointerHandle<TType> const &lhs, std::nullptr_t rhs) {
        // Inequality is inverse of equality
        return !(lhs == rhs);
    }
    
    template <class TType> inline bool operator !=(std::nullptr_t lhs, MemoryMixPointerHandle<TType> const &rhs) {
        // Check inequality with above function by inverting parameters
        return rhs != lhs;
    }
    
    
    template <class TType> MemoryMixPointerHandle<TType>::MemoryMixPointerHandle(std::nullptr_t) : _type(POINTER_TYPE), _pointer(nullptr) {//TODO: tout mettre en dehors TODO: faut il un mode null ici ? car si je passe par le constructor avec pointer ou handle et que je lui passe un MemoryPointer null ou un MemoryHandle null ? + est ce qu'un null == mixpointerhandle avec pointer null ou handle null ? et est ce qu'un mixpointerhandle avec pointeur null == mixpointerhandle avec handle null ?
    }
    
    template <class TType> MemoryMixPointerHandle<TType>::MemoryMixPointerHandle(MemoryPointer<TType> const &pointer) : _type(POINTER_TYPE), _pointer(pointer) {
    }
    
    template <class TType> MemoryMixPointerHandle<TType>::MemoryMixPointerHandle(MemoryHandle<TType> const &handle) : _type(HANDLE_TYPE), _handle(handle) {
    }
    
    template <class TType> template <class TSubType> MemoryMixPointerHandle<TType>::MemoryMixPointerHandle(MemoryMixPointerHandle<TSubType> const &memoryMixPointerHandle) : _type(memoryMixPointerHandle._type) {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        if (memoryMixPointerHandle.isPointer()) {
            _type = POINTER_TYPE;
            _pointer = memoryMixPointerHandle._pointer;
        } else {
            _type = HANDLE_TYPE;
            _handle = memoryMixPointerHandle._handle;
        }
    }
    
    template <class TType> template <class TSubType> MemoryMixPointerHandle<TType>::MemoryMixPointerHandle(MemoryMixPointerHandle<TSubType> &&memoryMixPointerHandle) noexcept : _type(std::move(memoryMixPointerHandle._type)) {//TODO: a cause de ca il faudra surement checker a chaque fois qu'on utilise memoryAllocator dans les DataHandle / RawDataHandle et MemoryHandle pour eviter d'appeler qq chose sur nullptr + SI on move un DataHandle / MemoryHandle / ..., il est toujours valide si on essaie de faire l'operator () ou -> car mover le Handle ne fait que copier les valeurs scalaires, donc peut etre checker si _count != nullptr pour renvoyer le pointer
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        if (memoryMixPointerHandle.isPointer()) {
            _type = POINTER_TYPE;
            _pointer = std::move(memoryMixPointerHandle._pointer);
        } else {
            _type = HANDLE_TYPE;
            _handle = std::move(memoryMixPointerHandle._handle);
        }
    }
    
    template <class TType> MemoryMixPointerHandle<TType>& MemoryMixPointerHandle<TType>::operator =(MemoryMixPointerHandle memoryMixPointerHandle) noexcept {
        // Swap between this and memoryMixPointerHandle
        swap(*this, memoryMixPointerHandle);
        
        // Return itself
        return *this;
    }
    
    template <class TType> template <class TSubType> MemoryMixPointerHandle<TType>& MemoryMixPointerHandle<TType>::operator =(MemoryMixPointerHandle<TSubType> memoryMixPointerHandle) noexcept {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        // Swap between this and memoryMixPointerHandle
        swap(*this, memoryMixPointerHandle);
        
        // Return itself
        return *this;
    }
    
    template <class TType> TType *MemoryMixPointerHandle<TType>::get() const {
        //TODO: assert pour voir si *this != nullptr ?
        
        return (isPointer()) ? _pointer.get() : _handle.get();
    }
    
    template <class TType> template <class TTypeCheck> typename std::enable_if<!std::is_void<TTypeCheck>::value, TType>::type &MemoryMixPointerHandle<TType>::operator *() const {
        // Return reference to object pointed by Handle
        return *get();//TODO: voir si optimisé (inline)
    }
    
    template <class TType> TType *MemoryMixPointerHandle<TType>::operator ->() const {
        // Return pointer of object managed by Handle
        return get();//TODO: voir si optimisé (inline)
    }
    
    template <class TType> void MemoryMixPointerHandle<TType>::freeMemory() {
        if (isPointer()) {
            _pointer.freeMemory();
        } else {
            _handle.freeMemory();
        }
        // TODO: reset le memory allocator et le handle ?
    }
    /*
    template <class TType> TypeManager::TypeID MemoryMixPointerHandle<TType>::getType() const {
        //TODO: assert pour voir si *this != nullptr ?
        return (isPointer()) ? _pointer.getType() : _handle.getType();
    }*/
    
    template <class TType> template <class TSubType> inline void MemoryMixPointerHandle<TType>::swapWith(MemoryMixPointerHandle<TSubType> &memoryMixPointerHandle) {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Swap members
        swap(_type, memoryMixPointerHandle._type);
        
        if (isPointer()) {//TODO: pas bon, on doit swaper la plus grande données des 2 avec une verif sizeof
            swap(_pointer, memoryMixPointerHandle._pointer);
        } else {
            swap(_handle, memoryMixPointerHandle._handle);
        }
    }
    
    template <class TFirstType, class TSecondType> void swap(MemoryMixPointerHandle<TFirstType> &first, MemoryMixPointerHandle<TSecondType> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
    template <class TSameType> void swap(MemoryMixPointerHandle<TSameType> &first, MemoryMixPointerHandle<TSameType> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
}

#endif /* defined(__Tuple__MemoryMixPointerHandle__) */
