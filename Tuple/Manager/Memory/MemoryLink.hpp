//
//  MemoryLink.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 24/08/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__MemoryLink__
#define __Tuple__MemoryLink__

#include "TypeManager.hpp"


namespace Tuple {
    
    /*! @todo : je ne pense que ca serve a quelque chose d'avoir les move constructors et assignments car de toutes facons les 2 seules membres qu'on copie sont des types scalaires, donc on doit quand meme les copier, on ne sait pas optimiser en les déplacant (move), la technique des rvalue avec move ne sert que pour des objets qui sont lourd a copier !!!
     */
    
    template <class TType, template <class, class> class TTrait> class MemoryLink : public TTrait<TType, MemoryLink<TType, TTrait>> {
    public:
        /*! Helper shortcuts for types */
        using TBase = TTrait<TType, MemoryLink<TType, TTrait>>;//TODO: les laisser dans public ou les mettre dans private ?? a voir par rapport a loki
        using TMemoryAllocator = typename TBase::TMemoryAllocator;
        using TLink = typename TBase::TLink;
        
        /*!
         * Parametrized constructor.
         * @param memoryAllocator FixedMemoryAllocator which holds the memory pointed by the pointer
         * @param pointer The pointer which points to memory
         * @todo le faire private? avec un friend pour MemoryManager? ou le laisser public et mettre des asserts pour eviter que memoryAllocator soit nullptr et pointer soit nullptr ?
         */
        MemoryLink(TMemoryAllocator *memoryAllocator, TLink link);
        
        /*!
         * Empty constructor, to allow to reset MemoryPointer.
         * @todo explicit et aussi pour les copy/move constructors ?
         */
        MemoryLink(std::nullptr_t);
        
        // TODO: peut etre mettre quand meme les copy / move / assignment par défaut car si je defini par exemple le destructor, ils ne seront plus créés par défaut (je peux soit les définir moi-même, soit les mettre = default si le default fait exactement la meme chose, mais ne pas laisser le compilateur choisir lui meme s'il doit les créer ou non) : ok, faire pareil pour les autres classes si nécessaire
        
        /*!
         * Copy constructor (default is ok).
         * @param memoryHandle MemoryHandle to copy
         */
        MemoryLink(MemoryLink const &memoryLink) = default;
        
        /*!
         * Move constructor (default is ok).
         * @param memoryPointer MemoryPointer to move
         */
        //MemoryLink(MemoryLink &&memoryLink) = default;
        
        /*!
         * Coercion copying constructor, used for copying and converting a MemoryPointer if types are convertible.
         * @param memoryPointer Another MemoryPointer with TSubType to convert to MemoryPointer with TType type
         */
        template <class TSubType> MemoryLink(MemoryLink<TSubType, TTrait> const &memoryLink);
        
        /*!
         * Coercion moving constructor, used for moving and converting a MemoryPointer if types are convertible.
         * @param memoryPointer Another MemoryPointer with TSubType to convert to MemoryPointer with TType type
         */
        //template <class TSubType> MemoryLink(MemoryLink<TSubType, TTrait> &&memoryLink) noexcept;
        
        /*!
         * Copy / move assignment using copy and swap technic.
         * @param memoryPointer MemoryPointer to copy / move
         * @return This object
         */
        MemoryLink& operator =(MemoryLink memoryLink) noexcept;
        
        /*!
         * Coercion copying / moving assignment using copy and swap technic, used for copying / moving and converting a MemoryPointer if types are convertible.
         * @param memoryPointer Another MemoryPointer with TSubType to convert to MemoryPointer with TType type
         * @return This object
         * @todo attention avec le copy and swap car dans : http://en.cppreference.com/w/cpp/language/move_operator il est marqué que ca fait un appel au move constructor supplémentaire mais on a l'exception safety garanty mais ici normalement je n'en ai pas besoin car on ne fait que copier des pointeurs et des scalaires
         */
        template <class TSubType> MemoryLink<TType, TTrait>& operator =(MemoryLink<TSubType, TTrait> memoryLink) noexcept;
        
        /*!
         * * operator overload, enabled only for types that can have a pointer to dereference (All types but void).
         * @return Reference of Object of TType class pointed by Pointer
         */
        template <class TTypeCheck = TType> typename std::enable_if<!std::is_void<TTypeCheck>::value, TType>::type &operator *() const;
        
        /*!
         * -> operator overload.
         * @return Pointer to object of TType class pointed by Pointer
         * @todo Peut etre restreindre cette methode aux types non scalaires ? (classes et POD(struct) ?) car on ne peut pas faire -> sur un pointeur d'int par exemple
         */
        TType *operator ->() const;
        
        /*!
         * Memory allocator getter.
         * @return Memory allocator which owns data managed by this MemoryHandle
         * @todo peut etre private avec un friend pour MemoryManager ?
         */
        //TMemoryAllocator *getMemoryAllocator() const;//TODO: je vais devoir retirer cette methode car je ne serai pas l'avoir dans MemoryMixPointerHandle car je peux retourner soit un Fixed soit un Moveable selon le type mais pour faire ca je devrai faire de nouveau un objet proxy dans MemoryMixPointerHandle et ca devient trop bordelique surtout que cette methode ne sert que dans le test d'egalité et je peux faire la fonction friend pour acceder directement au _memoryAllocator
        
        /*!
         * Link getter.
         * @return Link managed by this MemoryHandle
         * @todo peut etre private avec un friend pour MemoryManager ?
         */
        //TLink getLink() const;
        
        /*!
         * Size of data getter.
         * @return Size of data managed by this MemoryPointer
         * @todo normalement pas besoin car le size est necessaire seulement dans les MemoryAllocators, on n'en a pas besoin en externe
         */
        //size_t getSize() const;
        
    private:
        /*! Allows access of private members from CRTP base class */
        /*! @todo Quand on utilise le vrai type plutot que son raccourci, la syntaxe doit etre : friend class TTrait<TType, MemoryLink<TType, TTrait>>; */
        friend TBase;
        
        /*! Allows access of private members from operator free functions */
        template <class TOtherType, class TDerived, class TAnotherType, class TOtherDerived> friend bool operator ==(TTrait<TOtherType, TDerived> const &lhs, TTrait<TAnotherType, TOtherDerived> const &rhs);
        template <class TOtherType, class TDerived> friend bool operator ==(TTrait<TOtherType, TDerived> const &lhs, std::nullptr_t rhs);
        
        /*! Allows access of private members from other MemoryLink class (with other template parameter) */
        template<class TOtherType, template <class, class> class TOtherTrait> friend class MemoryLink;
        
        /*! Allows access of private members from downcastTo function */
        template <class TSubType, class TOtherType, template <class, class> class TOtherTrait> friend MemoryLink<TSubType, TOtherTrait> downcastTo(MemoryLink<TOtherType, TOtherTrait> &&memoryLink);
        
        /*!
         * Swap friend free function used by coercion copying / moving assignment.
         * @param first First MemoryPointer with TFirstType to swap
         * @param second Second MemoryPointer with TSecondType to swap
         * @todo les fonctions friend peuvent etre definies en dehors de la classe, donc a regarder pour toutes les fonctions friend : ok voir pour les autres
         */
        template <class TOtherType, class TAnotherType, template <class, class> class TOtherTrait> friend void swap(MemoryLink<TOtherType, TOtherTrait> &first, MemoryLink<TAnotherType, TOtherTrait> &second) noexcept;
        
        /*!
         * Swap friend free function used when swap same types of MemoryPointer, needed to avoid ambiguous methods call between the template swap and std::swap (due to template parameter).
         * @param first First MemoryPointer with TSameType to swap
         * @param second Second MemoryPointer with TSameType to swap
         */
        template <class TOtherType, template <class, class> class TOtherTrait> friend void swap(MemoryLink<TOtherType, TOtherTrait> &first, MemoryLink<TOtherType, TOtherTrait> &second) noexcept;
        
        /*!
         * Swap method (to avoid duplicating code between swap friend functions).
         * Declared as method and not friend function to be able to hide it with private.
         * @param memoryHandle MemoryHandle with TSubType to swap with
         */
        template <class TSubType> void swapWith(MemoryLink<TSubType, TTrait> &memoryLink);
        
        /*! Memory allocator */
        /*! @todo pointer ou ref ou weak ptr ? */
        TMemoryAllocator *_memoryAllocator;
        
        /*! Link */
        TLink _link;
    };
    
    template <class TSubType, class TType, template <class, class> class TTrait> MemoryLink<TSubType, TTrait> downcastTo(MemoryLink<TType, TTrait> &&memoryLink) {
        // Check types for downcast (need to use type instead TType because handle is maybe an upcasted version of another handle)
        // TODO: l'expression doit etre entre parenthese sinon ca foire a la compilation a cause de l'expansion de la macro assert avec l'expression template
        //assert((TBase::getType()) == TypeManager::getTypeIDForType<TSubType>());//TODO: pas bon, il faut checker qu'on peut convertir mais au runtime, car ici avec ce test si on a 3 niveau de hierarchie et qu'on a un vrai type de SubChild et que le type est Parent et qu'on veut downcaster en Child, ca ne marchera pas alors qu'on pourrai vu que Child est un parent de SubChild !!! avec dynamic_cast mais ca bouffe des ressources : on sait peut etre le faire avec une liste statique de tous les types de toutes les classes intermediaires et il faudra checker qu'il y en a au moins une que son type est ok, voir le livre d'andrei alexandrescu pour savoir comment faire
        
        // Check types for downcast TODO: ca ne veut pas dire que c'est safe de downcaster TType en TSubType car peut etre que l'objet ici n'est pas reellement un TSubType mais un TType ou une classe entre TType et TSubType mais on ne sait pas faire mieux sans sauver le vrai type
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        // Copy / Convert without check
        return MemoryLink<TSubType, TTrait>(memoryLink._memoryAllocator, memoryLink._link);
    }
    
    /*!
     * Equality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is equal to rhs object else false
     */
    template <class TType, template <class, class> class TTrait> inline bool operator ==(std::nullptr_t lhs, MemoryLink<TType, TTrait> const &rhs) {
        // Check equality with above function by inverting parameters
        return rhs == lhs;
    }
    
    /*!
     * Inequality operator overload.
     * @param lhs First object to compare
     * @param rhs Second object to compare
     * @return true if lhs object is not equal to rhs object else false
     */
    template <class TType, class TOtherType, template <class, class> class TTrait> inline bool operator !=(MemoryLink<TType, TTrait> const &lhs, MemoryLink<TOtherType, TTrait> const &rhs) {
        // Inequality is inverse of equality
        return !(lhs == rhs);
    }
    
    template <class TType, template <class, class> class TTrait> inline bool operator !=(MemoryLink<TType, TTrait> const &lhs, std::nullptr_t rhs) {
        // Inequality is inverse of equality
        return !(lhs == rhs);
    }
    
    template <class TType, template <class, class> class TTrait> inline bool operator !=(std::nullptr_t lhs, MemoryLink<TType, TTrait> const &rhs) {
        // Check inequality with above function by inverting parameters
        return rhs != lhs;
    }
    
    
    template <class TType, template <class, class> class TTrait> MemoryLink<TType, TTrait>::MemoryLink(TMemoryAllocator *memoryAllocator, TLink link) : _memoryAllocator(memoryAllocator), _link(link) {
    }
    
    template <class TType, template <class, class> class TTrait> MemoryLink<TType, TTrait>::MemoryLink(std::nullptr_t) : _memoryAllocator(TBase::MemoryAllocatorNull()), _link(TBase::LinkNull()) {
    }
    
    template <class TType, template <class, class> class TTrait> template <class TSubType> MemoryLink<TType, TTrait>::MemoryLink(MemoryLink<TSubType, TTrait> const &memoryLink) : _memoryAllocator(memoryLink._memoryAllocator), _link(memoryLink._link) {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
    }
    /*
    template <class TType, template <class, class> class TTrait> template <class TSubType> MemoryLink<TType, TTrait>::MemoryLink(MemoryLink<TSubType, TTrait> &&memoryLink) : _memoryAllocator(std::move(memoryLink._memoryAllocator)), _link(std::move(memoryLink._link)) {//TODO: a cause de ca il faudra surement checker a chaque fois qu'on utilise memoryAllocator dans les DataHandle / RawDataHandle et MemoryHandle pour eviter d'appeler qq chose sur nullptr + SI on move un DataHandle / MemoryHandle / ..., il est toujours valide si on essaie de faire l'operator () ou -> car mover le Handle ne fait que copier les valeurs scalaires, donc peut etre checker si _count != nullptr pour renvoyer le pointer
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
    }*/
    
    template <class TType, template <class, class> class TTrait> MemoryLink<TType, TTrait>& MemoryLink<TType, TTrait>::operator =(MemoryLink memoryLink) noexcept {
        // Swap between this and memoryLink
        swap(*this, memoryLink);
        
        // Return itself
        return *this;
    }
    
    template <class TType, template <class, class> class TTrait> template <class TSubType> MemoryLink<TType, TTrait>& MemoryLink<TType, TTrait>::operator =(MemoryLink<TSubType, TTrait> memoryLink) noexcept {
        // Check types for upcast
        static_assert(std::is_convertible<TSubType *, TType *>::value, "Must be a derived class of TType class");
        
        // Swap between this and memoryLink
        swap(*this, memoryLink);
        
        // Return itself
        return *this;
    }
    
    template <class TType, template <class, class> class TTrait> template <class TTypeCheck> typename std::enable_if<!std::is_void<TTypeCheck>::value, TType>::type &MemoryLink<TType, TTrait>::operator *() const {
        // Return reference to object pointed by pointer
        return *TBase::get();//TODO: voir si optimisé (inline)
    }
    
    template <class TType, template <class, class> class TTrait> TType *MemoryLink<TType, TTrait>::operator ->() const {
        // Return pointer of object managed by pointer
        return TBase::get();//TODO: voir si optimisé (inline)
    }
    /*
    template <class TType, class TMemoryAllocator, class TLink> TMemoryAllocator *MemoryLink<TType, TMemoryAllocator, TLink>::getMemoryAllocator() const {
        // Return memory allocator
        return _memoryAllocator;
    }
    
    template <class TType, class TMemoryAllocator, class TLink> TLink MemoryLink<TType, TMemoryAllocator, TLink>::getLink() const {
        // Return link
        return _link;
    }*/
    
    template <class TType, template <class, class> class TTrait> template <class TSubType> inline void MemoryLink<TType, TTrait>::swapWith(MemoryLink<TSubType, TTrait> &memoryLink) {
        // Enable ADL, see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
        using std::swap;
        
        // Swap members
        swap(_memoryAllocator, memoryLink._memoryAllocator);
        swap(_link, memoryLink._link);
    }
    
    template <class TType, class TOtherType, template <class, class> class TTrait> void swap(MemoryLink<TType, TTrait> &first, MemoryLink<TOtherType, TTrait> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
    template <class TType, template <class, class> class TTrait> void swap(MemoryLink<TType, TTrait> &first, MemoryLink<TType, TTrait> &second) noexcept {
        // Swap first with second
        first.swapWith(second);
    }
    
}

#endif /* defined(__Tuple__MemoryLink__) */
