//
//  MemoryManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 8/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__MemoryManager__
#define __Tuple__MemoryManager__

#include <assert.h>
#include <unordered_map>
//#include <typeinfo>//TODO: pourquoi je l'avais mis ?
#include "MemoryPointer.hpp"
#include "MemoryHandle.hpp"
#include "MemoryMixPointerHandle.hpp"

/*
 
 TODO:
 
 voir un template parameter pour choisir entre un reference linked list ou count.
 Avec count, faire comme dans le lien et avoir le pointeur de l'objet (ou plutot le MemoryHandle) dans le count, donc on alloue le count directement et plus de mode unique/shared? : http://ootips.org/yonat/4dev/smart-pointers.html
 Ou avoir aussi le systeme comme j'ai actuellement avec le mode unique/shared
 Ou avoir tous ces modes avec un choix via mixin : NoSmart (aucune gestion, autrement dit on n'utilise pas les smart link mais directement les memory handle), SimpleCount (allocation directe du compteur, tous les smart links se comportent en mode shared, LinkedList (pas d'allocation de compteur mais 2 pointeurs pour une liste doublement chainée), LazyCount (ne crée le count que si nécessaire) équivaut au mode unique/shared
 avoir template <bool UseHandle, bool UseSmartPtr> dans le game engine ?
 */

namespace Tuple {
    
    // TODO: a voir par apres :
    //template <typename TType> using TypeHandle = MemoryHandle<TType>;
    //using RawDataHandle = MemoryHandle<void>;
    
    
    /*!
     * This class manages the memory, it can creates and destroys objects, POD
     * and raw memory, it delegates these tasks to memory allocators.
     * This class knows which memory allocator to call when we asking it to
     * create/destroy and allocate/free objects/data.
     */
    class MemoryManager {
    public:
        // @see http://stackoverflow.com/questions/2717513/c-choose-function-by-return-type :
        template <class TType, typename... TArgs> class MemoryProxy {
        public:
            // Constructor
            MemoryProxy(MemoryManager *memoryManager, TArgs&&... args) : _memoryManager(memoryManager), _args(std::forward<TArgs>(args)...) {};
            
            // TODO: on utilise TBaseType a la place de TType sinon on a des problemes quand on fait une conversion dans le constructor en un type de base
            template <class TBaseType> operator MemoryPointer<TBaseType>() const { return newPointedDataWithTypeUnpacked(typename gens<sizeof...(TArgs)>::type()); };
            template <class TBaseType> operator MemoryHandle<TBaseType>() const { return newHandledDataWithTypeUnpacked(typename gens<sizeof...(TArgs)>::type()); };
            
        private:
            // Parameters pack @see http://stackoverflow.com/questions/7858817/unpacking-a-tuple-to-call-a-matching-function-pointer
            template <int ...> struct seq {};
            template <int N, int ...S> struct gens : gens<N-1, N-1, S...> {};
            template <int ...S> struct gens<0, S...> { using type = seq<S...>; };
            
            // Memory manager
            MemoryManager *_memoryManager;
            
            // Arguments saved as tuple
            std::tuple<TArgs...> _args;//TODO: gros probleme, je pense que ca alloue de la mémoire pour sauver les args dans std::tuple !!! : NON ca a l'air ok, ca n'alloue pas de mémoire mais a vérifier quand meme !!! : ca sauve les args par heritage récursif via les templates donc ok, peut etre aussi voir std::bind?
            
            // Unpacked version of newPointedDataWithType of MemoryManager, used to convert std::tuple to args
            template<int ...S> MemoryPointer<TType> newPointedDataWithTypeUnpacked(seq<S...>) const { return _memoryManager->newPointedDataWithType<TType>(std::get<S>(_args) ...); };
            
            // Unpacked version of newHandledDataWithType of MemoryManager, used to convert std::tuple to args
            template<int ...S> MemoryHandle<TType> newHandledDataWithTypeUnpacked(seq<S...>) const { return _memoryManager->newHandledDataWithType<TType>(std::get<S>(_args) ...); };
        };
        
        // Delete data
        template <class TType> static void deleteData(MemoryPointer<TType> &memoryPointer);
        template <class TType> static void deleteData(MemoryHandle<TType> &memoryHandle);
        template <class TType> static void deleteData(MemoryMixPointerHandle<TType> &memoryMixPointerHandle);
        
        /*!
         * Register a memory allocator to manages a class
         * @param TClass Class to link to MemoryAllocator
         * @param memoryAllocator MemoryAllocator that manages class in a unique ptr
         */
        template <class TClass> void registerMemoryAllocatorForClass(std::unique_ptr<FixedMemoryAllocator> memoryAllocator);
        template <class TClass> void registerMemoryAllocatorForClass(std::unique_ptr<MoveableMemoryAllocator> memoryAllocator);
        
        // Create a new raw data with tag
        //RawDataHandle newRawDataWithTag(std::string const &tag, TypeManager::TypeID size);
        
        // Create a new data with type
        template <class TType, typename... TArgs> MemoryProxy<TType, TArgs&&...> newDataWithType(TArgs&&... args);
        
    private:
        /*!
         * Get memory allocator for a class type
         * @param classType The class type linked to memory allocator
         * @return The MemoryAllocator associated to class type
         */
        template <class TMemoryAllocatorType> TMemoryAllocatorType *getMemoryAllocatorForClassType(TypeManager::TypeID classType, std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>> const &memoryAllocatorMap) const;
        FixedMemoryAllocator *getFixedMemoryAllocatorForClassType(TypeManager::TypeID classType) const;
        MoveableMemoryAllocator *getMoveableMemoryAllocatorForClassType(TypeManager::TypeID classType) const;
        
        template <class TType, class TMemoryAllocatorType> void registerMemoryAllocatorForClass(std::unique_ptr<TMemoryAllocatorType> memoryAllocator, std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>> &memoryAllocatorMap);
        
        // TODO: peut etre mettre ces methodes en public (pour celui qui sait ce qu'il veut, il peut passer outre le systeme de MemoryProxy + std::tuple) ?
        template <class TType, typename... TArgs> MemoryPointer<TType> newPointedDataWithType(TArgs&&... args);
        template <class TType, typename... TArgs> MemoryHandle<TType> newHandledDataWithType(TArgs&&... args);
        
        template <class TType, template <class> class TLinkType> static void deleteDataFromLink(TLinkType<typename std::enable_if<std::is_pod<TType>::value, TType>::type> &memoryLink);
        template <class TType, template <class> class TLinkType> static void deleteDataFromLink(TLinkType<typename std::enable_if<!std::is_pod<TType>::value, TType>::type> &memoryLink);
        
        /*! Memory allocators */
        std::unordered_map<TypeManager::TypeID, std::unique_ptr<FixedMemoryAllocator>> _fixedMemoryAllocators;
        std::unordered_map<TypeManager::TypeID, std::unique_ptr<MoveableMemoryAllocator>> _moveableMemoryAllocators;
    };
    
    
    template <class TMemoryAllocatorType> TMemoryAllocatorType *MemoryManager::getMemoryAllocatorForClassType(TypeManager::TypeID classType, std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>> const &memoryAllocatorMap) const {
        // Try to get a memory allocator for this class type
        /*typename std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>>::const_iterator*/auto itMemoryAllocator = memoryAllocatorMap.find(classType);
        
        // If not found
        if (itMemoryAllocator == std::end(memoryAllocatorMap)) {
            // Throw exception
            throw std::invalid_argument("object is not managed by MemoryManager");
        }
        
        // Return memory allocator
        return itMemoryAllocator->second.get();
    }
    
    
    template <class TType, class TMemoryAllocatorType> void MemoryManager::registerMemoryAllocatorForClass(std::unique_ptr<TMemoryAllocatorType> memoryAllocator, std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>> &memoryAllocatorMap) {
        // Check parameters
        assert(memoryAllocator);
        
        // Get class type of TType
        /*TypeManager::TypeID*/auto typeID = TypeManager::getTypeIDForType<TType>();
        
        // Try to insert memory allocator to memory allocators map
        /*std::pair<typename std::unordered_map<TypeManager::TypeID, std::unique_ptr<TMemoryAllocatorType>>::const_iterator, bool>*/auto ret = memoryAllocatorMap.insert(std::make_pair(typeID, std::move(memoryAllocator)));
        
        // If failed (because class type key already exists)
        if (ret.second == false) {
            // Throw an exception
            throw std::logic_error("Already registered a memory allocator for this class");
        }
    }
    
    template <class TType, typename... TArgs> MemoryPointer<TType> MemoryManager::newPointedDataWithType(TArgs&&... args) {//TODO: mettre en commun avec celle du dessous
        // Get class type of TType
        /*TypeManager::TypeID*/auto typeID = TypeManager::getTypeIDForType<TType>();
        
        // Get memory allocator for class type
        /*FixedMemoryAllocator **/auto memoryAllocator = getFixedMemoryAllocatorForClassType(typeID);
        
        // Allocate memory
        /*TType **/auto pointer = static_cast<TType *>(memoryAllocator->allocateFixedMemory(sizeof(TType)));
        
        try {
            // Placement new constructor with possible args (see http://thbecker.net/articles/rvalue_references/section_08.html for explanation on rvalue and forward)
            new (pointer) TType(std::forward<TArgs>(args)...);//TODO: voir si ok d'appeler le constructor avec un POD
        }
        catch (...) {
            // Free memory
            memoryAllocator->freeFixedMemory(pointer);
            
            // Rethrow exception
            throw;
        }
        
        // Create memory pointer
        MemoryPointer<TType> memoryPointer(memoryAllocator, pointer);
        
        // Return memory pointer
        return memoryPointer;
    }
    
    template <class TType, typename... TArgs> MemoryHandle<TType> MemoryManager::newHandledDataWithType(TArgs&&... args) {
        // Get class type of TType
        /*TypeManager::TypeID*/auto typeID = TypeManager::getTypeIDForType<TType>();
        
        // Get memory allocator for class type
        /*MoveableMemoryAllocator **/auto memoryAllocator = getMoveableMemoryAllocatorForClassType(typeID);
        
        // Allocate memory
        /*Handle*/auto handle = memoryAllocator->allocateMoveableMemory(sizeof(TType));
        
        // Get memory address of handle
        /*void **/auto memoryAddr = memoryAllocator->getMemoryForHandle(handle);
        
        try {
            // Placement new constructor with possible args (see http://thbecker.net/articles/rvalue_references/section_08.html for explanation on rvalue and forward)
            new (memoryAddr) TType(std::forward<TArgs>(args)...);//TODO: voir si ok d'appeler le constructor avec un POD
        }
        catch (...) {
            // Free memory
            memoryAllocator->freeMoveableMemory(handle);
            
            // Rethrow exception
            throw;
        }
        
        // Create memory handle
        MemoryHandle<TType> memoryHandle(memoryAllocator, handle);
        
        // Return memory handle
        return memoryHandle;
    }
    
    template <class TType, template <class> class TLinkType> void MemoryManager::deleteDataFromLink(TLinkType<typename std::enable_if<std::is_pod<TType>::value, TType>::type> &memoryLink) {
        // Free memory from its memory allocator
        memoryLink.freeMemory();
    }
    
    template <class TType, template <class> class TLinkType> void MemoryManager::deleteDataFromLink(TLinkType<typename std::enable_if<!std::is_pod<TType>::value, TType>::type> &memoryLink) {
        // Call object destructor (don't put in a try catch block because normally destructor must never throw an exception : http://stackoverflow.com/questions/130117/throwing-exceptions-out-of-a-destructor )
        // Explicit call destructor even on a base pointer will call correctly destructor on real class (only if destructor is virtual) : http://stackoverflow.com/questions/1036019/does-calling-a-destructor-explicitly-destroy-an-object-completely
        /*! @todo see http://compgroups.net/comp.lang.c++.moderated/address-of-destructor/456617 */
         memoryLink->~TType();// TODO: avec un static if je n'aurai meme pas besoin de 2 deleteDataFromLink avec le enable_if, juste un static if ici avec !is_pod
        
        // Free memory from its memory allocator
        memoryLink.freeMemory();
    }
    
    template <class TType> void MemoryManager::deleteData(MemoryPointer<TType> &memoryPointer) {//TODO: peut etre se passer de ces methodes et avoir directement la methode deleteData qui est comme deleteDataFromLink ?
        // Delete data from memoryPointer
        deleteDataFromLink<TType, MemoryPointer>(memoryPointer);//TODO: je suis obligé de préciser les types template quand j'appelle cette methode (et pareil pour ceux d'en dessous), il ne sais pas le deduire d'apres le parametre, voir pq : c'est surement parce que MemoryPointer est en fait un MemoryLink avec un MemoryPointerTrait
    }
    
    template <class TType> void MemoryManager::deleteData(MemoryHandle<TType> &memoryHandle) {
        // Delete data from memoryHandle
        deleteDataFromLink<TType, MemoryHandle>(memoryHandle);
    }
    
    template <class TType> void MemoryManager::deleteData(MemoryMixPointerHandle<TType> &memoryMixPointerHandle) {
        // Delete data from memoryMixPointerHandle
        deleteDataFromLink<TType, MemoryMixPointerHandle>(memoryMixPointerHandle);
    }
    
    template <class TType> void MemoryManager::registerMemoryAllocatorForClass(std::unique_ptr<FixedMemoryAllocator> memoryAllocator) {
        // Register a fixed memory allocator
        registerMemoryAllocatorForClass<TType>(std::move(memoryAllocator), _fixedMemoryAllocators);
    }
    
    template <class TType> void MemoryManager::registerMemoryAllocatorForClass(std::unique_ptr<MoveableMemoryAllocator> memoryAllocator) {
        // Register a moveable memory allocator
        registerMemoryAllocatorForClass<TType>(std::move(memoryAllocator), _moveableMemoryAllocators);
    }
    
    template <class TType, typename... TArgs> MemoryManager::MemoryProxy<TType, TArgs&&...> MemoryManager::newDataWithType(TArgs&&... args) {
        // Return a memory proxy of this MemoryManager
        return MemoryProxy<TType, TArgs&&...>(this, std::forward<TArgs>(args)...);
    }
    
}

#endif /* defined(__Tuple__MemoryManager__) */
