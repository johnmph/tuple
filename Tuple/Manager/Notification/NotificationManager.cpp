//
//  NotificationManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 15/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "NotificationManager.hpp"


namespace Tuple {
    
    Notification::~Notification () {
    }
    
    NotificationListenerBase::NotificationListenerBase(TypeManager::TypeID notificationType, int priority) : _senderTypeID(0), _senderObject(nullptr), _notificationTypeID(notificationType), _priority(priority) {
        // Check parameters
    }
    
    NotificationListenerBase::~NotificationListenerBase() {
    }
    
    bool NotificationListenerBase::compare(Tuple::NotificationListenerBase const &rhs) const {
        // Equals if both notification type, sender and priority are equals (no really need to check both sender object and class but no problem to do it because we can't have both object and class set but if we don't do that, we need to check if both are not empty)
        return ((rhs._notificationTypeID == _notificationTypeID) && (rhs._senderTypeID == _senderTypeID) && (rhs._senderObject == _senderObject) && (rhs._priority == _priority));
    }
    
    TypeManager::TypeID NotificationListenerBase::getNotificationTypeID() const {
        // Return notification type id
        return _notificationTypeID;
    }
    
    int NotificationListenerBase::getPriority() const {
        // Return priority
        return _priority;
    }
    
    void NotificationListenerBase::notify(Tuple::Notification &notification) const {
        // Notify that we received a notification
        receivedNotification(notification);
    }
    
    void NotificationManager::registerListener(std::shared_ptr<NotificationListenerBase> listener) {
        // Get notification listeners for notification type supported by listener (create it if doesn't exist)
        /*std::vector<std::shared_ptr<NotificationListenerBase>> &*/auto &listenersForType = _notificationListeners[listener->getNotificationTypeID()];
        
        // Default insertion at first place
        /*std::vector<std::shared_ptr<NotificationListenerBase>>::const_iterator*/auto itListener = listenersForType.cbegin();
        
        // Browse all notification listeners
        for (; itListener != listenersForType.cend(); ++itListener) {
            // If already registered
            if (*(*itListener) == *listener) {
                // Throw an exception
                throw std::logic_error("Already registered this listener");
            }
            
            // If no need to check anymore (because listener can't be equals anymore to remaining listeners because priorities are differents)
            if ((*itListener)->getPriority() < listener->getPriority()) {
                // Exit loop
                break;
            }
        }
        
        // Insert at correct place
        listenersForType.insert(itListener, listener);
    }
    
    void NotificationManager::unregisterListener(std::shared_ptr<NotificationListenerBase> listener) {
        // Get notification type
        /*TypeManager::TypeID*/auto notificationTypeID = listener->getNotificationTypeID();
        
        // Try to get notification listeners for notification type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<NotificationListenerBase>>>::iterator*/auto itNotificationListener = _notificationListeners.find(notificationTypeID);
        
        // If not found
        if (itNotificationListener == std::end(_notificationListeners)) {
            // Throw exception
            throw std::invalid_argument("Listener not registered");
        }
        
        // Get notification listeners
        /*std::vector<std::shared_ptr<NotificationListenerBase>> &*/auto &listenersForType = itNotificationListener->second;
        
        // Browse all notification listeners
        for (/*std::vector<std::shared_ptr<NotificationListenerBase>>::iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
            // If found
            if (*(*itListener) == *listener) {
                // Delete it
                listenersForType.erase(itListener);
                
                // If no listener for this type
                if (listenersForType.empty()) {
                    // Delete this entry from map
                    _notificationListeners.erase(notificationTypeID);
                }
                
                // Exit
                return;
            }
        }
        
        // Throw exception
        throw std::invalid_argument("Listener not registered");
    }
    
    bool NotificationManager::isListenerRegistered(std::shared_ptr<NotificationListenerBase> listener) const {
        /*! @todo est ce qu'on peut eviter le copier coller avec unregisterListener pour retrouver le listener ? */
        // Get notification type
        /*TypeManager::TypeID*/auto notificationTypeID = listener->getNotificationTypeID();
        
        // Try to get notification listeners for notification type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<NotificationListenerBase>>>::const_iterator*/auto itNotificationListener = _notificationListeners.find(notificationTypeID);
        
        // If not found
        if (itNotificationListener == std::end(_notificationListeners)) {
            // Exit
            return false;
        }
        
        // Get notification listeners
        /*std::vector<std::shared_ptr<NotificationListenerBase>> const &*/auto const &listenersForType = itNotificationListener->second;
        
        // Browse all notification listeners
        for (/*std::vector<std::shared_ptr<NotificationListenerBase>>::const_iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
            // If found
            if (*(*itListener) == *listener) {
                // Exit
                return true;
            }
        }
        
        // Not found
        return false;
    }
    
}
