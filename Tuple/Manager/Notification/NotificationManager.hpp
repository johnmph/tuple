//
//  NotificationManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 15/12/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__NotificationManager__
#define __Tuple__NotificationManager__

#include <assert.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include "TypeManager.hpp"

// TODO: peut etre utiliser les lambdas/closure c++11 : http://www.cprogramming.com/c++11/c++11-lambda-closures.html
// TODO: il y a surement moyen de faire une version compile-time si ce qu'une classe ecoute est connu au compile time
// TODO: il y a un probleme : on ne sait pas enregistrer X fois un listener pour un type de notification pour X targets (1x par target mais avec le meme type de notification). En plus pour l'optimisation, il faudrait que le filtre selon les targets soit fait avec un seul test (comme pour les types de notifications via une map plutot que de faire un test a chaque fois dans la boucle des listeners) : NON normalement c'est bon car le == est overridé pour ne pas etre egal si targets différents mais rajouter le test dans les tests cases
// TODO: Si on a une hierarchie de Notification et qu'on s'enregistre pour un type de notification, on ne recevra pas les notifications de type filles du type pour lequel on s'est enregistré : Il suffit de ne pas avoir la classe de base Notification, et de pouvoir notifier avec n'importe quel type (via template dans la methode sendNotification, et supprimer la classe de base Notification)
// TODO: on peut simplifier les sendNotification (avec lvalue et rvalue) en une seule methode avec une universal reference
// TODO: peut etre avoir NotificationListener en privé et dans le notification manager passer directement le callback et le type de notification et ca retourne un id de listener pour pouvoir le unregister ou retourner le notification listener donc plus en privé mais constructor private ainsi on ne sait le créer que via le notification manager ?
namespace Tuple {
    
    /*!
     * Base class for notifications, must be derived when creating custom
     * notifications.
     * @todo les objects notifications pourront etre créés via le memory manager pour optimiser la création des notifications ?
     * @todo séparer les classes Notification et NotificationListener dans d'autres fichiers ?
     */
    class Notification {
    public:
        /*!
         * Destructor.
         */
        virtual ~Notification() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
    };
    
    /*!
     * This class is only needed for saving NotificationListener in smart ptr
     * and containers, NotificationListener class can not be saved directly
     * because it is a templated class (without specifying the template parameters),
     * this technic is called Type Erasure.
     * @see http://www.artima.com/cppsource/type_erasure.html
     * @todo plutot renommer celle ci en NotificationListener et NotificationListener en TypedNotificationListener ou NotificationListenerTyped ?
     */
    class NotificationListenerBase {
    public:
        /*!
         * Constructor.
         * @param sender The sender filter, only notifications sent by this sender will be received
         * @param notificationTypeID The type of notification to receive
         * @param priority The priority of the listener, higher number is lower priority
         */
        template <class TSenderType> NotificationListenerBase(TSenderType *sender, TypeManager::TypeID notificationTypeID, int priority = 0);
        
        /*!
         * Constructor.
         * @param notificationTypeID The type of notification to receive
         * @param priority The priority of the listener, higher number is lower priority
         */
        NotificationListenerBase(TypeManager::TypeID notificationTypeID, int priority = 0);
        
        /*!
         * Destructor.
         */
        virtual ~NotificationListenerBase() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        /*!
         * Give the notification type that listener listens.
         * @return The notification type
         */
        TypeManager::TypeID getNotificationTypeID() const;
        
        /*!
         * Give the priority, higher number is lower priority.
         * Listeners with higher priorities will be notified first.
         * @return The priority
         */
        int getPriority() const;
        
        /*!
         * Check if listener is allowed to receive notifications of
         * a specified sender. If both sender class and sender object
         * are set, this method checks first sender object.
         * @param sender The sender to check
         * @return true if listener will receive notifications from sender else false
         */
        template <class TSenderType> bool isAllowedSender(TSenderType *sender) const;
        
        /*!
         * Set the sender class that listener will be allowed to receive
         * notifications, normally we must set this kind of parameter
         * in the constructor but it is a template parameter and it is
         * not possible to have a template constructor.
         * @param TSenderType The type of the sender
         */
        template <class TSenderType> void setSenderType();
        
        /*!
         * Notify that listener have received a notification.
         * This method must be called only by NotificationManager.
         * @param notification The received notification
         */
        void notify(Notification &notification) const;//TODO: plutot private avec un friend sur le NotificationManager
        
    protected:
        /*!
         * Compare this listener with another notification listener,
         * used by NotificationManager by calling == operator.
         * @param rhs Object to compare
         * @return true if rhs is equal to this object else false
         */
        virtual bool compare(NotificationListenerBase const &rhs) const;
        
    private:
        /*! Allows access of private members from operator free functions */
        friend bool operator ==(NotificationListenerBase const &lhs, NotificationListenerBase const &rhs);
        
        /*!
         * Called when a notification is received.
         */
        virtual void receivedNotification(Notification &notification) const = 0;
        
        /*! Notification type that listener will listen */
        TypeManager::TypeID _notificationTypeID;
        
        /*! Sender class type for filter notifications */
        TypeManager::TypeID _senderTypeID;
        
        /*! Sender object for filter notifications */
        void *_senderObject;
        
        /*! Priority, higher value is lower priority */
        int _priority;
    };
    
    /*!
     * This templated class is a NotificationListenerBase that will
     * call a callback from a target when it will receive a notification.
     * @param TTarget The target class
     * @param TNotification The notification class that listener listens
     */
    template <class TTarget, class TNotification> class NotificationListener : public NotificationListenerBase {
    public:
        /*! The callback method type */
        using Callback = void(TTarget::*)(TNotification &);
        
        /*!
         * Constructor.
         * @param target The target object that have the callback
         * @param callback The callback method to call when we receive a notification
         * @param sender The sender filter, only notifications sent by this sender will be received
         * @param priority The priority of the listener, higher number is lower priority
         */
        template <class TSenderType> NotificationListener(TTarget &target, Callback callback, TSenderType *sender, int priority = 0);
        
        /*!
         * Constructor.
         * @param target The target object that have the callback
         * @param callback The callback method to call when we receive a notification
         * @param priority The priority of the listener, higher number is lower priority
         */
        NotificationListener(TTarget &target, Callback callback, int priority = 0);
        
    private:
        /*!
         * Compare this listener with another notification listener,
         * used by NotificationManager by calling == operator.
         * @param rhs Object to compare
         * @return true if rhs is equal to this object else false
         */
        bool compare(NotificationListenerBase const &rhs) const override;
        
        /*!
         * Called when a notification is received.
         */
        void receivedNotification(Notification &notification) const override;
        
        /*! The target object that have the callback */
        TTarget &_target;
        
        /*! The callback method to call when we receive a notification */
        Callback _callback;
    };
    
    /*!
     * This class is only needed for saving NotificationListener in smart ptr
     * and containers, NotificationListener class can not be saved directly
     * because it is a templated class (without specifying the template parameters).
     */
    class NotificationManager {
    public:
        /*!
         * Register a listener.
         * @param listener The listener to register in a shared ptr
         */
        void registerListener(std::shared_ptr<NotificationListenerBase> listener);
        
        /*!
         * Unregister a listener.
         * @param listener The listener to unregister in a shared ptr
         */
        void unregisterListener(std::shared_ptr<NotificationListenerBase> listener);
        
        /*!
         * Check if a listener is registed.
         * @param listener The listener to check in a shared ptr
         * @return true if listener is registered else false
         * @todo passer peut etre directement un pointer ou une reference const plutot qu'un shared_ptr ?
         */
        bool isListenerRegistered(std::shared_ptr<NotificationListenerBase> listener) const;
        
        /*!
         * Send a notification to registered listeners.
         * @param sender The sender that send the notification
         * @param notification The notification to send
         */
        template <class TSenderType> void sendNotification(TSenderType *sender, Notification &notification) const;
        
        /*!
         * Send a notification to registered listeners with notification
         * in a rvalue, useful to send notification with one line of code.
         * @param sender The sender that send the notification
         * @param notification The notification to send in a rvalue
         */
        template <class TSenderType> void sendNotification(TSenderType *sender, Notification &&notification) const;
        
    private:
        /*! Notification listeners */
        /*! @todo voir si plus rapide et mieux pour les conversions sans static_cast d'avoir un unordered_map de size_t, std::unique_ptr<std::vector<std::shared_ptr<NotificationListenerBase>>>, ainsi quand le unordered_map est redimensionné il copie un shared_ptr plutot qu'un vector complet ? on aurait plus les vectors contigus en mémoire mais on n'en a pas vraiment besoin ici : a voir car il y a peut etre le copy ellision quand on redimensionne l'unordered_map, a voir (et pareil pour les autres todo ainsi) */
        std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<NotificationListenerBase>>> _notificationListeners;
        
        /*!
         * Send a notification to registered listeners.
         * @param sender The sender that send the notification
         * @param notification The notification to send
         * @param notificationTypeID The type of the notification
         */
        template <class TSenderType> void sendNotificationWithType(TSenderType *sender, Notification &notification, TypeManager::TypeID notificationTypeID) const;
    };
    
    /*!
     * Equality operator overload, used essentially in containers.
     * @param lhs Object to compare
     * @param rhs Object to compare
     * @return true if lhs and rhs are equal else false
     * @todo ecrire aussi l'inequality operator ??
     */
    inline bool operator ==(NotificationListenerBase const &lhs, NotificationListenerBase const &rhs) {
        // Compare with rhs
        return lhs.compare(rhs);
    }
    
    template <class TSenderType> NotificationListenerBase::NotificationListenerBase(TSenderType *sender, TypeManager::TypeID notificationTypeID, int priority) : _senderTypeID(0), _senderObject(sender), _notificationTypeID(notificationTypeID), _priority(priority) {
        // Check parameters
        assert(sender != nullptr);
    }
    
    template <class TSenderType> bool NotificationListenerBase::isAllowedSender(TSenderType *sender) const {
        // If no sender filter
        if ((_senderTypeID == 0) && (_senderObject == nullptr)) {
            // Allowed
            return true;
        }
        
        // Check if sender object or class is allowed
        return (sender == _senderObject) ? true : (TypeManager::getTypeIDForType<TSenderType>() == _senderTypeID);
    }
    
    template <class TSenderType> void NotificationListenerBase::setSenderType() {
        // Can only be used with no sender specified constructor
        assert(_senderObject == nullptr);
        
        // Set sender type id
        _senderTypeID = TypeManager::getTypeIDForType<TSenderType>();
    }
    
    template <class TTarget, class TNotification> template <class TSenderType> NotificationListener<TTarget, TNotification>::NotificationListener(TTarget &target, Callback callback, TSenderType *sender, int priority) : NotificationListenerBase(sender, TypeManager::getTypeIDForType<TNotification>(), priority), _target(target), _callback(callback) {
        // Check parameters
        assert(callback != nullptr);
    }
    
    template <class TTarget, class TNotification> NotificationListener<TTarget, TNotification>::NotificationListener(TTarget &target, Callback callback, int priority) : NotificationListenerBase(TypeManager::getTypeIDForType<TNotification>(), priority), _target(target), _callback(callback) {
        // Check parameters
        assert(callback != nullptr);
    }
    
    template <class TTarget, class TNotification> bool NotificationListener<TTarget, TNotification>::compare(NotificationListenerBase const &rhs) const {
        // Check with parent check
        if (!NotificationListenerBase::compare(rhs)) {
            // Not equal
            return false;
        }
        
        // Try to get rhs with correct class
        /*NotificationListener<TTarget, TNotification> const **/ auto typedRhs = dynamic_cast<NotificationListener<TTarget, TNotification> const *>(&rhs);
        
        // If listener is not the same type of this listener
        if (typedRhs == nullptr) {
            // Not equal
            return false;
        }
        
        // Check target pointer and callback
        return ((&typedRhs->_target == &_target) && (typedRhs->_callback == _callback));
    }
    
    template <class TTarget, class TNotification> void NotificationListener<TTarget, TNotification>::receivedNotification(Notification &notification) const {
        // Call callback method of target with correct notification class
        (_target.*_callback)(*static_cast<TNotification *>(&notification));
    }
    
    template <class TSenderType> void NotificationManager::sendNotificationWithType(TSenderType *sender, Notification &notification, TypeManager::TypeID notificationTypeID) const {
        // Try to get notification listeners for notification type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<NotificationListenerBase>>>::const_iterator*/auto itNotificationListener = _notificationListeners.find(notificationTypeID);
        
        // If not found
        if (itNotificationListener == std::end(_notificationListeners)) {
            // Exit
            return;
        }
        
        // Get notification listeners for notification type
        /*std::vector<std::shared_ptr<NotificationListenerBase>> const &*/ auto const &listenersForType = itNotificationListener->second;
        
        // Browse all notification listeners
        for (/*std::vector<std::shared_ptr<NotificationListenerBase>>::const_iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
            // Check if sender is allowed in current listener
            if ((*itListener)->isAllowedSender(sender)) {
                // Notify listener
                (*itListener)->notify(notification);
            }
        }
    }
    
    template <class TSenderType> void NotificationManager::sendNotification(TSenderType *sender, Notification &notification) const {
        // Get notification type
        /*TypeManager::TypeID*/auto notificationTypeID = TypeManager::getTypeIDForInstance(notification);
        
        // Send notification with notification type
        sendNotificationWithType(sender, notification, notificationTypeID);
        
        /*! @todo le code qui suit ne sert qu'a pouvoir s'enregistrer a la classe Notification pour pouvoir s'enregistrer a toutes les notifications, peut etre pas necessaire, a voir quand tout sera fini ou ajouter un define pour l'activer explicitement */
        
        // Get base type
        /*! @todo il faut separer dans une methode */
        //TypeManager::TypeID baseTypeID = TypeManager::getTypeIDForType<Notification>();
        
        // Send notification with base type (to allow registering to all notifications)
        //sendNotificationWithType(sender, notification, baseTypeID);
    }
    
    template <class TSenderType> void NotificationManager::sendNotification(TSenderType *sender, Notification &&notification) const {
        // Send notification with lvalue
        return sendNotification(sender, notification);
    }
    
}

#endif /* defined(__Tuple__NotificationManager__) */
