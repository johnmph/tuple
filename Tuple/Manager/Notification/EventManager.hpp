//
//  EventManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/06/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__EventManager__
#define __Tuple__EventManager__

#include <assert.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include "TypeManager.hpp"

// TODO: peut etre utiliser les lambdas/closure c++11 : http://www.cprogramming.com/c++11/c++11-lambda-closures.html
// TODO: il y a surement moyen de faire une version compile-time si ce qu'une classe ecoute est connu au compile time
// TODO: il y a un probleme : on ne sait pas enregistrer X fois un listener pour un type de event pour X targets (1x par target mais avec le meme type de event). En plus pour l'optimisation, il faudrait que le filtre selon les targets soit fait avec un seul test (comme pour les types de events via une map plutot que de faire un test a chaque fois dans la boucle des listeners)
// TODO: Si on a une hierarchie de Event et qu'on s'enregistre pour un type de event, on ne recevra pas les events de type filles du type pour lequel on s'est enregistré : Il suffit de ne pas avoir la classe de base Event, et de pouvoir notifier avec n'importe quel type (via template dans la methode sendEvent, et supprimer la classe de base Event)
// TODO: on peut simplifier les sendEvent (avec lvalue et rvalue) en une seule methode avec une universal reference
// TODO: peut etre avoir EventListener en privé et dans le event manager passer directement le callback et le type de event et ca retourne un id de listener pour pouvoir le unregister ou retourner le event listener donc plus en privé mais constructor private ainsi on ne sait le créer que via le event manager ?


// TODO: il faudra faire des profiling pour voir le temps (car normalement un reinterpret_cast ne coute rien vu que c'est au compile time mais il peut désactiver certaines optimisations ( http://stackoverflow.com/questions/3575234/reinterpret-cast-cast-cost )


/*
 
 TODO :
 
 Si on ne peut pas choisir la methode / fonction / closure ..., alors on n'a pas besoin de renvoyer des EventListener (et donc de les avoir public) car pour se désenregistrer, il suffit de rappeler avec les memes parametres que l'enregistrement (le type de l'event et le target) car le callback reste toujours le meme car choisit ici dans l'EventManager (eventReceived(TEvent &))
 
 Maintenant, si on veut pouvoir choisir la methode / fonction / closure ..., alors on est obligé de renvoyer un identifiant de listener (comme l'EventListener lui même par exemple) car on ne sait pas comparer des std functions (meme si on serait, elles seraient différentes meme en leur passant le meme closure car ca crée a chaque fois un objet différent) et donc on ne serait jamais comparer pour se désenregistrer (par exemple je m'enregistre avec une closure puis plus tard je veux me désenregistrer avec cette closure mais comment faire pour la retrouver dans l'EventManager puisque la comparaison ne marchera pas).
 
 Et puisqu'on devra retourner un EventListener, pourquoi ne pas en profiter pour l'utiliser comme un unique_ptr, c'est à dire que c'est lui qui gérera le désenregistrement automatique a l'eventManager quand il sera détruit. On pourra donc le récuperer via un std::move car il doit etre unique puis quand l'objet est détruit, ses membres aussi et donc les membres EventListener qui désinscriront l'objet a l'EventManager.
 
 On pourra bien evidemment se désinscrire avant via unregisterListener ou bien en resetant le membre EventListener ? en tout cas si on laisse le désenregistrement via unregisterListener, il faudra faire attention de mettre a jour les EventListener pour eviter de pouvoir se désenregistrer une 2eme fois.
 
 
 */


/*
 
 #include <iostream>
 #include <functional>
 
 using namespace std;
 
 
 void test(int x) {
 std::cout << "test(" << x << ")" << std::endl;
 }
 
 void test2(int x, int y) {
 std::cout << "test(" << x << ", " << y << ")" << std::endl;
 }
 
 template<typename T, typename... U> size_t getAddress(std::function<T(U...)> f) {
 typedef T(fnType)(U...);
 fnType ** fnPointer = f.template target<fnType*>();
 return (size_t) *fnPointer;
 }
 
 template <class T, class T2> std::function<T> functionConvert(std::function<T2> const &f) {
 auto p = f.template target<T2*>();
 return std::function<T>(reinterpret_cast<T *>(*p));
 }
 
 template <class T, class T2, class T3> std::function<T> functionConvert(std::function<T3> const &f) {
 auto p = f.template target<T2>();
 std::cout << "p = " << *p << std::endl;
 //std::cout << "type = " << typeid(T2).name() << std::endl;
 //std::cout << "type2 = " << f.target_type().name() << std::endl;
 return std::function<T>(reinterpret_cast<T *>(**p));
 }
 
 int main() {
 std::function<void(int)> f(test);
 auto f2 = functionConvert<void(void *)>(f);
 auto l = [](int x) { std::cout << "test(" << x << ")" << std::endl; };
 auto fl = functionConvert<void(void *), decltype(l)>(std::function<void(int)>(l));
 //auto b = std::bind(test2, std::placeholders::_1, 54);
 //auto fb = functionConvert<void(void *), decltype(b)>(std::function<void(int)>(b));
 //std::cout << "address = " << fb.target_type().name() << std::endl;
 //std::cout << "address = " << getAddress(std::function<void(int)>(l)) << std::endl;
	// your code goes here
	f2((void *) 12);
	fl((void *) 24);
	//fb((void *) 36);
	
	return 0;
 }
 
 /////// OU MIEUX :
 
 #include <iostream>
 #include <functional>
 #include <vector>
 
 using namespace std;
 
 
 void test(int x) {
 std::cout << "testi(" << x << ")" << std::endl;
 }
 
 void test(float x) {
 std::cout << "testf(" << x << ")" << std::endl;
 }
 
 void test(int x, int y) {
 std::cout << "testii(" << x << ", " << y << ")" << std::endl;
 }
 
 class Foo {
 public:
 void test(int x) {
 std::cout << "Foo::testi(" << x << ")" << std::endl;
 };
 };
 
 template <class T> void addToVector(std::vector<std::function<void(void *)>> &v, void(*f)(T)) {
 v.push_back(std::function<void(void *)>(reinterpret_cast<void(*)(void *)>(f)));
 }
 
 template <class T> using PF = void(*)(T);
 
 template <class T> PF<T> getFromVector(std::vector<std::function<void(void *)>> &v, int index) {
 auto p = v[index].template target<void(*)(void *)>();
 return reinterpret_cast<PF<T>>(*p);
 }
 
 int main() {
 std::vector<std::function<void(void *)>> v;
 
 addToVector<int>(v, &test);
 addToVector<float>(v, &test);
 addToVector<char *>(v, [](char *x) { std::cout << "testc(" << x << ")" << std::endl; });
 //addToVector<int>(v, std::bind(test, std::placeholders::_1, 66););
 Foo foo;
 addToVector<int>(v, std::bind(&Foo::test, &foo, std::placeholders::_1));
 
	auto f1 = getFromVector<int>(v, 0);
	auto f2 = getFromVector<float>(v, 1);
	auto f3 = getFromVector<char *>(v, 2);
	//auto f4 = getFromVector<int>(v, 3);
	auto f4 = getFromVector<int>(v, 3);
	
	(*f1)(12);
	(*f2)(24.0f);
	(*f3)("hello world");
	//(*f4)(45);
	(*f4)(45);
	
	return 0;
 }

 
*///TODO: voir avec cette technique (simple cast d'un std::function) et profiler pour voir lequel est le plus rapide : GROS PROBLEME AVEC CES TECHNIQUES : elles ne fonctionnent pas pour un bind et donc aussi pour des methodes, seulement pour des fonctions, donc ca ne va pas

namespace Tuple {
    
    /*!
     * Base class for events, must be derived when creating custom
     * events.
     * @todo les objects events pourront etre créés via le memory manager pour optimiser la création des events ?
     * @todo séparer les classes Event et EventListener dans d'autres fichiers ?
     */
    class Event {
    public:
        /*!
         * Destructor.
         */
        virtual ~Event() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
    };
    
    /*!
     * This class is only needed for saving EventListener in smart ptr
     * and containers, EventListener class can not be saved directly
     * because it is a templated class (without specifying the template parameters).
     */
    class EventManager {
    public:
        ~EventManager() {
            std::cout << "~EventManager()" << std::endl;
            
            // Browse all listeners
            for (auto &itEventListener : _eventListeners) {
                // Call deleter method for this vector listeners
                _deleters[itEventListener.first](itEventListener.second);
            }
        };
        
        /*!
         * Register a listener.
         * @param listener The listener to register in a shared ptr
         */
        template <class TEvent, class TTarget> void registerListener(TTarget &target, int priority = 0);
        
        /*!
         * Unregister a listener.
         * @param listener The listener to unregister in a shared ptr
         */
        template <class TEvent, class TTarget> void unregisterListener(TTarget &target);
        //template <class TTarget> bool unregisterListener(TTarget const &target);
        
        /*!
         * Check if a listener is registered.
         * @param listener The listener to check in a shared ptr
         * @return true if listener is registered else false
         * @todo passer peut etre directement un pointer ou une reference const plutot qu'un shared_ptr ?
         */
        template <class TEvent, class TTarget> bool isListenerRegistered(TTarget const &target) const;
        
        /*!
         * Send a event to registered listeners.
         * @param sender The sender that send the event
         * @param event The event to send
         */
        template <class TSenderType, class TEvent> void sendEvent(TSenderType *sender, TEvent &&event) const;
        
    private:
        
        template <class TEvent> static void deleteListenersVector(void *address);
        
        /*!
         * This class is only needed for saving EventListener in smart ptr
         * and containers, EventListener class can not be saved directly
         * because it is a templated class (without specifying the template parameters),
         * this technic is called Type Erasure.
         * @see http://www.artima.com/cppsource/type_erasure.html
         * @todo plutot renommer celle ci en EventListener et EventListener en TypedEventListener ou EventListenerTyped ?
         */
        template <class TEvent> class EventListener {
        public:
            /*! The callback method type */
            using Callback = std::function<void(TEvent &)>;
            
            /*!
             * Constructor.
             * @param target The target object that have the callback
             * @param callback The callback method to call when we receive a event
             * @param priority The priority of the listener, higher number is lower priority
             */
            EventListener(void *target, Callback callback, int priority = 0);
            
            ~EventListener() {
                std::cout << "~EventListener::" << typeid(TEvent).name() << std::endl;
            };
            
            /*!
             * Give the priority, higher number is lower priority.
             * Listeners with higher priorities will be notified first.
             * @return The priority
             */
            int getPriority() const;
            
            void *getTarget() const;
            
            /*!
             * Notify that listener have received a event.
             * This method must be called only by EventManager.
             * @param event The received event
             */
            void notify(TEvent &event) const;
            
        private:
            /*! Allows access of private members from operator free functions *//*
            friend bool operator ==(EventListener<TEvent> const &lhs, EventListener<TEvent> const &rhs) {
                // Compare with rhs
                return lhs.compare(rhs);
            }
            
            template <class TTarget> friend bool operator ==(EventListener<TEvent> const &lhs, TTarget const &rhs) {
                // Compare with rhs
                return lhs._callback.template target<Callback>() == &rhs;
            }*/
            
            /*!
             * Compare this listener with another event listener,
             * used by EventManager by calling == operator.
             * @param rhs Object to compare
             * @return true if rhs is equal to this object else false
             */
            bool compare(EventListener const &rhs) const;
            
            /*! Priority, higher value is lower priority */
            int _priority;
            
            Callback _callback;
            
            void *_target;
        };
        
        /*! Allows access of private members from operator free functions */
        //template <class TEvent> friend bool operator ==(EventListener<TEvent> const &lhs, EventListener<TEvent> const &rhs);
        
        /*! Event listeners */
        /*! @todo voir si plus rapide et mieux pour les conversions sans static_cast d'avoir un unordered_map de size_t, std::unique_ptr<std::vector<std::shared_ptr<EventListenerBase>>>, ainsi quand le unordered_map est redimensionné il copie un shared_ptr plutot qu'un vector complet ? on aurait plus les vectors contigus en mémoire mais on n'en a pas vraiment besoin ici : a voir car il y a peut etre le copy ellision quand on redimensionne l'unordered_map, a voir (et pareil pour les autres todo ainsi) */
        std::unordered_map<TypeManager::TypeID, void *> _eventListeners;
        
        static std::unordered_map<TypeManager::TypeID, std::function<void(void *)>> _deleters;
    };
    
    /*!
     * Equality operator overload, used essentially in containers.
     * @param lhs Object to compare
     * @param rhs Object to compare
     * @return true if lhs and rhs are equal else false
     * @todo ecrire aussi l'inequality operator ??
     */
/*    template <class TEvent> inline bool operator ==(EventManager::EventListener<TEvent> const &lhs, EventManager::EventListener<TEvent> const &rhs) {
        // Compare with rhs
        return lhs.compare(rhs);
    }*/
    
    template <class TEvent> EventManager::EventListener<TEvent>::EventListener(void *target, Callback callback, int priority) : _target(target), _callback(callback), _priority(priority) {
        // Check parameters
    }
    
    
    template <class TSenderType, class TEvent> void EventManager::sendEvent(TSenderType *sender, TEvent &&event) const {
        // Get event type
        auto eventTypeID = TypeManager::getTypeIDForInstance(event);
        
        // Try to get event listeners for event type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<EventListener>>>::const_iterator*/auto itEventListener = _eventListeners.find(eventTypeID);
        
        // If found
        if (itEventListener != std::end(_eventListeners)) {
            // Get event listeners for event type
            ///*std::vector<std::shared_ptr<EventListener>> const &*/ auto const &listenersForType = itEventListener->second;
            auto const &listenersForType = *(reinterpret_cast<std::vector<EventListener<TEvent>> *>(itEventListener->second));
            
            // Browse all event listeners
            for (/*std::vector<std::shared_ptr<EventListener>>::const_iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
                // Notify listener
                itListener->notify(event);//TODO: passer aussi en parametre le sender (ou bien tout ce qui est nécessaire est contenu dans l'event ?)
            }
        }
        
        /*! @todo le code qui suit ne sert qu'a pouvoir s'enregistrer a la classe Event pour pouvoir s'enregistrer a toutes les events, peut etre pas necessaire, a voir quand tout sera fini ou ajouter un define pour l'activer explicitement */
        
        // Get base type
        /*! @todo il faut separer dans une methode */
        //TypeManager::TypeID baseTypeID = TypeManager::getTypeIDForType<Event>();
        
        // Send event with base type (to allow registering to all events)
        //sendEventWithType(sender, event, baseTypeID);
    }
    
    
    template <class TEvent, class TTarget> void EventManager::registerListener(TTarget &target, int priority) {
        // Get event type
        auto eventTypeID = TypeManager::getTypeIDForType<TEvent>();
        
        // Get event listeners for event type supported by listener (create it if doesn't exist)
        ///*std::vector<std::shared_ptr<EventListener>> &*/auto &listenersForType = _eventListeners[eventTypeID];
        auto pListenersForType = reinterpret_cast<std::vector<EventListener<TEvent>> *>(_eventListeners[eventTypeID]);
        
        if (pListenersForType == nullptr) {
            pListenersForType = new std::vector<EventListener<TEvent>>;
            _eventListeners[eventTypeID] = pListenersForType;
        }
        auto &listenersForType = *pListenersForType;
        
        // Default insertion at first place
        /*std::vector<std::shared_ptr<EventListener>>::const_iterator*/auto itListener = listenersForType.cbegin();
        
        // Browse all event listeners
        for (; itListener != listenersForType.cend(); ++itListener) {
            // If already registered
            if (itListener->getTarget() == &target) {
                // Throw an exception
                throw std::logic_error("Already registered this listener");
            }
            
            // If no need to check anymore (because listener can't be equals anymore to remaining listeners because priorities are differents)
            if (itListener->getPriority() < priority/*listener.getPriority()*/) {
                // Exit loop
                break;
            }
        }
        
        // Create listener
        void (TTarget::*method)(TEvent &) = &TTarget::eventReceived;
        auto func = typename EventListener<TEvent>::Callback(std::bind(method, &target, std::placeholders::_1));
        
        // Insert at correct place
        //listenersForType.insert(itListener, listener);
        listenersForType.emplace(itListener, &target, func, priority);
        
        
        // Add deleter method for this type if necessary
        if (_deleters.find(eventTypeID) == std::end(_deleters)) {
            _deleters[eventTypeID] = std::function<void(void *)>(&deleteListenersVector<TEvent>);
        }
    }
    
    
    template <class TEvent, class TTarget> void EventManager::unregisterListener(TTarget &target) {
        // Get event type
        auto eventTypeID = TypeManager::getTypeIDForType<TEvent>();
        
        // Try to get event listeners for event type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<EventListener>>>::iterator*/auto itEventListener = _eventListeners.find(eventTypeID);
        
        // If not found
        if (itEventListener == std::end(_eventListeners)) {
            // Throw exception
            throw std::invalid_argument("Listener not registered");
        }
        
        // Get event listeners
        ///*std::vector<std::shared_ptr<EventListener>> &*/auto &listenersForType = itEventListener->second;
        auto &listenersForType = *(reinterpret_cast<std::vector<EventListener<TEvent>> *>(itEventListener->second));
        
        // Browse all event listeners
        for (/*std::vector<std::shared_ptr<EventListener>>::iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
            // If found
            if (itListener->getTarget() == &target) {
                // Delete it
                listenersForType.erase(itListener);
                
                // If no listener for this type
                if (listenersForType.empty()) {
                    // Delete listeners for type vector
                    delete &listenersForType;
                    
                    // Delete this entry from map
                    _eventListeners.erase(eventTypeID);
                }
                
                // Exit
                return;
            }
        }
        
        // Throw exception
        throw std::invalid_argument("Listener not registered");
    }
    
    template <class TEvent, class TTarget> bool EventManager::isListenerRegistered(TTarget const &target) const {
        /*! @todo est ce qu'on peut eviter le copier coller avec unregisterListener pour retrouver le listener ? */
        // Get event type
        auto eventTypeID = TypeManager::getTypeIDForType<TEvent>();
        
        // Try to get event listeners for event type supported by listener
        /*std::unordered_map<TypeManager::TypeID, std::vector<std::shared_ptr<EventListener>>>::const_iterator*/auto itEventListener = _eventListeners.find(eventTypeID);
        
        // If not found
        if (itEventListener == std::end(_eventListeners)) {
            // Exit
            return false;
        }
        
        // Get event listeners
        ///*std::vector<std::shared_ptr<EventListener>> const &*/auto const &listenersForType = itEventListener->second;
        auto const &listenersForType = *(reinterpret_cast<std::vector<EventListener<TEvent>> *>(itEventListener->second));
        
        // Browse all event listeners
        for (/*std::vector<std::shared_ptr<EventListener>>::const_iterator*/auto itListener = std::begin(listenersForType); itListener != std::end(listenersForType); ++itListener) {
            // If found
            if (itListener->getTarget() == &target) {
                // Exit
                return true;
            }
        }
        
        // Not found
        return false;
    }
    
    
    template <class TEvent> void EventManager::EventListener<TEvent>::notify(TEvent &event) const {
        _callback(event);
    }
    /*
    template <class TEvent> bool EventManager::EventListener<TEvent>::compare(EventListener const &rhs) const {
        // Equals if both sender and priority are equals (no really need to check both sender object and class but no problem to do it because we can't have both object and class set but if we don't do that, we need to check if both are not empty)
        return (rhs._priority == _priority);
    }*/
    
    template <class TEvent> int EventManager::EventListener<TEvent>::getPriority() const {
        // Return priority
        return _priority;
    }
    
    template <class TEvent> void *EventManager::EventListener<TEvent>::getTarget() const {
        // Return target
        return _target;
    }
    
    
    
    template <class TEvent> void EventManager::deleteListenersVector(void *address) {
        // Get event type
        //auto eventTypeID = TypeManager::getTypeIDForType<TEvent>();
        
        // TODO: est ce que c'est safe de convertir un unique_ptr<void *> en unique_ptr<EventListener<TEvent>> ? (est ce qu'ils ont la meme taille et le meme agencement mémoire ?) ou sinon on s'en passe et on utilise un pointeur raw, de toutes facons c'est cette classe qui gere le ownership
        auto pListenersForType = reinterpret_cast<std::vector<EventListener<TEvent>> *>(address);
        
        delete pListenersForType;
    }

    
}

#endif /* defined(__Tuple__EventManager__) */
