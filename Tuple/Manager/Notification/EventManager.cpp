//
//  EventManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 30/06/15.
//  Copyright (c) 2015 Jonathan Baliko. All rights reserved.
//

#include "EventManager.hpp"


namespace Tuple {
    
    // TODO: pour effacer l'unordered_map quand plus besoin, on peut avoir un compteur statique d'objets EventManager mais pas obligatoire de faire ca
    std::unordered_map<TypeManager::TypeID, std::function<void(void *)>> EventManager::_deleters;
    
    Event::~Event () {
    }
    
}
