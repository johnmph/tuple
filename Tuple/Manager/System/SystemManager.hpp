//
//  SystemManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 20/10/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__SystemManager__
#define __Tuple__SystemManager__

#include <vector>
#include "System.hpp"


namespace Tuple {
    
    class SystemManager {
    public:
        // Constructor
        SystemManager();
        
        // Destructor
        ~SystemManager();//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        void addSystem(System *system);//TODO : system ref ou pointeur ?
        void insertSystem(System *system, int position);//TODO: peut etre pas obligé ici d'avoir add et insert, juste un avec le parametre position =-1 par défaut qui insert a la derniere place ? + system ref ou pointeur ?
        void removeSystem(System *system);//TODO : system ref ou pointeur ?
        void removeAllSystems();
        void reorderSystem(System *system, int newPosition);//TODO : system ref ou pointeur ?
        template <class TSystem> System *getSystemWithClass() const;//TODO : system ref ou pointeur ?
        System *getSystemAtPosition(int position) const;//TODO : system ref ou pointeur ?
        int getPositionOfSystem(System *system) const;//TODO : system ref ou pointeur ?
        std::vector<System *> getAllSystems() const;//TODO : system ref ou pointeur ? const ?, que se passe t'il si je supprime un system pour ceux qui ont recupéré les systems avec ceci ? car c'est une copie du vector que l'on recoit
        
    private:
        /*! Systems */
        std::vector<System *> _systems;//TODO: pas de smart ptr car créé avec le memory manager, il faut les supprimer dans le dealloc de cet objet
    };
    
    template <class TSystem> System *SystemManager::getSystemWithClass() const {
    }
    
}

#endif /* defined(__Tuple__SystemManager__) */
