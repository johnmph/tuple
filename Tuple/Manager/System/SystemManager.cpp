//
//  SystemManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 20/10/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "SystemManager.hpp"


namespace Tuple {
    
    SystemManager::SystemManager() {
    }
    
    SystemManager::~SystemManager() {
        // Remove all systems
        removeAllSystems();
    }
    
    void SystemManager::addSystem(System *system) {
    }
    
    void SystemManager::insertSystem(System *system, int position) {
    }
    
    void SystemManager::removeSystem(System *system) {
    }
    
    void SystemManager::removeAllSystems() {
    }
    
    void SystemManager::reorderSystem(System *system, int newPosition) {
    }
    
    System *SystemManager::getSystemAtPosition(int position) const {
        return nullptr;
    }
    
    int SystemManager::getPositionOfSystem(System *system) const {
        return 0;
    }
    
    std::vector<System *> SystemManager::getAllSystems() const {
        return std::vector<System *>();
    }
    
}
