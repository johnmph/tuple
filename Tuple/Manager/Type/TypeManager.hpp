//
//  TypeManager.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__TypeManager__
#define __Tuple__TypeManager__

//#include <iostream>
#include <unordered_map>


namespace Tuple {//TODO: peut etre plutot utiliser : http://en.cppreference.com/w/cpp/types/type_index
    
    /*!
     * This class manages the type of classes and objects,
     * it can find type of POLYMORPHIC object with a pointer
     * to their base classes (not POD).
     */
    class TypeManager {
    public:
        /*! Type type */
        using TypeID = size_t;
        
        /*! Method call address type */
        using MethodCallAddress = void(*)(void *);//TODO: remplacer par des lambdas ?
        
        /*!
         * Get the type of a class.
         * Even if this class method is constexpr, we can't use it in compile time due to
         * the use of typeid in implementation, but it is lets as constexpr in case
         * if implementation changes.
         * @param TType Type to convert to a type
         * @return The type of the class
         */
        template <class TType> static constexpr TypeID getTypeIDForType();
        
        /*!
         * Get the type of a object class.
         * @param object Object of TClass class to convert to a type
         * @return The type of the object class
         */
        template <class TType> static TypeID getTypeIDForInstance(TType const &instance);
        
        // Calculate and add a class destructor wrapper for a type
        template <class TType> static void addMethodCallAddressForType(MethodCallAddress methodCallAddress);
        
        // Call destructor of object with dynamic type
        static void callMethodForTypeID(TypeID typeID, void *object);//TODO: voir pour le nom, peut etre renommer callDestructorForID et avoir la methode du dessus appelée addClassDestructorWrapperForID avec un parametre id ?
        
    private:
        /*! Method call addresses */
        static std::unordered_map<TypeID, MethodCallAddress> _methodCallAddresses;
    };
    
    template <class TType> constexpr auto TypeManager::getTypeIDForType() -> TypeID {
        // Class type is got from RTTI typeid hashcode
        /*! @todo a voir pour ca et en dessous car il parait que RTTI est tres mauvais en performance !!! */
        return typeid(TType).hash_code();
    }
    
    template <class TType> auto TypeManager::getTypeIDForInstance(TType const &instance) -> TypeID {
        // Class type is got from RTTI typeid hashcode
        return typeid(instance).hash_code();
    }
    
    template <class TType> void TypeManager::addMethodCallAddressForType(MethodCallAddress methodCallAddress) {
        // Get class type
        /*TypeID*/auto typeID = getTypeIDForType<TType>();
        
        // Add method call address to method call addresses if not already in it
        _methodCallAddresses.insert(std::make_pair(typeID, methodCallAddress));
    }
    
}

#endif /* defined(__Tuple__TypeManager__) */
