//
//  TypeManager.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "TypeManager.hpp"


namespace Tuple {
    
    /*! Method call addresses */
    std::unordered_map<TypeManager::TypeID, TypeManager::MethodCallAddress> TypeManager::_methodCallAddresses;
    
    
    void TypeManager::callMethodForTypeID(TypeID typeID, void *object) {
        // Try to get a destructor wrapper address for this type
        /*std::unordered_map<TypeID, MethodCallAddress>::const_iterator*/auto itMethodCallAddress = _methodCallAddresses.find(typeID);
        
        // If found
        if (itMethodCallAddress != std::end(_methodCallAddresses)) {
            // Get method call address
            /*MethodCallAddress const &*/ auto const &methodCallAddress = itMethodCallAddress->second;
            
            // Call it
            methodCallAddress(object);
        }
    }
    
}
