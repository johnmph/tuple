//
//  FixedMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/06/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "FixedMemoryAllocator.hpp"
#include <assert.h>


namespace Tuple {
    
    FixedMemoryAllocator::FixedMemoryAllocator() : _allocatedCount(0) {
    }
    
    /*
    FixedMemoryAllocator::FixedMemoryAllocator(unsigned int elementsCount) {
    }*/
    
    FixedMemoryAllocator::~FixedMemoryAllocator() {
        //TODO: appeler une methode virtuelle pour checker si le memory allocator est vide (le client a bien désalloué tout ce qu'il avait alloué) et faire un assert ou une exception si pas tout desalloué MAIS ATTENTION: Attention, on ne peut pas thrower une exception dans un destructor : http://stackoverflow.com/questions/130117/throwing-exceptions-out-of-a-destructor et http://bin-login.name/ftp/pub/docs/programming_languages/cpp/cffective_cpp/MEC/MI11_FR.HTM
        //TODO: peut etre plutot des notifications pour envoyer une notif warning qu'un objet log par exemple pourra recuperer
        //assert(_allocatedCount == 0);
        if (_allocatedCount) {
            std::cout << "Allocated count = " << _allocatedCount << std::endl;
        }
    }
    
    void *FixedMemoryAllocator::allocateFixedMemory(size_t size) {
        // Check parameters
        assert(size > 0);
        
        // Acquire fixed memory
        void *memory = acquireFixedMemory(size);
        
        // Increment allocated count after acquire memory to be sure that this memory is acquired (no exception throwed)
        ++_allocatedCount;
        
        // Return it
        return memory;
    }
    
    void FixedMemoryAllocator::freeFixedMemory(void *memory) {
        // Check parameters
        assert(memory != nullptr);
        
        // Release fixed memory
        releaseFixedMemory(memory);
        
        // Decrement allocated count after release memory to be sure that this memory is released (no exception throwed)
        --_allocatedCount;
    }
    
}
