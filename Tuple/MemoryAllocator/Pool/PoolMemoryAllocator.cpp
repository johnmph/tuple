//
//  PoolMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 9/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "PoolMemoryAllocator.hpp"
#include <assert.h>


/*! @todo voir pour alignement: normalement ok car une class / struct est automatiquement alignée pour tenir dans un array (avec padding dans la class / struct si necessaire) */
/*! @todo comment faire quand je copie la mémoire pour que les objets soit bien copiés ? si ce sont des pod pas de probleme mais si ce sont des objets ? -> copy constructor : non normalement pas de probleme car on move les objects (c'est a dire qu'ils sont copiés byte par byte et l'ancien objet est effacé sans que son destructor ne soit appelé car on ne manipule que de la mémoire ici) et donc une fois l'objet bougé (on garde le bon pointer via le system de handle) supprimé, son destructor fonctionnera de la meme facon que l'ancien object : pas de prob pour les pointeurs, references, ... */

namespace Tuple {
    
    PoolMemoryAllocator::PoolMemoryAllocator(size_t elementSize, unsigned int defaultElementsCount) : MoveableMemoryAllocator(defaultElementsCount), _elementSize(elementSize), _storage(nullptr), _totalElementsCount(defaultElementsCount), _usedElementsCount(0) {
        // Check parameters
        assert(defaultElementsCount > 0);
        assert(elementSize > 0);
        
        // Allocate storage
        _storage = allocateStorage(_totalElementsCount);
    }
    
    PoolMemoryAllocator::~PoolMemoryAllocator() {
        // Free storage
        freeStorage();
    }
    
    size_t PoolMemoryAllocator::getElementSize() const {
        // Return size of an element
        return _elementSize;
    }
    //TODO: getElementType() getter aussi ?
    unsigned int PoolMemoryAllocator::getTotalElementsCount() const {
        // Return total elements count
        return _totalElementsCount;
    }
    
    unsigned int PoolMemoryAllocator::getUsedElementsCount() const {
        // Return used elements count
        return _usedElementsCount;
    }
    
    /*! @todo ou inverser le probleme pour ne plus avoir le parametre size ici mais dans le constructor de MemoryAllocator
     // This method doesn't need size because element size needs to be known on the constructor but we can't change signature because it's an overriden method */
    void *PoolMemoryAllocator::acquireMoveableMemory(size_t size) {
        // Check parameters
        assert(size == _elementSize);
        
        // Check if we are out of memory
        if (_usedElementsCount >= _totalElementsCount) {
            // Grow storage
            grow();
        }
        
        // Get address of acquired memory
        void *addr = static_cast<unsigned char *>(_storage) + (_usedElementsCount * _elementSize);
        
        // Increment used elements count
        ++_usedElementsCount;
        
        // Return address of acquired memory
        return addr;
    }
    
    void PoolMemoryAllocator::releaseMoveableMemory(void *addr) {
        // Convert addr to index in storage array
        /*! @todo voir si alignement */
        unsigned int index = static_cast<unsigned int>(static_cast<unsigned char *>(addr) - static_cast<unsigned char *>(_storage)) / _elementSize;
        
        // Check index
        /*! @todo laisser ce check ? */
        if (index >= _usedElementsCount) {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("addr is incorrect");
        }
        
        // If not last element
        if (index != (_usedElementsCount - 1)) {
            // Calculate addr of last element
            void *lastElementAddr = static_cast<unsigned char *>(_storage) + ((_usedElementsCount - 1) * _elementSize);
            
            // Copy last element on element to remove
            memcpy(addr, lastElementAddr, _elementSize);
            
            // Notify that last element has moved
            moveMemory(lastElementAddr, addr);
        }
        
        // Remove last element
        --_usedElementsCount;
    }
    
    void *PoolMemoryAllocator::allocateStorage(unsigned int count) {
        // Allocate memory
        /*! @todo pouvoir passer par un autre memory allocator (stack allocator) par exemple */
        void *addr = malloc(count * _elementSize);
        
        // If error on malloc
        if (!addr) {
            // Throw an exception
            throw std::bad_alloc();
        }
        
        // Return addr
        return addr;
    }
    
    void PoolMemoryAllocator::freeStorage() {
        // If storage doesn't exist
        if (!_storage) {
            // Exit
            return;
        }
        
        // Free storage
        free(_storage);
        
        // Reset storage
        _storage = nullptr;
    }
    
    void PoolMemoryAllocator::grow() {
        // Calculate new size
        /*! @todo utiliser un functor pour choisir la taille ? */
        unsigned int newTotalElementsCount = _totalElementsCount * 2;
        
        // Allocate new storage
        void *newStorage = allocateStorage(newTotalElementsCount);
        
        // Copy old storage to new storage
        memcpy(newStorage, _storage, newTotalElementsCount * _elementSize);
        
        // Calculate offset between new and old storage in bytes
        size_t offset = static_cast<unsigned char *>(newStorage) - static_cast<unsigned char *>(_storage);
        
        // Notify that all memory has moved
        moveAllMemory(offset);
        
        // Free old storage
        freeStorage();
        
        // Set storage to new storage
        _storage = newStorage;
        
        // Set new total elements count
        _totalElementsCount = newTotalElementsCount;
    }
    
    void PoolMemoryAllocator::shrink() {
        // Check if no possibility to shrink
        if (_usedElementsCount >= _totalElementsCount) {
            // Exit
            return;
        }
        
        // Allocate new storage
        void *newStorage = allocateStorage(_usedElementsCount);
        
        // Copy old storage to new storage
        memcpy(newStorage, _storage, _usedElementsCount * _elementSize);
        
        // Calculate offset between new and old storage in bytes
        size_t offset = static_cast<unsigned char *>(newStorage) - static_cast<unsigned char *>(_storage);
        
        // Notify that all memory has moved
        moveAllMemory(offset);
        
        // Free old storage
        freeStorage();
        
        // Set storage to new storage
        _storage = newStorage;
        
        // Set new total elements count
        _totalElementsCount = _usedElementsCount;
    }
    
}
