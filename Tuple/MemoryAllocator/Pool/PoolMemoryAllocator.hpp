//
//  PoolMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 9/08/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__PoolMemoryAllocator__
#define __Tuple__PoolMemoryAllocator__

#include "MoveableMemoryAllocator.hpp"

//TODO: voir ca pour optimiser la structure : http://henri-garreta.developpez.com/tutoriels/algorithmique/cours-structure-donnee/?page=structures-lineaires (utiliser une valeur sentinelle pour eviter le test sur null par exemple)
namespace Tuple {
    
    class PoolMemoryAllocator : public MoveableMemoryAllocator {
    public:
        // Constructor
        PoolMemoryAllocator(size_t elementSize, unsigned int defaultElementsCount);
        
        // Destructor
        ~PoolMemoryAllocator();
        
        // Get size of an element
        size_t getElementSize() const;
        
        // Get total count of elements
        unsigned int getTotalElementsCount() const;
        
        // Get used count of elements
        unsigned int getUsedElementsCount() const;
        
        //TODO: remettre le grow percentage : growAmountPercentage : peut etre a la place de growAmountPercentage avoir un growFunctor + une grow methode qui est appelé quand la pool est remplie et qu'on demande un objet, ainsi on peut mettre le code que l'on veut dans le functor pour grower la pool avec la methode grow
        
        // Shrink pool to remove unused memory space, use this only if you run out of memory
        void shrink();
        
    private:
        // Methods
        void *acquireMoveableMemory(size_t size) override;
        void releaseMoveableMemory(void *addr) override;
        
        void *allocateStorage(unsigned int count);
        void freeStorage();
        
        void grow();
        
        // Size of an element
        size_t _elementSize;
        
        // Storage
        void *_storage;
        
        // Total count of elements
        unsigned int _totalElementsCount;
        
        // Used count of elements
        unsigned int _usedElementsCount;
    };
    
}

#endif /* defined(__Tuple__PoolMemoryAllocator__) */
