//
//  FrameMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "FrameMemoryAllocator.hpp"


namespace Tuple {
    
    void *FrameMemoryAllocator::acquireFixedMemory(size_t size) {
        return nullptr;
    }
    
    void FrameMemoryAllocator::releaseFixedMemory(void *addr) {
    }
    
}
