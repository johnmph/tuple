//
//  FrameMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__FrameMemoryAllocator__
#define __Tuple__FrameMemoryAllocator__

#include "FixedMemoryAllocator.hpp"


namespace Tuple {
    
    class FrameMemoryAllocator : public FixedMemoryAllocator {
        // Methods
        void *acquireFixedMemory(size_t size) override;
        void releaseFixedMemory(void *addr) override;
    };
    
}

#endif /* defined(__Tuple__FrameMemoryAllocator__) */
