//
//  DoubleFrameMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "DoubleFrameMemoryAllocator.hpp"


namespace Tuple {
    
    void *DoubleFrameMemoryAllocator::acquireFixedMemory(size_t size) {
        return nullptr;
    }
    
    void DoubleFrameMemoryAllocator::releaseFixedMemory(void *addr) {
    }
    
}
