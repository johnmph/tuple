//
//  DoubleFrameMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__DoubleFrameMemoryAllocator__
#define __Tuple__DoubleFrameMemoryAllocator__

#include "FrameMemoryAllocator.hpp"


namespace Tuple {
    
    class DoubleFrameMemoryAllocator : public FixedMemoryAllocator {
        // Methods
        void *acquireFixedMemory(size_t size) override;
        void releaseFixedMemory(void *addr) override;
    };
    
}

#endif /* defined(__Tuple__DoubleFrameMemoryAllocator__) */
