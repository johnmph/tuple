//
//  StdMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "StdMemoryAllocator.hpp"


namespace Tuple {
    
    void *StdMemoryAllocator::acquireFixedMemory(size_t size) {
        return nullptr;
    }
    
    void StdMemoryAllocator::releaseFixedMemory(void *addr) {
    }
    
}
