//
//  StdMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 12/01/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__StdMemoryAllocator__
#define __Tuple__StdMemoryAllocator__

#include "FixedMemoryAllocator.hpp"


namespace Tuple {
    
    class StdMemoryAllocator : public FixedMemoryAllocator {//todo: http://stackoverflow.com/questions/354442/looking-for-c-stl-like-vector-class-but-using-stack-storage
        // Methods
        void *acquireFixedMemory(size_t size) override;
        void releaseFixedMemory(void *addr) override;
    };
    
}

#endif /* defined(__Tuple__StdMemoryAllocator__) */
