//
//  MoveableMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/06/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#include "MoveableMemoryAllocator.hpp"
#include <assert.h>


namespace Tuple {
    
    MoveableMemoryAllocator::MoveableMemoryAllocator() : _allocatedCount(0) {
    }
    
    MoveableMemoryAllocator::MoveableMemoryAllocator(unsigned int elementsCount) : _allocatedCount(0), _handleManager(elementsCount) {
    }
    
    MoveableMemoryAllocator::~MoveableMemoryAllocator() {
        //TODO: appeler une methode virtuelle pour checker si le memory allocator est vide (le client a bien désalloué tout ce qu'il avait alloué) et faire un assert ou une exception si pas tout desalloué MAIS ATTENTION: Attention, on ne peut pas thrower une exception dans un destructor : http://stackoverflow.com/questions/130117/throwing-exceptions-out-of-a-destructor et http://bin-login.name/ftp/pub/docs/programming_languages/cpp/cffective_cpp/MEC/MI11_FR.HTM
        //TODO: peut etre plutot des notifications pour envoyer une notif warning qu'un objet log par exemple pourra recuperer
        //assert(_allocatedCount == 0);
        if (_allocatedCount) {
            std::cout << "Allocated count = " << _allocatedCount << std::endl;
        }
    }
    
    Handle MoveableMemoryAllocator::allocateMoveableMemory(size_t size) {
        // Check parameters
        assert(size > 0);
        
        // Acquire moveable memory and create an handle of this memory
        Handle handle = _handleManager.createHandle(acquireMoveableMemory(size));
        
        // Increment allocated count after acquire memory to be sure that this memory is acquired (no exception throwed)
        ++_allocatedCount;
        
        // Return handle
        return handle;
    }
    
    void MoveableMemoryAllocator::freeMoveableMemory(Handle const &handle) {
        // Get memory of handle
        void *memory = _handleManager.getDataPointer(handle);
        
        // Check if memory is not valid
        if (memory == nullptr) {
            // Throw an exception
            /*! @todo changer */
            throw std::invalid_argument("handle is invalid");
        }
        
        // Destroy handle
        _handleManager.destroyHandle(handle);
        
        // Release moveable memory
        releaseMoveableMemory(memory);
        
        // Decrement allocated count after release memory to be sure that this memory is released (no exception throwed)
        --_allocatedCount;
    }
    
    void MoveableMemoryAllocator::moveMemory(void *oldAddress, void *newAddress) {
        // Update handle entry of handle manager
        _handleManager.updateHandleEntry(oldAddress, newAddress);
    }
    
    void MoveableMemoryAllocator::moveAllMemory(size_t addressByteOffset) {
        // Update all handle entries of handle manager
        _handleManager.updateHandleEntries(addressByteOffset);
    }
    
    void *MoveableMemoryAllocator::getMemoryForHandle(Handle const &handle) const {
        // Delegate to handle manager
        return _handleManager.getDataPointer(handle);
    }
    
}
