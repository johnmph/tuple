//
//  FixedMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 25/06/14.
//  Copyright (c) 2014 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__FixedMemoryAllocator__
#define __Tuple__FixedMemoryAllocator__

#include <iostream>


namespace Tuple {
    
    /*! @todo qui doit gérer les handles ? les memoryAllocator ou MemoryManager ?? Si c'est l'object allocator, comment va t'il savoir quand un objet bouge en mémoire pour mettre a jour les handles (par exemple si une pool bouge ses objets pour redimensionner ou quand un element est supprimé et qu'on bouge le dernier dans le trou) ? Si ce sont les memory allocator, comment supprimer l'objet quand le count ref = 0 puisqu'il faut appeler deleteObject de MemoryManager pour supprimer l'object ? Peut etre laisser object allocator gerer le ref count et les memory allocator le reste ? */
    /*! @todo: il est possible de se passer du MemoryManager en ayant la méthode template newData ici car de toute facons elle n'est pas virtuelle, c'est acquireMemory qui est virtuelle et elle n'est pas template, voir si c'est mieux de se passer du MemoryManager ou non (peut etre moins d'appels indirects qui passe du MemoryManager au MemoryAllocator ?) */
    /*! @todo rajouter un iterator ? */
    class FixedMemoryAllocator {
    public:
        // Constructor
        FixedMemoryAllocator();
        //explicit FixedMemoryAllocator(unsigned int elementsCount);//TODO: normalement ca ne doit pas etre ici
        
        // Destructor
        virtual ~FixedMemoryAllocator() = 0;//TODO: voir aussi d'apres la regle des 3 s'il faut mettre les copy constructor et assignment en private
        
        // Methods
        void *allocateFixedMemory(size_t size);
        void freeFixedMemory(void *memory);//TODO: normalement, lui passer aussi le size !!! (mais pas sur, seul stackmemoryallocator en a besoin et peut etre qu'il peut calculer car on ne peut free que les derniers alloués (LIFO))
        
    private:
        // Acquire fixed memory, must throw an exception if not successfully acquired memory
        virtual void *acquireFixedMemory(size_t size) = 0;
        
        // Release fixed memory, guarantee that addr is correct
        virtual void releaseFixedMemory(void *addr) = 0;
        
        // Allocated count
        unsigned int _allocatedCount;
    };
    
}

#endif /* defined(__Tuple__FixedMemoryAllocator__) */
