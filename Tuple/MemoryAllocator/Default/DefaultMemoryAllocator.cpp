//
//  DefaultMemoryAllocator.cpp
//  Tuple
//
//  Created by Jonathan Baliko on 1/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#include "DefaultMemoryAllocator.hpp"
#include <stdlib.h>


namespace Tuple {
    
    DefaultMemoryAllocator::DefaultMemoryAllocator() : mem(nullptr), current(0) {
        mem = (unsigned char *) malloc(5000 * 128);
    }
    
    void *DefaultMemoryAllocator::acquireFixedMemory(size_t size) {
        // Default allocation
        /*! @todo est ce que malloc fait l'alignement ? normalement oui, il garantit l'alignement mais a tester http://stackoverflow.com/questions/3526702/what-to-consider-with-regards-to-alignment-when-designing-a-memory-pool (peut etre avec alignas() ?) */
        void *addr = &mem[size * current++];//malloc(size);
        /*
        // Save pointer
        //_pointers.insert(addr);
        _pointers[addr] = type;
        */
        // Return pointer
        return addr;
    }
    
    void DefaultMemoryAllocator::releaseFixedMemory(void *addr) {
        /*
        // Remove pointer and check if erased (if not, it's because it doesn't exist)
        if (_pointers.erase(addr) != 1) {
            // Throw an exception
            // TODO: changer
            throw std::invalid_argument("addr is incorrect");
        }*/
        
        // Default deallocation
        //free(addr);
    }
    
    void *DefaultMemoryAllocator::acquireMoveableMemory(size_t size) {
        return acquireFixedMemory(size);
    }
    
    void DefaultMemoryAllocator::releaseMoveableMemory(void *addr) {
        releaseFixedMemory(addr);
    }
    
}
