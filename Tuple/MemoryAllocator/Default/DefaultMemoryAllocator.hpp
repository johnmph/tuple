//
//  DefaultMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 1/09/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__DefaultMemoryAllocator__
#define __Tuple__DefaultMemoryAllocator__

#include "FixedMemoryAllocator.hpp"
#include "MoveableMemoryAllocator.hpp"
#include <unordered_map>


namespace Tuple {
    
    class DefaultMemoryAllocator : public FixedMemoryAllocator, public MoveableMemoryAllocator {//TODO: je l'ai mis aussi Moveable pour les tests, a retirer par apres
        //std::unordered_map<void *, size_t> _pointers;//TODO: changer size_t par une struct qui a 2 size_t (un pour la size et un pour le type), pour l'instant ici que le type est sauvé
        // Methods
        void *acquireFixedMemory(size_t size) override;
        void releaseFixedMemory(void *addr) override;
        
        //TODO: ces 2 methodes sont pour le moveable :
        void *acquireMoveableMemory(size_t size) override;
        void releaseMoveableMemory(void *addr) override;
    public:
        DefaultMemoryAllocator();
        unsigned char *mem;
        int current;
    };
    
}

#endif /* defined(__Tuple__DefaultMemoryAllocator__) */
