//
//  StackMemoryAllocator.hpp
//  Tuple
//
//  Created by Jonathan Baliko on 29/11/13.
//  Copyright (c) 2013 Jonathan Baliko. All rights reserved.
//

#ifndef __Tuple__StackMemoryAllocator__
#define __Tuple__StackMemoryAllocator__

#include "FixedMemoryAllocator.hpp"


namespace Tuple {
    
    class StackMemoryAllocator : public FixedMemoryAllocator {
        // Methods
        void *acquireFixedMemory(size_t size) override;
        void releaseFixedMemory(void *addr) override;
    };
    
}

#endif /* defined(__Tuple__StackMemoryAllocator__) */
